package com.mirshekari.metroshiraz;

import android.content.Context;
import android.graphics.Typeface;

public class FontTypeface {
    private Context context;

    public FontTypeface(Context context){
        this.context = context;
    }

    public Typeface getTypefaceAndroid(){
        Typeface typeFace = Typeface.DEFAULT;
        String strFont = "assets/fonts/iransansweb_medium.ttf";
        try {
            if (!strFont.equals("")){
                String strLeft = strFont.substring(0, 13);
                if (strLeft.equals("assets/fonts/")){
                    typeFace = Typeface.createFromAsset(context.getAssets(), strFont.replace("assets/", ""));
                } else {
                    typeFace = Typeface.createFromFile(strFont);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return typeFace;
    }
}