package com.mirshekari.metroshiraz.ChooseStation;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.widget.TextView;

import com.mirshekari.metroshiraz.ChooseCity.BackgroundTask;
import com.mirshekari.metroshiraz.Choose_station;
import com.mirshekari.metroshiraz.Connection;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.Facilities.BackgroundTaskFacilities;
import com.mirshekari.metroshiraz.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mohsen on 11/7/2017.
 */

public class BackgroundTaskStation extends AsyncTask<Void,Void,Void> {
    ProgressDialog progressDialog;
    Context ctx;
    SharedPreferences shared,sharedcity;
    String gu_id,line_id,line_number;
    Dialog dialog;
    public BackgroundTaskStation(Context ctx) {
        this.ctx = ctx;
    }
    @Override
    protected void onPreExecute() {
        shared = ctx.getSharedPreferences("MyPref", MODE_PRIVATE);
        sharedcity = ctx.getSharedPreferences("Line", MODE_PRIVATE);
        gu_id = shared.getString("gu","null");
        line_id = sharedcity.getString("line","null");
        line_number = sharedcity.getString("linenumber","null");
        dialog = new Dialog(ctx);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.loading);
        Typeface samim = Typeface.createFromAsset(ctx.getAssets(), "fonts/samim.ttf");
        TextView messageTextView = (TextView)dialog.findViewById(R.id.message);
        messageTextView.setTypeface(samim);
        messageTextView.setText("در حال دریافت ایستگاه های خط "+line_number);
        messageTextView.setTypeface(samim);
        dialog.show();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            String json_url = Connection.portal+"/GetStations/"+gu_id+"/"+line_id+"/0";
            URL url = new URL(json_url);
            HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while((line=bufferedReader.readLine())!=null)
            {
                stringBuilder.append(line+"\n");
                Thread.sleep(5000);
            }
            httpURLConnection.disconnect();
            String json_data = stringBuilder.toString().trim();
            JSONArray jsonarray = new JSONArray(json_data);
            DBHelper dbHelper = new DBHelper(ctx);
            SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();
            int count = 0;
            while(count<jsonarray.length())
            {
                JSONObject o = jsonarray.getJSONObject(count);
                        count++;
                dbHelper.putStationsInformation(o.getString("Address"),o.getInt("Id"),o.getInt("LineId"),
                        o.getString("Location"),o.getInt("StationNumber"),
                        o.getInt("Status"),o.getString("Title"),o.getString("Facilities"),o.getString("IsCross"),sqLiteDatabase);
            }
            dbHelper.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }
    @Override
    protected void onPostExecute(Void aVoid) {
        dialog.dismiss();
        ctx.startActivity(new Intent(ctx,Choose_station.class));
    }
}

