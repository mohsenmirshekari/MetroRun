package com.mirshekari.metroshiraz.ChooseStation;

/**
 * Created by Mohsen on 11/7/2017.
 */

public class StationsContract {
    public StationsContract() {}


    public static class StationsEntry
    {
        public static final String TABLE_NAME = "stations";
        public static final String ADDRESS = "address";
        public static final String STATION_ID = "station_id";
        public static final String LINE_ID = "line_id";
        public static final String LOCATION = "location";
        public static final String STATION_NUMBER = "station_number";
        public static final String STATUS = "status";
        public static final String TITLE = "title";
        public static final String FACILITIES = "facilities";
        public static final String ISCROSS = "iscross";
    }
}
