package com.mirshekari.metroshiraz.ChooseStation;

/**
 * Created by Mohsen on 11/7/2017.
 */

public class StationsC {
    private String address;
    private int stationid;
    private int lineid;
    private String location;
    private int stationnumber;
    private int status;
    private String title;
    private String facilities;
    private String iscross;


    public StationsC(String address, int stationid, int lineid, String location, int stationnumber, int status, String title, String facilities, String iscross) {
        this.address = address;
        this.stationid = stationid;
        this.lineid = lineid;
        this.location = location;
        this.stationnumber = stationnumber;
        this.status = status;
        this.title = title;
        this.facilities = facilities;
        this.iscross = iscross;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getStationid() {
        return stationid;
    }

    public void setStationid(int stationid) {
        this.stationid = stationid;
    }

    public int getLineid() {
        return lineid;
    }

    public void setLineid(int lineid) {
        this.lineid = lineid;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getStationnumber() {
        return stationnumber;
    }

    public void setStationnumber(int stationnumber) {
        this.stationnumber = stationnumber;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFacilities() {
        return facilities;
    }

    public void setFacilities(String facilities) {
        this.facilities = facilities;
    }

    public String getIscross() {
        return iscross;
    }

    public void setIscross(String iscross) {
        this.iscross = iscross;
    }
}