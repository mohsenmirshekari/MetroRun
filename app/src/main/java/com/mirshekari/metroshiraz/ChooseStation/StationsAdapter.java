package com.mirshekari.metroshiraz.ChooseStation;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mirshekari.metroshiraz.AnalyticsApplication;
import com.mirshekari.metroshiraz.Choose_station;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.R;
import com.mirshekari.metroshiraz.Schedule.ScheduleContract;
import com.mirshekari.metroshiraz.Station;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mohsen on 11/7/2017.
 */

public class StationsAdapter extends RecyclerView.Adapter <StationsAdapter.RecyclerViewHolder>  {
    SharedPreferences shared,stationshared,sharess;
    SharedPreferences.Editor editor;
    ArrayList<StationsC> arrayList = new ArrayList<>();
    Context context;
    String bgcolor;
    SQLiteDatabase db;
    String lang_name;
    View view;
    SharedPreferences sharedLang;
    String lastId;
    Tracker mTracker;

    public StationsAdapter(ArrayList<StationsC> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    public StationsAdapter(ArrayList<StationsC> arrayList)
    {
        this.arrayList = arrayList;
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_lines_ltr,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        context = parent.getContext();
        return recyclerViewHolder;
    }
    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final StationsC stationsC = arrayList.get(position);
        DBHelper dbHelper = new DBHelper(context);

        db = dbHelper.getReadableDatabase();

        sharedLang = context.getSharedPreferences("MyPref",MODE_PRIVATE);
        lang_name = sharedLang.getString("langname","null");

        Typeface samim = Typeface.createFromAsset(context.getAssets(), "fonts/samim.ttf");
        Typeface irsans_light = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_light.ttf");
        shared = context.getSharedPreferences("Line", MODE_PRIVATE);
        bgcolor = shared.getString("backgroundcolor","null");

        Drawable img = context.getResources().getDrawable( R.drawable.circleright );

        if(lang_name.equals("فارسي") || lang_name.equals("عربی"))
        {
            holder.title.setCompoundDrawablesWithIntrinsicBounds( null, null, img, null);
            holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
        }
        else
        {
          /*  holder.title.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null);
            holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);*/
            holder.title.setCompoundDrawablesWithIntrinsicBounds( null, null, img, null);
            holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
        }
        AnalyticsApplication application = (AnalyticsApplication) context.getApplicationContext();
        mTracker = application.getDefaultTracker();
         mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Stations")
                .setAction(stationsC.getTitle())
                .build());


        if(stationsC.getStationnumber()==1)
        {

            if(lang_name.equals("فارسي") || lang_name.equals("عربی")) {
                holder.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circleright_bottom, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
            }
            else{
               /* holder.title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.circleright_bottom, 0, 0, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);*/
                holder.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circleright_bottom, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
            }
        }

        Cursor cursor =  db.rawQuery("SELECT "+ StationsContract.StationsEntry.STATION_NUMBER+" FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.LINE_ID+" = "+stationsC.getLineid()+";",null);
        while (cursor.moveToNext())
        {
            cursor.moveToLast();
            lastId = cursor.getString(0);
        }
        cursor.close();
        if(stationsC.getStationnumber()==Integer.valueOf(lastId))
        {
            if(lang_name.equals("فارسي") || lang_name.equals("عربی")) {
                holder.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circleright_top, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
            }
            else{
              /*  holder.title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.circleright_top, 0, 0, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);*/
                holder.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circleright_top, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
            }
        }




        holder.title.setText(stationsC.getTitle());
        holder.title.setTypeface(irsans_light);
        String colorst = bgcolor;
        int color = Color.parseColor(colorst);
        holder.linearLayout.setBackgroundColor(color);


        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

         /*       shared =context.getSharedPreferences("Line", MODE_PRIVATE);
                editor = shared.edit();
                editor.putString("line", String.valueOf(stationsC.getStationid()));
                editor.putString("linenumber", String.valueOf(stationsC.getLineid()));
                editor.commit();
*/

              /*  if (isDatabaseEmpty() == 0){
                    BackgroundTaskStation backgroundTaskLine = new BackgroundTaskStation(context);
                    backgroundTaskLine.execute();
                }
                else if(isDatabaseEmpty()>0) {
                    context.startActivity(new Intent(context,Choose_station.class));
                }
*/

            /*    stationshared =context.getSharedPreferences("Station", MODE_PRIVATE);
                editor = shared.edit();
                editor.putString("stid", String.valueOf(stationsC.getStationid()));
                editor.commit();
*/


                SharedPreferences shared = context.getSharedPreferences("StationInfo", MODE_PRIVATE);
                SharedPreferences.Editor editor = shared.edit();
                editor.putString("station_id", String.valueOf(stationsC.getStationid()));
                editor.putString("station_title",stationsC.getTitle());
                editor.putInt("line_id",stationsC.getLineid());
                editor.commit();


            //    sharess = context.getSharedPreferences("Station", MODE_PRIVATE);
              //  String iddd = shared.getString("stid","null");
                //Toast.makeText(context,iddd,Toast.LENGTH_LONG).show();
         //       BackgroundTaskSchedule backgroundTaskSchedule = new BackgroundTaskSchedule(context);
           //     backgroundTaskSchedule.execute();



                     context.startActivity(new Intent(context, Station.class));


            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder
    {
        TextView title;
        LinearLayout linearLayout;
        ImageView inter,statusImage;
        RecyclerViewHolder(View view)
        {
            super(view);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.click_layout);
            title = (TextView) itemView.findViewById(R.id.title);
            inter = (ImageView) itemView.findViewById(R.id.inter);
            statusImage = (ImageView) itemView.findViewById(R.id.status);
        }
    }

    public int isDatabaseEmpty()
    {
        String count = "SELECT count(*) FROM "+ ScheduleContract.ScheduleEntry.TABLE_NAME_1;
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        return icount;

}}
