package com.mirshekari.metroshiraz;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mirshekari.metroshiraz.ChooseLine.LinesContract;
import com.mirshekari.metroshiraz.ChooseStation.StationsC;
import com.mirshekari.metroshiraz.ChooseStation.StationsContract;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.Routing.Source.SearchAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Routing_search extends AppCompatActivity {

    SharedPreferences shared;
    String sourceid,destinationid,city_id,gu_id;
    SQLiteDatabase db;
    DBHelper dbHelper;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public Context context;
    ArrayList<StationsC> arrayList = new ArrayList<>();
    String s1,s2,lineId,lineColor,sourceName,destinationName,to,selectedCost;
    LinearLayout errorLayout;
    RelativeLayout layout;
    TextView text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routing_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView topBar_title = (TextView) findViewById(R.id.topBar_title);
        setTitle("");
        setSupportActionBar(toolbar);
        Typeface sans_medium = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_medium.ttf");
        topBar_title.setTypeface(sans_medium);
        Typeface sans_light = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_light.ttf");


        errorLayout = (LinearLayout) findViewById(R.id.errorLayout);

        text = (TextView) findViewById(R.id.text);

        text.setTypeface(sans_light);

        shared = getSharedPreferences("RoutingInfo",MODE_PRIVATE);
        sourceid = shared.getString("source_id","null");
        selectedCost = shared.getString("selectedCost","null");
        destinationid = shared.getString("destination_id","null");

        layout = (RelativeLayout) findViewById(R.id.layout);

        recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setHasFixedSize(true);

        dbHelper = new DBHelper(getApplicationContext());
        db = dbHelper.getReadableDatabase();

        SharedPreferences sharedPreferences = getSharedPreferences("City",MODE_PRIVATE);
        city_id = sharedPreferences.getString("city","null");

        SharedPreferences sharedPreferences1 = getSharedPreferences("MyPref",MODE_PRIVATE);
        gu_id = sharedPreferences1.getString("gu","null");

        adapter = new SearchAdapter(arrayList,getApplicationContext());

        Cursor sourceNameCursor = db.rawQuery("SELECT * FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.STATION_ID+" = "+sourceid+";",null);
        Cursor destinationNameCursor = db.rawQuery("SELECT * FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.STATION_ID+" = "+destinationid+";",null);

        while (sourceNameCursor.moveToNext()){
            sourceName = sourceNameCursor.getString(6);
        }
        while (destinationNameCursor.moveToNext()){
            destinationName = destinationNameCursor.getString(6);
        }


        // Dictionary
        Cursor cursor = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.TO+";",null);
        while (cursor.moveToNext()){
            to = " "+cursor.getString(0)+" ";
        }
        // ->

        topBar_title.setText(sourceName+to+destinationName);

        final String URL_DATA = Connection.portal+"PathFinder/"+gu_id+"/"+city_id+"/"+sourceid+"/"+destinationid;
        android.util.Log.d("CheckRouting",URL_DATA);
        final StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_DATA, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                if(s.length()==0){
                    errorLayout.setVisibility(View.VISIBLE);
                }
                try {
                    JSONArray jsonArray = new JSONArray(s);
                    for(int j=0;j<=jsonArray.length();j++){
                        JSONObject jsonObject = jsonArray.getJSONObject(j);
                        String cost = jsonObject.getString("Cost");
                        if(cost.equals(selectedCost)){
                            JSONArray array = jsonObject.getJSONArray("Stations");

                            for(int i=0;i<=array.length();i++) {


                                String id = array.getString(i);

                                String id_for_color = array.getString(array.length()-1);
                                Cursor lineidCursor = db.rawQuery("SELECT * FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.STATION_ID+" = "+id_for_color+";",null);
                                while (lineidCursor.moveToNext()){
                                    lineId = lineidCursor.getString(2);
                                }
                                Cursor lineColorCursor = db.rawQuery("SELECT * FROM "+ LinesContract.LinesEntry.TABLE_NAME+" WHERE "+ LinesContract.LinesEntry.LINE_ID + " = "+lineId+";",null);
                                while (lineColorCursor.moveToNext()){
                                    lineColor = lineColorCursor.getString(0);
                                }
                                layout.setBackgroundColor(Color.parseColor(lineColor));

                                try{
                                    Cursor cursor = db.rawQuery("SELECT * FROM " + StationsContract.StationsEntry.TABLE_NAME + " WHERE " + StationsContract.StationsEntry.STATION_ID + " = " + id + ";", null);
                                    while (cursor.moveToNext()) {
                                        android.util.Log.d("qkwyetqwxd",cursor.getString(6));

                                  /*      Cursor c1 = db.rawQuery("SELECT * FROM stations WHERE station_id = "+array.getString(i)+";",null);
                                        Cursor c2 = db.rawQuery("SELECT * FROM stations WHERE station_id = "+array.getString(i+1)+";",null);

                                        while (c1.moveToNext()){
                                            s1 = c1.getString(6);
                                        }
                                        while (c2.moveToNext()){
                                            s2 = c2.getString(6);
                                        }*/
                                        //if(!s1.equals(s2)){
                                            StationsC linesC = new StationsC(cursor.getString(0), cursor.getInt(1), cursor.getInt(2), cursor.getString(3), cursor.getInt(4), cursor.getInt(5), cursor.getString(6), cursor.getString(7), cursor.getString(8));
                                            arrayList.add(linesC);
                                            adapter = new SearchAdapter(arrayList, getApplicationContext());}
                                 //   }
                                    adapter = new SearchAdapter(arrayList, getApplicationContext());
                                    recyclerView.setAdapter(adapter);
                                }catch (Exception e){}


                            }
                        }
                    }


                    String response = s;
                    s = s.substring(1, response.length()-1);


                    String array[] = s.split(",");


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                         errorLayout.setVisibility(View.VISIBLE);

                    }
                });
        MySingleton.getInstance(getApplicationContext()).addToRequestque(stringRequest);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
