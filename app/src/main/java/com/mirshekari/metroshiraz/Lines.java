package com.mirshekari.metroshiraz;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.mirshekari.metroshiraz.Aboutus.AboutUsContract;
import com.mirshekari.metroshiraz.ChooseLine.LinesContract;
import com.mirshekari.metroshiraz.ChooseStation.StationsContract;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.Facilities.BackgroundTaskFacilities;
import com.mirshekari.metroshiraz.Facilities.FacilitiesContract;
import com.mirshekari.metroshiraz.Fragments.Aboutus_fragment;
import com.mirshekari.metroshiraz.Fragments.Feedback;
import com.mirshekari.metroshiraz.Fragments.Lines_fragment;
import com.mirshekari.metroshiraz.Fragments.MessagesFragment;
import com.mirshekari.metroshiraz.Fragments.NearestStation_fragment;
import com.mirshekari.metroshiraz.Fragments.NewLines;
import com.mirshekari.metroshiraz.Fragments.Routing_fragment;
import com.mirshekari.metroshiraz.Fragments.Setting_fragment;
import com.mirshekari.metroshiraz.Fragments.TourismFragment;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.Place.Groups.GroupsContract;
import com.mirshekari.metroshiraz.Place.Private.PrivatePlacesContract;
import com.mirshekari.metroshiraz.Place.Public.PublicPlacesContract;
import com.mirshekari.metroshiraz.Schedule.ScheduleContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import pl.droidsonroids.gif.GifImageView;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class Lines extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
        NavigationView navigationView = null;
        SQLiteDatabase db;
        DBHelper dbHelper;
        Context context;
        int route;
        SharedPreferences shared,sharedcity;
        SharedPreferences.Editor sheditor;
        TextView topBar_title;
        Cursor cursorG;
        String soctest2,icon,background,slide1,slide2,slide3,slide4,continueDic,banner,place_id,
                latestupdate,latestmessage,update_in_progress_dic,update_must_dic,update_available,
                yesDic,noDic,later,lines_list,routing,online_map,nearest_station_to_me,setting,aboutus,
                tourism,instagram,telegram,soctest,gu_id,city,line_id,version,station_id,isImportant,status,
                adviceDic,notificationsDic;
        RequestQueue queue;

    @Override
        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lines);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("");
        Typeface irsans_medium = Typeface.createFromAsset(getAssets(), "fonts/iransansweb.ttf");
        topBar_title = (TextView) findViewById(R.id.topBar_title);
        topBar_title.setText("لیست خطوط");
        topBar_title.setTypeface(irsans_medium);
        NewLines fragment = new NewLines();
        android.support.v4.app.FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.commit();




            shared = getSharedPreferences("MyPref", MODE_PRIVATE);
            sharedcity = getSharedPreferences("City", MODE_PRIVATE);
            gu_id = shared.getString("gu","null");
            latestupdate = shared.getString("latestupdate","null");
            android.util.Log.d("awudklwugw",latestupdate);
            latestmessage = shared.getString("latestmessage","null");
            city = sharedcity.getString("city","null");
            sheditor = shared.edit();
        dbHelper = new DBHelper(this);
        db = dbHelper.getWritableDatabase();


       // checkMessage();
        checkUpdate();

        //Dictionary
            cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.LINES_LIST+";",null);
            while (cursorG.moveToNext()){
                lines_list = cursorG.getString(0);
            }
            cursorG.close();

             cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.ROUTING+";",null);
            while (cursorG.moveToNext()){
                routing = cursorG.getString(0);
            }
        cursorG.close();

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.ONLINE_MAP+";",null);
            while (cursorG.moveToNext()){
                online_map = cursorG.getString(0);
            }
        cursorG.close();

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.NEAREST_STATION_TO_ME+";",null);
            while (cursorG.moveToNext()){
                nearest_station_to_me = cursorG.getString(0);
            }
        cursorG.close();

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.SETTING+";",null);
            while (cursorG.moveToNext()){
                setting = cursorG.getString(0);
            }
        cursorG.close();

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.ARMO_TITLE+";",null);
            while (cursorG.moveToNext()){
                aboutus = cursorG.getString(0);
            }
        cursorG.close();

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.TOURISM+";",null);
            while (cursorG.moveToNext()){
                tourism = cursorG.getString(0);
            }
        cursorG.close();

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.NEW_UPDATE_AVAILABLE+";",null);
            while (cursorG.moveToNext())
            {
                update_available = cursorG.getString(0);
            }
        cursorG.close();


        cursorG= db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.MUST_UPDATE+";",null);
             while (cursorG.moveToNext())
             {
                 update_must_dic = cursorG.getString(0);
             }
        cursorG.close();

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.UPDATE_PROGRESS+";",null);
             while (cursorG.moveToNext())
             {
                 update_in_progress_dic = cursorG.getString(0);
             }
        cursorG.close();

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.YES+";",null);
              while (cursorG.moveToNext())
              {
                  yesDic = cursorG.getString(0);
              }
        cursorG.close();

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.NO+";",null);
                while (cursorG.moveToNext())
                {
                    noDic = cursorG.getString(0);
                }
        cursorG.close();

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.LATER+";",null);
                while (cursorG.moveToNext())
                {
                    later = cursorG.getString(0);
                }
        cursorG.close();

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.CONTINUE+";",null);
                while (cursorG.moveToNext())
                {
                    continueDic = cursorG.getString(0);
                }
        cursorG.close();

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.SEND_COMMENT+";",null);
        while (cursorG.moveToNext())
        {
            adviceDic = cursorG.getString(0);
        }
        cursorG.close();

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.NOTIFICATIONS+";",null);
        while (cursorG.moveToNext())
        {
            notificationsDic = cursorG.getString(0);
        }
        cursorG.close();

        // ->

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        changeTypeface(navigationView);



        if(isDatabaseEmpty()==0)
        {
            BackgroundTaskFacilities backgroundTaskFacilities = new BackgroundTaskFacilities(this);
            backgroundTaskFacilities.execute();
        }



    }


    public int isDatabaseEmpty()
    {
        String count = "SELECT count(*) FROM facilities";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        return icount;
    }

    public int isDatabaseScheduleEmpty()
    {
        String count = "SELECT count(*) FROM "+ ScheduleContract.ScheduleEntry.TABLE_NAME_0;
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        return icount;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

    }


    public void setActionBarTitle(String title) {
        topBar_title.setText(title);
    }



    private void changeTypeface(NavigationView navigationView) {
        FontTypeface fontTypeface = new FontTypeface(this);
        Typeface typeface = fontTypeface.getTypefaceAndroid();

        MenuItem item;

        item = navigationView.getMenu().findItem(R.id.nav_lines);
        item.setTitle(lines_list);
        item.setIcon(R.drawable.ic_action_list);
        applyFontToItem(item, typeface);

        item = navigationView.getMenu().findItem(R.id.nav_route);
        item.setTitle(routing);
        item.setIcon(R.drawable.ic_action_route);
        applyFontToItem(item, typeface);

        item = navigationView.getMenu().findItem(R.id.nav_tourism);
        item.setTitle(tourism);
        item.setIcon(R.drawable.ic_tourism);
        applyFontToItem(item, typeface);

        item = navigationView.getMenu().findItem(R.id.nav_map);
        item.setTitle(online_map);
        item.setIcon(R.drawable.ic_action_map);
        applyFontToItem(item, typeface);

        item = navigationView.getMenu().findItem(R.id.nav_neareststation);
        item.setTitle(nearest_station_to_me);
        item.setIcon(R.drawable.ic_action_near);
        applyFontToItem(item, typeface);


        item = navigationView.getMenu().findItem(R.id.nav_messages);
        item.setTitle(notificationsDic);
        item.setIcon(R.drawable.ic_messages);
        applyFontToItem(item, typeface);

        item = navigationView.getMenu().findItem(R.id.nav_setting);
        item.setTitle(setting);
        item.setIcon(R.drawable.ic_action_setting);
        applyFontToItem(item, typeface);

        item = navigationView.getMenu().findItem(R.id.nav_aboutus);
        item.setTitle(aboutus);
        item.setIcon(R.drawable.ic_action_aboutus);
        applyFontToItem(item, typeface);


    }
    private void applyFontToItem(MenuItem item, Typeface font) {
        SpannableString mNewTitle = new SpannableString(item.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font, 16), 0 ,
                mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        item.setTitle(mNewTitle);
    }
    public boolean isGooglePlayAvailable() {
        boolean googlePlayStoreInstalled;
        int val= GooglePlayServicesUtil.isGooglePlayServicesAvailable(Lines.this);
        googlePlayStoreInstalled = val == ConnectionResult.SUCCESS;
        return googlePlayStoreInstalled;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_lines) {
            NewLines fragment = new NewLines();
            android.support.v4.app.FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
        }
        else if (id == R.id.nav_route) {
           /* Routing_fragment fragment = new Routing_fragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();*/
        }
        else if (id == R.id.nav_tourism) {
        /*    TourismFragment fragment = new TourismFragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();*/
        }
        else if (id == R.id.nav_map) {
            if(isGooglePlayAvailable()==true)
            {
                startActivity(new Intent(Lines.this, OnlineMap.class));
            }else if(isGooglePlayAvailable()==false) {
                Toast.makeText(getApplicationContext(),"خدمات گوگل شما قابل استفاده نمیباشد! ابتدا خدمات گوگل خود را آپدیت کنید.",Toast.LENGTH_SHORT).show();
            }
        }
        else if (id == R.id.nav_neareststation) {
            NearestStation_fragment fragment = new NearestStation_fragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
        }
        else if(id == R.id.nav_messages) {
            MessagesFragment fragment = new MessagesFragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
        }
        else if (id == R.id.nav_setting) {
            Setting_fragment fragment = new Setting_fragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
        } else if (id == R.id.nav_aboutus) {
            Aboutus_fragment fragment = new Aboutus_fragment();

            android.support.v4.app.FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    public void checkUpdate()
    {
        if(queue==null)
        {
            queue = Volley.newRequestQueue(getApplicationContext());
        }
        final String url = Connection.portal+"/checkUpdate.php?city="+city+"&id="+latestupdate;
        StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(final String response)
                    {
                        try {
                            if(response.length()!=4)
                            {
                                final Dialog dialog22 = new Dialog(Lines.this);
                                dialog22.setContentView(R.layout.yes_no_dialog);
                                Typeface sans_medium = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_medium.ttf");
                                Typeface sans_light = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_medium.ttf");
                                final Dialog dialogsuccess = new Dialog(Lines.this);
                                dialogsuccess.setContentView(R.layout.success_dialog);
                                TextView title = (TextView) dialog22.findViewById(R.id.title);
                                TextView text = (TextView) dialogsuccess.findViewById(R.id.text);
                                final TextView yes = (TextView) dialog22.findViewById(R.id.yestxt);
                                TextView no = (TextView) dialog22.findViewById(R.id.notxt);
                                LinearLayout yes2 = (LinearLayout) dialog22.findViewById(R.id.yes);
                                LinearLayout no2 = (LinearLayout) dialog22.findViewById(R.id.no);
                                text.setTypeface(sans_medium);
                                yes.setText(yesDic);
                                no.setText(noDic);
                                title.setText(update_available);
                                title.setTypeface(sans_light);
                                yes.setTypeface(sans_light);
                                no.setTypeface(sans_light);

                                dialog22.show();

                                no.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog22.dismiss();
                                    }
                                });
                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        try {
                                          yes.setText("در حال انجام عملیات...");
                                                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                                                final String url = Connection.portal + "/getlatestUpdate.php?city="+city;
                                                StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                                                        new Response.Listener<String>() {
                                                            @Override
                                                            public void onResponse(String response) {
                                                                try {
                                                                    JSONArray jsonArray = new JSONArray(response);
                                                                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                                                                    SharedPreferences shared = getSharedPreferences("MyPref", MODE_PRIVATE);
                                                                    SharedPreferences.Editor editor = shared.edit();
                                                                    editor.putString("latestupdate",jsonObject.getString("latest_id"));
                                                                    //   editor.putString("latestmessage",array[1]);
                                                                    editor.commit();
                                                                    dbHelper.deleteTable(LinesContract.LinesEntry.TABLE_NAME,db);
                                                                    dbHelper.deleteTable(StationsContract.StationsEntry.TABLE_NAME,db);
                                                                    dbHelper.deleteTable(PublicPlacesContract.PublicPlacesEntry.TABLE_NAME,db);
                                                                    dbHelper.deleteTable(PrivatePlacesContract.PrivatePlacesEntry.TABLE_NAME,db);
                                                                    dbHelper.deleteTable(GroupsContract.GroupsEntry.TABLE_NAME,db);
                                                                    dbHelper.deleteTable(AboutUsContract.AboutUsEntry.TBName,db);
                                                                    dbHelper.deleteTable(FacilitiesContract.FacilitiesEntry.TABLE_NAME,db);
                                                                    dbHelper.deleteTable(ScheduleContract.ScheduleEntry.TABLE_NAME_0,db);
                                                                    dbHelper.deleteTable(ScheduleContract.ScheduleEntry.TABLE_NAME_1,db);
                                                                    dbHelper.deleteTable(ScheduleContract.ScheduleEntry.TABLE_NAME_2,db);
                                                                    dbHelper.deleteTable(ScheduleContract.ScheduleEntry.TABLE_NAME_3,db);
                                                                    dbHelper.deleteTable(ScheduleContract.ScheduleEntry.TABLE_NAME_4,db);
                                                                    dbHelper.deleteTable(ScheduleContract.ScheduleEntry.TABLE_NAME_5,db);
                                                                    dbHelper.deleteTable(ScheduleContract.ScheduleEntry.TABLE_NAME_6,db);
                                                                    dialog22.dismiss();
                                                                    finish();
                                                                    startActivity(new Intent(Lines.this,Download.class));
                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            }
                                                        },
                                                        new Response.ErrorListener() {
                                                            @Override
                                                            public void onErrorResponse(VolleyError error) {}
                                                        });


                                                int socketTimeout = 30000;
                                                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                                                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

                                                strRequest.setRetryPolicy(policy);
                                                queue.add(strRequest);


                                        } catch (Exception e)
                                        {

                                        }
                                    }
                                });
                                JSONArray jsonArrayCheck = new JSONArray(response);
                                JSONObject jsonObjectCheck = jsonArrayCheck.getJSONObject(0);
                                isImportant = jsonObjectCheck.getString("emergency");
                                if(isImportant.equals("true"))
                                {
                                    no2.setVisibility(View.GONE);
                                    yes.setText("باشه");
                                    title.setText(update_must_dic);
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            String url = "https://cafebazaar.ir/app/com.mirshekari.metroshiraz/?l=fa";
                                            Intent i = new Intent(Intent.ACTION_VIEW);
                                            i.setData(Uri.parse(url));
                                            startActivity(i);
                                        }
                                    });
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {

                    }
                });



        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        strRequest.setRetryPolicy(policy);
        queue.add(strRequest);
    }
    public String getAppVersion()
    {
        try {
            PackageInfo pInfo = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }



    public void checkMessage()
    {
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        final String url = Connection.portal + "/MessageCheck/" + gu_id + "/" + latestmessage + "/"+city+"/"+"0";
        final Dialog dialog = new Dialog(Lines.this);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_message);
        Typeface irsans_bold = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_bold.ttf");
        Typeface irsans_medium = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_medium.ttf");
        Typeface irsans_light = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_light.ttf");

        final TextView title = (TextView) dialog.findViewById(R.id.title);
        title.setTypeface(irsans_bold);
       final TextView body = (TextView) dialog.findViewById(R.id.body);
        body.setTypeface(irsans_light);
        final Button btn = (Button) dialog.findViewById(R.id.btn);
        btn.setTypeface(irsans_medium);

        StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                                for(int i=0;i<jsonArray.length();i++){
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    String Highlight = jsonObject.getString("Highlight");
                                    String Body = jsonObject.getString("Text");
                                    String Title = jsonObject.getString("Title");
                                    title.setText(Title);
                                    body.setText(Body);
                                    btn.setText(continueDic);
                                    btn.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();
                                        }
                                    });
                                    if(title.length()>1)
                                    {
                                        dialog.show();
                                    }
                                }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                });


        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        strRequest.setRetryPolicy(policy);
        queue.add(strRequest);
    }

}