package com.mirshekari.metroshiraz.Fragments;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.mirshekari.metroshiraz.ChooseStation.StationsContract;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.Place.Public.PublicPlacesC;
import com.mirshekari.metroshiraz.Place.Public.PublicPlacesContract;
import com.mirshekari.metroshiraz.R;
import com.mirshekari.metroshiraz.Routing.Source.SourcePlaceAdapter;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Routing_chooseSourcePrivatePlace extends Fragment {

    EditText name;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public Context context;
    SQLiteDatabase db;
    DBHelper dbHelper;
    Cursor cursorPlaces;
    ArrayList<PublicPlacesC> arrayList = new ArrayList<>();
    String search,enterPlaceNameDic;

    public Routing_chooseSourcePrivatePlace() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_routing_choose_source_private_place, container, false);

        recyclerView = (RecyclerView)v.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setHasFixedSize(true);
        dbHelper = new DBHelper(getActivity());
        db = dbHelper.getReadableDatabase();
        Typeface sans_medium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_medium.ttf");

        name = (EditText) v.findViewById(R.id.station_name);
        name.setTypeface(sans_medium);

        // Dictionary
        Cursor cursorEnterPlaceName = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.ENTER_PLACE_NAME+";",null);
        while (cursorEnterPlaceName.moveToNext())
        {
            enterPlaceNameDic = cursorEnterPlaceName.getString(0);
        }
        cursorEnterPlaceName.close();
        // ->
        name.setHint(enterPlaceNameDic);


        adapter = new SourcePlaceAdapter(arrayList,getActivity());

        cursorPlaces = db.rawQuery("SELECT * FROM " + PublicPlacesContract.PublicPlacesEntry.TABLE_NAME+" WHERE "+ PublicPlacesContract.PublicPlacesEntry.ISPRIVATE+" = 'true';", null);


        while(cursorPlaces.moveToNext()) {
            Cursor check = db.rawQuery("SELECT " + StationsContract.StationsEntry.STATUS + " FROM " + StationsContract.StationsEntry.TABLE_NAME + " WHERE " + StationsContract.StationsEntry.STATION_ID + " = " + cursorPlaces.getString(10) + ";", null);
            while (check.moveToNext()) {
                if (check.getString(0).equals("0")) {
                    PublicPlacesC linesC = new PublicPlacesC(cursorPlaces.getString(0), cursorPlaces.getString(1),
                            cursorPlaces.getInt(2), cursorPlaces.getString(3),
                            cursorPlaces.getString(4), cursorPlaces.getString(5), cursorPlaces.getString(6),
                            cursorPlaces.getInt(7), cursorPlaces.getString(8), cursorPlaces.getString(9),
                            cursorPlaces.getInt(10), cursorPlaces.getString(11),
                            cursorPlaces.getString(12),
                            cursorPlaces.getString(13), cursorPlaces.getString(14),
                            cursorPlaces.getString(15), cursorPlaces.getString(16),
                            cursorPlaces.getString(17), cursorPlaces.getString(18),
                            cursorPlaces.getString(19));
                    arrayList.add(linesC);
                    adapter = new SourcePlaceAdapter(arrayList, getActivity());
                }

            }
        }
        cursorPlaces.close();
        adapter = new SourcePlaceAdapter(arrayList,getActivity());
        recyclerView.setAdapter(adapter);


        name.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                search = "%"+name.getText().toString()+"%";
                String sea = "'"+search+"'";

                arrayList.clear();

                Cursor cursorPlacess = db.rawQuery("SELECT * FROM " + PublicPlacesContract.PublicPlacesEntry.TABLE_NAME+" WHERE "+ PublicPlacesContract.PublicPlacesEntry.ISPRIVATE+" = 'true' AND "+ PublicPlacesContract.PublicPlacesEntry.TITLE+" LIKE "+sea+";", null);


                while(cursorPlacess.moveToNext()) {
                    Cursor check = db.rawQuery("SELECT " + StationsContract.StationsEntry.STATUS + " FROM " + StationsContract.StationsEntry.TABLE_NAME + " WHERE " + StationsContract.StationsEntry.STATION_ID + " = " + cursorPlaces.getString(10) + ";", null);
                    while (check.moveToNext()) {
                        if (check.getString(0).equals("0")) {

                            PublicPlacesC linesC = new PublicPlacesC(cursorPlacess.getString(0), cursorPlacess.getString(1),
                                    cursorPlacess.getInt(2), cursorPlacess.getString(3),
                                    cursorPlacess.getString(4), cursorPlacess.getString(5), cursorPlacess.getString(6),
                                    cursorPlacess.getInt(7), cursorPlacess.getString(8), cursorPlacess.getString(9),
                                    cursorPlacess.getInt(10), cursorPlacess.getString(11),
                                    cursorPlacess.getString(12),
                                    cursorPlacess.getString(13), cursorPlacess.getString(14),
                                    cursorPlacess.getString(15), cursorPlacess.getString(16),
                                    cursorPlacess.getString(17), cursorPlacess.getString(18),
                                    cursorPlacess.getString(19));
                            arrayList.add(linesC);
                            adapter = new SourcePlaceAdapter(arrayList, getActivity());
                        }
                    }
                }
                adapter = new SourcePlaceAdapter(arrayList,getActivity());
                recyclerView.setAdapter(adapter);
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });


        return v;
    }

}
