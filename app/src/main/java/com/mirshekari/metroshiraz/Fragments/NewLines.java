package com.mirshekari.metroshiraz.Fragments;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mirshekari.metroshiraz.AnalyticsApplication;
import com.mirshekari.metroshiraz.ChooseLine.LinesAdapter;
import com.mirshekari.metroshiraz.ChooseLine.LinesC;
import com.mirshekari.metroshiraz.ChooseLine.LinesContract;
import com.mirshekari.metroshiraz.ChooseStation.StationsContract;
import com.mirshekari.metroshiraz.Connection;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.Favorites.Location.FavoriteC;
import com.mirshekari.metroshiraz.Favorites.Location.FavoriteLocationsAdapter;
import com.mirshekari.metroshiraz.Favorites.Location.FavoriteLocationsContract;
import com.mirshekari.metroshiraz.Favorites.Location.Map_choose_favorite_location;
import com.mirshekari.metroshiraz.GPSTracker;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.Lines;
import com.mirshekari.metroshiraz.LinesDesign.Lines_counter_adapter;
import com.mirshekari.metroshiraz.LinesDesign.Lines_counter_items;
import com.mirshekari.metroshiraz.Place.Private.PrivatePlacesContract;
import com.mirshekari.metroshiraz.Place.Public.PublicPlacesContract;
import com.mirshekari.metroshiraz.PrivatePlaceActivity;
import com.mirshekari.metroshiraz.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.events.OnBannerClickListener;
import ss.com.bannerslider.views.BannerSlider;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewLines extends Fragment implements OnMapReadyCallback {

    MapView mapView;
    GoogleMap map;
    double latitude,longitud;
    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabaseLine;
    String dictionary,fave_dic,linesof_dic,city;
    BannerSlider bannerSlider;
    List<Banner> banners;
    private RecyclerView recyclerView,recyclerViewLocations;
    private RecyclerView.Adapter adapter,adapterLocations;
    Cursor cursorG,cursorFavorites;
    LinearLayout adLayout,mapLayout,add;
    TextView favetext,lineTitle;
    ArrayList<LinesC> arrayListLines = new ArrayList<>();
    ArrayList<FavoriteC> favoriteCS = new ArrayList<>();
    Cursor cursor;
    String firstSourceaddress,city2,color,color2,s_latitude,s_longitude,ss_latitude,ss_longitude,sss_latitude,sss_longitude;
    int i=0;
    Double mylat,mylong;
    Tracker mTracker;
    public NewLines() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_new_lines, container, false);
        AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mapView = (MapView) v.findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        dbHelper = new DBHelper(getActivity());
        sqLiteDatabaseLine = dbHelper.getReadableDatabase();
        mTracker.setScreenName("Lines");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        final GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false);
        recyclerViewLocations = (RecyclerView) v.findViewById(R.id.recyclerviewFavorite);
        recyclerViewLocations.setLayoutManager(gridLayoutManager);
        recyclerViewLocations.setHasFixedSize(true);

        cursorFavorites = sqLiteDatabaseLine.rawQuery("SELECT * FROM "+ FavoriteLocationsContract.FavoriteLocationsContractEntry.TABLE_NAME+";",null);
        while (cursorFavorites.moveToNext())
        {
            FavoriteC favoriteC = new FavoriteC(cursorFavorites.getString(2),cursorFavorites.getString(1),cursorFavorites.getString(3),cursorFavorites.getString(4));
            favoriteCS.add(favoriteC);
            adapterLocations = new FavoriteLocationsAdapter(favoriteCS,getActivity());
        }
        recyclerViewLocations.setAdapter(adapterLocations);

        add = (LinearLayout) v.findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Map_choose_favorite_location.class));
            }
        });

        SharedPreferences sharedPreferences2 = getActivity().getSharedPreferences("City",MODE_PRIVATE);
        city = sharedPreferences2.getString("cityName","Unknown");

        Typeface samim = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_medium.ttf");


        favetext = (TextView) v.findViewById(R.id.faveText);
        lineTitle = (TextView) v.findViewById(R.id.lines_title);
        favetext.setTypeface(samim);
        lineTitle.setTypeface(samim);
        bannerSlider = (BannerSlider) v.findViewById(R.id.banner_slider1);
        banners=new ArrayList<>();

        recyclerView = (RecyclerView)v.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        cursor = sqLiteDatabaseLine.rawQuery("SELECT * FROM stations;", null);

        adLayout = (LinearLayout) v.findViewById(R.id.adLayout);
        mapLayout = (LinearLayout) v.findViewById(R.id.ll1);


        // Dictionary
        Cursor cursor = sqLiteDatabaseLine.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.LINES_LIST+";",null);
        while (cursor.moveToNext()){
            dictionary = cursor.getString(0);
        }
        cursor = sqLiteDatabaseLine.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.LINES_OF+";",null);
        while (cursor.moveToNext()){
            linesof_dic = cursor.getString(0);
        }
        cursor = sqLiteDatabaseLine.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.FAVORITE_LOCATIONS+";",null);
        while (cursor.moveToNext()){
            fave_dic = cursor.getString(0);
        }
        cursor.close();
        // ->

        favetext.setText(fave_dic);
        lineTitle.setText(linesof_dic+city);

        // Set title bar
        ((Lines) getActivity()).setActionBarTitle(dictionary);
        // ->


        GPSTracker tracker = new GPSTracker(getActivity());
        if (!tracker.canGetLocation()) {
            tracker.showSettingsAlert();
        } else {
            mylat = tracker.getLatitude();
            mylong = tracker.getLongitude();
        }

        Cursor cursorListLines = sqLiteDatabaseLine.rawQuery("SELECT * FROM "+ LinesContract.LinesEntry.TABLE_NAME+" WHERE "+ LinesContract.LinesEntry.STATUS+" = "+"0"+" ORDER BY "+ LinesContract.LinesEntry.LINENUMBER+" ASC;",null);
        while(cursorListLines.moveToNext()) {
            String name = cursorListLines.getString(8);
            if(!name.contains("("))
            {
                LinesC linesC = new LinesC(cursorListLines.getString(0), cursorListLines.getString(1), cursorListLines.getInt(2), cursorListLines.getInt(3), cursorListLines.getInt(4), cursorListLines.getInt(5), cursorListLines.getInt(6), cursorListLines.getString(7), cursorListLines.getString(8), cursorListLines.getString(9), cursorListLines.getString(10), cursorListLines.getString(11));
                arrayListLines.add(linesC);
                adapter = new LinesAdapter(arrayListLines, getActivity());
            }
        }
        adapter = new LinesAdapter(arrayListLines);
        recyclerView.setAdapter(adapter);
        cursorListLines.close();

        mapLayout.post(new Runnable(){
            public void run(){
                int height = mapLayout.getHeight();
                adLayout.getLayoutParams().height = height-50;
            }
        });

            // getting Commercials From Sqlite ---------------------------------------------------------
            HashMap<String,String> url_maps = new HashMap<String, String>();
            cursorG = sqLiteDatabaseLine.rawQuery("SELECT * FROM privateplaces WHERE "+ PrivatePlacesContract.PrivatePlacesEntry.ISVIP+" = 'true';", null);
            if(cursorG.getCount()==0)
            {
                adLayout.setVisibility(View.GONE);
            }
            else{
                while (cursorG.moveToNext())
                {
                    String placeid = cursorG.getString(7);
                    cursorG= sqLiteDatabaseLine.rawQuery("SELECT * FROM publicplaces WHERE "+ PublicPlacesContract.PublicPlacesEntry.ID+" = "+placeid+";", null);
                    while (cursorG.moveToNext()){
                        String banner = cursorG.getString(19);
                        if(banner != null) {
                            banners.add(new RemoteBanner(Connection.images+banner));
                            SharedPreferences sharedPreferences = getActivity().getSharedPreferences("slider",MODE_PRIVATE);
                            SharedPreferences.Editor sheditor = sharedPreferences.edit();
                            sheditor.putString("index"+String.valueOf(bannerSlider.getCurrentSlidePosition()+1),cursorG.getString(2));
                            sheditor.commit();
                        }
                    }
                }
            }
            // getting Commercials From Sqlite ---------------------------------------------------------


            // Slider Image ------------------------------------------------------------------------------
            bannerSlider.setBanners(banners);
            bannerSlider.setOnBannerClickListener(new OnBannerClickListener() {
                @Override
                public void onClick(int position) {
                    Intent intent = new Intent(getActivity(),PrivatePlaceActivity.class);
                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences("slider",MODE_PRIVATE);
                    String index = sharedPreferences.getString("index"+position,"5,5");
                    intent.putExtra("id",index);
                    startActivity(intent);
                }
            });
            // Slider Image ------------------------------------------------------------------------------




        return v;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.getUiSettings().setMyLocationButtonEnabled(false);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            return;
        }

        latitude = mylat;
        longitud = mylong;
        map.moveCamera(CameraUpdateFactory.newLatLng( new LatLng(latitude, longitud)));
        CameraUpdate zoom=CameraUpdateFactory.zoomTo(15);
        map.animateCamera(zoom);
        map.setMyLocationEnabled(true);
        map.isMyLocationEnabled();


        while (cursor.moveToNext()){
            String lat = cursor.getString(3);
            String[] s = lat.split(",");
            s_latitude = s[0];
            s_longitude = s[1];

            if(i==0){
                if(!cursor.isLast()) {
                    cursor.moveToNext();
                    String lon = cursor.getString(3);
                    String[] l = lon.split(",");
                    ss_latitude = l[0];
                    ss_longitude =l[1];
                }
            }


            if(i!=0){
                cursor.moveToPrevious();
                String lat1 = cursor.getString(3);
                String[] s1 = lat1.split(",");
                s_latitude = s1[0];
                s_longitude = s1[1];
                if(!cursor.isLast()) {
                    cursor.moveToNext();
                    String lon = cursor.getString(3);
                    String[] l = lon.split(",");
                    ss_latitude = l[0];
                    ss_longitude =l[1];
                }
            }


            String line_id = cursor.getString(2);

            LatLng latLng = new LatLng(Double.valueOf(s_latitude),Double.valueOf(s_longitude));

            LatLng doran = new LatLng(Double.valueOf(s_latitude),Double.valueOf(s_longitude));
            googleMap.addMarker(new MarkerOptions()
                    .position(doran)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker))
            );

            LatLng doran2 = new LatLng(Double.valueOf(ss_latitude),Double.valueOf(ss_longitude));
            googleMap.addMarker(new MarkerOptions()
                    .position(doran)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker))
            );

            i++;
        }


    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}
