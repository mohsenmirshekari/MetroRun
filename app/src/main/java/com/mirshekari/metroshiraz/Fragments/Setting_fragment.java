package com.mirshekari.metroshiraz.Fragments;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mirshekari.metroshiraz.Aboutus.AboutUsContract;
import com.mirshekari.metroshiraz.AnalyticsApplication;
import com.mirshekari.metroshiraz.ChooseCity.CitiesContract;
import com.mirshekari.metroshiraz.ChooseLine.LinesContract;
import com.mirshekari.metroshiraz.ChooseStation.StationsContract;
import com.mirshekari.metroshiraz.Connection;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.Download;
import com.mirshekari.metroshiraz.Facilities.FacilitiesContract;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.Lines;
import com.mirshekari.metroshiraz.Place.Groups.GroupsContract;
import com.mirshekari.metroshiraz.Place.Private.PrivatePlacesContract;
import com.mirshekari.metroshiraz.Place.Public.PublicPlacesContract;
import com.mirshekari.metroshiraz.R;
import com.mirshekari.metroshiraz.Schedule.ScheduleContract;
import com.mirshekari.metroshiraz.UpdateSchedule;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class Setting_fragment extends Fragment {

    Cursor cursor,cursorG;
    SharedPreferences.Editor sheditor;
    String id;
    TextView title;
    RequestQueue queue;
    String continueDic,
            latestupdate,update_in_progress_dic,update_must_dic,update_available,
            later,routing,gu_id;
    String isImportant;
    String version,city;
    String station_id;
    TextView updateText,cityText,languageText;
    LinearLayout cityL,language,update;
    public Dialog dialog;
    SQLiteDatabase db;
    DBHelper dbHelper;
    Tracker mTracker;
    String dictionary;
    String updateDic,cityDic,langDic,warn1,warn2,yesDic,noDic;
    public Setting_fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_setting_fragment, container, false);

        Typeface irsans_light = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_light.ttf");
        AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        update = (LinearLayout) v.findViewById(R.id.update);
        mTracker.setScreenName("Setting fragment");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        SharedPreferences shared = getActivity().getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences sharedcity = getActivity().getSharedPreferences("City", MODE_PRIVATE);
        gu_id = shared.getString("gu","null");
        latestupdate = shared.getString("latestupdate","null");
        city = sharedcity.getString("city","null");
        sheditor = shared.edit();

        dbHelper = new DBHelper(getActivity());
        db = dbHelper.getReadableDatabase();

        // Dictionary
        Cursor cursor = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.SETTING+";",null);
        while (cursor.moveToNext()){
            dictionary = cursor.getString(0);
        }
        cursor.close();

        Cursor cursorUpdate = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.UPDATE+";",null);
        while (cursorUpdate.moveToNext()){
            updateDic = cursorUpdate.getString(0);
        }
        cursorUpdate.close();
        Cursor cursorCity = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.CHANGE_CITY+";",null);
        while (cursorCity.moveToNext()){
            cityDic = cursorCity.getString(0);
        }
cursorCity.close();
        Cursor cursorLang= db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.CHANGE_LANGUAGE+";",null);
        while (cursorLang.moveToNext()){
            langDic = cursorLang.getString(0);
        }
cursorLang.close();
        Cursor cursorWarn1 = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.REMOVE_WARNING_1+";",null);
        while (cursorWarn1.moveToNext()){
            warn1 = cursorWarn1.getString(0);
        }
        cursorWarn1.close();

        Cursor cursorWarn2 = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.REMOVE_WARNING_2+";",null);
        while (cursorWarn2.moveToNext()){
            warn2 = cursorWarn2.getString(0);
        }
        cursorWarn2.close();
        Cursor cursorYes = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.YES+";",null);
        while (cursorYes.moveToNext()){
            yesDic = cursorYes.getString(0);
        }
        cursorYes.close();
        Cursor cursorNo = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.NO+";",null);
        while (cursorNo.moveToNext()){
            noDic = cursorNo.getString(0);
        }
        cursorNo.close();

        cursorG= db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.MUST_UPDATE+";",null);
        while (cursorG.moveToNext())
        {
            update_must_dic = cursorG.getString(0);
        }
        cursorG.close();

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.UPDATE_PROGRESS+";",null);
        while (cursorG.moveToNext())
        {
            update_in_progress_dic = cursorG.getString(0);
        }
        cursorG.close();

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.YES+";",null);
        while (cursorG.moveToNext())
        {
            yesDic = cursorG.getString(0);
        }
        cursorG.close();

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.NO+";",null);
        while (cursorG.moveToNext())
        {
            noDic = cursorG.getString(0);
        }
        cursorG.close();

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.LATER+";",null);
        while (cursorG.moveToNext())
        {
            later = cursorG.getString(0);
        }
        cursorG.close();

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.CONTINUE+";",null);
        while (cursorG.moveToNext())
        {
            continueDic = cursorG.getString(0);
        }
        cursorG.close();
        // ->

        // Set title bar
        ((Lines) getActivity()).setActionBarTitle(dictionary);
        // ->


        updateText = (TextView) v.findViewById(R.id.updateText);
        cityText = (TextView) v.findViewById(R.id.cityText);
        languageText = (TextView) v.findViewById(R.id.languageText);
        languageText.setTypeface(irsans_light);
        updateText.setTypeface(irsans_light);
        cityText.setTypeface(irsans_light);

        updateText.setText(updateDic);
        cityText.setText(cityDic);
        languageText.setText(langDic);



        language = (LinearLayout) v.findViewById(R.id.language);
        language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(getActivity());
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.yes_no_dialog);
                Typeface samim = Typeface.createFromAsset(getActivity().getAssets(), "fonts/samim.ttf");
                Typeface irsans = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_light.ttf");

                SharedPreferences sharedPreferences = getActivity().getSharedPreferences("City", Context.MODE_PRIVATE);
                String cityname = sharedPreferences.getString("cityName","null");

                LinearLayout yes = (LinearLayout) dialog.findViewById(R.id.yes);
                LinearLayout no = (LinearLayout) dialog.findViewById(R.id.no);

                TextView yesText = (TextView) dialog.findViewById(R.id.yestxt);
                TextView noText = (TextView) dialog.findViewById(R.id.notxt);
                TextView title = (TextView) dialog.findViewById(R.id.title);
                yesText.setTypeface(irsans);
                title.setTypeface(irsans);
                noText.setTypeface(irsans);
                yesText.setText(yesDic);
                noText.setText(noDic);

                String t1 = warn1;
                String t2 = t1+" "+cityname+" ";
                String t = t2+warn2;

                title.setText(t);


                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        File file = new File("/data/data/com.mirshekari.metroshiraz/databases/metrorun_app");
                        file.delete();
                        Intent i = getActivity().getBaseContext().getPackageManager()
                                .getLaunchIntentForPackage( getActivity().getBaseContext().getPackageName() );
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                    }
                });

                dialog.show();
            }
        });
        language.setVisibility(View.GONE);

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkUpdate();
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Menu")
                        .setAction("Update Button"+" ("+city+")")
                        .build());
            }
        });

        cityL = (LinearLayout) v.findViewById(R.id.city);

        cityL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(getActivity());
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.yes_no_dialog);
                Typeface samim = Typeface.createFromAsset(getActivity().getAssets(), "fonts/samim.ttf");
                Typeface irsans = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_light.ttf");

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Menu")
                        .setAction("Change City Button"+" ("+city+")")
                        .build());

                SharedPreferences sharedPreferences = getActivity().getSharedPreferences("City", Context.MODE_PRIVATE);
                String cityname = sharedPreferences.getString("cityName","null");

                LinearLayout yes = (LinearLayout) dialog.findViewById(R.id.yes);
                LinearLayout no = (LinearLayout) dialog.findViewById(R.id.no);

                TextView yesText = (TextView) dialog.findViewById(R.id.yestxt);
                TextView noText = (TextView) dialog.findViewById(R.id.notxt);
                TextView title = (TextView) dialog.findViewById(R.id.title);
                yesText.setTypeface(irsans);
                title.setTypeface(irsans);
                noText.setTypeface(irsans);
                yesText.setText(yesDic);
                noText.setText(noDic);

                String t1 = warn1;
                String t2 = t1+" "+cityname+" ";
                String t = t2+warn2;

                title.setText(t);



                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dbHelper.deleteTable(LinesContract.LinesEntry.TABLE_NAME,db);
                        dbHelper.deleteTable(CitiesContract.CitiesEntry.TABLE_NAME,db);
                        dbHelper.deleteTable(StationsContract.StationsEntry.TABLE_NAME,db);
                        dbHelper.deleteTable(PublicPlacesContract.PublicPlacesEntry.TABLE_NAME,db);
                        dbHelper.deleteTable(PrivatePlacesContract.PrivatePlacesEntry.TABLE_NAME,db);
                        dbHelper.deleteTable(GroupsContract.GroupsEntry.TABLE_NAME,db);
                        dbHelper.deleteTable(AboutUsContract.AboutUsEntry.TBName,db);
                        dbHelper.deleteTable(FacilitiesContract.FacilitiesEntry.TABLE_NAME,db);
                        dbHelper.deleteTable(ScheduleContract.ScheduleEntry.TABLE_NAME_0,db);
                        dbHelper.deleteTable(ScheduleContract.ScheduleEntry.TABLE_NAME_1,db);
                        dbHelper.deleteTable(ScheduleContract.ScheduleEntry.TABLE_NAME_2,db);
                        dbHelper.deleteTable(ScheduleContract.ScheduleEntry.TABLE_NAME_3,db);
                        dbHelper.deleteTable(ScheduleContract.ScheduleEntry.TABLE_NAME_4,db);
                        dbHelper.deleteTable(ScheduleContract.ScheduleEntry.TABLE_NAME_5,db);
                        dbHelper.deleteTable(ScheduleContract.ScheduleEntry.TABLE_NAME_6,db);
                        Intent i = getActivity().getBaseContext().getPackageManager()
                                .getLaunchIntentForPackage( getActivity().getBaseContext().getPackageName() );
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                    }
                });

                dialog.show();
            }
        });
        return v;
    }

    public void checkUpdate()
    {
        if(queue==null)
        {
            queue = Volley.newRequestQueue(getActivity());
        }
        final String url = Connection.portal+"/checkUpdate.php?city="+city+"&id="+latestupdate;
        StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(final String response)
                    {
                        try {
                            if(response.length()!=4)
                            {
                                final Dialog dialog22 = new Dialog(getActivity());
                                dialog22.setContentView(R.layout.yes_no_dialog);
                                Typeface sans_medium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_medium.ttf");
                                Typeface sans_light = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_medium.ttf");
                                final Dialog dialogsuccess = new Dialog(getActivity());
                                dialogsuccess.setContentView(R.layout.success_dialog);
                                TextView title = (TextView) dialog22.findViewById(R.id.title);
                                TextView text = (TextView) dialogsuccess.findViewById(R.id.text);
                                final TextView yes = (TextView) dialog22.findViewById(R.id.yestxt);
                                TextView no = (TextView) dialog22.findViewById(R.id.notxt);
                                LinearLayout yes2 = (LinearLayout) dialog22.findViewById(R.id.yes);
                                LinearLayout no2 = (LinearLayout) dialog22.findViewById(R.id.no);
                                text.setTypeface(sans_medium);
                                yes.setText(yesDic);
                                no.setText(noDic);
                                title.setText(update_available);
                                title.setTypeface(sans_light);
                                yes.setTypeface(sans_light);
                                no.setTypeface(sans_light);

                                dialog22.show();

                                no.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog22.dismiss();
                                    }
                                });
                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        try {
                                            yes.setText("در حال انجام عملیات...");
                                            RequestQueue queue = Volley.newRequestQueue(getActivity());
                                            final String url = Connection.portal + "/getlatestUpdate?city="+city;
                                            StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                                                    new Response.Listener<String>() {
                                                        @Override
                                                        public void onResponse(String response) {
                                                            try {
                                                                JSONArray jsonArray = new JSONArray(response);
                                                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                                                SharedPreferences shared = getActivity().getSharedPreferences("MyPref", MODE_PRIVATE);
                                                                SharedPreferences.Editor editor = shared.edit();
                                                                editor.putString("latestupdate",jsonObject.getString("latest_id"));
                                                                //   editor.putString("latestmessage",array[1]);
                                                                editor.commit();
                                                                dbHelper.deleteTable(LinesContract.LinesEntry.TABLE_NAME,db);
                                                                dbHelper.deleteTable(StationsContract.StationsEntry.TABLE_NAME,db);
                                                                dbHelper.deleteTable(PublicPlacesContract.PublicPlacesEntry.TABLE_NAME,db);
                                                                dbHelper.deleteTable(PrivatePlacesContract.PrivatePlacesEntry.TABLE_NAME,db);
                                                                dbHelper.deleteTable(GroupsContract.GroupsEntry.TABLE_NAME,db);
                                                                dbHelper.deleteTable(AboutUsContract.AboutUsEntry.TBName,db);
                                                                dbHelper.deleteTable(FacilitiesContract.FacilitiesEntry.TABLE_NAME,db);
                                                                dbHelper.deleteTable(ScheduleContract.ScheduleEntry.TABLE_NAME_0,db);
                                                                dbHelper.deleteTable(ScheduleContract.ScheduleEntry.TABLE_NAME_1,db);
                                                                dbHelper.deleteTable(ScheduleContract.ScheduleEntry.TABLE_NAME_2,db);
                                                                dbHelper.deleteTable(ScheduleContract.ScheduleEntry.TABLE_NAME_3,db);
                                                                dbHelper.deleteTable(ScheduleContract.ScheduleEntry.TABLE_NAME_4,db);
                                                                dbHelper.deleteTable(ScheduleContract.ScheduleEntry.TABLE_NAME_5,db);
                                                                dbHelper.deleteTable(ScheduleContract.ScheduleEntry.TABLE_NAME_6,db);
                                                                dialog22.dismiss();
                                                                getActivity().finish();
                                                                startActivity(new Intent(getActivity(),Download.class));
                                                            } catch (JSONException e) {
                                                                e.printStackTrace();
                                                            }

                                                        }
                                                    },
                                                    new Response.ErrorListener() {
                                                        @Override
                                                        public void onErrorResponse(VolleyError error) {}
                                                    });


                                            int socketTimeout = 30000; // 30 seconds. You can change it
                                            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                                                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

                                            strRequest.setRetryPolicy(policy);
                                            queue.add(strRequest);


                                        } catch (Exception e)
                                        {

                                        }
                                    }
                                });
                                JSONArray jsonArrayCheck = new JSONArray(response);
                                JSONObject jsonObjectCheck = jsonArrayCheck.getJSONObject(0);
                                isImportant = jsonObjectCheck.getString("emergency");
                                if(isImportant.equals("true"))
                                {
                                    no2.setVisibility(View.GONE);
                                    title.setText(update_must_dic);
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            // Open google play link of app
                                        }
                                    });
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {

                    }
                });



        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        strRequest.setRetryPolicy(policy);
        queue.add(strRequest);
    }


    public String getAppVersion()
    {
        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }

}
