package com.mirshekari.metroshiraz.Fragments;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.Place.Groups.GroupsC;
import com.mirshekari.metroshiraz.Place.Groups.GroupsContract;
import com.mirshekari.metroshiraz.Place.Groups.GroupsTourismAdapter;
import com.mirshekari.metroshiraz.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChooseGroup extends Fragment {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    SQLiteDatabase db;
    DBHelper dbHelper;
    ArrayList<GroupsC> arrayList = new ArrayList<>();

    public ChooseGroup() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_choose_group, container, false);
        recyclerView = (RecyclerView)v.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        dbHelper = new DBHelper(getActivity());
        db = dbHelper.getReadableDatabase();


        Cursor cursor = db.rawQuery("SELECT * FROM "+ GroupsContract.GroupsEntry.TABLE_NAME,null);
        while (cursor.moveToNext()){
            GroupsC groupsC = new GroupsC(cursor.getInt(0),cursor.getString(1));
            arrayList.add(groupsC);
            adapter = new GroupsTourismAdapter(arrayList,getActivity());
        }
        adapter = new GroupsTourismAdapter(arrayList,getActivity());
        recyclerView.setAdapter(adapter);
        return v;
    }

}
