package com.mirshekari.metroshiraz.Fragments;


import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.Lines;
import com.mirshekari.metroshiraz.R;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class Routing_fragment extends Fragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    SharedPreferences sharedLang;
    SharedPreferences.Editor editor;
    String dictionary,byPlace,byStation,lang_name;
    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;
    Cursor cursorG;
    public Routing_fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_routing_fragment, container, false);
        viewPager = (ViewPager) v.findViewById(R.id.viewpager);

        sharedLang = getActivity().getSharedPreferences("MyPref",MODE_PRIVATE);
        lang_name = sharedLang.getString("langname","null");

        dbHelper = new DBHelper(getActivity());
        sqLiteDatabase = dbHelper.getReadableDatabase();
        // Dictionary
        cursorG = sqLiteDatabase.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.ROUTING+";",null);
        while (cursorG.moveToNext()){
            dictionary = cursorG.getString(0);
        }
        cursorG = sqLiteDatabase.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.BY_PLACE+";",null);
        while (cursorG.moveToNext()){
            byPlace = cursorG.getString(0);
        }
        cursorG = sqLiteDatabase.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.BY_STATION+";",null);
        while (cursorG.moveToNext()){
            byStation = cursorG.getString(0);
        }

        cursorG.close();
        // ->

        setupViewPager(viewPager,byPlace,byStation);

        // Set title bar
        ((Lines) getActivity()).setActionBarTitle(dictionary);
        // ->

        tabLayout = (TabLayout) v.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        if(lang_name.equals("فارسي") || lang_name.equals("عربی")){
            changeTabsFont();
        }
        else{
            changeTabsFontEnglish();
        }
        return v;
    }



    private void changeTabsFont() {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        Typeface fonts = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_light.ttf");
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(fonts);
                }
            }
        }
    }


    private void changeTabsFontEnglish() {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        Typeface fonts = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_medium.ttf");
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(fonts);
                }
            }
        }
    }

    private void setupViewPager(ViewPager viewPager,String byplace,String bystation) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new Routing_byStation(), bystation);
        adapter.addFragment(new Routing_byPlace(), byplace);
        viewPager.setAdapter(adapter);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
