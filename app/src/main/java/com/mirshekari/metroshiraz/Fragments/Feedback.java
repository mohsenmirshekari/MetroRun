package com.mirshekari.metroshiraz.Fragments;


import android.app.Dialog;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mirshekari.metroshiraz.Connection;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.Lines;
import com.mirshekari.metroshiraz.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class Feedback extends Fragment {

    EditText text;
    Button send;
    Cursor cursorG;
    String gu_id,adviceDic,sendDeic,writeDic,submittedDic,submitFailDic;
    SQLiteDatabase db;
    DBHelper dbHelper;

    public Feedback() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_feedback, container, false);

        Typeface irsans_light = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_light.ttf");
        Typeface irsans_medium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_medium.ttf");

        dbHelper = new DBHelper(getActivity());
        db = dbHelper.getWritableDatabase();


        // Dictionary

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.SEND_COMMENT+";",null);
        while (cursorG.moveToNext())
        {
            adviceDic = cursorG.getString(0);
        }

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.WRITE_COMMENT+";",null);
        while (cursorG.moveToNext())
        {
            writeDic = cursorG.getString(0);
        }

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.SEND+";",null);
        while (cursorG.moveToNext())
        {
            sendDeic = cursorG.getString(0);
        }

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.SUBMITTED+";",null);
        while (cursorG.moveToNext())
        {
            submittedDic = cursorG.getString(0);
        }

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.SUBMIT_FAIL+";",null);
        while (cursorG.moveToNext())
        {
            submitFailDic = cursorG.getString(0);
        }
        cursorG.close();
        // ->


        // Set title bar
        ((Lines) getActivity()).setActionBarTitle(adviceDic);
        // ->

        SharedPreferences shared = getActivity().getSharedPreferences("MyPref", MODE_PRIVATE);
        gu_id = shared.getString("gu","null");

        text = (EditText) v.findViewById(R.id.text);
        send = (Button) v.findViewById(R.id.send);
        text.setTypeface(irsans_light);
        send.setTypeface(irsans_medium);
        text.setHint(writeDic);
        send.setText(sendDeic);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(text.length()>0){
                    text.setBackgroundColor(Color.parseColor("#fafafa"));
                    RequestQueue queue = Volley.newRequestQueue(getActivity());
                    final String url = Connection.portal + "/Comment/" + gu_id+"/"+text.getText().toString();
                    StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {

                                    android.util.Log.d("wdawqqqqq",response);
                                    if(response.equals("true"))
                                    {
                                        send.setText(submittedDic);
                                        send.setBackgroundColor(Color.parseColor("#20bf6b"));
                                    }
                                    else
                                    {
                                        send.setText(submitFailDic);
                                        send.setBackgroundColor(Color.parseColor("#e74c3c"));
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {


                                }
                            });


                    int socketTimeout = 30000;
                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

                    strRequest.setRetryPolicy(policy);
                    queue.add(strRequest);
                }
                else{
                    send.setBackgroundColor(Color.parseColor("#e74c3c"));
                }
            }
        });
        return v;
    }

}
