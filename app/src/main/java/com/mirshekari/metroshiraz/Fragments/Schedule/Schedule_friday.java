package com.mirshekari.metroshiraz.Fragments.Schedule;


import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.LinearLayoutManagerWithSmoothScroller;
import com.mirshekari.metroshiraz.R;
import com.mirshekari.metroshiraz.Schedule.ScheduleAdapter;
import com.mirshekari.metroshiraz.Schedule.ScheduleC;
import com.mirshekari.metroshiraz.Schedule.ScheduleContract;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import static android.content.Context.MODE_PRIVATE;
import static com.mirshekari.metroshiraz.Fragments.Schedule.Schedule_saturday.faToEn;

public class Schedule_friday extends Fragment {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public Context context;
    DBHelper dbHelper,dbHelper1;
    SQLiteDatabase sqLiteDatabaseSchedule,sqLiteDatabaseSchedule1;
    Cursor cursor,cursorNextMove;
    ArrayList<ScheduleC> arrayList = new ArrayList<>();
    int dow;
    TextView nextTime;
    String routeid,route;
    String [] array;
    SharedPreferences sharedLang;
    String lang_name,nextmove,and,minute;
    public Schedule_friday() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_schedule_friday, container, false);
        nextTime = (TextView) v.findViewById(R.id.nextTime);
        Typeface irsans_light = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_light.ttf");
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("MyPref",MODE_PRIVATE);
        route = sharedPreferences.getString("route","nex");

        nextTime.setTypeface(irsans_light);

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        recyclerView = (RecyclerView) v.findViewById(R.id.rv);
        LinearLayoutManagerWithSmoothScroller linearLayoutManagerWithSmoothScroller = new LinearLayoutManagerWithSmoothScroller(getActivity());
        recyclerView.setLayoutManager(linearLayoutManagerWithSmoothScroller);
        recyclerView.setHasFixedSize(true);

        adapter = new ScheduleAdapter(arrayList,context);
        dbHelper = new DBHelper(getActivity());
        sqLiteDatabaseSchedule = dbHelper.getReadableDatabase();
        dow  = day;
        TimeZone tz = TimeZone.getTimeZone("GMT+03:30");
        Calendar c = Calendar.getInstance(tz);
        String time = String.format("%02d" , c.get(Calendar.HOUR_OF_DAY))+
                String.format("%02d" , c.get(Calendar.MINUTE));
        final Integer firun = Integer.valueOf(time)+100;

        sharedLang = getActivity().getSharedPreferences("MyPref",MODE_PRIVATE);
        lang_name = sharedLang.getString("langname","null");

        // Dictionary
        Cursor nextmoveCursor = sqLiteDatabaseSchedule.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.TRAIN_NEXT_MOVE+";",null);
        Cursor andCursor = sqLiteDatabaseSchedule.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.AND+";",null);
        Cursor minuteCursor = sqLiteDatabaseSchedule.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.MINUTE+";",null);

        while (nextmoveCursor.moveToNext()){
            nextmove = nextmoveCursor.getString(0);
        }

        while (andCursor.moveToNext()){
            and = andCursor.getString(0);
        }

        while (minuteCursor.moveToNext()){
            minute = minuteCursor.getString(0);
        }

        nextmoveCursor.close();
        andCursor.close();
        minuteCursor.close();
        // ->

        SharedPreferences shared = getActivity().getSharedPreferences("StationInfo", MODE_PRIVATE);
        String stid = shared.getString("station_id","null");
        Integer stationid = Integer.valueOf(stid)+1;
        Integer stationid1 = Integer.valueOf(stid);
        Integer stationid2 = stationid-1;
        Integer stationid3 = Integer.valueOf(stid)-1;
        Integer currentStation = Integer.valueOf(stid);
        Integer nextStation = Integer.valueOf(stid)+1;
        Integer prevStation = Integer.valueOf(stid)-1;


        Cursor cursorrouteCurrentStation = sqLiteDatabaseSchedule.rawQuery("SELECT route FROM "+ ScheduleContract.ScheduleEntry.TABLE_NAME_6+ " WHERE station_id = " + stid + ";", null);

        if (cursorrouteCurrentStation.moveToFirst()) {
            do {
                routeid = cursorrouteCurrentStation.getString(0);
            } while (cursorrouteCurrentStation.moveToNext());
        }

        cursor = sqLiteDatabaseSchedule.rawQuery("SELECT distinct * FROM "+ ScheduleContract.ScheduleEntry.TABLE_NAME_6+" WHERE station_id = "+currentStation+" ORDER BY time ASC",null);
        if(route.equals("next"))
        {
            while (cursor.moveToNext())
            {
                String time1 = cursor.getString(0);
                time1 = time1.substring(2, time1.length()-2);
                array = time1.split(",");
                break;
            }
        }
        else if(route.equals("prev"))
        {
            android.util.Log.d("wajkeywqqq","prev");
            while (cursor.moveToNext())
            {
                cursor.moveToLast();
                String time1 = cursor.getString(0);
                time1 = time1.substring(2, time1.length()-2);
                array = time1.split(",");

            }
        }


        for(int i=0;i<array.length;i++)
        {
            ScheduleC cities = new ScheduleC(array[i], i,"",currentStation,0);
            arrayList.add(cities);
            adapter = new ScheduleAdapter(arrayList,getActivity());
        }

        dbHelper.close();
        adapter = new ScheduleAdapter(arrayList);
        recyclerView.setAdapter(adapter);

        dbHelper1 = new DBHelper(getActivity());
        sqLiteDatabaseSchedule1 = dbHelper1.getReadableDatabase();
        cursorNextMove = sqLiteDatabaseSchedule1.rawQuery("SELECT * FROM " + ScheduleContract.ScheduleEntry.TABLE_NAME_6+" WHERE "+ ScheduleContract.ScheduleEntry.STATION_ID+"="+currentStation+" ORDER BY time ASC", null);
        for(int i=0;i<array.length;i++) {
            array[i] = array[i].substring(1, array[i].length()-1);
            if (Integer.valueOf(array[i])> firun) {
                if(dow==6)
                {
                    recyclerView.smoothScrollToPosition(i);
                }


                /**     int ftd = Integer.parseInt(Integer.toString(one).substring(0, 2));
                     int std = one % 100;
                     String ftd1 = faToEn(String.valueOf(ftd));
                     String std1 = faToEn(String.valueOf(std));
                     if(lang_name.equals("فارسي"))
                     {
                         nextTime.setText(nextmove+ftd1+" "+and+" "+std1+ " "+minute);
                     }
                     else
                     {
                         nextTime.setText(nextmove+" "+String.valueOf(ftd)+":"+String.valueOf(std));
                     }           **/
           break;
            }
        }
        return v;
    }

}
