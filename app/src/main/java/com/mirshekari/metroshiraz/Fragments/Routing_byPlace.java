package com.mirshekari.metroshiraz.Fragments;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mirshekari.metroshiraz.ChooseLine.LinesContract;
import com.mirshekari.metroshiraz.ChooseStation.StationsContract;
import com.mirshekari.metroshiraz.Connection;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.MySingleton;
import com.mirshekari.metroshiraz.R;
import com.mirshekari.metroshiraz.Routing.Source.SuggestionAdapter;
import com.mirshekari.metroshiraz.Routing.Source.SuggestionC;
import com.mirshekari.metroshiraz.Routing_ChoosePlaceSource;
import com.mirshekari.metroshiraz.Routing_ChoosePlace_destination;
import com.mirshekari.metroshiraz.Routing_search;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class Routing_byPlace extends Fragment {

    TextView txtFrom,txtTo;
    Button btnSearch,btnSource,btnDestination;
    SharedPreferences routingInfo,gu;
    SharedPreferences.Editor sheditor;
    SQLiteDatabase db;
    DBHelper dbHelper;
    ArrayList<SuggestionC> arrayList = new ArrayList<>();
    Dialog dialog;
    Cursor cursorG;
    SuggestionAdapter adapter;
    String gu_id,city_id,m,n,mt,nt,mLine,nLine,mColor,nColor,mi,ni,firstDic,dismissDic;
    RecyclerView recyclerView;
    String from,to,choosePlace,searchText;
    String choosePathDic;
    public Routing_byPlace() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_routing_by_place, container, false);
        Typeface sans_bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_bold.ttf");
        Typeface sans_medium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_medium.ttf");

        dbHelper = new DBHelper(getActivity());
        db = dbHelper.getReadableDatabase();
        gu = getActivity().getSharedPreferences("MyPref", MODE_PRIVATE);
        gu_id = gu.getString("gu","null");

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("City",MODE_PRIVATE);
        city_id = sharedPreferences.getString("city","null");

        // Dictionary
        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.TO+";",null);
        while (cursorG.moveToNext()){
            to = cursorG.getString(0);
        }

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.FROM+";",null);
        while (cursorG.moveToNext()){
            from = cursorG.getString(0);
        }

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.CHOOSE_PLACE+";",null);
        while (cursorG.moveToNext()){
            choosePlace = cursorG.getString(0);
        }

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.ROUTE_SEARCH+";",null);
        while (cursorG.moveToNext()){
            searchText = cursorG.getString(0);
        }

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.CHOOSE_SUGGESTED_PATH+";",null);
        while (cursorG.moveToNext()){
            choosePathDic = cursorG.getString(0);
        }

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.CHOOSE_FIRST+";",null);
        while (cursorG.moveToNext()){
            firstDic = cursorG.getString(0);
        }

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.DISMISS+";",null);
        while (cursorG.moveToNext()){
            dismissDic = cursorG.getString(0);
        }

        cursorG.close();
        // ->


        txtFrom = (TextView) v.findViewById(R.id.fromtxt);
        txtTo = (TextView) v.findViewById(R.id.totxt);
        txtFrom.setTypeface(sans_bold);
        txtTo.setTypeface(sans_bold);

        btnSearch = (Button) v.findViewById(R.id.btnSearch);
        btnSource = (Button) v.findViewById(R.id.btn_source);
        btnDestination = (Button) v.findViewById(R.id.btn_destination);
        btnSource.setTypeface(sans_medium);
        btnDestination.setTypeface(sans_medium);
        btnSearch.setTypeface(sans_medium);


        txtFrom.setText(from);
        txtTo.setText(to);
        btnSearch.setText(searchText);
        btnDestination.setText(choosePlace);
        btnSource.setText(choosePlace);

        btnSource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Routing_ChoosePlaceSource.class));
            }
        });

        btnDestination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Routing_ChoosePlace_destination.class));
            }
        });
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                if(getActivity()!=null){
                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        try{
                            routingInfo = getActivity().getSharedPreferences("RoutingInfo", Context.MODE_PRIVATE);
                            sheditor = routingInfo.edit();
                             m = routingInfo.getString("source_id","null");
                             mi = routingInfo.getString("source_placename","null");
                             n = routingInfo.getString("destination_id","null");
                             ni = routingInfo.getString("destination_placename","null");

                            if(!mi.equals("null")) {
                                btnSource.setText(mi);
                            }

                            if(!ni.equals("null")){
                                btnDestination.setText(ni); } }catch (Exception e){}
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {}
                        }, 1000);
                    }
                });
            }
        }}, 0, 1000);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(m.equals("null") || n.equals("null"))
                {
                    dialog = new Dialog(getActivity());
                    dialog.setCancelable(true);
                    dialog.setContentView(R.layout.error);
                    Typeface samim = Typeface.createFromAsset(getActivity().getAssets(), "fonts/samim.ttf");
                    Typeface irsans_bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_bold.ttf");
                    Typeface irsans_medium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_medium.ttf");
                    Button retry_btn = (Button) dialog.findViewById(R.id.retry_btn);
                    retry_btn.setTypeface(irsans_medium);
                    retry_btn.setText(dismissDic);
                    retry_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    TextView error = (TextView)dialog.findViewById(R.id.err_text);
                    error.setTypeface(irsans_medium);
                    error.setText(firstDic);
                    dialog.show();
                }
                else if(!m.equals("null") && !n.equals("null"))
                {
                    dialog = new Dialog(getActivity());
                    dialog.setCancelable(true);
                    dialog.setContentView(R.layout.dialog_directions);
                    Typeface samim = Typeface.createFromAsset(getActivity().getAssets(), "fonts/samim.ttf");
                    Typeface irsans_bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_bold.ttf");
                    Typeface irsans_medium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_medium.ttf");

                    recyclerView = (RecyclerView) dialog.findViewById(R.id.recyclerview);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    recyclerView.setHasFixedSize(true);
                    TextView sourceText = (TextView) dialog.findViewById(R.id.sourceText);
                    TextView destinationText = (TextView) dialog.findViewById(R.id.destinationText);
                    TextView presentation = (TextView) dialog.findViewById(R.id.presentation);
                    presentation.setTypeface(irsans_medium);
                    presentation.setText(choosePathDic);
                    sourceText.setText(mi);
                    destinationText.setText(ni);
                    sourceText.setTypeface(irsans_bold);
                    destinationText.setTypeface(irsans_bold);




                    Cursor cursor = db.rawQuery("SELECT "+ StationsContract.StationsEntry.LINE_ID+" FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.STATION_ID+" = "+m+";",null);
                    while (cursor.moveToNext()){
                        mLine = cursor.getString(0);
                    }
                    Cursor cursor2 = db.rawQuery("SELECT "+ StationsContract.StationsEntry.LINE_ID+" FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.STATION_ID+" = "+n+";",null);
                    while (cursor2.moveToNext()){
                        nLine = cursor2.getString(0);
                    }

                    Cursor cursor3 = db.rawQuery("SELECT "+ LinesContract.LinesEntry.BACKGROUND_COLOR+" FROM "+ LinesContract.LinesEntry.TABLE_NAME+" WHERE "+ LinesContract.LinesEntry.LINE_ID+" = "+mLine+";",null);
                    Cursor cursor4 = db.rawQuery("SELECT "+ LinesContract.LinesEntry.BACKGROUND_COLOR+" FROM "+ LinesContract.LinesEntry.TABLE_NAME+" WHERE "+ LinesContract.LinesEntry.LINE_ID+" = "+nLine+";",null);

                    while (cursor3.moveToNext()) {
                        mColor = cursor3.getString(0);
                    }
                    while (cursor4.moveToNext()){
                        nColor = cursor4.getString(0);
                    }

                    destinationText.setBackgroundColor(Color.parseColor(nColor));
                    sourceText.setBackgroundColor(Color.parseColor(mColor));

                    arrayList.clear();
                    String URL_DATA = Connection.portal+"PathFinder/"+gu_id+"/"+city_id+"/"+m+"/"+n;
                    android.util.Log.d("olwqyd",Connection.portal+"PathFinder/"+gu_id+"/"+city_id+"/"+m+"/"+n);
                    final StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_DATA, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            try {
                                JSONArray jsonArray = new JSONArray(s);
                                for(int i=0;i<jsonArray.length();i++){
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    JSONArray stations = jsonObject.getJSONArray("Stations");
                                    JSONArray lines = jsonObject.getJSONArray("Lines");
                                    SuggestionC suggestionC = new SuggestionC(jsonObject.getString("Cost"),String.valueOf(stations.length()),String.valueOf(lines),jsonObject.getString("Time"));
                                    arrayList.add(suggestionC);
                                    adapter = new SuggestionAdapter(arrayList,getActivity(),n,m,String.valueOf(stations.length()));
                                    recyclerView.setAdapter(adapter);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    },

                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    android.util.Log.d("w2ewwq ERROR",String.valueOf(error));

                                }
                            });

                    MySingleton.getInstance(getActivity()).addToRequestque(stringRequest);
                    RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            10000,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    requestQueue.add(stringRequest);


                    dialog.show();
                }

            }
        });


        return v;
    }

}
