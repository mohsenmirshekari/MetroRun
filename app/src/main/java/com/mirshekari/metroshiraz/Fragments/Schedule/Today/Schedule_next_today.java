package com.mirshekari.metroshiraz.Fragments.Schedule.Today;


import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.R;
import com.mirshekari.metroshiraz.Schedule.ScheduleAdapter;
import com.mirshekari.metroshiraz.Schedule.ScheduleC;
import com.mirshekari.metroshiraz.Schedule.ScheduleContract;

import java.util.ArrayList;
import java.util.Calendar;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class Schedule_next_today extends Fragment {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public Context context;
    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabaseSchedule;
    Cursor cursor;
    ArrayList<ScheduleC> arrayList = new ArrayList<>();
    int dow;
    public Schedule_next_today() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=  inflater.inflate(R.layout.fragment_schedule_next_today, container, false);
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        dow  = day;


        SharedPreferences shared = getActivity().getSharedPreferences("StationInfo", MODE_PRIVATE);
        String stid = shared.getString("station_id","null");
        Integer stationid = Integer.valueOf(stid)+1;
        Integer stationid2 = stationid-1;
        Integer stationid3 = Integer.valueOf(stid)-1;


        recyclerView = (RecyclerView)v.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setHasFixedSize(true);

        adapter = new ScheduleAdapter(arrayList,context);
        dbHelper = new DBHelper(getActivity());
        sqLiteDatabaseSchedule = dbHelper.getReadableDatabase();
        if(dow==1)
        {
            cursor = sqLiteDatabaseSchedule.rawQuery("SELECT * FROM "+ ScheduleContract.ScheduleEntry.TABLE_NAME_1+" WHERE station_id = "+stationid+";",null);
        }
        else if(dow==2)
        {
            cursor = sqLiteDatabaseSchedule.rawQuery("SELECT * FROM "+ ScheduleContract.ScheduleEntry.TABLE_NAME_2+" WHERE station_id = "+stationid+";",null);
        }
        else if(dow==3)
        {
            cursor = sqLiteDatabaseSchedule.rawQuery("SELECT * FROM "+ ScheduleContract.ScheduleEntry.TABLE_NAME_3+" WHERE station_id = "+stationid+";",null);
        }
        else if(dow==4)
        {
            cursor = sqLiteDatabaseSchedule.rawQuery("SELECT * FROM "+ ScheduleContract.ScheduleEntry.TABLE_NAME_4+" WHERE station_id = "+stationid+";",null);
        }
        else if(dow==5)
        {
            cursor = sqLiteDatabaseSchedule.rawQuery("SELECT * FROM "+ ScheduleContract.ScheduleEntry.TABLE_NAME_5+" WHERE station_id = "+stationid+";",null);
        }
        else if(dow==6)
        {
            cursor = sqLiteDatabaseSchedule.rawQuery("SELECT * FROM "+ ScheduleContract.ScheduleEntry.TABLE_NAME_6+" WHERE station_id = "+stationid+";",null);
        }
        else if(dow==7)
        {
            cursor = sqLiteDatabaseSchedule.rawQuery("SELECT * FROM "+ ScheduleContract.ScheduleEntry.TABLE_NAME_0+" WHERE station_id = "+stationid+";",null);
        }


    /*    while(cursor.moveToNext())
        {
            ScheduleC cities = new ScheduleC(cursor.getInt(0), cursor.getInt(1),cursor.getString(2),cursor.getInt(3),cursor.getInt(4));
            arrayList.add(cities);
            adapter = new ScheduleAdapter(arrayList,getActivity());
        }
        dbHelper.close();
        adapter = new ScheduleAdapter(arrayList);
        recyclerView.setAdapter(adapter);
*/
        return v;
    }

}
