package com.mirshekari.metroshiraz.Fragments;


import android.content.Context;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.Lines;
import com.mirshekari.metroshiraz.R;
import com.mirshekari.metroshiraz.Routing.Source.TourismQuee.QueeContract;

import org.w3c.dom.Text;

import java.util.Timer;
import java.util.TimerTask;

/**
 * A simple {@link Fragment} subclass.
 */
public class TourismFragment extends Fragment {

    ImageView slider;
    public static Integer[] mThumbIds = {
            R.drawable.dev_background,R.drawable.dev_background,R.drawable.dev_background,R.drawable.dev_background,
    };
    Thread th;
    int i=0;
    Animation out,in;
    String dictionary,present1,present2,showList;
    TextView Textpresent1,Textpresent2;
    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;
    Button go;
    public TourismFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_tourism, container, false);
        out = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
        in = AnimationUtils.loadAnimation(getActivity(), R.anim.fadein3);

        dbHelper = new DBHelper(getActivity());
        sqLiteDatabase = dbHelper.getReadableDatabase();
        Typeface irsans_light = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_light.ttf");

        Textpresent1 = (TextView) v.findViewById(R.id.present1);
        Textpresent2 = (TextView) v.findViewById(R.id.present2);

        Textpresent2.setTypeface(irsans_light);
        Textpresent1.setTypeface(irsans_light);

        go = (Button) v.findViewById(R.id.go);
        go.setTypeface(irsans_light);
        // Dictionary
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.TOURISM+";",null);
        while (cursor.moveToNext()){
            dictionary = cursor.getString(0);
        }
        Cursor cursorPresent = sqLiteDatabase.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.TOURISM_PRESENT_1+";",null);
        while (cursorPresent.moveToNext()){
            present1 = cursorPresent.getString(0);
        }
        Cursor cursorPresent2 = sqLiteDatabase.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.TOURISM_PRESENT_2+";",null);
        while (cursorPresent2.moveToNext()){
            present2 = cursorPresent2.getString(0);
        }
        Cursor cursorShowList = sqLiteDatabase.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.SHOW_LIST+";",null);
        while (cursorShowList.moveToNext()){
            showList = cursorShowList.getString(0);
        }
        cursor.close();
        cursorPresent.close();
        cursorPresent2.close();
        cursorShowList.close();
        // ->

        Textpresent1.setText(present1);
        Textpresent2.setText(present2);

        go.setText(showList);

        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbHelper.deleteQueeTable(sqLiteDatabase);
                TourismList fragment = new TourismList();
                android.support.v4.app.FragmentTransaction fragmentTransaction =  getFragmentManager() .beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        // Set title bar
        ((Lines) getActivity()).setActionBarTitle(dictionary);
        // ->



        slider = (ImageView) v.findViewById(R.id.slider);
        try{
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if(getActivity()!=null){
                    getActivity().runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            try{
                                slider.setAnimation(in);
                                slider.startAnimation(in);
                                slider.setImageResource(mThumbIds[i]);
                                i++;
                                if(i >= mThumbIds.length){
                                    i = 0;
                                }
                            }catch (Exception e){}
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                }
                            }, 3000);
                        }
                    });
                }}
            }, 0, 3000);
        }catch (Exception e){}


        return v;
    }


}
