package com.mirshekari.metroshiraz.Fragments;


import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mirshekari.metroshiraz.AnalyticsApplication;
import com.mirshekari.metroshiraz.ChooseLine.LinesContract;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GPSTracker;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.Lines;
import com.mirshekari.metroshiraz.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import ss.com.bannerslider.banners.Banner;

/**
 * A simple {@link Fragment} subclass.
 */
public class NearestStation_fragment extends Fragment {
    SQLiteDatabase db;
    DBHelper dbHelper;
    Geocoder geocoder;
    Cursor cursor;
    String dictionary,stationDic,routingDic;
    List<Float> banners;
    List<String> stationid;
    double mylatitude,mylongitude,dest_latitude,dest_longitude;
    float distance,maxFload;
    TextView title_small,direction,info,txt;
    LinearLayout getDirection;
    Tracker mTracker;
    public NearestStation_fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_nearest_station_fragment, container, false);
        txt = (TextView) v.findViewById(R.id.txt);
        title_small = (TextView) v.findViewById(R.id.title_small);
        direction = (TextView) v.findViewById(R.id.direction);
        info = (TextView) v.findViewById(R.id.info);
        getDirection = (LinearLayout) v.findViewById(R.id.getDirection);
        Typeface samim = Typeface.createFromAsset(getActivity().getAssets(), "fonts/samim.ttf");
        AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Nearest station to me");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());


        txt.setTypeface(samim);
        title_small.setTypeface(samim);
        direction.setTypeface(samim);
        info.setTypeface(samim);

        Animation myFadeInAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fadein1);
        title_small.startAnimation(myFadeInAnimation);
        Animation myFadeInAnimation2 = AnimationUtils.loadAnimation(getActivity(), R.anim.fadein2);
        txt.startAnimation(myFadeInAnimation2);
        Animation myFadeInAnimation3 = AnimationUtils.loadAnimation(getActivity(), R.anim.fadein3);
        info.startAnimation(myFadeInAnimation3);
        Animation myFadeInAnimation4 = AnimationUtils.loadAnimation(getActivity(), R.anim.fadein4);
        direction.startAnimation(myFadeInAnimation4);

        Location mylocation = new Location("");
        Location dest_location = new Location("");
        banners = new ArrayList<>();
        stationid = new ArrayList<>();
        dbHelper = new DBHelper(getActivity());
        db = dbHelper.getReadableDatabase();

        // Dictionary
        Cursor cursor1 = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.NEAREST_STATION_TO_ME+";",null);
        while (cursor1.moveToNext()){
            dictionary = cursor1.getString(0);
        }
        cursor1 = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.STATION+";",null);
        while (cursor1.moveToNext()){
            stationDic = cursor1.getString(0);
        }
         cursor1 = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.MAPROUTING+";",null);
        while (cursor1.moveToNext()){
            routingDic = cursor1.getString(0);
        }
        cursor1.close();
        // ->

        // Set title bar
        ((Lines) getActivity()).setActionBarTitle(dictionary);
        // ->


        title_small.setText(stationDic);
        direction.setText(routingDic);

        GPSTracker tracker = new GPSTracker(getActivity());
        if (!tracker.canGetLocation()) {
            tracker.showSettingsAlert();
        } else {
            mylatitude = tracker.getLatitude();
            mylongitude = tracker.getLongitude();
        }
        cursor = db.rawQuery("SELECT * FROM stations", null);
        while (cursor.moveToNext()) {
            String id = cursor.getString(1);
            String a1 = cursor.getString(3);
            String a2[] = a1.split(",");
            dest_latitude = Double.valueOf(a2[0]);
            dest_longitude = Double.valueOf(a2[1]);
            dest_location.setLatitude(dest_latitude);
            dest_location.setLongitude(dest_longitude);
            mylocation.setLatitude(mylatitude);
            mylocation.setLongitude(mylongitude);
            distance = mylocation.distanceTo(dest_location);
            banners.add(distance);
            stationid.add(id);
            int m = Integer.valueOf(Math.round(distance));

        }

      /*  for(int i=0;i<banners.size();i++)
        {
            if(banners.get(i)>banners.get(0)){
                maxFload = banners.get(i);
            }
        }*/

        int minIndex = banners.indexOf(Collections.min(banners));
        String nearestStationId = String.valueOf(stationid.get(minIndex));
        Cursor cursorStation = db.rawQuery("SELECT * from stations WHERE station_id = " + nearestStationId + ";", null);
        while (cursorStation.moveToNext())
        {
            String gh = cursorStation.getString(6);
            txt.setText(gh);
            String line_id = cursorStation.getString(2);
            Cursor lineCursor = db.rawQuery("SELECT * FROM lines WHERE line_ID = "+line_id+";",null);
            while (lineCursor.moveToNext()){
                String line_title = lineCursor.getString(8);
                info.setText(line_title);
            }

        }

        getDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr=" + mylatitude + "," + mylongitude + "&daddr=" + dest_latitude + "," + dest_longitude));
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(i);
            }
        });

        return v;
    }

}
