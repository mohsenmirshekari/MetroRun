package com.mirshekari.metroshiraz.Fragments;


import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mirshekari.metroshiraz.Aboutus.AboutUsContract;
import com.mirshekari.metroshiraz.AnalyticsApplication;
import com.mirshekari.metroshiraz.ChooseLine.LinesContract;
import com.mirshekari.metroshiraz.ChooseStation.StationsContract;
import com.mirshekari.metroshiraz.Connection;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.Lines;
import com.mirshekari.metroshiraz.Place.Groups.GroupsContract;
import com.mirshekari.metroshiraz.Place.Private.PrivatePlacesContract;
import com.mirshekari.metroshiraz.Place.Public.PublicPlacesContract;
import com.mirshekari.metroshiraz.R;
import com.nineoldandroids.animation.ObjectAnimator;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;

public class Aboutus_fragment extends Fragment {

    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabaseLine;
    Cursor cursor;
    SharedPreferences.Editor sheditor;
    public ImageView logoView;
    TextView title,description,android_text,server_text,telegram_text,continueText;
    ScrollView scrollView;
    LinearLayout more,below,dev_layout,social_layout;
    CircleImageView im1,im2,telegramImage;
    int route;
    SQLiteDatabase db;
    String isImportant,version,line_id,station_id,soctest,telegram,instagram,soctest2,icon,background,banner,place_id,slide1,slide2,slide3,slide4,latestupdate,address,BackenDeveloper,Description,FrontEndDeveloper,id,Logo,Title,WebsiteURL,dictionary,telegramDic,gu_id,city;
    Tracker mTracker;
    public Aboutus_fragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_aboutus_fragment, container, false);
        Typeface sans = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_bold.ttf");
        Typeface sansMedium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb.ttf");
        AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("About us"+" ("+city+")");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        SharedPreferences shared = getActivity().getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences sharedcity = getActivity().getSharedPreferences("City", MODE_PRIVATE);
        gu_id = shared.getString("gu","null");
        latestupdate = shared.getString("latestupdate","null");
        city = sharedcity.getString("city","null");
        sheditor = shared.edit();


        android_text = (TextView) v.findViewById(R.id.android_text);
        server_text = (TextView) v.findViewById(R.id.server_text);
        telegram_text = (TextView) v.findViewById(R.id.telegram_text);
        continueText = (TextView) v.findViewById(R.id.continu);
        continueText.setTypeface(sansMedium);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        dev_layout = (LinearLayout)v.findViewById(R.id.dev_layout);
        social_layout = (LinearLayout) v.findViewById(R.id.social_layout);
        social_layout.setBackgroundColor(Color.parseColor("#3B5998"));

        social_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto","info@metro-app.ir", null));
                intent.putExtra(Intent.EXTRA_SUBJECT, getAppVersion()+"متروگرام ");
                startActivity(Intent.createChooser(intent, "یکی از برنامه های زیر را انتخاب کنید :"));
            }
        });

        im1 = (CircleImageView)v.findViewById(R.id.im1);
        im2 = (CircleImageView)v.findViewById(R.id.im2);
        telegramImage = (CircleImageView)v.findViewById(R.id.telegramImage);

        im1.setImageDrawable(getResources().getDrawable(R.drawable.android_robot));
        im2.setImageDrawable(getResources().getDrawable(R.drawable.server_robot));
        telegramImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_envelope));

        scrollView = (ScrollView) v.findViewById(R.id.scrollView);
        more = (LinearLayout) v.findViewById(R.id.more);
        below = (LinearLayout) v.findViewById(R.id.below);

        description = (TextView) v.findViewById(R.id.description);

        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ObjectAnimator.ofInt(scrollView, "scrollY",  below.getTop()).setDuration(2000).start();
            }
        });

        logoView = (ImageView) v.findViewById(R.id.logoView);

        dbHelper = new DBHelper(getActivity());
        sqLiteDatabaseLine = dbHelper.getReadableDatabase();


        // Dictionary
        Cursor cursor1 = sqLiteDatabaseLine.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.ARMO_TITLE+";",null);
        while (cursor1.moveToNext()){
            dictionary = cursor1.getString(0);
        }

        Cursor cursorTelegram = sqLiteDatabaseLine.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.TELEGRAM_CHANNEL+";",null);
        while (cursorTelegram.moveToNext()){
            telegramDic = cursorTelegram.getString(0);
        }
        // ->

        // Set title bar
        ((Lines) getActivity()).setActionBarTitle(dictionary);
        // ->

        dbHelper = new DBHelper(getActivity());
        db = dbHelper.getWritableDatabase();

        title = (TextView) v.findViewById(R.id.title);

        cursor = sqLiteDatabaseLine.rawQuery("SELECT * FROM "+ AboutUsContract.AboutUsEntry.TBName+";",null);

        while (cursor.moveToNext()){
            address = cursor.getString(0);
            BackenDeveloper = cursor.getString(1);
            Description = cursor.getString(2);
            FrontEndDeveloper = cursor.getString(3);
            id = cursor.getString(4);
            Logo = cursor.getString(5);
            Title = cursor.getString(6);
            WebsiteURL = cursor.getString(7);
            Logo = Logo.replace("-","/");
        }

        title.setText(Title);
        title.setTypeface(sans);

        android_text.setText(FrontEndDeveloper);
        android_text.setTypeface(sansMedium);

        server_text.setText(BackenDeveloper);
        server_text.setTypeface(sansMedium);

        description.setText(Description);
        description.setTypeface(sansMedium);

        telegram_text.setTypeface(sansMedium);

        telegram_text.setText(telegramDic);


        // Gets linearlayout
        RelativeLayout layout = (RelativeLayout) v.findViewById(R.id.top);
        LinearLayout layout2 = (LinearLayout) v.findViewById(R.id.below);
        LinearLayout ovl1 = (LinearLayout) v.findViewById(R.id.overlay1);
        ovl1.getBackground().setAlpha(210);

        ViewGroup.LayoutParams params = layout.getLayoutParams();
        params.height = height;
        params.width = width;
        layout.setLayoutParams(params);

        ViewGroup.LayoutParams params2 = layout2.getLayoutParams();
        params2.height = height;
        params2.width = width;
        layout2.setLayoutParams(params2);


        // Load Logo from FileSystem
        String URLL = Logo.replace("/","-");
        File folder = new File(getActivity().getFilesDir(),"Images");
        File file = new File(folder,URLL);

        if(file.exists())
        {
            Picasso.with(getActivity()).load(file).into(logoView);
        }
        else{
            imageDownload(getActivity(), Logo,logoView);
        }

        return v;
    }

    public static void imageDownload(Context ctx, String url,ImageView logoView){
        Picasso.with(ctx)
                .load(Connection.images+url)
                .into(getTarget(url,ctx));
        Picasso.with(ctx)
                .load(Connection.images+url)
                .into(logoView);
    }

    private static Target getTarget(final String url, final Context context){
        Target target = new Target(){

            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        String URLL = url.replace("/","-");
                        File folder = new File(context.getFilesDir(),"Images");
                        if(!folder.exists()){
                            folder.mkdir();
                        }
                        File file = new File(folder,URLL);
                        try {
                            file.createNewFile();
                            FileOutputStream ostream = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 80, ostream);
                            ostream.flush();
                            ostream.close();

                        } catch (IOException e) {
                            Log.e("IOException", e.getLocalizedMessage());
                        }
                    }
                }).start();

            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        return target;
    }
    public void checkUpdate()
    {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        final String url = Connection.portal+"UpdateCheck/"+gu_id+"/"+latestupdate+"/"+city+"/0";

        StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(final String response)
                    {
                        try {
                            if(response.length()!=2)
                            {
                                final Dialog dialog22 = new Dialog(getActivity());
                                dialog22.setContentView(R.layout.yes_no_dialog);
                                final Dialog dialogsuccess = new Dialog(getActivity());
                                dialogsuccess.setContentView(R.layout.success_dialog);
                                TextView title = (TextView) dialog22.findViewById(R.id.title);
                                TextView text = (TextView) dialogsuccess.findViewById(R.id.text);
                                TextView yes = (TextView) dialog22.findViewById(R.id.yestxt);
                                TextView no = (TextView) dialog22.findViewById(R.id.notxt);
                                LinearLayout yes2 = (LinearLayout) dialog22.findViewById(R.id.yes);
                                LinearLayout no2 = (LinearLayout) dialog22.findViewById(R.id.no);

                                Typeface sans_medium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_medium.ttf");
                                Typeface sans_light = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_medium.ttf");
                                text.setTypeface(sans_medium);

                                title.setText("بسته جدید بروزرسانی درون\u200Cبرنامه\u200Cای آماده دریافت می\u200Cباشد. آیا ماید به دریافت این بسته هستید؟");


                                title.setTypeface(sans_light);


                                JSONArray jsonArrayCheck = new JSONArray(response);
                                JSONObject jsonObjectCheck = jsonArrayCheck.getJSONObject(0);
                                isImportant = jsonObjectCheck.getString("IsImportant");
                                if(isImportant.equals("true"))
                                {
                                    no2.setVisibility(View.GONE);
                                    title.setText("بسته جدید بروزرسانی درون\u200Cبرنامه\u200Cای آماده دریافت می\u200Cباشد. برای استفاده از اپلیکیشن متروران دریافت این بسته ضروری است!");
                                }


                                yes.setText("باشه");
                                no.setText("بعداً");

                                no.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog22.dismiss();
                                    }
                                });
                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        try {
                                            JSONArray jsonArray = new JSONArray(response);
                                            for(int i=0;i<jsonArray.length();i++) {
                                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                                JSONObject objectId = jsonArray.getJSONObject(jsonArray.length()-1);
                                                String lastestIDDD = objectId.getString("Id");
                                                sheditor.putString("latestupdate", lastestIDDD);
                                                sheditor.commit();

                                                JSONArray Ids = jsonObject.getJSONArray("Ids");
                                                JSONObject IdsO = Ids.getJSONObject(0);

                                                final String id = IdsO.getString("Id");
                                                String type = IdsO.getString("Type");

                                                if (type.equals("Line")) {
                                                    RequestQueue queue = Volley.newRequestQueue(getActivity());
                                                    final String url = Connection.portal + "/GetLines/" + gu_id + "/" + city + "/" + id;
                                                    final Dialog dialog = new Dialog(getActivity());
                                                    dialog.setCancelable(false);
                                                    dialog.setContentView(R.layout.loading);
                                                    Typeface samim = Typeface.createFromAsset(getActivity().getAssets(), "fonts/samim.ttf");
                                                    TextView messageTextView = (TextView) dialog.findViewById(R.id.message);
                                                    messageTextView.setTypeface(samim);
                                                    messageTextView.setText("در حال بروزرسانی خطوط");
                                                    messageTextView.setTypeface(samim);
                                                    dialog.show();
                                                    StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                                                            new Response.Listener<String>() {
                                                                @Override
                                                                public void onResponse(String response) {
                                                                    DBHelper dbHelper = new DBHelper(getActivity());
                                                                    SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();

                                                                    try {
                                                                        JSONArray jsonArray = new JSONArray(response);
                                                                        JSONObject o = jsonArray.getJSONObject(0);
                                                                        JSONArray images = o.getJSONArray("Images");

                                                                        Cursor checkCursor = db.rawQuery("SELECT * FROM "+ LinesContract.LinesEntry.TABLE_NAME+" WHERE "+ LinesContract.LinesEntry.LINE_ID+" = "+id+";",null);

                                                                        if(checkCursor.getCount()>0)
                                                                        {
                                                                            String Images = images.getString(0);

                                                                            dbHelper.updateLineInformation(o.getString("BackgroundColor"), o.getString("CityId"),
                                                                                    o.getInt("FirstStationNumber"), o.getInt("Id"), o.getInt("LastStationNumber")
                                                                                    , o.getInt("LineNumber"), o.getInt("Status"), o.getString("TextColor"), o.getString("Title"), images.getString(0), o.getString("MultiEnd"), o.getString("MultiendDetails"), sqLiteDatabase);
                                                                        }

                                                                        else if(checkCursor.getCount()==0)
                                                                        {
                                                                            String Images = images.getString(0);
                                                                            dbHelper.putLineInformation(o.getString("BackgroundColor"), o.getString("CityId"),
                                                                                    o.getInt("FirstStationNumber"), o.getInt("Id"), o.getInt("LastStationNumber")
                                                                                    , o.getInt("LineNumber"), o.getInt("Status"), o.getString("TextColor"), o.getString("Title"), images.getString(0), o.getString("MultiEnd"), o.getString("MultiendDetails"), sqLiteDatabase);
                                                                        }


                                                                        dialog.dismiss();
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }


                                                                }
                                                            },
                                                            new Response.ErrorListener() {
                                                                @Override
                                                                public void onErrorResponse(VolleyError error) {

                                                                }
                                                            });


                                                    int socketTimeout = 30000; // 30 seconds. You can change it
                                                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                                                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

                                                    strRequest.setRetryPolicy(policy);
                                                    queue.add(strRequest);

                                                }
                                                // Update Line -->

                                                // Update Station --------------------------------------------------
                                                if (type.equals("Station")) {
                                                    Cursor cursor = db.rawQuery("SELECT " + StationsContract.StationsEntry.LINE_ID + " FROM " + StationsContract.StationsEntry.TABLE_NAME + " WHERE " + StationsContract.StationsEntry.STATION_ID + " = " + id + ";", null);
                                                    while (cursor.moveToNext()) {
                                                        line_id = cursor.getString(0);
                                                    }
                                                    try {
                                                        android.util.Log.d("qwjeyfqwpdqwhgedqhwgc", line_id);
                                                    } catch (Exception e) {
                                                    }
                                                    // station deatails
                                                    RequestQueue queue = Volley.newRequestQueue(getActivity());
                                                    final String url = Connection.portal + "/GetStations/" + gu_id + "/" + line_id + "/" + id;
                                                    final Dialog dialog = new Dialog(getActivity());
                                                    dialog.setCancelable(false);
                                                    dialog.setContentView(R.layout.loading);
                                                    Typeface samim = Typeface.createFromAsset(getActivity().getAssets(), "fonts/samim.ttf");
                                                    TextView messageTextView = (TextView) dialog.findViewById(R.id.message);
                                                    messageTextView.setTypeface(samim);
                                                    messageTextView.setText("در حال بروزرسانی ایستگاه ها");
                                                    messageTextView.setTypeface(samim);
                                                    dialog.show();
                                                    StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                                                            new Response.Listener<String>() {
                                                                @Override
                                                                public void onResponse(String response) {
                                                                    DBHelper dbHelper = new DBHelper(getActivity());
                                                                    SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();

                                                                    try {
                                                                        JSONArray jsonArray = new JSONArray(response);
                                                                        JSONObject os = jsonArray.getJSONObject(0);
                                                                        Cursor checkCursor = db.rawQuery("SELECT * FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.STATION_ID+" = "+id+";",null);


                                                                        if(checkCursor.getCount()==0)
                                                                        {
                                                                            dbHelper.putStationsInformation(os.getString("Address"), os.getInt("Id"), os.getInt("LineId"),
                                                                                    os.getString("Location"), os.getInt("StationNumber"),
                                                                                    os.getInt("Status"), os.getString("Title"), os.getString("Facilities"), os.getString("IsCross"), sqLiteDatabase);
                                                                        }
                                                                        else if(checkCursor.getCount()>0)
                                                                        {
                                                                            dbHelper.updateStationsInformation(os.getString("Address"), os.getInt("Id"), os.getInt("LineId"),
                                                                                    os.getString("Location"), os.getInt("StationNumber"),
                                                                                    os.getInt("Status"), os.getString("Title"), os.getString("Facilities"), os.getString("IsCross"), sqLiteDatabase);
                                                                        }

                                                                        dialog.dismiss();
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }


                                                                }
                                                            },
                                                            new Response.ErrorListener() {
                                                                @Override
                                                                public void onErrorResponse(VolleyError error) {

                                                                }
                                                            });


                                                    int socketTimeout = 30000; // 30 seconds. You can change it
                                                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                                                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

                                                    strRequest.setRetryPolicy(policy);
                                                    queue.add(strRequest);

                                                    // ->

                                                    // station schedule
                                                    try
                                                    {
                                                        //     db.rawQuery("DELETE ",null)


                                                        RequestQueue queue2 = Volley.newRequestQueue(getActivity());

                                                        final String url2 = Connection.portal+"/GetSchedules/" + gu_id + "/" + id;
                                                        StringRequest strRequest2 = new StringRequest(Request.Method.GET, url2,
                                                                new Response.Listener<String>() {
                                                                    @Override
                                                                    public void onResponse(String response) {
                                                                        DBHelper dbHelper = new DBHelper(getActivity());
                                                                        SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();
                                                                        try {
                                                                            JSONObject object = new JSONObject(response);
                                                                            String stid = object.getString("StationId");
                                                                            String stno = object.getString("StationNo");
                                                                            String previus = object.getString("Previous");
                                                                            String next = object.getString("Next");
                                                                            if(next.length()!=4 && previus.length()==4)
                                                                            {
                                                                                route = 0;
                                                                                JSONObject object2 = object.getJSONObject("Next");
                                                                                JSONArray Jarray2 = object2.getJSONArray("Days");
                                                                                JSONObject ab0 = Jarray2.getJSONObject(0);
                                                                                JSONObject ab1 = Jarray2.getJSONObject(1);
                                                                                JSONObject ab2 = Jarray2.getJSONObject(2);
                                                                                JSONObject ab3 = Jarray2.getJSONObject(3);
                                                                                JSONObject ab4 = Jarray2.getJSONObject(4);
                                                                                JSONObject ab5 = Jarray2.getJSONObject(5);
                                                                                JSONObject ab6 = Jarray2.getJSONObject(5);

                                                                                JSONArray array0 = ab0.getJSONArray("Plan");
                                                                                JSONArray array1 = ab1.getJSONArray("Plan");
                                                                                JSONArray array2 = ab2.getJSONArray("Plan");
                                                                                JSONArray array3 = ab3.getJSONArray("Plan");
                                                                                JSONArray array4 = ab4.getJSONArray("Plan");
                                                                                JSONArray array5 = ab5.getJSONArray("Plan");
                                                                                JSONArray array6 = ab6.getJSONArray("Plan");



                                                                                dbHelper.putSchedule0Information(String.valueOf(array0), route, "saturday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule1Information(String.valueOf(array1), route, "sunday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule2Information(String.valueOf(array2), route, "monday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule3Information(String.valueOf(array3), route, "tuesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule4Information(String.valueOf(array4), route, "wednesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule5Information(String.valueOf(array5), route, "thursday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule6Information(String.valueOf(array6), route, "friday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                            }

                                                                            if(next.length()!=4 && previus.length()!=4)
                                                                            {
                                                                                route = 1;
                                                                                // DOWNLOADING THE NEXT SCHEDULE
                                                                                JSONObject object2 = object.getJSONObject("Next");
                                                                                JSONArray Jarray2 = object2.getJSONArray("Days");
                                                                                JSONObject ab0 = Jarray2.getJSONObject(0);
                                                                                JSONObject ab1 = Jarray2.getJSONObject(1);
                                                                                JSONObject ab2 = Jarray2.getJSONObject(2);
                                                                                JSONObject ab3 = Jarray2.getJSONObject(3);
                                                                                JSONObject ab4 = Jarray2.getJSONObject(4);
                                                                                JSONObject ab5 = Jarray2.getJSONObject(5);
                                                                                JSONObject ab6 = Jarray2.getJSONObject(5);

                                                                                JSONArray array0 = ab0.getJSONArray("Plan");
                                                                                JSONArray array1 = ab1.getJSONArray("Plan");
                                                                                JSONArray array2 = ab2.getJSONArray("Plan");
                                                                                JSONArray array3 = ab3.getJSONArray("Plan");
                                                                                JSONArray array4 = ab4.getJSONArray("Plan");
                                                                                JSONArray array5 = ab5.getJSONArray("Plan");
                                                                                JSONArray array6 = ab6.getJSONArray("Plan");
                                                                                Log.d("SCHEDULE ","DOWNLOADING NEXT SCHEDULE WITH ROUTE 1");

                                                                                dbHelper.putSchedule0Information(String.valueOf(array0), route, "saturday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule1Information(String.valueOf(array1), route, "sunday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule2Information(String.valueOf(array2), route, "monday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule3Information(String.valueOf(array3), route, "tuesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule4Information(String.valueOf(array4), route, "wednesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule5Information(String.valueOf(array5), route, "thursday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule6Information(String.valueOf(array6), route, "friday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);




                                                                                // DOWNLOADING THE NEXT SCHEDULE
                                                                                Log.d("SCHEDULE ","DOWNLOADING PREVIOUS SCHEDULE WITH ROUTE 1");
                                                                                JSONObject object3 = object.getJSONObject("Previous");
                                                                                JSONArray Jarray3 = object3.getJSONArray("Days");
                                                                                JSONObject aab0 = Jarray3.getJSONObject(0);
                                                                                JSONObject aab1 = Jarray3.getJSONObject(1);
                                                                                JSONObject aab2 = Jarray3.getJSONObject(2);
                                                                                JSONObject aab3 = Jarray3.getJSONObject(3);
                                                                                JSONObject aab4 = Jarray3.getJSONObject(4);
                                                                                JSONObject aab5 = Jarray3.getJSONObject(5);
                                                                                JSONObject aab6 = Jarray3.getJSONObject(5);

                                                                                JSONArray arrayy0 = aab0.getJSONArray("Plan");
                                                                                JSONArray arrayy1 = aab1.getJSONArray("Plan");
                                                                                JSONArray arrayy2 = aab2.getJSONArray("Plan");
                                                                                JSONArray arrayy3 = aab3.getJSONArray("Plan");
                                                                                JSONArray arrayy4 = aab4.getJSONArray("Plan");
                                                                                JSONArray arrayy5 = aab5.getJSONArray("Plan");
                                                                                JSONArray arrayy6 = aab6.getJSONArray("Plan");

                                                                                dbHelper.putSchedule0Information(String.valueOf(arrayy0), route, "saturday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule1Information(String.valueOf(arrayy1), route, "sunday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule2Information(String.valueOf(arrayy2), route, "monday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule3Information(String.valueOf(arrayy3), route, "tuesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule4Information(String.valueOf(arrayy4), route, "wednesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule5Information(String.valueOf(arrayy5), route, "thursday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule6Information(String.valueOf(arrayy6), route, "friday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                            }


                                                                            if(next.length()==4 && previus.length()!=4)
                                                                            {
                                                                                route = 2;
                                                                                // DOWNLOADING THE PREVIUS SCHEDULE
                                                                                JSONObject object2 = object.getJSONObject("Previous");
                                                                                JSONArray Jarray2 = object2.getJSONArray("Days");
                                                                                JSONObject ab0 = Jarray2.getJSONObject(0);
                                                                                JSONObject ab1 = Jarray2.getJSONObject(1);
                                                                                JSONObject ab2 = Jarray2.getJSONObject(2);
                                                                                JSONObject ab3 = Jarray2.getJSONObject(3);
                                                                                JSONObject ab4 = Jarray2.getJSONObject(4);
                                                                                JSONObject ab5 = Jarray2.getJSONObject(5);
                                                                                JSONObject ab6 = Jarray2.getJSONObject(5);

                                                                                JSONArray array0 = ab0.getJSONArray("Plan");
                                                                                JSONArray array1 = ab1.getJSONArray("Plan");
                                                                                JSONArray array2 = ab2.getJSONArray("Plan");
                                                                                JSONArray array3 = ab3.getJSONArray("Plan");
                                                                                JSONArray array4 = ab4.getJSONArray("Plan");
                                                                                JSONArray array5 = ab5.getJSONArray("Plan");
                                                                                JSONArray array6 = ab6.getJSONArray("Plan");
                                                                                Log.d("SCHEDULE ","DOWNLOADING PREVIUS SCHEDULE WITH ROUTE 2");

                                                                                dbHelper.putSchedule0Information(String.valueOf(array0), route, "saturday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule1Information(String.valueOf(array1), route, "sunday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule2Information(String.valueOf(array2), route, "monday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule3Information(String.valueOf(array3), route, "tuesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule4Information(String.valueOf(array4), route, "wednesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule5Information(String.valueOf(array5), route, "thursday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule6Information(String.valueOf(array6), route, "friday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);


                                                                            }
                                                                        } catch (JSONException e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    }
                                                                },
                                                                new Response.ErrorListener() {
                                                                    @Override
                                                                    public void onErrorResponse(VolleyError error) {

                                                                    }
                                                                });


                                                        int socketTimeout2 = 30000; // 30 seconds. You can change it
                                                        RetryPolicy policy2 = new DefaultRetryPolicy(socketTimeout2,
                                                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

                                                        strRequest2.setRetryPolicy(policy2);
                                                        queue2.add(strRequest2);
                                                    }catch (Exception e){}
                                                    // ->
                                                }
                                                // Update Station -->

                                                // Updating About Us -----------------------------------------------
                                                if (type.equals("AboutUs")) {
                                                    RequestQueue queue = Volley.newRequestQueue(getActivity());
                                                    final String url = Connection.portal + "getaboutus/" + gu_id + "/" + getAppVersion() + "/" + id;
                                                    final Dialog dialog = new Dialog(getActivity());
                                                    dialog.setCancelable(false);
                                                    dialog.setContentView(R.layout.loading);
                                                    Typeface samim = Typeface.createFromAsset(getActivity().getAssets(), "fonts/samim.ttf");
                                                    TextView messageTextView = (TextView) dialog.findViewById(R.id.message);
                                                    messageTextView.setTypeface(samim);
                                                    messageTextView.setText("در حال بروزرسانی بخش درباره ما");
                                                    messageTextView.setTypeface(samim);
                                                    dialog.show();
                                                    StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                                                            new Response.Listener<String>() {
                                                                @Override
                                                                public void onResponse(String response) {
                                                                    DBHelper dbHelper = new DBHelper(getActivity());
                                                                    SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();

                                                                    try {
                                                                        JSONObject oA = new JSONObject(response);
                                                                        dbHelper.updateAboutUsInformation(oA.getString("Address"), oA.getString("BackendDeveloper"), oA.getString("Description"), oA.getString("FrontendDeveloper"), oA.getString("Id"), oA.getString("Logo"), oA.getString("Title"), oA.getString("WebsiteURL"), sqLiteDatabase);
                                                                        dialog.dismiss();
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }


                                                                }
                                                            },
                                                            new Response.ErrorListener() {
                                                                @Override
                                                                public void onErrorResponse(VolleyError error) {

                                                                }
                                                            });


                                                    int socketTimeout = 30000; // 30 seconds. You can change it
                                                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                                                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

                                                    strRequest.setRetryPolicy(policy);
                                                    queue.add(strRequest);

                                                }
                                                // Updating About Us ->

                                                // Updating PlaceGroups
                                                if (type.equals("PlaceGroup")) {
                                                    RequestQueue queue = Volley.newRequestQueue(getActivity());
                                                    final String url = Connection.portal + "/GetGroups/" + gu_id + "/" + id;
                                                    final Dialog dialog = new Dialog(getActivity());
                                                    dialog.setCancelable(false);
                                                    dialog.setContentView(R.layout.loading);
                                                    Typeface samim = Typeface.createFromAsset(getActivity().getAssets(), "fonts/samim.ttf");
                                                    TextView messageTextView = (TextView) dialog.findViewById(R.id.message);
                                                    messageTextView.setTypeface(samim);
                                                    messageTextView.setText("در حال بروزرسانی بخش بخش مکان ها");
                                                    messageTextView.setTypeface(samim);
                                                    dialog.show();
                                                    StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                                                            new Response.Listener<String>() {
                                                                @Override
                                                                public void onResponse(String response) {
                                                                    DBHelper dbHelper = new DBHelper(getActivity());
                                                                    SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();

                                                                    try {
                                                                        JSONArray jsonArray = new JSONArray(response);
                                                                        JSONObject g = jsonArray.getJSONObject(0);

                                                                        Cursor check = db.rawQuery("SELECT * FROM "+ GroupsContract.GroupsEntry.TABLE_NAME+" WHERE "+ GroupsContract.GroupsEntry.ID+" = "+id+";",null);
                                                                        if(check.getCount()==0)
                                                                        {
                                                                            dbHelper.putGroupsInformation(g.getInt("Id"), g.getString("Name"),sqLiteDatabase);
                                                                        }
                                                                        else if(check.getCount()>0)
                                                                        {
                                                                            dbHelper.updateGroupsInformation(g.getInt("Id"), g.getString("Name"),sqLiteDatabase);
                                                                        }

                                                                        dialog.dismiss();
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }


                                                                }
                                                            },
                                                            new Response.ErrorListener() {
                                                                @Override
                                                                public void onErrorResponse(VolleyError error) {

                                                                }
                                                            });


                                                    int socketTimeout = 30000; // 30 seconds. You can change it
                                                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                                                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

                                                    strRequest.setRetryPolicy(policy);
                                                    queue.add(strRequest);
                                                }


                                                // Updating PlaceGroups->


                                                // Updating keys ---------------------------------------------------
                                                if (type.equals("Key")) {
                                                    RequestQueue queue = Volley.newRequestQueue(getActivity());
                                                    final String url = Connection.portal + "/GetKey/" + gu_id + "/" + getAppVersion() + "/" + id;
                                                    final Dialog dialog = new Dialog(getActivity());
                                                    dialog.setCancelable(false);
                                                    dialog.setContentView(R.layout.loading);
                                                    Typeface samim = Typeface.createFromAsset(getActivity().getAssets(), "fonts/samim.ttf");
                                                    TextView messageTextView = (TextView) dialog.findViewById(R.id.message);
                                                    messageTextView.setTypeface(samim);
                                                    messageTextView.setText("در حال بروزرسانی بخش واژه نامه");
                                                    messageTextView.setTypeface(samim);
                                                    dialog.show();
                                                    StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                                                            new Response.Listener<String>() {
                                                                @Override
                                                                public void onResponse(String response) {
                                                                    DBHelper dbHelper = new DBHelper(getActivity());
                                                                    SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();
                                                                    try {
                                                                        JSONArray jsonarray = new JSONArray(response);
                                                                        JSONObject o = jsonarray.getJSONObject(0);
                                                                        Cursor check = db.rawQuery("SELECT * FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+id+";",null);
                                                                        if(check.getCount()==0)
                                                                        {
                                                                            dbHelper.putKeysInformation(o.getString("Id"), o.getString("Key"), o.getString("Text"), sqLiteDatabase);
                                                                        }
                                                                        else if(check.getCount()>0)
                                                                        {
                                                                            dbHelper.updateKeysInformation(o.getString("Id"), o.getString("Key"), o.getString("Text"), sqLiteDatabase);
                                                                        }
                                                                        dialog.dismiss();
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }


                                                                }
                                                            },
                                                            new Response.ErrorListener() {
                                                                @Override
                                                                public void onErrorResponse(VolleyError error) {

                                                                }
                                                            });


                                                    int socketTimeout = 30000; // 30 seconds. You can change it
                                                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                                                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

                                                    strRequest.setRetryPolicy(policy);
                                                    queue.add(strRequest);
                                                }

                                                // Updating Keys ->


                                                // Updating Places
                                                if (type.equals("Place")) {
                                                    Cursor cursor = db.rawQuery("SELECT " + PublicPlacesContract.PublicPlacesEntry.STATIONID + " FROM " + PublicPlacesContract.PublicPlacesEntry.TABLE_NAME + " WHERE " + PublicPlacesContract.PublicPlacesEntry.ID + " = " + id + ";", null);
                                                    while (cursor.moveToNext()) {
                                                        station_id = cursor.getString(0);
                                                    }
                                                    RequestQueue queue = Volley.newRequestQueue(getActivity());
                                                    final String url = Connection.portal + "/GetPublicPlaces/" + gu_id + "/" + station_id + "/0";
                                                    final Dialog dialog = new Dialog(getActivity());
                                                    dialog.setCancelable(false);
                                                    dialog.setContentView(R.layout.loading);
                                                    Typeface samim = Typeface.createFromAsset(getActivity().getAssets(), "fonts/samim.ttf");
                                                    TextView messageTextView = (TextView) dialog.findViewById(R.id.message);
                                                    messageTextView.setTypeface(samim);
                                                    messageTextView.setText("در حال بروزرسانی بخش اماکن");
                                                    messageTextView.setTypeface(samim);
                                                    dialog.show();
                                                    StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                                                            new Response.Listener<String>() {
                                                                @Override
                                                                public void onResponse(String response) {
                                                                    DBHelper dbHelper = new DBHelper(getActivity());
                                                                    SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();
                                                                    try {
                                                                        JSONArray jsonarrayPubPlace = new JSONArray(response);
                                                                        JSONObject opp = jsonarrayPubPlace.getJSONObject(0);
                                                                        JSONArray photo = opp.getJSONArray("PhotoLinks");
                                                                        JSONArray socialmedia = opp.getJSONArray("SocialMedia");

                                                                        if (socialmedia.length() > 0) {
                                                                            if (socialmedia.length() == 1) {
                                                                                soctest = socialmedia.getString(0);
                                                                                if (soctest.contains("Telegram")) {
                                                                                    telegram = socialmedia.getString(0);
                                                                                    telegram = telegram.substring(9);
                                                                                } else if (soctest.contains("Instagram")) {
                                                                                    instagram = socialmedia.getString(0);
                                                                                    instagram = instagram.substring(9);
                                                                                }
                                                                            } else if (socialmedia.length() == 2) {
                                                                                soctest = socialmedia.getString(0);
                                                                                if (soctest.contains("Telegram")) {
                                                                                    telegram = socialmedia.getString(0);
                                                                                    telegram = telegram.substring(9);
                                                                                } else if (soctest.contains("Instagram")) {
                                                                                    instagram = socialmedia.getString(0);
                                                                                    instagram = instagram.substring(9);
                                                                                }

                                                                                soctest2 = socialmedia.getString(1);
                                                                                if (soctest.contains("Telegram")) {
                                                                                    telegram = socialmedia.getString(1);
                                                                                    telegram = telegram.substring(8);
                                                                                } else if (soctest.contains("Instagram")) {
                                                                                    instagram = socialmedia.getString(1);
                                                                                    instagram = instagram.substring(8);
                                                                                }
                                                                            }

                                                                        } else if (socialmedia.length() == 0) {
                                                                            instagram = "";
                                                                            telegram = "";
                                                                        }


                                                                        try {
                                                                            icon = photo.getString(0);
                                                                            icon = icon.replace("-", "/");

                                                                        } catch (Exception e) {
                                                                        }

                                                                        if (photo.length() > 1) {
                                                                            background = photo.getString(1);
                                                                            background = background.replace("-", "/");
                                                                        }
                                                                        if (photo.length() > 2) {
                                                                            slide1 = photo.getString(2);
                                                                            slide1 = slide1.replace("-", "/");
                                                                        }
                                                                        if (photo.length() > 3) {
                                                                            slide2 = photo.getString(3);
                                                                            slide2 = slide2.replace("-", "/");
                                                                        }
                                                                        if (photo.length() > 4) {
                                                                            slide3 = photo.getString(4);
                                                                            slide3 = slide3.replace("-", "/");
                                                                        }
                                                                        if (photo.length() > 5) {
                                                                            slide4 = photo.getString(5);
                                                                            slide4 = slide4.replace("-", "/");
                                                                        }
                                                                        if (photo.length() > 6) {
                                                                            banner = photo.getString(6);
                                                                            banner = banner.replace("-", "/");
                                                                        }

                                                                        Cursor check = db.rawQuery("SELECT * FROM "+ PublicPlacesContract.PublicPlacesEntry.TABLE_NAME+" WHERE "+ PublicPlacesContract.PublicPlacesEntry.ID+" = "+id+";",null);
                                                                        if(check.getCount()==0)
                                                                        {
                                                                            dbHelper.putPublicPlacesInformation(opp.getString("BackgroundColorId"), opp.getString("Description"), opp.getInt("Id"), opp.getString("IsPrivate"), opp.getString("Phone"), opp.getString("PhotoLinks"), opp.getString("Photos"), opp.getInt("PlaceGroupId"), instagram, telegram, opp.getInt("StationId"), opp.getString("TextColorId"), opp.getString("Title"), icon, background, slide1, slide2, slide3, slide4, banner, sqLiteDatabase);
                                                                        }
                                                                        else if(check.getCount()>0)
                                                                        {
                                                                            dbHelper.updatePublicPlacesInformation(opp.getString("BackgroundColorId"), opp.getString("Description"), opp.getInt("Id"), opp.getString("IsPrivate"), opp.getString("Phone"), opp.getString("PhotoLinks"), opp.getString("Photos"), opp.getInt("PlaceGroupId"), instagram, telegram, opp.getInt("StationId"), opp.getString("TextColorId"), opp.getString("Title"), icon, background, slide1, slide2, slide3, slide4, banner, sqLiteDatabase);
                                                                        }

                                                                        dialog.dismiss();
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }


                                                                }
                                                            },
                                                            new Response.ErrorListener() {
                                                                @Override
                                                                public void onErrorResponse(VolleyError error) {

                                                                }
                                                            });


                                                    int socketTimeout = 30000; // 30 seconds. You can change it
                                                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                                                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

                                                    strRequest.setRetryPolicy(policy);
                                                    queue.add(strRequest);
                                                }
                                                // Updating Places ->


                                                // Updating PlaceProiles
                                                if (type.equals("PlaceProfile")) {

                                                    Cursor privateidCursor = db.rawQuery("SELECT " + PrivatePlacesContract.PrivatePlacesEntry.PLACE_ID + " FROM " + PrivatePlacesContract.PrivatePlacesEntry.TABLE_NAME + " WHERE " + PrivatePlacesContract.PrivatePlacesEntry.ID + " = " + id + ";", null);
                                                    while (privateidCursor.moveToNext()) {
                                                        place_id = privateidCursor.getString(0);
                                                    }
                                                    Cursor placestationidcursor = db.rawQuery("SELECT " + PublicPlacesContract.PublicPlacesEntry.STATIONID + " FROM " + PublicPlacesContract.PublicPlacesEntry.TABLE_NAME + " WHERE " + PublicPlacesContract.PublicPlacesEntry.ID + " = " + place_id + ";", null);
                                                    while (placestationidcursor.moveToNext()) {
                                                        station_id = placestationidcursor.getString(0);
                                                    }
                                                    RequestQueue queue = Volley.newRequestQueue(getActivity());
                                                    final String url = Connection.portal + "/getPrivatePlaces/" + gu_id + "/" + station_id + "/"+id;
                                                    final Dialog dialog = new Dialog(getActivity());
                                                    dialog.setCancelable(false);
                                                    dialog.setContentView(R.layout.loading);
                                                    Typeface samim = Typeface.createFromAsset(getActivity().getAssets(), "fonts/samim.ttf");
                                                    TextView messageTextView = (TextView) dialog.findViewById(R.id.message);
                                                    messageTextView.setTypeface(samim);
                                                    messageTextView.setText("در حال بروزرسانی بخش اماکن");
                                                    messageTextView.setTypeface(samim);
                                                    dialog.show();
                                                    StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                                                            new Response.Listener<String>() {
                                                                @Override
                                                                public void onResponse(String response) {
                                                                    DBHelper dbHelper = new DBHelper(getActivity());
                                                                    SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();
                                                                    try {
                                                                        JSONArray jsonarray = new JSONArray(response);
                                                                        JSONObject opv = jsonarray.getJSONObject(0);
                                                                        Cursor checkCursor = db.rawQuery("SELECT * FROM "+ PrivatePlacesContract.PrivatePlacesEntry.TABLE_NAME+" WHERE "+ PrivatePlacesContract.PrivatePlacesEntry.ID+" = "+id,null);


                                                                        if (checkCursor.getCount()>0)
                                                                        {
                                                                            dbHelper.updatePrivatePlacesInformation(opv.getString("Commercial"), opv.getString("EndDate"), opv.getInt("Id"), opv.getString("IsVIP"), opv.getString("Mobiles"), opv.getString("Photos"), opv.getString("Website"), opv.getString("PlaceId"), sqLiteDatabase);
                                                                        }
                                                                        else if(checkCursor.getCount()==0)
                                                                        {
                                                                            dbHelper.putPrivatePlacesInformation(opv.getString("Commercial"), opv.getString("EndDate"), opv.getInt("Id"), opv.getString("IsVIP"), opv.getString("Mobiles"), opv.getString("Photos"), opv.getString("Website"), opv.getString("PlaceId"), sqLiteDatabase);
                                                                        }



                                                                        dialog.dismiss();
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }


                                                                }
                                                            },
                                                            new Response.ErrorListener() {
                                                                @Override
                                                                public void onErrorResponse(VolleyError error) {

                                                                }
                                                            });


                                                    int socketTimeout = 30000; // 30 seconds. You can change it
                                                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                                                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

                                                    strRequest.setRetryPolicy(policy);
                                                    queue.add(strRequest);
                                                }


                                                // ->


                                                // Updating Facilities

                                                if(type.equals("Facility"))
                                                {
                                                    RequestQueue queue = Volley.newRequestQueue(getActivity());
                                                    final String url = Connection.portal+"/Getfacilities/"+gu_id+"/0";
                                                    final Dialog dialog = new Dialog(getActivity());
                                                    dialog.setCancelable(false);
                                                    dialog.setContentView(R.layout.loading);
                                                    Typeface samim = Typeface.createFromAsset(getActivity().getAssets(), "fonts/samim.ttf");
                                                    TextView messageTextView = (TextView) dialog.findViewById(R.id.message);
                                                    messageTextView.setTypeface(samim);
                                                    messageTextView.setText("در حال بروزرسانی امکانات ایستگاه ها");
                                                    messageTextView.setTypeface(samim);
                                                    dialog.show();
                                                    StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                                                            new Response.Listener<String>() {
                                                                @Override
                                                                public void onResponse(String response) {
                                                                    DBHelper dbHelper = new DBHelper(getActivity());
                                                                    SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();
                                                                    try {
                                                                        JSONArray jsonarray = new JSONArray(response);
                                                                        JSONObject o = jsonarray.getJSONObject(0);
                                                                        dbHelper.updateFacilitiesInformation(o.getString("Icon"), o.getInt("Id"), o.getString("Title"), sqLiteDatabase);
                                                                        dialog.dismiss();
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }


                                                                }
                                                            },
                                                            new Response.ErrorListener() {
                                                                @Override
                                                                public void onErrorResponse(VolleyError error) {

                                                                }
                                                            });


                                                    int socketTimeout = 30000; // 30 seconds. You can change it
                                                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                                                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

                                                    strRequest.setRetryPolicy(policy);
                                                    queue.add(strRequest);

                                                }

                                                // ->

                                            }
                                            dialog22.dismiss();
                                            dialogsuccess.show();
                                        } catch (Exception e)
                                        {

                                        }
                                    }
                                });

                                dialog22.show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {

                    }
                });



        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        strRequest.setRetryPolicy(policy);
        queue.add(strRequest);
    }
    public String getAppVersion()
    {
        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }
}
