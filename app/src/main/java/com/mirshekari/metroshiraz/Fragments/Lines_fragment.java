package com.mirshekari.metroshiraz.Fragments;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mirshekari.metroshiraz.ChooseLine.BackgroundTaskLine;
import com.mirshekari.metroshiraz.ChooseLine.LinesAdapter;
import com.mirshekari.metroshiraz.ChooseLine.LinesC;
import com.mirshekari.metroshiraz.ChooseLine.LinesContract;
import com.mirshekari.metroshiraz.Connection;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.Lines;
import com.mirshekari.metroshiraz.LinesDesign.Lines_counter_adapter;
import com.mirshekari.metroshiraz.LinesDesign.Lines_counter_items;
import com.mirshekari.metroshiraz.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.events.OnBannerClickListener;
import ss.com.bannerslider.views.BannerSlider;

/**
 * A simple {@link Fragment} subclass.
 */
public class Lines_fragment extends Fragment{
    private RecyclerView recyclerView,recyclerViewCounter;
    private RecyclerView.Adapter adapter;
    private RecyclerView.Adapter adapterCounter;
    public Context context;
    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabaseLine;
    Cursor cursor;
    String commercial,commercial_id,dictionary;
    BannerSlider bannerSlider;
    List<Banner> banners;
    ArrayList<LinesC> arrayList = new ArrayList<>();
    ArrayList<Lines_counter_items> counter_items = new ArrayList<>();
    int c = 1;
    LinearLayout ll1;
    String slave;
    String [] ar;
    public Lines_fragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v =  inflater.inflate(R.layout.fragment_lines_fragment, container, false);



        dbHelper = new DBHelper(getActivity());
        sqLiteDatabaseLine = dbHelper.getReadableDatabase();

        final GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(true);

        recyclerViewCounter = (RecyclerView) v.findViewById(R.id.counterView);
        recyclerViewCounter.setLayoutManager(new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false));
        recyclerViewCounter.setHasFixedSize(true);

        // Dictionary
        Cursor cursor = sqLiteDatabaseLine.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.LINES_LIST+";",null);
        while (cursor.moveToNext()){
            dictionary = cursor.getString(0);
        }

        // ->

        // Set title bar
        ((Lines) getActivity()).setActionBarTitle(dictionary);
        // ->


        bannerSlider = (BannerSlider) v.findViewById(R.id.banner_slider1);
        banners=new ArrayList<>();

        ll1 = (LinearLayout) v.findViewById(R.id.ll1);

        adapter = new LinesAdapter(arrayList,getActivity());
        adapterCounter = new Lines_counter_adapter(counter_items,getActivity());

        cursor = sqLiteDatabaseLine.rawQuery("SELECT * FROM "+ LinesContract.LinesEntry.TABLE_NAME+" WHERE "+ LinesContract.LinesEntry.STATUS+" = "+"0"+" ORDER BY "+ LinesContract.LinesEntry.LINENUMBER+" ASC;",null);

        if (cursor.getCount()==0)
        {
            BackgroundTaskLine backgroundTaskLine = new BackgroundTaskLine(getActivity());
            backgroundTaskLine.execute();
        }


        while(cursor.moveToNext()) {
            String name = cursor.getString(8);
                    if(!name.contains("("))
                    {
                        LinesC linesC = new LinesC(cursor.getString(0), cursor.getString(1), cursor.getInt(2), cursor.getInt(3), cursor.getInt(4), cursor.getInt(5), cursor.getInt(6), cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10), cursor.getString(11));
                        arrayList.add(linesC);
                        Lines_counter_items linesCounterItems = new Lines_counter_items(c);
                        counter_items.add(linesCounterItems);
                        c++;
                        adapter = new LinesAdapter(arrayList, getActivity());
                    }
        }
            adapter = new LinesAdapter(arrayList);
            adapterCounter = new Lines_counter_adapter(counter_items, getActivity());
            recyclerViewCounter.setAdapter(adapterCounter);
            recyclerView.setAdapter(adapter);

        // getting Commercials From Sqlite ---------------------------------------------------------
        HashMap<String,String> url_maps = new HashMap<String, String>();
        Cursor cursors = sqLiteDatabaseLine.rawQuery("SELECT * FROM privateplaces;", null);
        while (cursors.moveToNext())
        {
            commercial = cursors.getString(0);
            char a = commercial.charAt(1); // This is The Index of Commercial String
            String stationsCommercial = "1";
            char b = stationsCommercial.charAt(0);
            if(a==b)
            {
                commercial_id = cursors.getString(2);
                Cursor cursorsPublic = sqLiteDatabaseLine.rawQuery("SELECT * FROM publicplaces WHERE id = "+commercial_id+";", null);

                if (cursorsPublic.getCount() == 1){
                 ll1.setVisibility(View.GONE);
             }
                while (cursorsPublic.moveToNext()){
                    String banner = cursorsPublic.getString(19);
                    if(banner != null) {
                        Log.d("LooLoo",banner);
                        banners.add(new RemoteBanner(Connection.images+banner));
                    }
                }
            }
            // Slider Image ------------------------------------------------------------------------------
            bannerSlider.setBanners(banners);
            bannerSlider.setOnBannerClickListener(new OnBannerClickListener() {
                @Override
                public void onClick(int position) {
                    // Toast.makeText(getApplicationContext(), "Banner with position " + String.valueOf(position) + " clicked!", Toast.LENGTH_SHORT).show();
                }
            });
            // Slider Image ------------------------------------------------------------------------------
        }
        // getting Commercials From Sqlite ---------------------------------------------------------

        return v;
    }


}
