package com.mirshekari.metroshiraz.Fragments.Schedule.main;


import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.Fragments.Schedule.Schedule_friday;
import com.mirshekari.metroshiraz.Fragments.Schedule.Schedule_monday;
import com.mirshekari.metroshiraz.Fragments.Schedule.Schedule_saturday;
import com.mirshekari.metroshiraz.Fragments.Schedule.Schedule_sunday;
import com.mirshekari.metroshiraz.Fragments.Schedule.Schedule_thursday;
import com.mirshekari.metroshiraz.Fragments.Schedule.Schedule_tuesday;
import com.mirshekari.metroshiraz.Fragments.Schedule.Schedule_wednesday;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class Schedule_main_prev extends Fragment {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    int dofw;
    String saturday,sunday,monday,tuesday,wednesday,thursday,friday,lang_name;
    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;
    SharedPreferences sharedLang;
    public Schedule_main_prev() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_schedule_main_prev, container, false);
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK)-1;


        sharedLang = getActivity().getSharedPreferences("MyPref",MODE_PRIVATE);
        lang_name = sharedLang.getString("langname","null");

        dbHelper = new DBHelper(getActivity());
        sqLiteDatabase = dbHelper.getReadableDatabase();

        if(day==7)
        {
            dofw = 1;
        }
        else if(day==1)
        {
            dofw = 2;
        }
        else if(day==2)
        {
            dofw = 3;
        }
        else if(day==3)
        {
            dofw = 4;
        }
        else if(day==4)
        {
            dofw = 5;
        }
        else if(day==5)
        {
            dofw = 6;
        }
        else if(day==6)
        {
            dofw = 0;
        }
        viewPager = (ViewPager) v.findViewById(R.id.viewpager);


        // Dictionary
        Cursor satCursor = sqLiteDatabase.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.SATURDAY+";",null);
        while (satCursor.moveToNext()){
            saturday = satCursor.getString(0);
        }
        satCursor.close();
        Cursor sunCursor = sqLiteDatabase.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.SUNDAY+";",null);
        while (sunCursor.moveToNext()){
            sunday = sunCursor.getString(0);
        }
        sunCursor.close();
        Cursor monCursor = sqLiteDatabase.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.MONDAY+";",null);
        while (monCursor.moveToNext()){
            monday = monCursor.getString(0);
        }
        monCursor.close();
        Cursor tueCursor = sqLiteDatabase.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.TUESDAY+";",null);
        while (tueCursor.moveToNext()){
            tuesday = tueCursor.getString(0);
        }
        tueCursor.close();
        Cursor wedCursor = sqLiteDatabase.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.WEDNESDAY+";",null);
        while (wedCursor.moveToNext()){
            wednesday = wedCursor.getString(0);
        }
        wedCursor.close();
        Cursor thuCursor = sqLiteDatabase.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.THURSDAY+";",null);
        while (thuCursor.moveToNext()){
            thursday = thuCursor.getString(0);
        }
        thuCursor.close();
        Cursor friCursor = sqLiteDatabase.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.FRIDAY+";",null);
        while (friCursor.moveToNext()){
            friday = friCursor.getString(0);
        }
        friCursor.close();
        // ->

        tabLayout = (TabLayout) v.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupViewPager(viewPager,saturday,sunday,monday,tuesday,wednesday,thursday,friday);
        TabLayout.Tab tab = tabLayout.getTabAt(dofw);
        tab.select();
        if(lang_name.equals("فارسي")){
            changeTabsFont();
        }
        else{
            changeTabsFontEnglish();
        }

        return v;
    }
    private void setupViewPager(ViewPager viewPager,String saturday1,String sunday1,String monday1,String tuesday1,String wednesday1,String thursday1,String friday1) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new Schedule_saturday(), saturday1);
        adapter.addFragment(new Schedule_sunday(), sunday1);
        adapter.addFragment(new Schedule_monday(), monday1);
        adapter.addFragment(new Schedule_tuesday(), tuesday1);
        adapter.addFragment(new Schedule_wednesday(), wednesday1);
        adapter.addFragment(new Schedule_thursday(), thursday1);
        adapter.addFragment(new Schedule_friday(), friday1);
        viewPager.setAdapter(adapter);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    private void changeTabsFont() {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        Typeface fonts = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_light.ttf");
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(fonts);
                }
            }
        }
    }

    private void changeTabsFontEnglish() {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        Typeface fonts = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_medium.ttf");
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(fonts);
                }
            }
        }
    }
}
