package com.mirshekari.metroshiraz.Fragments;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mirshekari.metroshiraz.ChooseStation.StationsContract;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.GetKeys.TourismPlacesAdapter;
import com.mirshekari.metroshiraz.Place.Groups.GroupsC;
import com.mirshekari.metroshiraz.Place.Public.PublicPlacesC;
import com.mirshekari.metroshiraz.Place.Public.PublicPlacesContract;
import com.mirshekari.metroshiraz.R;
import com.mirshekari.metroshiraz.Routing.Source.TourismQuee.QueeContract;
import com.mirshekari.metroshiraz.Routing.Source.TourismResult;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class TourismList extends Fragment {


    TextView placeType,present,txt404;
    LinearLayout click_search,click_type;
    Typeface irsans_light,irsans_medium;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    SQLiteDatabase db;
    DBHelper dbHelper;
    ArrayList<PublicPlacesC> arrayList = new ArrayList<>();
    ArrayList<PublicPlacesC> arrayList2 = new ArrayList<>();
    Cursor cursorPlaces;
    LinearLayout pg,layout404;
    Button done;
    ArrayList<String> arr;

    String [] array;
    SharedPreferences sharedGroup,sharedLang;
    String searchByDic,searchDic,accept_n_continue_dic,editDic,andDic,tourismDic,nothingDic,groupId,lang_name,groupName,first,last;
    public TourismList() {}

    private static int TIME_OUT = 1000;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_tourism_list, container, false);
        irsans_light = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_light.ttf");
        irsans_medium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_medium.ttf");

        recyclerView = (RecyclerView)v.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        dbHelper = new DBHelper(getActivity());
        db = dbHelper.getReadableDatabase();

        arr = new ArrayList<String>();

        sharedLang = getActivity().getSharedPreferences("MyPref",MODE_PRIVATE);
        lang_name = sharedLang.getString("langname","null");


        // Dictionary
        Cursor searchByCursor = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.SEARCH_BY+";",null);
        Cursor searchCursor = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.SEARCH+";",null);
        Cursor acceptCursor = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.ACCEPT_N_CONTINUE+";",null);
        Cursor editCursor = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.EDIT+";",null);
        Cursor andCursor = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.AND+";",null);
        Cursor tourismCursor = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.TOURISM+";",null);
        Cursor nothingmCursor = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.NOPLACEFOUND+";",null);


        while (searchByCursor.moveToNext())
        {
            searchByDic = searchByCursor.getString(0);
        }
        while (searchCursor.moveToNext())
        {
            searchDic = searchCursor.getString(0);
        }
        while (acceptCursor.moveToNext())
        {
           accept_n_continue_dic= acceptCursor.getString(0);
        }
        while (editCursor.moveToNext())
        {
            editDic = editCursor.getString(0);
        }
        while (andCursor.moveToNext())
        {
            andDic = andCursor.getString(0);
        }
        while (tourismCursor.moveToNext())
        {
            tourismDic = tourismCursor.getString(0);
        }
        while (nothingmCursor.moveToNext())
        {
            nothingDic = nothingmCursor.getString(0);
        }

        searchByCursor.close();
        searchCursor.close();
        acceptCursor.close();
        editCursor.close();
        andCursor.close();
        tourismCursor.close();
        nothingmCursor.close();
        // ->

        txt404 = (TextView) v.findViewById(R.id.txt404);
        txt404.setTypeface(irsans_light);
        txt404.setText(nothingDic);
        done = (Button) v.findViewById(R.id.btn_done);
        done.setTypeface(irsans_medium);
        done.setText(searchDic);

        pg = (LinearLayout) v.findViewById(R.id.pg);
        pg.setVisibility(View.VISIBLE);
        layout404 = (LinearLayout)v.findViewById(R.id.layout404);


        click_search = (LinearLayout) v.findViewById(R.id.click_search);
        click_type = (LinearLayout) v.findViewById(R.id.click_type);

        placeType = (TextView) v.findViewById(R.id.typename);
        present = (TextView) v.findViewById(R.id.present);

        placeType.setTypeface(irsans_light);
        present.setTypeface(irsans_light);
        present.setText(searchByDic);


        click_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChooseGroup fragment = new ChooseGroup();
                android.support.v4.app.FragmentTransaction fragmentTransaction =  getFragmentManager() .beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });


        Timer timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                if(getActivity()!=null){
                    getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            sharedGroup = getActivity().getSharedPreferences("GroupSelected", Context.MODE_PRIVATE);
                            groupId = sharedGroup.getString("typeId","null");
                            groupName = sharedGroup.getString("typeName","null");
                            if(!groupName.equals("null")){
                             placeType.setText(searchByDic+" "+groupName);
                         }
                         else{
                        placeType.setText(tourismDic);
                         }
                        }
                        catch (Exception e){}
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {}
                        }, 1000);
                    }
                });}
            }
        }, 0, 1000);




        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(groupId.equals("null"))
                {
                    cursorPlaces = db.rawQuery("SELECT * FROM "+ PublicPlacesContract.PublicPlacesEntry.TABLE_NAME+" WHERE "+ PublicPlacesContract.PublicPlacesEntry.PLACEGROUPEID+" = 4;",null);
                }
                else{
                    cursorPlaces = db.rawQuery("SELECT * FROM "+ PublicPlacesContract.PublicPlacesEntry.TABLE_NAME+" WHERE "+ PublicPlacesContract.PublicPlacesEntry.PLACEGROUPEID+" = "+groupId+";",null);
                }

                arrayList.clear();

                while (cursorPlaces.moveToNext()) {
                    Cursor check = db.rawQuery("SELECT " + StationsContract.StationsEntry.STATUS + " FROM " + StationsContract.StationsEntry.TABLE_NAME + " WHERE " + StationsContract.StationsEntry.STATION_ID + " = " + cursorPlaces.getString(10) + ";", null);
                    while (check.moveToNext()) {
                        if (check.getString(0).equals("0")) {
                            PublicPlacesC publicPlacesC = new PublicPlacesC(cursorPlaces.getString(0), cursorPlaces.getString(1),
                                    cursorPlaces.getInt(2), cursorPlaces.getString(3),
                                    cursorPlaces.getString(4), cursorPlaces.getString(5), cursorPlaces.getString(6),
                                    cursorPlaces.getInt(7), cursorPlaces.getString(8), cursorPlaces.getString(9),
                                    cursorPlaces.getInt(10), cursorPlaces.getString(11),
                                    cursorPlaces.getString(12),
                                    cursorPlaces.getString(13), cursorPlaces.getString(14),
                                    cursorPlaces.getString(15), cursorPlaces.getString(16),
                                    cursorPlaces.getString(17), cursorPlaces.getString(18),
                                    cursorPlaces.getString(19));
                            arrayList.add(publicPlacesC);
                            adapter = new TourismPlacesAdapter(arrayList, getActivity());
                        }

                    }
                }
                if(arrayList.size()==0){
                    layout404.setVisibility(View.VISIBLE);
                }
                adapter = new TourismPlacesAdapter(arrayList,getActivity());
                recyclerView.setAdapter(adapter);
                pg.setVisibility(View.GONE);
            }
        }, TIME_OUT);


        click_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TourismSearchFragment fragment = new TourismSearchFragment();
                android.support.v4.app.FragmentTransaction fragmentTransaction =  getFragmentManager() .beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.tourism_confirm_dialog);
                Typeface irsans_light_d = Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransansweb_light.ttf");
                TextView top = (TextView) dialog.findViewById(R.id.top);
                TextView bottom = (TextView) dialog.findViewById(R.id.bottom);
                TextView accept = (TextView) dialog.findViewById(R.id.accept);
                TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
                if(lang_name.equals("فارسي"))
                {
                    bottom.setCompoundDrawablesWithIntrinsicBounds( 0, 0, R.drawable.square_bottomcircle, 0);
                    top.setCompoundDrawablesWithIntrinsicBounds( 0, 0, R.drawable.square_topcircle, 0);

                }
                else{
                    bottom.setCompoundDrawablesWithIntrinsicBounds( R.drawable.square_bottomcircle, 0, 0, 0);
                    top.setCompoundDrawablesWithIntrinsicBounds( R.drawable.square_topcircle, 0, 0, 0);
                }
                    accept.setTypeface(irsans_light_d);
                cancel.setTypeface(irsans_light_d);
                accept.setText(accept_n_continue_dic);
                cancel.setText(editDic);
                final Cursor cursor = db.rawQuery("SELECT * FROM "+ QueeContract.QueeEntry.TABLE_NAME+";",null);
                while (cursor.moveToNext()){
                    cursor.moveToFirst();
                    first = cursor.getString(2);
                    cursor.moveToLast();
                    last = cursor.getString(2);

                }
                top.setText(first);
                bottom.setText(last+" "+andDic+"...");
                top.setTypeface(irsans_light_d);
                bottom.setTypeface(irsans_light_d);
                dialog.show();

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Cursor cursor1 = db.rawQuery("SELECT * FROM quee;",null);
                        while (cursor1.moveToNext())
                        {
                        arr.add(cursor1.getString(1));
                        }

                        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("TourismResult",MODE_PRIVATE);
                        SharedPreferences.Editor sheditor = sharedPreferences.edit();
                        sheditor.putString("array",String.valueOf(arr));
                        sheditor.commit();
                        Intent intent = new Intent(getActivity(), TourismResult.class);
                        startActivity(intent);
                    }
                });

            }
        });

        return v;
    }

}
