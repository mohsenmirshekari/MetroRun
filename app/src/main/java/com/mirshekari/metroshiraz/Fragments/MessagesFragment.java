package com.mirshekari.metroshiraz.Fragments;


import android.app.Dialog;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mirshekari.metroshiraz.AnalyticsApplication;
import com.mirshekari.metroshiraz.ChooseLine.LinesContract;
import com.mirshekari.metroshiraz.Connection;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.Lines;
import com.mirshekari.metroshiraz.MessagesAdapter;
import com.mirshekari.metroshiraz.MessagesC;
import com.mirshekari.metroshiraz.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessagesFragment extends Fragment {

    String gu_id,city,dic;
    ArrayList<MessagesC> arrayList = new ArrayList<>();
    SQLiteDatabase db;
    DBHelper dbHelper;
    RecyclerView recyclerView;
    Cursor cursorG;
    Tracker mTracker;

    public MessagesFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_messages, container, false);
        SharedPreferences shared = getActivity().getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences sharedcity = getActivity().getSharedPreferences("City", MODE_PRIVATE);
        gu_id = shared.getString("gu","null");
        city = sharedcity.getString("city","null");
        AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Messages"+" ("+city+")");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());


        dbHelper = new DBHelper(getActivity());
        db = dbHelper.getWritableDatabase();


        // Dictionary

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.NOTIFICATIONS+";",null);
        while (cursorG.moveToNext())
        {
            dic = cursorG.getString(0);
        }
        cursorG.close();
        // ->

        recyclerView = (RecyclerView)v.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        // Set title bar
        ((Lines) getActivity()).setActionBarTitle(dic);
        // ->


        RequestQueue queue = Volley.newRequestQueue(getActivity());
        final String url = Connection.portal + "/getMessages.php?city="+city;
        StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for(int i=0;i<jsonArray.length();i++)
                            {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String cityid = jsonObject.getString("city");
                                String highlight = jsonObject.getString("title");
                                String id = jsonObject.getString("id");
                                String text = jsonObject.getString("body");
                                String title = jsonObject.getString("title");
                                MessagesC messagesC = new MessagesC(cityid,highlight,id,text,title);
                                arrayList.add(messagesC);
                                MessagesAdapter messagesAdapter = new MessagesAdapter(arrayList,getActivity());
                                recyclerView.setAdapter(messagesAdapter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });


        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        strRequest.setRetryPolicy(policy);
        queue.add(strRequest);
        return v;
    }

}
