package com.mirshekari.metroshiraz.ChooseLanguage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mirshekari.metroshiraz.Choose_language;
import com.mirshekari.metroshiraz.GetKeys.BackgroundTaskKeys;
import com.mirshekari.metroshiraz.MySingleton;
import com.mirshekari.metroshiraz.R;
import com.mirshekari.metroshiraz.SetLanguage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mohsen on 10/13/2017.
 */

public class MyAdapter_Language extends RecyclerView.Adapter<MyAdapter_Language.ViewHolder> {
    SharedPreferences shared;
    String guid;
    ListItem_Language listItem;
    private List<ListItem_Language> listItems;
    Context context;
    public MyAdapter_Language(List<ListItem_Language> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_chooselanguage,parent,false);
        return new ViewHolder(v);
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        listItem = listItems.get(position);

        String base64String = listItem.getLang_id();
        byte[] decodedString = Base64.decode(base64String, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        holder.imageView.setImageBitmap(decodedByte);

        holder.id = listItem.getFlag();
        Typeface samim = Typeface.createFromAsset(context.getAssets(), "fonts/samim.ttf");
        holder.header.setText(listItem.getTitle());
        holder.header.setTypeface(samim);
        holder.lang_name.setText(listItem.getNameode());
        holder.lang_name.setTypeface(samim);
        shared = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        guid = shared.getString("gu", "null");



        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, SetLanguage.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("lang_id", holder.id);
                intent.putExtra("lang_name", holder.lang_name.getText());
                context.startActivity(intent);
            }
        });

    }
    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView header,lang_name;
        ArrayList<ListItem_Language> listItem = new ArrayList<ListItem_Language>();
        RelativeLayout relativeLayout;
        ImageView imageView;
        String id;

        public ViewHolder(View itemView)
        {
            super(itemView);
            header = (TextView) itemView.findViewById(R.id.header);
            relativeLayout = (RelativeLayout)itemView.findViewById(R.id.clicklayout);
            lang_name = (TextView)itemView.findViewById(R.id.lang_name);
            imageView = (ImageView)itemView.findViewById(R.id.flagimg);
        }
    }
    public interface OnItemClickListener {
        void onItemClick(ListItem_Language item);
    }
    public MyAdapter_Language() {
    }
}
