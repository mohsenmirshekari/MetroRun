package com.mirshekari.metroshiraz.ChooseLanguage;

/**
 * Created by Mohsen on 10/13/2017.
 */

public class ListItem_Language {
    private String lang_id;
    private String flag;
    private String isrtl;
    private String nameode;
    private String title;

    public ListItem_Language(String lang_id, String flag, String isrtl, String nameode, String title) {
        this.lang_id = lang_id;
        this.flag = flag;
        this.isrtl = isrtl;
        this.nameode = nameode;
        this.title = title;
    }

    public String getLang_id() {
        return lang_id;
    }

    public void setLang_id(String lang_id) {
        this.lang_id = lang_id;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getIsrtl() {
        return isrtl;
    }

    public void setIsrtl(String isrtl) {
        this.isrtl = isrtl;
    }

    public String getNameode() {
        return nameode;
    }

    public void setNameode(String nameode) {
        this.nameode = nameode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
