package com.mirshekari.metroshiraz.Facilities;

/**
 * Created by Mohsen Mirshekari on 12/4/2017.
 */

public class FacilitiesContract {
    public static class FacilitiesEntry {
        public static final String TABLE_NAME = "facilities";
        public static final String ICON = "icon";
        public static final String ID = "id";
        public static final String TITLE = "title";
    }
}
