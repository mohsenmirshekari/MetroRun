package com.mirshekari.metroshiraz.Facilities;

/**
 * Created by Mohsen Mirshekari on 12/4/2017.
 */

public class FacilitiesC {
    private String Icon;
    private int Id;
    private String Title;

    public FacilitiesC(String icon, int id, String title) {
        Icon = icon;
        Id = id;
        Title = title;
    }

    public String getIcon() {
        return Icon;
    }

    public void setIcon(String icon) {
        Icon = icon;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }
}
