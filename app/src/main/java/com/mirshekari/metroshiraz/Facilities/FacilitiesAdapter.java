package com.mirshekari.metroshiraz.Facilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mohsen Mirshekari on 12/9/2017.
 */

public class FacilitiesAdapter extends RecyclerView.Adapter <FacilitiesAdapter.RecyclerViewHolder> {
    SharedPreferences shared, stationshared, sharess;
    SharedPreferences.Editor editor;
    ArrayList<FacilitiesC> arrayList = new ArrayList<>();
    Context context;
    String bgcolor;
    SQLiteDatabase db;


    public FacilitiesAdapter(ArrayList<FacilitiesC> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    public FacilitiesAdapter(ArrayList<FacilitiesC> arrayList) {
        this.arrayList = arrayList;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_facilities, parent, false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        context = parent.getContext();
        return recyclerViewHolder;
    }


    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final FacilitiesC facilitiesC = arrayList.get(position);
        DBHelper dbHelper = new DBHelper(context);

        db = dbHelper.getReadableDatabase();

        Typeface samim = Typeface.createFromAsset(context.getAssets(), "fonts/samim.ttf");
        Typeface irsans_light = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_light.ttf");
        shared = context.getSharedPreferences("Line", MODE_PRIVATE);
        bgcolor = shared.getString("backgroundcolor", "null");

        holder.title.setText(facilitiesC.getTitle());
        holder.title.setTypeface(irsans_light);
        String base64String = facilitiesC.getIcon();
        byte[] decodedString = Base64.decode(base64String, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        Drawable drawable = new BitmapDrawable(decodedByte);
        holder.icon.setImageDrawable(drawable);

    }
    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder
    {
        TextView title;
        CircleImageView icon;
        RecyclerViewHolder(View view)
        {
            super(view);
            icon = (CircleImageView) itemView.findViewById(R.id.icon);
            title = (TextView) itemView.findViewById(R.id.title);
        }
    }
}
