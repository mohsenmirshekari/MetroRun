package com.mirshekari.metroshiraz.Facilities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.widget.TextView;

import com.mirshekari.metroshiraz.Connection;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.Lines;
import com.mirshekari.metroshiraz.R;
import com.mirshekari.metroshiraz.Splash;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mohsen Mirshekari on 12/4/2017.
 */

public class BackgroundTaskFacilities extends AsyncTask<Void,Void,Void> {
    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;
    Context ctx;
    SharedPreferences shared,sharedcity,sharedline,cityName;
    String gu_id,station_no,city_name,dictionary;
    int route,numrows;
    Dialog dialog;
    public BackgroundTaskFacilities(Context ctx) {
        this.ctx = ctx;
    }

    @Override
    protected void onPreExecute() {
        dbHelper = new DBHelper(ctx);
        sqLiteDatabase = dbHelper.getWritableDatabase();
        shared = ctx.getSharedPreferences("MyPref", MODE_PRIVATE);
        sharedcity = ctx.getSharedPreferences("Station", MODE_PRIVATE);
        cityName = ctx.getSharedPreferences("City", MODE_PRIVATE);
        city_name = cityName.getString("cityName","شما");
        gu_id = shared.getString("gu","null");
        station_no = sharedcity.getString("stid","null");
        sharedline = ctx.getSharedPreferences("LineInfo",MODE_PRIVATE);

        //Dictionary

        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.CITY_INFO_DOWNLOADING+";",null);

        while (cursor.moveToNext()){
            dictionary = cursor.getString(2);
        }
        // ->


        dialog = new Dialog(ctx);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.loading);
        Typeface samim = Typeface.createFromAsset(ctx.getAssets(), "fonts/samim.ttf");
        TextView messageTextView = (TextView)dialog.findViewById(R.id.message);
        messageTextView.setTypeface(samim);
        messageTextView.setText(dictionary+city_name);
        messageTextView.setTypeface(samim);
        dialog.show();


    }

    @Override
    protected Void doInBackground(Void... voids) {
            try {
                String json_url = Connection.portal+"/Getfacilities/"+gu_id+"/0";
                URL url = new URL(json_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line + "\n");
                    Thread.sleep(5000);
                }
                httpURLConnection.disconnect();
                String json_data = stringBuilder.toString().trim();
                JSONArray jsonarray = new JSONArray(json_data);
                DBHelper dbHelper = new DBHelper(ctx);
                SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();
                int count = 0;

                while (count < jsonarray.length()) {
                    JSONObject o = jsonarray.getJSONObject(count);
                    count++;
                    dbHelper.putFacilitiesInformation(o.getString("Icon"), o.getInt("Id"), o.getString("Title"), sqLiteDatabase);

                }
                dbHelper.close();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        dialog.dismiss();
        ctx.startActivity(new Intent(ctx,Lines.class));

    }
}
