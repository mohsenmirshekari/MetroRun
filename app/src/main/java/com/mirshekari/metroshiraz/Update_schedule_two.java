package com.mirshekari.metroshiraz;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mirshekari.metroshiraz.ChooseStation.StationsContract;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;


public class Update_schedule_two extends AppCompatActivity {
    Context ctx;
    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;
    SharedPreferences shared,sharedcity,cityName ;
    String gu_id,city_id,city_name,line_title,version;
    int line_id=1;
    int station_id=1;
    int route;
    String dictionary;
    TextView messageTextView;
    RequestQueue queue1;
    TextView text;
    String lastStation,downloadDic;
    RequestQueue queue;
    private RequestQueue requestQueue;
    int stationid,i;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_schedule);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dbHelper = new DBHelper(Update_schedule_two.this);
        sqLiteDatabase = dbHelper.getReadableDatabase();
        //Dictionary
        Typeface irsans_light = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_light.ttf");

        text = (TextView) findViewById(R.id.text);
        text.setTypeface(irsans_light);

        shared = getSharedPreferences("MyPref", MODE_PRIVATE);
        sharedcity = getSharedPreferences("City", MODE_PRIVATE);
        gu_id = shared.getString("gu","null");
        city_id = sharedcity.getString("city","null");
        cityName = getSharedPreferences("City", MODE_PRIVATE);
        city_name = cityName.getString("cityName","شما");

        Cursor dic = sqLiteDatabase.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.DOWNLOADING_STATIONS+";",null);
        while (dic.moveToNext())
        {
            downloadDic = dic.getString(0);
        }
        dic.close();
        text.setText(downloadDic);

        if(queue==null)
        {
            queue = Volley.newRequestQueue(getApplicationContext());
        }
        final String url = Connection.portal+"/GetLines/" + gu_id + "/" + city_id + "/0";
        StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONArray jsonArray = new JSONArray(response);

                            for(int i=0;i<jsonArray.length();i++)
                            {
                                final JSONObject o = jsonArray.getJSONObject(i);
                                line_id = Integer.valueOf(o.getString("Id"));
                                final RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                                final String url =Connection.portal + "/GetStations/" + gu_id + "/" + line_id + "/0";
                                StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {

                                                try {
                                                    JSONArray jsonArray = new JSONArray(response);
                                                    for(int i=0;i<jsonArray.length();i++)
                                                    {
                                                        JSONObject os = jsonArray.getJSONObject(i);
                                                        stationid = os.getInt("Id");
                                                        if(requestQueue==null)
                                                        {
                                                            requestQueue = Volley.newRequestQueue(getApplicationContext());
                                                        }
                                                        final String url = Connection.portal+"/GetSchedules/" + gu_id + "/" + stationid;
                                                        StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                                                                new Response.Listener<String>() {
                                                                    @Override
                                                                    public void onResponse(String response) {
                                                                        DBHelper dbHelper = new DBHelper(Update_schedule_two.this);
                                                                        SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();
                                                                        try {
                                                                            JSONObject object = new JSONObject(response);
                                                                            String stid = object.getString("StationId");
                                                                            String stno = object.getString("StationNo");
                                                                            String previus = object.getString("Previous");
                                                                            String next = object.getString("Next");
                                                                            if(next.length()!=4 && previus.length()==4)
                                                                            {
                                                                                android.util.Log.d("adkwugdwawaa","1");
                                                                                route = 0;
                                                                                JSONObject object2 = object.getJSONObject("Next");
                                                                                JSONArray Jarray2 = object2.getJSONArray("Days");
                                                                                JSONObject ab0 = Jarray2.getJSONObject(0);
                                                                                JSONObject ab1 = Jarray2.getJSONObject(1);
                                                                                JSONObject ab2 = Jarray2.getJSONObject(2);
                                                                                JSONObject ab3 = Jarray2.getJSONObject(3);
                                                                                JSONObject ab4 = Jarray2.getJSONObject(4);
                                                                                JSONObject ab5 = Jarray2.getJSONObject(5);
                                                                                JSONObject ab6 = Jarray2.getJSONObject(5);

                                                                                JSONArray array0 = ab0.getJSONArray("Plan");
                                                                                JSONArray array1 = ab1.getJSONArray("Plan");
                                                                                JSONArray array2 = ab2.getJSONArray("Plan");
                                                                                JSONArray array3 = ab3.getJSONArray("Plan");
                                                                                JSONArray array4 = ab4.getJSONArray("Plan");
                                                                                JSONArray array5 = ab5.getJSONArray("Plan");
                                                                                JSONArray array6 = ab6.getJSONArray("Plan");

                                                                                dbHelper.putSchedule0Information(String.valueOf(array0), route, "saturday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule1Information(String.valueOf(array1), route, "sunday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule2Information(String.valueOf(array2), route, "monday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule3Information(String.valueOf(array3), route, "tuesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule4Information(String.valueOf(array4), route, "wednesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule5Information(String.valueOf(array5), route, "thursday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule6Information(String.valueOf(array6), route, "friday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                            }

                                                                            if(next.length()!=4 && previus.length()!=4)
                                                                            {
                                                                                android.util.Log.d("adkwugdwawaa","2");
                                                                                route = 1;
                                                                                // DOWNLOADING THE NEXT SCHEDULE
                                                                                JSONObject object2 = object.getJSONObject("Next");
                                                                                JSONArray Jarray2 = object2.getJSONArray("Days");
                                                                                JSONObject ab0 = Jarray2.getJSONObject(0);
                                                                                JSONObject ab1 = Jarray2.getJSONObject(1);
                                                                                JSONObject ab2 = Jarray2.getJSONObject(2);
                                                                                JSONObject ab3 = Jarray2.getJSONObject(3);
                                                                                JSONObject ab4 = Jarray2.getJSONObject(4);
                                                                                JSONObject ab5 = Jarray2.getJSONObject(5);
                                                                                JSONObject ab6 = Jarray2.getJSONObject(6);


                                                                                JSONArray array0 = ab0.getJSONArray("Plan");
                                                                                JSONArray array1 = ab1.getJSONArray("Plan");
                                                                                JSONArray array2 = ab2.getJSONArray("Plan");
                                                                                JSONArray array3 = ab3.getJSONArray("Plan");
                                                                                JSONArray array4 = ab4.getJSONArray("Plan");
                                                                                JSONArray array5 = ab5.getJSONArray("Plan");
                                                                                JSONArray array6 = ab6.getJSONArray("Plan");

                                                                                android.util.Log.d("dwagdwa 0",stid+" *** "+String.valueOf(ab0));
                                                                                android.util.Log.d("dwagdwa 0",stid+" *** "+String.valueOf(ab1));
                                                                                android.util.Log.d("dwagdwa 0",stid+" *** "+String.valueOf(ab2));
                                                                                android.util.Log.d("dwagdwa 0",stid+" *** "+String.valueOf(ab3));
                                                                                android.util.Log.d("dwagdwa 0",stid+" *** "+String.valueOf(ab4));
                                                                                android.util.Log.d("dwagdwa 0",stid+" *** "+String.valueOf(ab5));
                                                                                android.util.Log.d("dwagdwa 8",stid+" *** "+String.valueOf(ab6));

                                                                                dbHelper.putSchedule0Information(String.valueOf(array0), route, "saturday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule1Information(String.valueOf(array1), route, "sunday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule2Information(String.valueOf(array2), route, "monday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule3Information(String.valueOf(array3), route, "tuesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule4Information(String.valueOf(array4), route, "wednesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule5Information(String.valueOf(array5), route, "thursday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule6Information(String.valueOf(array6), route, "friday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);


                                                                                // DOWNLOADING THE NEXT SCHEDULE
                                                                                Log.d("SCHEDULE ","DOWNLOADING PREVIOUS SCHEDULE WITH ROUTE 1");
                                                                                JSONObject object3 = object.getJSONObject("Previous");
                                                                                JSONArray Jarray3 = object3.getJSONArray("Days");
                                                                                JSONObject aab0 = Jarray3.getJSONObject(0);
                                                                                JSONObject aab1 = Jarray3.getJSONObject(1);
                                                                                JSONObject aab2 = Jarray3.getJSONObject(2);
                                                                                JSONObject aab3 = Jarray3.getJSONObject(3);
                                                                                JSONObject aab4 = Jarray3.getJSONObject(4);
                                                                                JSONObject aab5 = Jarray3.getJSONObject(5);
                                                                                JSONObject aab6 = Jarray3.getJSONObject(6);

                                                                                JSONArray arrayy0 = aab0.getJSONArray("Plan");
                                                                                JSONArray arrayy1 = aab1.getJSONArray("Plan");
                                                                                JSONArray arrayy2 = aab2.getJSONArray("Plan");
                                                                                JSONArray arrayy3 = aab3.getJSONArray("Plan");
                                                                                JSONArray arrayy4 = aab4.getJSONArray("Plan");
                                                                                JSONArray arrayy5 = aab5.getJSONArray("Plan");
                                                                                JSONArray arrayy6 = aab6.getJSONArray("Plan");

                                                                                dbHelper.putSchedule0Information(String.valueOf(arrayy0), route, "saturday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule1Information(String.valueOf(arrayy1), route, "sunday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule2Information(String.valueOf(arrayy2), route, "monday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule3Information(String.valueOf(arrayy3), route, "tuesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule4Information(String.valueOf(arrayy4), route, "wednesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule5Information(String.valueOf(arrayy5), route, "thursday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule6Information(String.valueOf(arrayy6), route, "friday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                            }


                                                                            if(next.length()==4 && previus.length()!=4)
                                                                            {
                                                                                route = 2;
                                                                                android.util.Log.d("adkwugdwawaa","3");
                                                                                // DOWNLOADING THE PREVIUS SCHEDULE
                                                                                JSONObject object2 = object.getJSONObject("Previous");
                                                                                JSONArray Jarray2 = object2.getJSONArray("Days");
                                                                                JSONObject ab0 = Jarray2.getJSONObject(0);
                                                                                JSONObject ab1 = Jarray2.getJSONObject(1);
                                                                                JSONObject ab2 = Jarray2.getJSONObject(2);
                                                                                JSONObject ab3 = Jarray2.getJSONObject(3);
                                                                                JSONObject ab4 = Jarray2.getJSONObject(4);
                                                                                JSONObject ab5 = Jarray2.getJSONObject(5);
                                                                                JSONObject ab6 = Jarray2.getJSONObject(5);

                                                                                JSONArray array0 = ab0.getJSONArray("Plan");
                                                                                JSONArray array1 = ab1.getJSONArray("Plan");
                                                                                JSONArray array2 = ab2.getJSONArray("Plan");
                                                                                JSONArray array3 = ab3.getJSONArray("Plan");
                                                                                JSONArray array4 = ab4.getJSONArray("Plan");
                                                                                JSONArray array5 = ab5.getJSONArray("Plan");
                                                                                JSONArray array6 = ab6.getJSONArray("Plan");
                                                                                Log.d("SCHEDULE ","DOWNLOADING PREVIUS SCHEDULE WITH ROUTE 2");

                                                                                dbHelper.putSchedule0Information(String.valueOf(array0), route, "saturday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule1Information(String.valueOf(array1), route, "sunday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule2Information(String.valueOf(array2), route, "monday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule3Information(String.valueOf(array3), route, "tuesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule4Information(String.valueOf(array4), route, "wednesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule5Information(String.valueOf(array5), route, "thursday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                                                                                dbHelper.putSchedule6Information(String.valueOf(array6), route, "friday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);


                                                                            }
                                                                        } catch (JSONException e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    }
                                                                },
                                                                new Response.ErrorListener() {
                                                                    @Override
                                                                    public void onErrorResponse(VolleyError error) {

                                                                    }
                                                                });

                                                        int socketTimeout = 30000; // 30 seconds. You can change it
                                                        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                                                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

                                                        strRequest.setRetryPolicy(policy);
                                                        requestQueue.add(strRequest);

                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {

                                            }
                                        });


                                int socketTimeout = 30000; // 30 seconds. You can change it
                                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

                                strRequest.setRetryPolicy(policy);
                                queue.add(strRequest);

                            }


                        }catch (Exception e){}
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });


        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        strRequest.setRetryPolicy(policy);
        queue.add(strRequest);



        new Timer().schedule(new TimerTask(){
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        finish();
                    }
                });
            }
        }, 30000);




    }

    @Override
    public void onBackPressed() {
    }
}
