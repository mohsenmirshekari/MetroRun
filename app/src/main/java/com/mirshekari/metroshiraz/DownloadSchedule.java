package com.mirshekari.metroshiraz;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mirshekari.metroshiraz.ChooseStation.StationsContract;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class DownloadSchedule extends AppCompatActivity {
    Context ctx;
    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;
    SharedPreferences shared,sharedcity,cityName ;
    String gu_id,city_id,city_name;
    String dictionary;
    TextView text;
    Tracker mTracker;
    private RequestQueue requestQueue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_schedule);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dbHelper = new DBHelper(DownloadSchedule.this);
        sqLiteDatabase = dbHelper.getReadableDatabase();
        //Dictionary
        Typeface irsans_light = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_light.ttf");

        text = (TextView) findViewById(R.id.text);
        text.setTypeface(irsans_light);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        shared = getSharedPreferences("MyPref", MODE_PRIVATE);
        sharedcity = getSharedPreferences("City", MODE_PRIVATE);
        gu_id = shared.getString("gu","null");
        city_id = sharedcity.getString("city","null");
        cityName = getSharedPreferences("City", MODE_PRIVATE);
        city_name = cityName.getString("cityName","شما");


        if(requestQueue==null)
        {
            requestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        final String url = Connection.portal+"/GetSchedules.php?city="+city_id;
        StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        DBHelper dbHelper = new DBHelper(DownloadSchedule.this);
                        SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for(int i=0;i<jsonArray.length();i++)
                            {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                if(jsonObject.getString("day").equals("saturday"))
                                {
                                    dbHelper.putSchedule0Information(String.valueOf(jsonObject.getString("time")), Integer.valueOf(jsonObject.getString("route")), jsonObject.getString("day"), Integer.valueOf(jsonObject.getString("station_id")), Integer.valueOf(jsonObject.getString("station_no")), sqLiteDatabase);
                                }
                                else if(jsonObject.getString("day").equals("sunday"))
                                {
                                    dbHelper.putSchedule1Information(String.valueOf(jsonObject.getString("time")), Integer.valueOf(jsonObject.getString("route")), jsonObject.getString("day"), Integer.valueOf(jsonObject.getString("station_id")), Integer.valueOf(jsonObject.getString("station_no")), sqLiteDatabase);
                                }
                                else if(jsonObject.getString("day").equals("monday"))
                                {
                                    dbHelper.putSchedule2Information(String.valueOf(jsonObject.getString("time")), Integer.valueOf(jsonObject.getString("route")), jsonObject.getString("day"), Integer.valueOf(jsonObject.getString("station_id")), Integer.valueOf(jsonObject.getString("station_no")), sqLiteDatabase);
                                }
                                else if(jsonObject.getString("day").equals("tuesday"))
                                {
                                    dbHelper.putSchedule3Information(String.valueOf(jsonObject.getString("time")), Integer.valueOf(jsonObject.getString("route")), jsonObject.getString("day"), Integer.valueOf(jsonObject.getString("station_id")), Integer.valueOf(jsonObject.getString("station_no")), sqLiteDatabase);
                                }
                                else if(jsonObject.getString("day").equals("wednesday"))
                                {
                                    dbHelper.putSchedule4Information(String.valueOf(jsonObject.getString("time")), Integer.valueOf(jsonObject.getString("route")), jsonObject.getString("day"), Integer.valueOf(jsonObject.getString("station_id")), Integer.valueOf(jsonObject.getString("station_no")), sqLiteDatabase);
                                }
                                else if(jsonObject.getString("day").equals("thursday"))
                                {
                                    dbHelper.putSchedule5Information(String.valueOf(jsonObject.getString("time")), Integer.valueOf(jsonObject.getString("route")), jsonObject.getString("day"), Integer.valueOf(jsonObject.getString("station_id")), Integer.valueOf(jsonObject.getString("station_no")), sqLiteDatabase);
                                }
                                else if(jsonObject.getString("day").equals("friday"))
                                {
                                    dbHelper.putSchedule6Information(String.valueOf(jsonObject.getString("time")), Integer.valueOf(jsonObject.getString("route")), jsonObject.getString("day"), Integer.valueOf(jsonObject.getString("station_id")), Integer.valueOf(jsonObject.getString("station_no")), sqLiteDatabase);
                                }
                            }
                        } catch (JSONException e) {
                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("Debug")
                                    .setAction("DownloadSchedule.java : "+String.valueOf(e))
                                    .build());
                            e.printStackTrace();
                        } finally {
                            Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        strRequest.setRetryPolicy(policy);
        requestQueue.add(strRequest);


            }

}
