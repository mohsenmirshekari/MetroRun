package com.mirshekari.metroshiraz.ChooseLine;

/**
 * Created by Mohsen on 11/3/2017.
 */

public class LinesC {
    private String BackgroundColor;
    private String City_ID;
    private int FirstStationNumber;
    private int Line_ID;
    private int LastSTationNumber;
    private int LineNumber;
    private int Status;
    private String TextColor;
    private String Title;
    private String Image;
    private String MultiEnd;
    private String MultiEndDetails;

    public LinesC(String backgroundColor, String city_ID, int firstStationNumber, int line_ID, int lastSTationNumber, int lineNumber, int status, String textColor, String title, String image, String multiEnd, String multiEndDetails) {
        BackgroundColor = backgroundColor;
        City_ID = city_ID;
        FirstStationNumber = firstStationNumber;
        Line_ID = line_ID;
        LastSTationNumber = lastSTationNumber;
        LineNumber = lineNumber;
        Status = status;
        TextColor = textColor;
        Title = title;
        Image = image;
        MultiEnd = multiEnd;
        MultiEndDetails = multiEndDetails;
    }

    public String getBackgroundColor() {
        return BackgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        BackgroundColor = backgroundColor;
    }

    public String getCity_ID() {
        return City_ID;
    }

    public void setCity_ID(String city_ID) {
        City_ID = city_ID;
    }

    public int getFirstStationNumber() {
        return FirstStationNumber;
    }

    public void setFirstStationNumber(int firstStationNumber) {
        FirstStationNumber = firstStationNumber;
    }

    public int getLine_ID() {
        return Line_ID;
    }

    public void setLine_ID(int line_ID) {
        Line_ID = line_ID;
    }

    public int getLastSTationNumber() {
        return LastSTationNumber;
    }

    public void setLastSTationNumber(int lastSTationNumber) {
        LastSTationNumber = lastSTationNumber;
    }

    public int getLineNumber() {
        return LineNumber;
    }

    public void setLineNumber(int lineNumber) {
        LineNumber = lineNumber;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getTextColor() {
        return TextColor;
    }

    public void setTextColor(String textColor) {
        TextColor = textColor;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getMultiEnd() {
        return MultiEnd;
    }

    public void setMultiEnd(String multiEnd) {
        MultiEnd = multiEnd;
    }

    public String getMultiEndDetails() {
        return MultiEndDetails;
    }

    public void setMultiEndDetails(String multiEndDetails) {
        MultiEndDetails = multiEndDetails;
    }
}