package com.mirshekari.metroshiraz.ChooseLine;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.mirshekari.metroshiraz.ChooseCity.BackgroundTask;
import com.mirshekari.metroshiraz.Choose_city;
import com.mirshekari.metroshiraz.Connection;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.Facilities.BackgroundTaskFacilities;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.Lines;
import com.mirshekari.metroshiraz.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mohsen on 11/4/2017.
 */

public class BackgroundTaskLine extends AsyncTask<Void,Void,Void> {

    ProgressDialog progressDialog;
    Context ctx;
    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;
    SharedPreferences shared,sharedcity,cityName ;
    String gu_id,city_id,city_name,line_title,version;
    int line_id=1;
    int station_id=1;
    int route;
    Dialog dialog;
    String icon,background,slide1,slide2,slide3,slide4,banner,instagram,telegram,soctest,soctest2,dictionary;
    TextView messageTextView;
    public BackgroundTaskLine(Context ctx) {
        this.ctx = ctx;
    }


    @Override
    protected void onPreExecute() {
        dbHelper = new DBHelper(ctx);
        sqLiteDatabase = dbHelper.getReadableDatabase();
        //Dictionary

        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.CITY_INFO_DOWNLOADING+";",null);

        while (cursor.moveToNext()){
            dictionary = cursor.getString(2);
        }
        // ->
        shared = ctx.getSharedPreferences("MyPref", MODE_PRIVATE);
        sharedcity = ctx.getSharedPreferences("City", MODE_PRIVATE);
        gu_id = shared.getString("gu","null");
        city_id = sharedcity.getString("city","null");
        cityName = ctx.getSharedPreferences("City", MODE_PRIVATE);
        city_name = cityName.getString("cityName","شما");
        dialog = new Dialog(ctx);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.loading);
        Typeface samim = Typeface.createFromAsset(ctx.getAssets(), "fonts/samim.ttf");
        messageTextView = (TextView)dialog.findViewById(R.id.message);
        messageTextView.setTypeface(samim);
        messageTextView.setText(dictionary +city_name);
        messageTextView.setTypeface(samim);
        dialog.show();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            String json_url = Connection.portal+"/GetLines/" + gu_id + "/" + city_id + "/0";
            String json_public_places = Connection.portal+"/GetPublicPlaces/" + gu_id + "/" + station_id + "/0";
            String json_private_places = Connection.portal+"/getPrivatePlaces/"+gu_id+"/"+station_id+"/0";
            String json_groups = Connection.portal+"/GetGroups/"+gu_id+"/0";
            String aboutus_url = Connection.portal+"getaboutus/"+gu_id+"/"+getAppVersion()+"/"+"0";

            /// Groups

            URL url_groupe = new URL(json_groups);
            HttpURLConnection httpURLConnectionGroups = (HttpURLConnection) url_groupe.openConnection();
            InputStream inputStreamGroups = httpURLConnectionGroups.getInputStream();
            BufferedReader bufferedReaderGroupe = new BufferedReader(new InputStreamReader(inputStreamGroups));
            StringBuilder stringBuilderGroups = new StringBuilder();
            String lineGroupe;
            while ((lineGroupe = bufferedReaderGroupe.readLine()) != null) {
                stringBuilderGroups.append(lineGroupe + "\n");
                Thread.sleep(5000);
            }
            httpURLConnectionGroups.disconnect();
            String json_dataGroups = stringBuilderGroups.toString().trim();
            JSONArray jsonarrayGroups = new JSONArray(json_dataGroups);
            /// Groups


            ///AboutUs
            URL url_aboutus = new URL(aboutus_url);
            HttpURLConnection httpURLConnectionaboutus = (HttpURLConnection) url_aboutus.openConnection();
            InputStream inputStreamaboutus = httpURLConnectionaboutus.getInputStream();
            BufferedReader bufferedReaderaboutus = new BufferedReader(new InputStreamReader(inputStreamaboutus));
            StringBuilder stringBuilderaboutus= new StringBuilder();
            String lineaboutus;
            while ((lineaboutus = bufferedReaderaboutus.readLine()) != null) {
                stringBuilderaboutus.append(lineaboutus + "\n");
                Thread.sleep(5000);
            }
            httpURLConnectionaboutus.disconnect();
            String json_dataaboutus = stringBuilderaboutus.toString().trim();
            JSONObject oA = new JSONObject(json_dataaboutus);
            ///AboutUs

            ///Public Places
            URL url_pubplace = new URL(json_public_places);
            HttpURLConnection httpURLConnectionPubPlace = (HttpURLConnection) url_pubplace.openConnection();
            InputStream inputStreamPubPlace = httpURLConnectionPubPlace.getInputStream();
            BufferedReader bufferedReaderPubPlace = new BufferedReader(new InputStreamReader(inputStreamPubPlace));
            StringBuilder stringBuilderPubPlace = new StringBuilder();
            String linePubPlace;
            while ((linePubPlace = bufferedReaderPubPlace.readLine()) != null) {
                stringBuilderPubPlace.append(linePubPlace + "\n");
                Thread.sleep(5000);
            }
            httpURLConnectionPubPlace.disconnect();
            String json_dataPubPlace = stringBuilderPubPlace.toString().trim();
            JSONArray jsonarrayPubPlace = new JSONArray(json_dataPubPlace);
            ///Public Places /*


            /** Private Places **/

            URL url_pvplace = new URL(json_private_places);
            HttpURLConnection httpURLConnectionpvPlace = (HttpURLConnection) url_pvplace.openConnection();
            InputStream inputStreamPvPlace = httpURLConnectionpvPlace.getInputStream();
            BufferedReader bufferedReaderPvPlace = new BufferedReader(new InputStreamReader(inputStreamPvPlace));
            StringBuilder stringBuilderPvPlace = new StringBuilder();
            String linePvPlace;
            while ((linePvPlace = bufferedReaderPvPlace.readLine()) != null) {
                stringBuilderPvPlace.append(linePvPlace + "\n");
                Thread.sleep(5000);
            }
            httpURLConnectionpvPlace.disconnect();
            String json_dataPvPlace = stringBuilderPvPlace.toString().trim();
            JSONArray jsonarrayPvPlace = new JSONArray(json_dataPvPlace);

            /** Private Places **/



            URL url = new URL(json_url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line + "\n");
                Thread.sleep(5000);
            }
            httpURLConnection.disconnect();
            String json_data = stringBuilder.toString().trim();
            JSONArray jsonarray = new JSONArray(json_data);
            DBHelper dbHelper = new DBHelper(ctx);
            SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();
            int count = 0;
            int countStation = 0;
            int countPublicPlace = 0;
            int countPrivatePlace = 0;
            int countGroups = 0;

            while (countGroups<jsonarrayGroups.length())
            {
                JSONObject g = jsonarrayGroups.getJSONObject(countGroups);
                dbHelper.putGroupsInformation(g.getInt("Id"), g.getString("Name"),sqLiteDatabase);
                countGroups++;
            }

            // Putting AboutUs Information
            dbHelper.putAboutUsInformation(oA.getString("Address"),oA.getString("BackendDeveloper"),oA.getString("Description"),oA.getString("FrontendDeveloper"),oA.getString("Id"),oA.getString("Logo"),oA.getString("Title"),oA.getString("WebsiteURL"),sqLiteDatabase);
            //- Putting AboutUs Information


            while (count < jsonarray.length()) {
                JSONObject o = jsonarray.getJSONObject(count);
                JSONArray images = o.getJSONArray("Images");

                String Images = images.getString(0);

                dbHelper.putLineInformation(o.getString("BackgroundColor"), o.getString("CityId"),
                        o.getInt("FirstStationNumber"), o.getInt("Id"), o.getInt("LastStationNumber")
                        , o.getInt("LineNumber"), o.getInt("Status"), o.getString("TextColor"), o.getString("Title"), images.getString(0),o.getString("MultiEnd"),o.getString("MultiendDetails"), sqLiteDatabase);


                line_id = Integer.valueOf(o.getString("Id"));
                line_title = o.getString("Title");
                count++;

                ///Station
                //line_id++;
                String json_url_station = Connection.portal + "/GetStations/" + gu_id + "/" + line_id + "/0";
                URL url_station = new URL(json_url_station);
                HttpURLConnection httpURLConnectionStation = (HttpURLConnection) url_station.openConnection();
                InputStream inputStreamStation = httpURLConnectionStation.getInputStream();
                BufferedReader bufferedReaderStation = new BufferedReader(new InputStreamReader(inputStreamStation));
                StringBuilder stringBuilderStation = new StringBuilder();
                String lineStation;
                while ((lineStation = bufferedReaderStation.readLine()) != null) {
                    stringBuilderStation.append(lineStation + "\n");
                    Thread.sleep(5000);
                }
                httpURLConnectionStation.disconnect();
                String json_dataStation = stringBuilderStation.toString().trim();
                JSONArray jsonarrayStation = new JSONArray(json_dataStation);
                android.util.Log.d("Samanja", String.valueOf(line_id));


                for (int i = 0; i < jsonarrayStation.length(); i++) {
                    JSONObject os = jsonarrayStation.getJSONObject(i);
                    dbHelper.putStationsInformation(os.getString("Address"), os.getInt("Id"), os.getInt("LineId"),
                            os.getString("Location"), os.getInt("StationNumber"),
                            os.getInt("Status"), os.getString("Title"), os.getString("Facilities"),os.getString("IsCross"), sqLiteDatabase);
                    station_id = Integer.valueOf(o.getString("Id"));


                    countStation++;

                    ///Station


                    while (countPublicPlace < jsonarrayPubPlace.length()) {
                        JSONObject opp = jsonarrayPubPlace.getJSONObject(countPublicPlace);
                        JSONArray photo = opp.getJSONArray("PhotoLinks");
                        JSONArray socialmedia = opp.getJSONArray("SocialMedia");

                        if (socialmedia.length() > 0) {
                            if (socialmedia.length() == 1) {
                                soctest = socialmedia.getString(0);
                                if (soctest.contains("Telegram")) {
                                    telegram = socialmedia.getString(0);
                                    telegram = telegram.substring(9);
                                } else if (soctest.contains("Instagram")) {
                                    instagram = socialmedia.getString(0);
                                    instagram = instagram.substring(9);
                                }
                            } else if (socialmedia.length() == 2) {
                                soctest = socialmedia.getString(0);
                                if (soctest.contains("Telegram")) {
                                    telegram = socialmedia.getString(0);
                                    telegram = telegram.substring(9);
                                } else if (soctest.contains("Instagram")) {
                                    instagram = socialmedia.getString(0);
                                    instagram = instagram.substring(9);
                                }

                                soctest2 = socialmedia.getString(1);
                                if (soctest.contains("Telegram")) {
                                    telegram = socialmedia.getString(1);
                                    telegram = telegram.substring(8);
                                } else if (soctest.contains("Instagram")) {
                                    instagram = socialmedia.getString(1);
                                    instagram = instagram.substring(8);
                                }
                            }

                        } else if (socialmedia.length() == 0) {
                            instagram = "";
                            telegram = "";
                        }


                        try{
                            icon = photo.getString(0);
                            icon = icon.replace("-", "/");

                        }catch (Exception e){}

                        if (photo.length() > 1) {
                            background = photo.getString(1);
                            background = background.replace("-", "/");
                        }
                        if (photo.length() > 2) {
                            slide1 = photo.getString(2);
                            slide1 = slide1.replace("-", "/");
                        }
                        if (photo.length() > 3) {
                            slide2 = photo.getString(3);
                            slide2 = slide2.replace("-", "/");
                        }
                        if (photo.length() > 4) {
                            slide3 = photo.getString(4);
                            slide3 = slide3.replace("-", "/");
                        }
                        if (photo.length() > 5) {
                            slide4 = photo.getString(5);
                            slide4 = slide4.replace("-", "/");
                        }
                        if (photo.length() > 6) {
                            banner = photo.getString(6);
                            banner = banner.replace("-", "/");
                        }

                        dbHelper.putPublicPlacesInformation(opp.getString("BackgroundColorId"), opp.getString("Description"), opp.getInt("Id"), opp.getString("IsPrivate"), opp.getString("Phone"), opp.getString("PhotoLinks"), opp.getString("Photos"), opp.getInt("PlaceGroupId"), instagram, telegram, opp.getInt("StationId"), opp.getString("TextColorId"), opp.getString("Title"), icon, background, slide1, slide2, slide3, slide4, banner, sqLiteDatabase);
                        countPublicPlace++;

                        while (countPrivatePlace < jsonarrayPvPlace.length()) {
                            JSONObject opv = jsonarrayPvPlace.getJSONObject(countPrivatePlace);
                            dbHelper.putPrivatePlacesInformation(opv.getString("Commercial"), opv.getString("EndDate"), opv.getInt("Id"), opv.getString("IsVIP"), opv.getString("Mobiles"), opv.getString("Photos"), opv.getString("Website"),opv.getString("PlaceId"), sqLiteDatabase);
                            countPrivatePlace++;
                        }
                    }
                }
            }

            dbHelper.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

        @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }
    @Override
    protected void onPostExecute(Void aVoid) {
        dialog.dismiss();

       // ctx.startActivity(new Intent(ctx,Lines.class));
        BackgroundTaskFacilities backgroundTaskFacilities = new BackgroundTaskFacilities(ctx);
        backgroundTaskFacilities.execute();
    }
    public String getAppVersion()
    {
        try {
            PackageInfo pInfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }

}

