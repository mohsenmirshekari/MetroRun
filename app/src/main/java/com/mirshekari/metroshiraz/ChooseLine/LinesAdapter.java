package com.mirshekari.metroshiraz.ChooseLine;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mirshekari.metroshiraz.AnalyticsApplication;
import com.mirshekari.metroshiraz.ChooseStation.BackgroundTaskStation;
import com.mirshekari.metroshiraz.ChooseStation.StationsContract;
import com.mirshekari.metroshiraz.Choose_station;
import com.mirshekari.metroshiraz.Connection;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.Lines;
import com.mirshekari.metroshiraz.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;
import static com.mirshekari.metroshiraz.Fragments.Aboutus_fragment.imageDownload;
import static com.mirshekari.metroshiraz.Fragments.Schedule.Schedule_saturday.faToEn;

/**
 * Created by Mohsen on 11/3/2017.
 */

public class LinesAdapter extends RecyclerView.Adapter <LinesAdapter.RecyclerViewHolder> {
    SharedPreferences shared,sharedLang;
    SharedPreferences.Editor editor;
    ArrayList<LinesC> arrayList = new ArrayList<>();
    Context context;
    SQLiteDatabase db;
    Cursor cursor;
    public Dialog dialog;
    Tracker mTracker;
    String last,first,twopathwarn,lang_name,from,to,dic_status,active,inactive,un_main,station_count,connectedLineNumber,fStation,lStation,status,multiend,multienddetails,connectedLineTitle,connectedLineLastStationNumber,connectedLineLastStationTitle,thisLineLastStationTitle,connectedLineBackgroundcolor,connectedLineTextColor;
    int stationNo = 0;
    public LinesAdapter(ArrayList<LinesC> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    public LinesAdapter(ArrayList<LinesC> arrayList) {
        this.arrayList = arrayList;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_lines_flat,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        context = parent.getContext();
        return recyclerViewHolder;
    }


    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final LinesC linesC = arrayList.get(position);
        final DBHelper dbHelper = new DBHelper(context);

        sharedLang = context.getSharedPreferences("MyPref",MODE_PRIVATE);
        lang_name = sharedLang.getString("langname","null");



        db = dbHelper.getReadableDatabase();
        Typeface samim = Typeface.createFromAsset(context.getAssets(), "fonts/samim.ttf");
        final Typeface irsans_light = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_light.ttf");



        Cursor cursorIds = db.rawQuery("SELECT * FROM "+ LinesContract.LinesEntry.TABLE_NAME+" WHERE "+ LinesContract.LinesEntry.STATUS+" = "+"0"+" AND +"+ LinesContract.LinesEntry.TITLE+" NOT LIKE '%(%' ORDER BY "+ LinesContract.LinesEntry.LINENUMBER+" ASC;",null);
        while (cursorIds.moveToNext())
        {
            cursorIds.moveToFirst();
            first = cursorIds.getString(3);
            cursorIds.moveToLast();
            last = cursorIds.getString(3);
            break;
        }
        cursorIds.close();

        if(String.valueOf(linesC.getLine_ID()).equals(first))
        {
            if(lang_name.equals("فارسي") || lang_name.equals("عربی")) {
                holder.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circleright_bottom, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
            }
            else{
               /* holder.title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.circleright_bottom, 0, 0, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);*/
                holder.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circleright_bottom, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
            }
        }

        else if(String.valueOf(linesC.getLine_ID()).equals(last))
        {
            if(lang_name.equals("فارسي") || lang_name.equals("عربی")) {
                holder.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circleright_top, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
            }
            else{
               /* holder.title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.circleright_top, 0, 0, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);*/
                holder.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circleright_top, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
            }
        }
        else{
            if(lang_name.equals("فارسي") || lang_name.equals("عربی")) {
                holder.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circleright, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
            }
            else{
                holder.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circleright, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
               /* holder.title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.circleright, 0, 0, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);*/
            }
        }
        // Dictionary


        Cursor cursorFrom = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.FROM+";",null);
        while (cursorFrom.moveToNext()){
            from = " "+cursorFrom.getString(0)+" ";
        }
        cursorFrom.close();
        Cursor cursorTo = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.TO+";",null);
        while (cursorTo.moveToNext()){
            to = " "+cursorTo.getString(0)+" ";
        }
        cursorTo.close();
        Cursor cursorStatus = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.STATUS+";",null);
        while (cursorStatus.moveToNext()){
            dic_status = cursorStatus.getString(0);
        }
        cursorStatus.close();
        Cursor cursorStatusActive = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.ACTIVE+";",null);
        while (cursorStatusActive.moveToNext()){
            active = cursorStatusActive.getString(0);
        }
        cursorStatusActive.close();
        Cursor cursorStatusInActive = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.INACTIVE+";",null);
        while (cursorStatusInActive.moveToNext()){
            inactive = cursorStatusInActive.getString(0);
        }
        cursorStatusInActive.close();
        Cursor cursorStatusUnderMaintenance = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.UNDER_CONSTRUCTION+";",null);
        while (cursorStatusUnderMaintenance.moveToNext()){
            un_main = cursorStatusUnderMaintenance.getString(0);
        }
        cursorStatusUnderMaintenance.close();
        Cursor cursorStationCount = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.NUMBER_OF_STATIONS+";",null);
        while (cursorStationCount.moveToNext()){
            station_count = cursorStationCount.getString(0);
        }
        cursorStationCount.close();
        Cursor cursorTwoPathWarn = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.CHOOSE_PATH+";",null);
        while (cursorTwoPathWarn.moveToNext()){
            twopathwarn = cursorTwoPathWarn.getString(0);
        }
        cursorTwoPathWarn.close();

        // />



        stationNo=0;






        //Getting the status of line
        Cursor statusCursor = db.rawQuery("SELECT * FROM "+ LinesContract.LinesEntry.TABLE_NAME+" WHERE "+ LinesContract.LinesEntry.LINE_ID+" = "+linesC.getLine_ID()+";",null);
        while (statusCursor.moveToNext()){
            status = statusCursor.getString(6);
        }
        if(status.equals("0")){
        }
        if(status.equals("1")) {
            holder.linearLayout.setEnabled(false);
        }
        if(status.equals("2")){
            holder.linearLayout.setEnabled(false);
        }


        holder.title.setText(String.valueOf(arrayList.get(position).getTitle()));
        holder.title.setTypeface(samim);

        String colorst = linesC.getBackgroundColor();
        String color1 = colorst.substring(1);


        holder.linearLayout.setBackgroundColor(Color.parseColor(linesC.getBackgroundColor()));

        // Multiend

        SharedPreferences sharedcity = context.getSharedPreferences("City", MODE_PRIVATE);
        String city = sharedcity.getString("city","null");
        AnalyticsApplication application = (AnalyticsApplication) context.getApplicationContext();
        mTracker = application.getDefaultTracker();
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Lines")
                .setAction(linesC.getTitle()+" ("+city+")")
                .build());

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Cursor c1 = db.rawQuery("SELECT * FROM "+ LinesContract.LinesEntry.TABLE_NAME+" WHERE "+ LinesContract.LinesEntry.LINE_ID+" = "+linesC.getLine_ID()+";",null);
                while (c1.moveToNext()){
                    multiend = c1.getString(10);
                    multienddetails = c1.getString(11);
                }

                if(multiend.equals("true")){
                    Dialog dialog = new Dialog(context);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.multiend_dialog);
                    Typeface sans_light = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_light.ttf");
                    TextView messageTextView = (TextView)dialog.findViewById(R.id.title);
                    LinearLayout layout = (LinearLayout) dialog.findViewById(R.id.layout);
                    layout.setBackgroundColor(Color.parseColor(linesC.getBackgroundColor()));
                    Button btn1 = (Button) dialog.findViewById(R.id.btn1);
                    Button btn2 = (Button) dialog.findViewById(R.id.btn2);
                    messageTextView.setTextColor(Color.parseColor(linesC.getTextColor()));
                    messageTextView.setText(twopathwarn);
                    btn1.setBackgroundColor(Color.parseColor(linesC.getTextColor()));
                    btn2.setBackgroundColor(Color.parseColor(linesC.getTextColor()));
                    btn1.setTypeface(irsans_light);
                    btn2.setTypeface(irsans_light);
                    messageTextView.setTypeface(sans_light);

                    dialog.show();
                    dialog.setCancelable(true);



                            String [] mu = multienddetails.split("-");
                            final String connectedLine = mu[0];
                    //Getting first station of line
                    Cursor cursor1 = db.rawQuery("SELECT * FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.STATION_ID+" = "+linesC.getFirstStationNumber()+";",null);
                    while (cursor1.moveToNext()){
                        fStation = cursor1.getString(6);
                    }
                    cursor1.close();
                    //Getting last station of line
                    Cursor cursor2 = db.rawQuery("SELECT * FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.STATION_ID+" = "+linesC.getLastSTationNumber()+";",null);
                    while (cursor2.moveToNext()){
                        lStation = cursor2.getString(6);
                    }
                    android.util.Log.d("wlhkdgjc","SELECT * FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.STATION_ID+" = "+linesC.getFirstStationNumber()+" AND "+StationsContract.StationsEntry.LINE_ID+" ="+connectedLine+";");
                    cursor2.close();
                            Cursor c2 = db.rawQuery("SELECT * FROM "+ LinesContract.LinesEntry.TABLE_NAME+" WHERE "+ LinesContract.LinesEntry.LINE_ID+" = "+connectedLine+";",null);
                            while (c2.moveToNext()){
                                connectedLineTitle = c2.getString(8);
                                connectedLineLastStationNumber = c2.getString(4);
                                connectedLineNumber = c2.getString(5);
                                connectedLineBackgroundcolor = c2.getString(0);
                                connectedLineTextColor = c2.getString(7);
                            }
                            Cursor c3 = db.rawQuery("SELECT * FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.STATION_ID+" = "+connectedLineLastStationNumber+";",null);
                            while (c3.moveToNext()){
                                connectedLineLastStationTitle = c3.getString(6);
                            }
                            Cursor c4 = db.rawQuery("SELECT * FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.STATION_ID+" = "+linesC.getLastSTationNumber()+";",null);
                            while (c4.moveToNext()){
                                thisLineLastStationTitle = c4.getString(6);
                            }
                    btn1.setText(fStation + to + thisLineLastStationTitle);
                    btn2.setText(fStation + to + connectedLineLastStationTitle);


                            btn1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    shared =context.getSharedPreferences("Line", MODE_PRIVATE);
                                    editor = shared.edit();
                                    editor.putString("line", String.valueOf(linesC.getLine_ID()));
                                    editor.putString("linenumber", String.valueOf(linesC.getLineNumber()));
                                    editor.putString("backgroundcolor", String.valueOf(linesC.getBackgroundColor()));
                                    editor.putString("textColor",String.valueOf(linesC.getTextColor()));
                                    editor.putString("lineTitle",String.valueOf(linesC.getTitle()));
                                    editor.commit();

                                    if(isDatabaseEmpty()==0) {
                                        BackgroundTaskStation backgroundTaskStation = new BackgroundTaskStation(context);
                                        backgroundTaskStation.execute();
                                    }
                                    else {
                                        context.startActivity(new Intent(context,Choose_station.class));
                                    }
                                }
                            });

                            btn2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    shared =context.getSharedPreferences("Line", MODE_PRIVATE);
                                    editor = shared.edit();
                                    editor.putString("line", String.valueOf(connectedLine));
                                    editor.putString("linenumber", String.valueOf(connectedLineNumber));
                                    editor.putString("backgroundcolor", String.valueOf(connectedLineBackgroundcolor));
                                    editor.putString("textColor",String.valueOf(connectedLineTextColor));
                                    editor.putString("lineTitle",fStation + ""+to+"" + connectedLineLastStationTitle);
                                    editor.commit();

                                    if(isDatabaseEmpty()==0) {
                                        BackgroundTaskStation backgroundTaskStation = new BackgroundTaskStation(context);
                                        backgroundTaskStation.execute();
                                    }
                                    else {
                                        context.startActivity(new Intent(context,Choose_station.class));
                                    }

                                }
                            });

                        }
                        //---------------------------------------------------------------------------
                        else{
                    shared =context.getSharedPreferences("Line", MODE_PRIVATE);
                    editor = shared.edit();
                    editor.putString("line", String.valueOf(linesC.getLine_ID()));
                    editor.putString("linenumber", String.valueOf(linesC.getLineNumber()));
                    editor.putString("backgroundcolor", String.valueOf(linesC.getBackgroundColor()));
                    editor.putString("textColor",String.valueOf(linesC.getTextColor()));
                    editor.putString("lineTitle",String.valueOf(linesC.getTitle()));
                    editor.commit();

                    if(isDatabaseEmpty()==0) {
                        BackgroundTaskStation backgroundTaskStation = new BackgroundTaskStation(context);
                        backgroundTaskStation.execute();
                    }
                    else {
                        context.startActivity(new Intent(context,Choose_station.class));
                    }

                }


            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder
    {
        TextView title;
        LinearLayout linearLayout;
        View line;
        RecyclerViewHolder(View view)
        {
            super(view);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.click_layout);
            title = (TextView) itemView.findViewById(R.id.title);
            line = (View) itemView.findViewById(R.id.line);
        }
    }
    public int isDatabaseEmpty()
    {
        String count = "SELECT count(*) FROM cities";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        mcursor.close();
        return icount;
    }
    public static void imageDownload(Context ctx, String url,ImageView imageView){
        Picasso.with(ctx)
                .load(Connection.images+url)
                .into(getTarget(url,ctx));
        Picasso.with(ctx)
                .load(Connection.images+url)
                .into(imageView);
    }

    private static Target getTarget(final String url, final Context context){
        Target target = new Target(){

            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        String URLL = url.replace("/","-");
                        File folder = new File(context.getFilesDir(),"Images");
                        if(!folder.exists()){
                            folder.mkdir();
                        }
                        File file = new File(folder,URLL);
                        try {
                            file.createNewFile();
                            FileOutputStream ostream = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 80, ostream);
                            ostream.flush();
                            ostream.close();

                        } catch (IOException e) {
                            Log.e("IOException", e.getLocalizedMessage());
                        }
                    }
                }).start();

            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        return target;
    }

}
