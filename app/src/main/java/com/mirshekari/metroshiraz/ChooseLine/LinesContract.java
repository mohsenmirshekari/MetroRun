package com.mirshekari.metroshiraz.ChooseLine;

/**
 * Created by Mohsen on 11/3/2017.
 */

public class LinesContract {
    public LinesContract() {}

    public static class LinesEntry
    {
        public static final String TABLE_NAME = "lines";
        public static final String BACKGROUND_COLOR = "backgroundColor";
        public static final String CITY_ID = "city_ID";
        public static final String FIRSTSTATIONNUMBER = "firstStationNumber";
        public static final String LINE_ID = "line_ID";
        public static final String LASTSTATIONNUMBER = "lastSTationNumber";
        public static final String LINENUMBER = "lineNumber";
        public static final String STATUS = "status";
        public static final String TEXTCOLOR = "textColor";
        public static final String TITLE = "title";
        public static final String IMAGE = "image";
        public static final String MULTIEND = "multiend";
        public static final String MULTIENDDETAIL = "multiend_detail";

    }



}
