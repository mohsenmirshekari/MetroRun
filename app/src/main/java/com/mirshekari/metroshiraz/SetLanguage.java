package com.mirshekari.metroshiraz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mirshekari.metroshiraz.ChooseCity.BackgroundTask;
import com.mirshekari.metroshiraz.ChooseLine.BackgroundTaskLine;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.BackgroundTaskKeys;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;

public class SetLanguage extends AppCompatActivity {
    SharedPreferences shared;
    String guid,lang_id,lang_name;
    ProgressBar pg;
    SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_language);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("");
        DBHelper dbHelper = new DBHelper(this);
        db = dbHelper.getReadableDatabase();

        pg = (ProgressBar) findViewById(R.id.pg);
        shared = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        guid = shared.getString("gu", "null");



        lang_id = getIntent().getStringExtra("lang_id");
        lang_name = getIntent().getStringExtra("lang_name");

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        final String url = Connection.portal+"/SetLanguage/"+guid+"/"+lang_id;
        StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {

                        if(response.length()==4) {
                            SharedPreferences savedlang = getApplicationContext().getSharedPreferences("LangPref", MODE_PRIVATE);
                            SharedPreferences.Editor editor = shared.edit();
                            editor.putString("langid", "1");
                            editor.putString("langname", "فارسي");
                            editor.commit();


                            SharedPreferences checkLang = getSharedPreferences("MyPref",MODE_PRIVATE);
                            String savedlanguageid  = checkLang.getString("langid","null");

                            if(savedlanguageid.equals(lang_id) && isKeysTableEmpty()>0)
                            {
                                Intent intent = new Intent(SetLanguage.this,Choose_city.class);
                                startActivity(intent);
                                finish();
                            }

                            if (isKeysTableEmpty()==0){
                                BackgroundTaskKeys backgroundTaskKeys = new BackgroundTaskKeys(SetLanguage.this);
                                backgroundTaskKeys.execute();
                            }


                        }
                        else
                        {
                        }


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {

                    }
                });



        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        strRequest.setRetryPolicy(policy);
        queue.add(strRequest);


    }
    public int isDatabaseEmpty()
    {
        String count = "SELECT count(*) FROM cities";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        mcursor.close();
        return icount;
    }
    public int isKeysTableEmpty()
    {
        String count = "SELECT count(*) FROM "+ KeysContract.KeysEntry.TABLE_NAME+";";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        mcursor.close();
        return icount;
    }
    public String getGID() {
        return guid;
    }
}
