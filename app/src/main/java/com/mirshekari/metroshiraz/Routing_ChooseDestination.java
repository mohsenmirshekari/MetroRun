package com.mirshekari.metroshiraz;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.mirshekari.metroshiraz.ChooseStation.StationsC;
import com.mirshekari.metroshiraz.ChooseStation.StationsContract;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.Routing.Source.Destination.DestinationAdapter;

import java.util.ArrayList;

public class Routing_ChooseDestination extends AppCompatActivity {
    EditText name;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public Context context;
    SQLiteDatabase db;
    DBHelper dbHelper;
    Cursor cursor,cursorG;
    ArrayList<StationsC> arrayList = new ArrayList<>();
    String search,enterPlaceNameDic,titleDic;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routing__choose_destination);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView topBar_title = (TextView) findViewById(R.id.topBar_title);
        setTitle("");
        setSupportActionBar(toolbar);
        Typeface sans_medium = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_medium.ttf");
        topBar_title.setTypeface(sans_medium);


        recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setHasFixedSize(true);
        dbHelper = new DBHelper(this);
        db = dbHelper.getReadableDatabase();

        name = (EditText) findViewById(R.id.station_name);
        name.setTypeface(sans_medium);
        name = (EditText) findViewById(R.id.station_name);
        name.setTypeface(sans_medium);
        // Dictionary
        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.ENTER_STATION_NAME+";",null);
        while (cursorG.moveToNext())
        {
            enterPlaceNameDic = cursorG.getString(0);
        }

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.SELECT_DESTINATION+";",null);
        while (cursorG.moveToNext())
        {
            titleDic = cursorG.getString(0);
        }
        topBar_title.setText(titleDic);
        cursorG.close();
        // ->

        name.setHint(enterPlaceNameDic);


        adapter = new DestinationAdapter(arrayList,getApplicationContext());

        cursor = db.rawQuery("SELECT * FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.STATUS+" = 0;",null);
        while(cursor.moveToNext())
        {
            StationsC linesC = new StationsC(cursor.getString(0),cursor.getInt(1),cursor.getInt(2),cursor.getString(3),cursor.getInt(4),cursor.getInt(5),cursor.getString(6),cursor.getString(7),cursor.getString(8));
            arrayList.add(linesC);
            adapter = new DestinationAdapter(arrayList,getApplicationContext());
        }

        adapter = new DestinationAdapter(arrayList,getApplicationContext());
        recyclerView.setAdapter(adapter);


        name.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                search = "%"+name.getText().toString()+"%";
                String sea = "'"+search+"'";

                arrayList.clear();

                cursor = db.rawQuery("SELECT * FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.TITLE + " LIKE "+sea+" AND "+ StationsContract.StationsEntry.STATUS+" = 0;",null);

                while(cursor.moveToNext())
                {
                    StationsC linesC = new StationsC(cursor.getString(0),cursor.getInt(1),cursor.getInt(2),cursor.getString(3),cursor.getInt(4),cursor.getInt(5),cursor.getString(6),cursor.getString(7),cursor.getString(8));
                    arrayList.add(linesC);
                    adapter = new DestinationAdapter(arrayList,getApplicationContext());
                }
                cursor.close();

                adapter = new DestinationAdapter(arrayList,getApplicationContext());
                recyclerView.setAdapter(adapter);


            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
