package com.mirshekari.metroshiraz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mirshekari.metroshiraz.ChooseLine.LinesContract;
import com.mirshekari.metroshiraz.ChooseStation.BackgroundTaskStation;
import com.mirshekari.metroshiraz.ChooseStation.StationsAdapter;
import com.mirshekari.metroshiraz.ChooseStation.StationsC;
import com.mirshekari.metroshiraz.ChooseStation.StationsContract;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.Schedule.ScheduleContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.events.OnBannerClickListener;
import ss.com.bannerslider.views.BannerSlider;

public class Choose_station extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public Context context;
    SQLiteDatabase sqLiteDatabaseLine;
    Cursor cursor;
    ArrayList<StationsC> arrayList = new ArrayList<>();
    SharedPreferences lineShares;
    long numberOfRows;
    SQLiteDatabase db;
    DBHelper dbHelper;
    String lineId;
    String commercial,commercial_id,lineTitle,multiDetails,masterLine;
    int iStation;
    Cursor cursorFacilities;
    BannerSlider bannerSlider;
    List<Banner> banners;
    Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_station);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView topBar_title = (TextView) findViewById(R.id.topBar_title);
        setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        lineShares = getSharedPreferences("Line",MODE_PRIVATE);
        Typeface irsans_medium = Typeface.createFromAsset(getAssets(), "fonts/iransansweb.ttf");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Choose station");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());


        bannerSlider = (BannerSlider) findViewById(R.id.banner_slider1);
        banners=new ArrayList<>();
        String bgcolor = lineShares.getString("backgroundcolor","03A678");
        String title= lineShares.getString("lineTitle","");
        lineId = lineShares.getString("line","1");
        recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setHasFixedSize(true);
        dbHelper = new DBHelper(this);
        db = dbHelper.getReadableDatabase();
        SQLiteDatabase sqLiteDatabase = dbHelper.getReadableDatabase();
        adapter = new StationsAdapter(arrayList,context);


        sqLiteDatabaseLine = dbHelper.getReadableDatabase();

        topBar_title.setText(title);
        topBar_title.setTypeface(irsans_medium);


        Cursor cursor = db.rawQuery("SELECT * FROM "+ LinesContract.LinesEntry.TABLE_NAME+" WHERE "+ LinesContract.LinesEntry.LINE_ID+" = "+lineId+";",null);
        while (cursor.moveToNext())
        {
            lineTitle = cursor.getString(8);
            multiDetails = cursor.getString(11);
        }
        cursor.close();

        if (lineTitle.contains("("))
        {
            Cursor cursor1 = db.rawQuery("SELECT * FROM "+ LinesContract.LinesEntry.TABLE_NAME+";",null);
            while (cursor1.moveToNext())
            {
                String det = cursor1.getString(11);
                String []detAr = det.split("-");



                if(detAr[0].equals(lineId))
                {
                    masterLine = cursor1.getString(3);
                    try {
                        iStation = Integer.valueOf(detAr[2]);
                    }catch (Exception e){}

                }
              //  break;
            }
            cursor1.close();


            Cursor cursor7 = sqLiteDatabase.rawQuery("SELECT * FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.LINE_ID+" = "+masterLine+" AND "+ StationsContract.StationsEntry.STATUS+" = 0 ORDER BY "+ StationsContract.StationsEntry.STATION_NUMBER+" ASC;",null);
            numberOfRows = DatabaseUtils.queryNumEntries(sqLiteDatabase, StationsContract.StationsEntry.TABLE_NAME);
            SharedPreferences shared6 = getSharedPreferences("LineInfo", MODE_PRIVATE);
            SharedPreferences.Editor editor = shared6.edit();
            editor.putString("Lenght",String.valueOf(numberOfRows));
            editor.commit();



            while(cursor7.moveToNext())
            {
                int sid = Integer.valueOf(cursor7.getString(1));
                android.util.Log.d("dqwljedqwq",String.valueOf(iStation));

                if(sid<iStation)
                {
                    StationsC linesC = new StationsC(cursor7.getString(0),cursor7.getInt(1),cursor7.getInt(2),cursor7.getString(3),cursor7.getInt(4),cursor7.getInt(5),cursor7.getString(6),cursor7.getString(7),cursor7.getString(8));
                    arrayList.add(linesC);
                    adapter = new StationsAdapter(arrayList,getApplicationContext());
                }
            }
            cursor7.close();

            Cursor slaveCusrsor = sqLiteDatabase.rawQuery("SELECT * FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.LINE_ID+" = "+lineId+" AND "+ StationsContract.StationsEntry.STATUS+" = 0 ORDER BY "+ StationsContract.StationsEntry.STATION_NUMBER+" ASC;",null);
            while (slaveCusrsor.moveToNext())
            {
                StationsC linesC = new StationsC(slaveCusrsor.getString(0),slaveCusrsor.getInt(1),slaveCusrsor.getInt(2),slaveCusrsor.getString(3),slaveCusrsor.getInt(4),slaveCusrsor.getInt(5),slaveCusrsor.getString(6),slaveCusrsor.getString(7),slaveCusrsor.getString(8));
                arrayList.add(linesC);
                adapter = new StationsAdapter(arrayList,getApplicationContext());
            }
            slaveCusrsor.close();
            adapter = new StationsAdapter(arrayList);
            recyclerView.setAdapter(adapter);
        }



        if(!lineTitle.contains("("))
        {
            Cursor cursor8 = sqLiteDatabase.rawQuery("SELECT * FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.LINE_ID+" = "+lineId+" AND "+ StationsContract.StationsEntry.STATUS+" = 0 ORDER BY "+ StationsContract.StationsEntry.STATION_NUMBER+" ASC;",null);
            numberOfRows = DatabaseUtils.queryNumEntries(sqLiteDatabase, StationsContract.StationsEntry.TABLE_NAME);
            SharedPreferences shared6 = getSharedPreferences("LineInfo", MODE_PRIVATE);
            SharedPreferences.Editor editor = shared6.edit();
            editor.putString("Lenght",String.valueOf(numberOfRows));
            editor.commit();

            while(cursor8.moveToNext())
            {
                StationsC linesC = new StationsC(cursor8.getString(0),cursor8.getInt(1),cursor8.getInt(2),cursor8.getString(3),cursor8.getInt(4),cursor8.getInt(5),cursor8.getString(6),cursor8.getString(7),cursor8.getString(8));
                arrayList.add(linesC);
                adapter = new StationsAdapter(arrayList,getApplicationContext());
            }
            cursor8.close();
            adapter = new StationsAdapter(arrayList);
            recyclerView.setAdapter(adapter);
        }


        // getting Commercials From Sqlite ---------------------------------------------------------
        HashMap<String,String> url_maps = new HashMap<String, String>();
        Cursor cursors = db.rawQuery("SELECT * FROM privateplaces;", null);
        while (cursors.moveToNext())
        {
            commercial = cursors.getString(0);
            char a = commercial.charAt(1); // This is The Index of Commercial String
            String stationsCommercial = "1";
            char b = stationsCommercial.charAt(0);
            if(a==b)
            {
                commercial_id = cursors.getString(2);
                Cursor cursorsPublic = db.rawQuery("SELECT * FROM publicplaces WHERE id = "+commercial_id+";", null);
                if(cursorsPublic.getCount() == 1){
                    bannerSlider.setVisibility(View.GONE);
                }
                while (cursorsPublic.moveToNext()){
                    String banner = cursorsPublic.getString(19);
                //    banners.add(new RemoteBanner(Connection.images+banner));
                    if(banner != null)
                    {
                        banners.add(new RemoteBanner(Connection.images+banner));
                    }
                }
            }
        }
        cursors.close();
        // getting Commercials From Sqlite ---------------------------------------------------------

        bannerSlider.setBanners(banners);
        bannerSlider.setOnBannerClickListener(new OnBannerClickListener() {
            @Override
            public void onClick(int position) {
               // Toast.makeText(getApplicationContext(), "Banner with position " + String.valueOf(position) + " clicked!", Toast.LENGTH_SHORT).show();
            }
        });

    }


    public int isDatabaseEmpty()
    {
        String count = "SELECT count(*) FROM "+ ScheduleContract.ScheduleEntry.TABLE_NAME_0;
        Cursor mcursor = sqLiteDatabaseLine.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        mcursor.close();
        return icount;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
