package com.mirshekari.metroshiraz;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.Fragments.Routing_chooseDestinationPrivatePlace;
import com.mirshekari.metroshiraz.Fragments.Routing_chooseDestinationPublicplace;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;

import java.util.ArrayList;
import java.util.List;

public class Routing_ChoosePlace_destination extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    SQLiteDatabase db;
    DBHelper dbHelper;
    Cursor cursorG;
    String generalDic,titleDic,businessDic;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routing__choose_place_destination);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView topBar_title = (TextView) findViewById(R.id.topBar_title);
        setTitle("");
        setSupportActionBar(toolbar);
        Typeface sans_medium = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_medium.ttf");
        topBar_title.setTypeface(sans_medium);
        dbHelper = new DBHelper(getApplicationContext());
        db = dbHelper.getReadableDatabase();


        // Dictionary
        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.PUBLIC+";",null);
        while (cursorG.moveToNext())
        {
            generalDic = cursorG.getString(0);
        }

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.BUSINESS+";",null);
        while (cursorG.moveToNext())
        {
            businessDic = cursorG.getString(0);
        }

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.SELECT_DESTINATION+";",null);
        while (cursorG.moveToNext())
        {
            titleDic = cursorG.getString(0);
        }
        topBar_title.setText(titleDic);

        cursorG.close();
        // ->
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager,businessDic,generalDic);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        changeTabsFont();

    }

    private void changeTabsFont() {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        Typeface fonts = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_light.ttf");
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(fonts);
                }
            }
        }
    }

    private void setupViewPager(ViewPager viewPager,String business,String general) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager() );
        adapter.addFragment(new Routing_chooseDestinationPrivatePlace(), business);
        adapter.addFragment(new Routing_chooseDestinationPublicplace(), general);

        viewPager.setAdapter(adapter);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
