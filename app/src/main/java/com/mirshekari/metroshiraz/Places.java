package com.mirshekari.metroshiraz;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.Place.Groups.GroupsAdapter;
import com.mirshekari.metroshiraz.Place.Groups.GroupsC;
import com.mirshekari.metroshiraz.Place.Groups.GroupsContract;
import com.mirshekari.metroshiraz.Place.Private.FragmentPrivatePlaces;
import com.mirshekari.metroshiraz.Place.Public.FragmentPublicPlaces;
import java.util.ArrayList;
import java.util.List;

public class Places extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    SQLiteDatabase db;
    DBHelper dbHelper;
    String generalDic,businessDic;
    Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dbHelper = new DBHelper(getApplicationContext());
        db = dbHelper.getReadableDatabase();

        // Dictionary
        Cursor cursorGeneral = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.PUBLIC+";",null);
        while (cursorGeneral.moveToNext())
        {
            generalDic = cursorGeneral.getString(0);
        }

        Cursor cursorBusiness = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.BUSINESS+";",null);
        while (cursorBusiness.moveToNext())
        {
            businessDic = cursorBusiness.getString(0);
        }
        // ->
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Places");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager,businessDic,generalDic);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        changeTabsFont();
    }

    private void changeTabsFont() {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        Typeface fonts = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_light.ttf");
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(fonts);
                }
            }
        }
    }

    private void setupViewPager(ViewPager viewPager,String business,String general) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentPrivatePlaces(), business);
        adapter.addFragment(new FragmentPublicPlaces(), general);
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
