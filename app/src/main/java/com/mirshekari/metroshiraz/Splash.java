package com.mirshekari.metroshiraz;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.Volley;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.api.GoogleApiClient;
import com.mirshekari.metroshiraz.ChooseLanguage.ListItem_Language;
import com.mirshekari.metroshiraz.ChooseLanguage.MyAdapter_Language;
import com.mirshekari.metroshiraz.ChooseLine.LinesContract;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.BackgroundTaskKeys;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


public class Splash extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGTH = 3000;
    Context context;
    String version,guid,lastlocation,dic,dic2;
    static final int REQUEST_LOCATION = 1;
    Location mLastLocation;
    GoogleApiClient mGoogleApiClient;
    SharedPreferences prefs = null;
    DBHelper dbHelper;
    SQLiteDatabase db;
    boolean fl;
    Tracker mTracker;
   public Dialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Splash");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        prefs = getSharedPreferences("com.mirshekari.metroshiraz", MODE_PRIVATE);

        dbHelper = new DBHelper(this);
        db = dbHelper.getWritableDatabase();


        // Dictionary
        Cursor cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.NO_INTERNET+";",null);
        while (cursorG.moveToNext()){
            dic = cursorG.getString(0);
        }
        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.SERVER_ERROR+";",null);
        while (cursorG.moveToNext()){
            dic2 = cursorG.getString(0);
        }
        cursorG.close();
        // ->

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }


     if(isDatabaseEmpty()==0 && isNetworkAvailable()==true)
     {
         BackgroundTaskKeys backgroundTaskKeys = new BackgroundTaskKeys(Splash.this);
         backgroundTaskKeys.execute();
         SharedPreferences savedlang = getApplicationContext().getSharedPreferences("LangPref", MODE_PRIVATE);
         SharedPreferences.Editor editor = savedlang.edit();
         editor.putString("langid", "1");
         editor.putString("langname", "فارسي");
         editor.commit();
         mTracker.send(new HitBuilders.EventBuilder()
                 .setCategory("Splash")
                 .setAction("New User")
                 .build());
     }
     else
     {
         SharedPreferences sharedcity = getSharedPreferences("City", MODE_PRIVATE);
         String city = sharedcity.getString("city","null");
         mTracker.send(new HitBuilders.EventBuilder()
                 .setCategory("Splash")
                 .setAction("App started ("+city+")")
                 .build());
         new Timer().schedule(new TimerTask(){
             public void run() {
                 runOnUiThread(new Runnable() {
                     public void run() {
                         Intent intent = (new Intent(Splash.this,Lines.class));
                         startActivity(intent);
                         finish();
                     }
                 });
             }
         }, 2700);

     }

     if(isDatabaseEmpty()==0&&isNetworkAvailable()==false)
     {
         showError(dic);
     }

     if (isNetworkAvailable()==false && isDatabaseEmpty()>0)
     {
         Intent intent = (new Intent(Splash.this,Lines.class));
         startActivity(intent);
         finish();
     }

    }

    public int isDatabaseEmpty()
    {
        String count = "SELECT count(*) FROM "+ LinesContract.LinesEntry.TABLE_NAME+";";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        return icount;
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void checkConnectivity() {
        if (isNetworkAvailable() == true) {

        } else if (isNetworkAvailable() == false) {

        }
    }


    public void showError(String errortext)
    {
        dialog = new Dialog(Splash.this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.error);
        Typeface samim = Typeface.createFromAsset(getAssets(), "fonts/samim.ttf");
        TextView errtext = (TextView)dialog.findViewById(R.id.err_text);
        Button retrybtn = (Button)dialog.findViewById(R.id.retry_btn);
        retrybtn.setTypeface(samim);
        errtext.setText(errortext);
        errtext.setTypeface(samim);
        retrybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkConnectivity();
            }
        });
        dialog.show();
    }



    public void getLanguages()
    {
        if (isDatabaseEmpty() == 0) {
            final Dialog dialog = new Dialog(Splash.this);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.layout_recyclerview);
            final RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recyclerview);
            final List<ListItem_Language>  listItems = new ArrayList<>();
            SharedPreferences shared = getSharedPreferences("MyPref", MODE_PRIVATE);
            String guid = shared.getString("gu","null");
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            final String URL_DATA = Connection.portal+"/GetLanguages"+"/"+guid+"/"+"0";
            final StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_DATA, new Response.Listener<String>() {
                @Override
                public void onResponse(String s) {
                    try {
                        JSONArray jsonarray = new JSONArray(s);
                        for(int i=0;i<jsonarray.length();i++)
                        {
                            JSONObject o = jsonarray.getJSONObject(i);
                            ListItem_Language item = new ListItem_Language(o.getString("Flag"),o.getString("Id"),o.getString("IsRTL"),o.getString("NameCode"),o.getString("Title"));
                            listItems.add(item);
                        }
                        RecyclerView.Adapter adapter = new MyAdapter_Language(listItems,getApplicationContext());
                        recyclerView.setAdapter(adapter);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            showError(dic2);
                        }
                    })
            {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    // params.put("","");
                    return params;
                }
            };
            MySingleton.getInstance(getApplicationContext()).addToRequestque(stringRequest);
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);
            dialog.show();
        }
        else{
            Intent intent = new Intent(Splash.this,Lines.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }
    }

    public boolean isFirstLaunch()
    {
        if (prefs.getBoolean("firstrun", true)) {
            fl = true;
            prefs.edit().putBoolean("firstrun", false).commit();
        }
        return fl;
    }

    public String getAppVersion()
    {
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }

    public String getAndroidVersion()
    {
        int a = android.os.Build.VERSION.SDK_INT;
        String sys_version = String.valueOf(a);
        return sys_version;
    }

    public void getID()
    {
        /**------------------------------------------------------------------------------------------------**/

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url = Connection.portal+"/ClientCheck/nEw-ClienT/"+getAppVersion()+"/"+getAndroidVersion()+"/Location";

        StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        guid = response.substring(1,response.length()-1);

                        SharedPreferences shared = getSharedPreferences("MyPref", MODE_PRIVATE);
                        SharedPreferences.Editor editor = shared.edit();
                        editor.putString("gu", guid);
                    //    editor.putString("latestupdate",array[1]);
                      //  editor.putString("latestmessage",array[2]);
                        editor.commit();
                        getLanguages();

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        // create a Dialog component
                        final Dialog dialog = new Dialog(Splash.this);

                        dialog.setCancelable(false);
                        //tell the Dialog to use the dialog.xml as it's layout description
                        dialog.setContentView(R.layout.error);
                        Typeface samim = Typeface.createFromAsset(getAssets(), "fonts/samim.ttf");
                        TextView errtext = (TextView)dialog.findViewById(R.id.err_text);
                        Button retrybtn = (Button)dialog.findViewById(R.id.retry_btn);
                        retrybtn.setTypeface(samim);
                        errtext.setText(dic2);
                        errtext.setTypeface(samim);
                        retrybtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                checkConnectivity();
                            }
                        });
                        dialog.show();
                    }
                });


        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        strRequest.setRetryPolicy(policy);
        queue.add(strRequest);
/**-------------------------------------------------------------------------------------------------**/
    }

}
