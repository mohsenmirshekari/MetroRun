package com.mirshekari.metroshiraz.Routing.Source;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mirshekari.metroshiraz.ChooseLine.LinesContract;
import com.mirshekari.metroshiraz.ChooseStation.StationsC;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.R;
import com.mirshekari.metroshiraz.Routing_search;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;
import static com.mirshekari.metroshiraz.Fragments.Schedule.Schedule_saturday.faToEn;

/**
 * Created by Mohsen Mirshekari on 2/26/2018.
 */

public class SuggestionAdapter extends RecyclerView.Adapter <SuggestionAdapter.RecyclerViewHolder>  {
    SharedPreferences shared,stationshared,sharess;
    SharedPreferences.Editor editor;
    ArrayList<SuggestionC> arrayList = new ArrayList<>();
    Context context;
    String bgcolor;
    SQLiteDatabase db;
    String colors;
    String numberofStation;
    SharedPreferences sharedLang;
    String distanceDic,intervalDic,stationsNoDic,linesBetweenDic,selectPathDic,kmDic,minuteDic,lang_name,stationDic;

    public SuggestionAdapter(ArrayList<SuggestionC> arrayList, Context context,String destination,String source,String stationNo) {
        this.arrayList = arrayList;
        this.context = context;
        this.numberofStation = stationNo;
    }

    @Override
    public SuggestionAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_routing_suggestion,parent,false);
        SuggestionAdapter.RecyclerViewHolder recyclerViewHolder = new SuggestionAdapter.RecyclerViewHolder(view);
        context = parent.getContext();
        return recyclerViewHolder;
    }


    @Override
    public void onBindViewHolder(final SuggestionAdapter.RecyclerViewHolder holder, int position) {
        final SuggestionC stationsC = arrayList.get(position);
        DBHelper dbHelper = new DBHelper(context);

        db = dbHelper.getReadableDatabase();

        Typeface samim = Typeface.createFromAsset(context.getAssets(), "fonts/samim.ttf");
        Typeface irsans_light = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_light.ttf");
        Typeface irsans_medium = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_medium.ttf");

        shared = context.getSharedPreferences("Line", MODE_PRIVATE);
        bgcolor = shared.getString("backgroundcolor","null");

        sharedLang = context.getSharedPreferences("MyPref",MODE_PRIVATE);
        lang_name = sharedLang.getString("langname","null");

        // Dictionary
        Cursor cursorDistance = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.DISTANCE+";",null);
        while (cursorDistance.moveToNext()){
            distanceDic = cursorDistance.getString(0);
        }
        cursorDistance.close();
        Cursor cursorInterval = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.INTERVAL+";",null);
        while (cursorInterval.moveToNext()){
            intervalDic = cursorInterval.getString(0);
        }
        cursorInterval.close();
        Cursor cursorStationsNoDic = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.STATIONS_BETWEEN_PATHS+";",null);
        while (cursorStationsNoDic.moveToNext()){
            stationsNoDic = cursorStationsNoDic.getString(0);
        }
        cursorStationsNoDic.close();
        Cursor cursorLinesBetween = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.LINES_BETWEEN_PATHS+";",null);
        while (cursorLinesBetween.moveToNext()){
            linesBetweenDic = cursorLinesBetween.getString(0);
        }
        cursorLinesBetween.close();
        Cursor cursorSelectPathDic = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.SELECT_SUGGESTED_PATH+";",null);
        while (cursorSelectPathDic.moveToNext()){
            selectPathDic = cursorSelectPathDic.getString(0);
        }
        cursorSelectPathDic.close();

        Cursor cursorKMDic = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.KM+";",null);
        while (cursorKMDic.moveToNext()){
            kmDic = cursorKMDic.getString(0);
        }
        cursorKMDic.close();

        Cursor cursorMinuteDic = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.MINUTE+";",null);
        while (cursorMinuteDic.moveToNext()){
            minuteDic = cursorMinuteDic.getString(0);
        }
        cursorMinuteDic.close();
        Cursor cursorStation = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.STATION+";",null);
        while (cursorStation.moveToNext()){
            stationDic = cursorStation.getString(0);
        }
        cursorStation.close();
        // ->
        float distanceBymeter = Float.valueOf(stationsC.getCost());
        int distanceBymeterInt = (int) distanceBymeter;
        int distanceBykm = distanceBymeterInt / 1000;
        String distance = distanceDic;
        holder.distance.setText(distance+faToEn(String.valueOf(distanceBykm))+ " کیلومتر ");
        holder.distance.setTypeface(irsans_light);
        holder.routeNo.setVisibility(View.GONE);

        if(lang_name.equals("فارسي" ) || lang_name.equals("عربی"))
        {
            holder.distance.setText(distance+faToEn(String.valueOf(distanceBykm))+ kmDic);
            holder.distance.setGravity(Gravity.CENTER_VERTICAL|Gravity.RIGHT);
        }
        else
        {
            holder.distance.setText(distance+String.valueOf(distanceBykm)+ kmDic);
            holder.distance.setGravity(Gravity.CENTER_VERTICAL|Gravity.LEFT);
        }


        String stationdNumberText = stationsNoDic;
        if(lang_name.equals("فارسي") || lang_name.equals("عربی"))
        {
            holder.stationsno.setText(stationdNumberText+faToEn(stationsC.getStations())+" "+stationDic);
            holder.stationsno.setTypeface(irsans_light);
            holder.stationsno.setGravity(Gravity.CENTER_VERTICAL|Gravity.RIGHT);
        }
        else
        {
            stationDic = Character.toLowerCase(stationDic.charAt(0)) + stationDic.substring(1);
            holder.stationsno.setText(stationdNumberText+stationsC.getStations()+" "+stationDic+"(s)");
            holder.stationsno.setTypeface(irsans_light);
            holder.stationsno.setGravity(Gravity.CENTER_VERTICAL|Gravity.LEFT);
        }

        String time = stationsC.getTime();

        if(lang_name.equals("فارسي")||lang_name.equals("عربی"))
        {
            holder.time.setText(faToEn(time)+minuteDic);
            holder.time.setTypeface(irsans_light);
            holder.time.setGravity(Gravity.CENTER_VERTICAL|Gravity.RIGHT);
        }
        else
        {
            holder.time.setText(time+minuteDic);
            holder.time.setTypeface(irsans_light);
            holder.time.setGravity(Gravity.CENTER_VERTICAL|Gravity.LEFT);
        }

        String linesArray = stationsC.getLines();
        linesArray = linesArray.substring(1,linesArray.length()-1);
       // linesArray = linesArray.replace(","," ← ");
        if(lang_name.equals("فارسي")||lang_name.equals("عربی"))
        {
            linesArray = linesArray.replace(","," ← ");
            linesArray = faToEn(linesArray);
        }
        else {
            linesArray = linesArray.replace(","," → ");
        }
        String lines = linesBetweenDic;
        holder.lines.setText(lines+linesArray);
        holder.lines.setTypeface(irsans_light);
        if(lang_name.equals("فارسي")||lang_name.equals("عربی"))
        {
            holder.lines.setGravity(Gravity.CENTER_VERTICAL|Gravity.RIGHT);
        }
        else
        {
            holder.lines.setGravity(Gravity.CENTER_VERTICAL|Gravity.LEFT);
        }

        holder.select.setTypeface(irsans_medium);
        holder.select.setText(selectPathDic);
        holder.select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = context.getSharedPreferences("RoutingInfo",MODE_PRIVATE);
                SharedPreferences.Editor sheditor = sharedPreferences.edit();
                sheditor.putString("selectedCost",stationsC.getCost());
                sheditor.commit();
                context.startActivity(new Intent(context, Routing_search.class));
            }
        });
    }
    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder
    {
        TextView routeNo,distance,stationsno,time,lines;
        LinearLayout linearLayout;
        Button select;
        RecyclerViewHolder(View view)
        {
            super(view);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.click_layout);
            routeNo = (TextView) itemView.findViewById(R.id.routeNo);
            distance = (TextView) itemView.findViewById(R.id.distance);
            stationsno = (TextView) itemView.findViewById(R.id.stationsNo);
            time = (TextView) itemView.findViewById(R.id.time);
            lines = (TextView) itemView.findViewById(R.id.lines);
            select = (Button) itemView.findViewById(R.id.select);
        }
    }
}
