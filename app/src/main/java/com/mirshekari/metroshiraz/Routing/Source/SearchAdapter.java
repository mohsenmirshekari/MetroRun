package com.mirshekari.metroshiraz.Routing.Source;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mirshekari.metroshiraz.ChooseLine.LinesContract;
import com.mirshekari.metroshiraz.ChooseStation.StationsC;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.R;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mohsen Mirshekari on 2/20/2018.
 */

public class SearchAdapter extends RecyclerView.Adapter <SearchAdapter.RecyclerViewHolder>  {
    SharedPreferences shared,stationshared,sharess;
    SharedPreferences.Editor editor;
    ArrayList<StationsC> arrayList = new ArrayList<>();
    Context context;
    String bgcolor;
    SQLiteDatabase db;
    String colors;
    public SearchAdapter(ArrayList<StationsC> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_lines,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        context = parent.getContext();
        return recyclerViewHolder;
    }
    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final StationsC stationsC = arrayList.get(position);
        DBHelper dbHelper = new DBHelper(context);

        db = dbHelper.getReadableDatabase();

        Typeface samim = Typeface.createFromAsset(context.getAssets(), "fonts/samim.ttf");
        Typeface irsans_light = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_light.ttf");
        shared = context.getSharedPreferences("Line", MODE_PRIVATE);
        bgcolor = shared.getString("backgroundcolor","null");

        Cursor cursor = db.rawQuery("SELECT * FROM "+ LinesContract.LinesEntry.TABLE_NAME+" WHERE "+ LinesContract.LinesEntry.LINE_ID+" = "+stationsC.getLineid(),null);
        while (cursor.moveToNext()){
            colors = cursor.getString(0);
        }

        holder.title.setText(stationsC.getTitle());
        holder.title.setTypeface(irsans_light);
        String colorst = colors;
        int color = Color.parseColor(colorst);
        holder.linearLayout.setBackgroundColor(color);




    }
    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder
    {
        TextView title;
        LinearLayout linearLayout;
        RecyclerViewHolder(View view)
        {
            super(view);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.click_layout);
            title = (TextView) itemView.findViewById(R.id.title);
        }
    }
}
