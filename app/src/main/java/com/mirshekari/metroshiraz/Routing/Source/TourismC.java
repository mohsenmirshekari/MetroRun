package com.mirshekari.metroshiraz.Routing.Source;

/**
 * Created by Mohsen Mirshekari on 3/9/2018.
 */

public class TourismC {
    private String cost;
    private String lines;
    private String stations;
    private String time;

    public TourismC(String cost, String lines, String stations, String time) {
        this.cost = cost;
        this.lines = lines;
        this.stations = stations;
        this.time = time;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getLines() {
        return lines;
    }

    public void setLines(String lines) {
        this.lines = lines;
    }

    public String getStations() {
        return stations;
    }

    public void setStations(String stations) {
        this.stations = stations;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
