package com.mirshekari.metroshiraz.Routing.Source.TourismQuee;

/**
 * Created by Mohsen Mirshekari on 3/2/2018.
 */

public class QueeC {
    private String id;
    private String station_id;
    private String title;

    public QueeC(String id, String station_id, String title) {
        this.id = id;
        this.station_id = station_id;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStation_id() {
        return station_id;
    }

    public void setStation_id(String station_id) {
        this.station_id = station_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
