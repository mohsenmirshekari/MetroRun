package com.mirshekari.metroshiraz.Routing.Source.TourismQuee;

/**
 * Created by Mohsen Mirshekari on 3/2/2018.
 */

public class QueeContract {
    public static class QueeEntry{
        public static final String TABLE_NAME = "quee";
        public static final String ID = "id";
        public static final String STATION_ID = "station_id";
        public static final String TITLE = "title";
    }
}
