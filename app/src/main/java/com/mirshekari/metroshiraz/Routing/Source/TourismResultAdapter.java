package com.mirshekari.metroshiraz.Routing.Source;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mirshekari.metroshiraz.ChooseLine.LinesContract;
import com.mirshekari.metroshiraz.ChooseStation.StationsContract;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.R;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;
import static com.mirshekari.metroshiraz.Fragments.Schedule.Schedule_saturday.faToEn;

/**
 * Created by Mohsen Mirshekari on 3/9/2018.
 */

public class TourismResultAdapter extends  RecyclerView.Adapter <TourismResultAdapter.RecyclerViewHolder>  {
        SharedPreferences shared,stationshared,sharess;
        SharedPreferences.Editor editor;
        ArrayList<TourismC> arrayList = new ArrayList<>();
        Context context;
        String bgcolor;
        SQLiteDatabase db;
        String colors;
        String numberofStation;
        SharedPreferences sharedLang;
        String lastIndex,firstIndex,lineId,stationName,stationColor;
        String distanceDic,intervalDic,stationsNoDic,linesBetweenDic,selectPathDic,kmDic,minuteDic,lang_name,stationDic;

    public TourismResultAdapter(ArrayList<TourismC> arrayList, Context context, String lastIndex, String firstIndex) {
        this.arrayList = arrayList;
        this.context = context;
        this.lastIndex = lastIndex;
        this.firstIndex = firstIndex;
    }

    @Override
    public TourismResultAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_lines_ltr,parent,false);
        TourismResultAdapter.RecyclerViewHolder recyclerViewHolder = new TourismResultAdapter.RecyclerViewHolder(view);
        context = parent.getContext();
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(final TourismResultAdapter.RecyclerViewHolder holder, int position) {
        final TourismC tourismC = arrayList.get(position);
        DBHelper dbHelper = new DBHelper(context);

        db = dbHelper.getReadableDatabase();

        Typeface samim = Typeface.createFromAsset(context.getAssets(), "fonts/samim.ttf");
        Typeface irsans_light = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_light.ttf");
        Typeface irsans_medium = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_medium.ttf");

        shared = context.getSharedPreferences("Line", MODE_PRIVATE);
        bgcolor = shared.getString("backgroundcolor","null");

        Cursor cc = db.rawQuery("SELECT "+ StationsContract.StationsEntry.TITLE+" FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.STATION_ID+" = "+tourismC.getStations()+";",null);
        while (cc.moveToNext())
        {
            stationName = cc.getString(0);
        }

        Cursor gg = db.rawQuery("SELECT "+ StationsContract.StationsEntry.LINE_ID+" FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.STATION_ID+" = "+tourismC.getStations()+";",null);
        while (gg.moveToNext())
        {
            lineId = gg.getString(0);
        }

       Cursor jsj = db.rawQuery("SELECT "+ LinesContract.LinesEntry.BACKGROUND_COLOR+" FROM "+ LinesContract.LinesEntry.TABLE_NAME+" WHERE "+ LinesContract.LinesEntry.LINE_ID+" = "+ lineId+";",null);

        android.util.Log.d("asdwabhkwf","SELECT "+ LinesContract.LinesEntry.BACKGROUND_COLOR+" FROM "+ LinesContract.LinesEntry.TABLE_NAME+" WHERE "+ LinesContract.LinesEntry.LINE_ID+" = "+ lineId+";");
        while (jsj.moveToNext())
        {
            stationColor = jsj.getString(0);
        }

        holder.linearLayout.setBackgroundColor(Color.parseColor(stationColor));


        sharedLang = context.getSharedPreferences("MyPref",MODE_PRIVATE);
        lang_name = sharedLang.getString("langname","null");

        holder.title.setTypeface(irsans_light);

        android.util.Log.d("adwgbawwa",lastIndex);

        if(lastIndex.equals("true"))
        {

            if(lang_name.equals("فارسي") || lang_name.equals("عربی")) {
                holder.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circleright, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
                holder.title.setText(stationName+" (پایان این مسیر) ");
            }
            else{
                holder.title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.circleright_bottom, 0, 0, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
                holder.title.setText(stationName);
            }
        }

        else{

            if(lang_name.equals("فارسي") || lang_name.equals("عربی")) {
                holder.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circleright, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
                holder.title.setText(stationName);
            }
            else{
                holder.title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.circleright_bottom, 0, 0, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
                holder.title.setText(stationName);
            }
        }




        }
    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder
    {
        TextView title;
        LinearLayout linearLayout;
        ImageView inter,statusImage;
        RecyclerViewHolder(View view)
        {
            super(view);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.click_layout);
            title = (TextView) itemView.findViewById(R.id.title);
            inter = (ImageView) itemView.findViewById(R.id.inter);
            statusImage = (ImageView) itemView.findViewById(R.id.status);
        }
    }
}
