package com.mirshekari.metroshiraz.Routing.Source;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mirshekari.metroshiraz.Connection;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.Lines;
import com.mirshekari.metroshiraz.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TourismResult extends AppCompatActivity {

    String array,gu_id,city_id,searchDic;
    RecyclerView recyclerView;
    LinearLayout pg;
    ArrayList<TourismC> arrayList = new ArrayList<>();
    TourismResultAdapter adapter;
    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tourism_result);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView topBar_title = (TextView) findViewById(R.id.topBar_title);
        setTitle("");
        setSupportActionBar(toolbar);
        Typeface irsans_light = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_light.ttf");
        Typeface irsans_medium = Typeface.createFromAsset(getAssets(), "fonts/iransansweb.ttf");

        pg = (LinearLayout) findViewById(R.id.pg);
        pg.setVisibility(View.VISIBLE);

        dbHelper = new DBHelper(getApplicationContext());
        sqLiteDatabase = dbHelper.getWritableDatabase();

        //Dictionary

        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.SEARCH+";",null);

        while (cursor.moveToNext()){
            searchDic = cursor.getString(2);
        }
        // ->

        topBar_title.setText(searchDic);
        topBar_title.setTypeface(irsans_medium);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String s = getIntent().getStringExtra("array");
        setTitle("");
        SharedPreferences sharedPreferences = getSharedPreferences("TourismResult",MODE_PRIVATE);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setHasFixedSize(true);

        array = sharedPreferences.getString("array","null");
        array = array.substring(1,array.length()-1);
        array = array.replace(",","-");
        array = array.replace(" ","");

        SharedPreferences shared = getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences sharedcity = getSharedPreferences("City", MODE_PRIVATE);
        gu_id = shared.getString("gu","null");
        city_id = sharedcity.getString("city","null");

        String fArr [] = array.split("-");

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        final String url = Connection.portal + "MetroTour/" + gu_id + "/" + city_id + "/" + fArr[0]+"/"+array;
        StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            arrayList.clear();
                            for(int i=0;i<jsonArray.length();i++)
                            {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                JSONArray jsonArray1 = jsonObject.getJSONArray("Routs");
                                JSONObject jsonObject1 = jsonArray1.getJSONObject(0);
                                String time = jsonObject1.getString("Time");
                                String cost = jsonObject1.getString("Cost");
                                JSONArray stationsArray = jsonObject1.getJSONArray("Stations");
                                int lastindex = stationsArray.length()-1;
                                JSONArray linesArray = jsonObject1.getJSONArray("Lines");
                                for(int j=0;j<stationsArray.length();j++)
                                {
                                        TourismC tourismC = new TourismC(cost,String.valueOf(linesArray),stationsArray.getString(j),time);
                                        arrayList.add(tourismC);
                                        adapter = new TourismResultAdapter(arrayList,getApplicationContext(),"false","false");
                                        recyclerView.setAdapter(adapter);
                                }
                                pg.setVisibility(View.GONE);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });


        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        strRequest.setRetryPolicy(policy);
        queue.add(strRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
