package com.mirshekari.metroshiraz.Routing.Source;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mirshekari.metroshiraz.ChooseLine.LinesContract;
import com.mirshekari.metroshiraz.ChooseStation.StationsC;
import com.mirshekari.metroshiraz.ChooseStation.StationsContract;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.Lines;
import com.mirshekari.metroshiraz.R;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mohsen Mirshekari on 2/20/2018.
 */

public class SourceAdapter extends RecyclerView.Adapter <SourceAdapter.RecyclerViewHolder> {
    SharedPreferences shared,stationshared,sharess;
    SharedPreferences.Editor editor;
    ArrayList<StationsC> arrayList = new ArrayList<>();
    Context context;
    String bgcolor;
    SQLiteDatabase db;
    String colors;
    Cursor cursorG;
    SharedPreferences sharedLang;
    String lang_name;
    String lastId;

    public SourceAdapter(ArrayList<StationsC> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_lines,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        context = parent.getContext();
        return recyclerViewHolder;
    }
    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final StationsC stationsC = arrayList.get(position);
        DBHelper dbHelper = new DBHelper(context);

        db = dbHelper.getReadableDatabase();


        sharedLang = context.getSharedPreferences("MyPref",MODE_PRIVATE);
        lang_name = sharedLang.getString("langname","null");


        Typeface samim = Typeface.createFromAsset(context.getAssets(), "fonts/samim.ttf");
        Typeface irsans_light = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_light.ttf");
        shared = context.getSharedPreferences("Line", MODE_PRIVATE);
        bgcolor = shared.getString("backgroundcolor","null");

        cursorG = db.rawQuery("SELECT * FROM "+ LinesContract.LinesEntry.TABLE_NAME+" WHERE "+ LinesContract.LinesEntry.LINE_ID+" = "+stationsC.getLineid(),null);
        while (cursorG.moveToNext()){
            colors = cursorG.getString(0);
        }

        holder.title.setTypeface(irsans_light);
        String colorst = colors;
        int color = Color.parseColor(colorst);
        holder.linearLayout.setBackgroundColor(color);

        Drawable img = context.getResources().getDrawable( R.drawable.circleright );

        holder.title.setText(stationsC.getTitle());

        if(lang_name.equals("فارسي") || lang_name.equals("عربی"))
        {
            holder.title.setCompoundDrawablesWithIntrinsicBounds( null, null, img, null);
            holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
        }
        else
        {
            holder.title.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null);
            holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
        }




        if(stationsC.getStationnumber()==1)
        {

            if(lang_name.equals("فارسي") || lang_name.equals("عربی")) {
                holder.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circleright_bottom, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
            }
            else{
                holder.title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.circleright_bottom, 0, 0, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
            }
        }

        cursorG =  db.rawQuery("SELECT "+ StationsContract.StationsEntry.STATION_NUMBER+" FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.LINE_ID+" = "+stationsC.getLineid()+";",null);
        while (cursorG.moveToNext())
        {
            cursorG.moveToLast();
            lastId = cursorG.getString(0);
        }
        if(stationsC.getStationnumber()==Integer.valueOf(lastId))
        {
            if(lang_name.equals("فارسي") || lang_name.equals("عربی")) {
                holder.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circleright_top, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
            }
            else{
                holder.title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.circleright_top, 0, 0, 0);
                holder.title.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
            }
        }

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                SharedPreferences shared = context.getSharedPreferences("RoutingInfo", MODE_PRIVATE);
                SharedPreferences.Editor editor = shared.edit();
                editor.putString("source_id", String.valueOf(stationsC.getStationid()));
                editor.putString("source_name", String.valueOf(stationsC.getTitle()));

                editor.commit();

                ((Activity)context).finish();

            }
        });

    }
    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder
    {
        TextView title;
        LinearLayout linearLayout;
        RecyclerViewHolder(View view)
        {
            super(view);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.click_layout);
            title = (TextView) itemView.findViewById(R.id.title);
        }
    }
}
