package com.mirshekari.metroshiraz.Routing.Source;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mirshekari.metroshiraz.Connection;
import com.mirshekari.metroshiraz.Fragments.Routing_chooseSourcePublicplace;
import com.mirshekari.metroshiraz.Place.Public.PublicPlacesC;
import com.mirshekari.metroshiraz.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;
import static com.mirshekari.metroshiraz.Fragments.Schedule.Schedule_saturday.faToEn;

/**
 * Created by Mohsen Mirshekari on 2/21/2018.
 */

public class SourcePlaceAdapter  extends RecyclerView.Adapter <SourcePlaceAdapter.RecyclerViewHolder>{
    ArrayList<PublicPlacesC> arrayList = new ArrayList<>();
    Context context;

    public SourcePlaceAdapter(ArrayList<PublicPlacesC> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }
    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_publicplaces,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        context = parent.getContext();
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final PublicPlacesC publicPlacesC = arrayList.get(position);

        Typeface samim = Typeface.createFromAsset(context.getAssets(), "fonts/samim.ttf");
        Typeface irsans_light = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_light.ttf");

        holder.title.setText(publicPlacesC.getTitle());
        holder.title.setTypeface(irsans_light);

        holder.title.setTextColor(Color.parseColor(publicPlacesC.getTextcolorid()));

      //  holder.click.setBackgroundColor(Color.parseColor(publicPlacesC.getBackgroundcolor()));

        Picasso.with(context).load(Connection.images+publicPlacesC.getIcon())
                .fit()
                .centerCrop()
                .into(holder.icon);


        holder.click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences shared = context.getSharedPreferences("RoutingInfo", MODE_PRIVATE);
                SharedPreferences.Editor editor = shared.edit();
                editor.putString("source_id", String.valueOf(publicPlacesC.getStationid()));
                editor.putString("source_placename",String.valueOf(publicPlacesC.getTitle()));
                editor.commit();

                android.util.Log.d("SALHASLDIH",String.valueOf(publicPlacesC.getStationid()));
                ((Activity)context).finish();

            }
        });


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class RecyclerViewHolder extends  RecyclerView.ViewHolder
    {
        TextView title;
        CircleImageView icon;
        LinearLayout click;
        View line;
        RecyclerViewHolder(View view)
        {
            super(view);
            title = (TextView) itemView.findViewById(R.id.title);
            icon = (CircleImageView) itemView.findViewById(R.id.icon);
            click = (LinearLayout) itemView.findViewById(R.id.click_layout);
        }
    }

}

