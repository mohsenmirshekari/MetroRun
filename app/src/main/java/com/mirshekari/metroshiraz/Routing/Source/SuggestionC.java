package com.mirshekari.metroshiraz.Routing.Source;

/**
 * Created by Mohsen Mirshekari on 2/26/2018.
 */

public class SuggestionC {
    private String cost;
    private String stations;
    private String lines;
    private String time;

    public SuggestionC(String cost, String stations, String lines, String time) {
        this.cost = cost;
        this.stations = stations;
        this.lines = lines;
        this.time = time;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getStations() {
        return stations;
    }

    public void setStations(String stations) {
        this.stations = stations;
    }

    public String getLines() {
        return lines;
    }

    public void setLines(String lines) {
        this.lines = lines;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
