package com.mirshekari.metroshiraz.ChooseCity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import com.mirshekari.metroshiraz.Choose_city;
import com.mirshekari.metroshiraz.Connection;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mohsen on 10/22/2017.
 */

public class BackgroundTask extends AsyncTask<Void,Void,Void> {
    ProgressDialog progressDialog;
    Context ctx;
    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;
    SharedPreferences shared;
    String gu_id;
    Dialog dialog;
    String background,mBackground,map;
    String mMap = "";
    String please_wait;

    public BackgroundTask(Context ctx)
    {
        this.ctx = ctx;
    }

    @Override
    protected void onPreExecute() {
        shared = ctx.getSharedPreferences("MyPref", MODE_PRIVATE);
        gu_id = shared.getString("gu","null");
        dbHelper = new DBHelper(ctx);
        sqLiteDatabase = dbHelper.getReadableDatabase();

        //Dictionary

        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.PLEASE_WAIT+";",null);

        while (cursor.moveToNext()){
            please_wait = cursor.getString(2);
        }
        // ->
        dialog = new Dialog(ctx);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.loading);
        Typeface samim = Typeface.createFromAsset(ctx.getAssets(), "fonts/samim.ttf");
        TextView messageTextView = (TextView)dialog.findViewById(R.id.message);
        messageTextView.setTypeface(samim);
        messageTextView.setText(please_wait);
        messageTextView.setTypeface(samim);
        dialog.show();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            String json_url = Connection.portal+"/getCities.php";
            URL url = new URL(json_url);
            HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while((line=bufferedReader.readLine())!=null)
            {
                stringBuilder.append(line+"\n");
                Thread.sleep(5000);
            }
            httpURLConnection.disconnect();
            String json_data = stringBuilder.toString().trim();
            JSONArray jsonarray = new JSONArray(json_data);
            DBHelper dbHelper = new DBHelper(ctx);
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            int count = 0;
             while(count<jsonarray.length())
             {
                 JSONObject o = jsonarray.getJSONObject(count);
                 count++;
                 dbHelper.putInformation(o.getInt("city_id"),o.getString("city_color"),o.getString("city_background"),o.getString("photo_map"),o.getString("city_title"),o.getString("city_background"),db);
             }
            dbHelper.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
             return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }
    @Override
    protected void onPostExecute(Void aVoid) {
        dialog.dismiss();
        ctx.startActivity(new Intent(ctx,Choose_city.class));
    }
}
