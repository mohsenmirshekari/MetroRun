package com.mirshekari.metroshiraz.ChooseCity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mirshekari.metroshiraz.AnalyticsApplication;
import com.mirshekari.metroshiraz.ChooseLine.BackgroundTaskLine;
import com.mirshekari.metroshiraz.ChooseStation.BackgroundTaskStation;
import com.mirshekari.metroshiraz.Choose_city;
import com.mirshekari.metroshiraz.Connection;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.Download;
import com.mirshekari.metroshiraz.Lines;
import com.mirshekari.metroshiraz.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;


/**
 * Created by Mohsen on 10/23/2017.
 */

public class CitiesAdapter extends RecyclerView.Adapter <CitiesAdapter.RecyclerViewHolder> {
    SharedPreferences shared,sharedcity;
    SharedPreferences.Editor editor;
    ArrayList<Cities> arrayList = new ArrayList<>();
    Context context;
    SQLiteDatabase db;
    String gu_id,city_id;
    Tracker mTracker;
    public CitiesAdapter(ArrayList<Cities> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }
    public CitiesAdapter(ArrayList<Cities> arrayList)
    {
        this.arrayList = arrayList;
    }
    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_choosecity,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        context = parent.getContext();
        return recyclerViewHolder;
    }



    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final Cities cities = arrayList.get(position);

        DBHelper dbHelper = new DBHelper(context);

        db = dbHelper.getReadableDatabase();
        shared = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        sharedcity = context.getSharedPreferences("City", MODE_PRIVATE);
        gu_id = shared.getString("gu","null");
        city_id = sharedcity.getString("city","null");
        AnalyticsApplication application = (AnalyticsApplication) context.getApplicationContext();
        mTracker = application.getDefaultTracker();
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Cities")
                .setAction(cities.getPhoto())
                .build());

        holder.cityname.setText(cities.getPhoto());

        String colorst = "#"+cities.getTitle();

        //int color = Color.parseColor(colorst);
     //   String base64String = cities.getBackgroundColor();
    //    byte[] decodedString = Base64.decode(base64String, Base64.DEFAULT);
    //    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
     //   Drawable drawable = new BitmapDrawable(decodedByte);
     //   holder.bg.setBackground(drawable);

            holder.tint.setBackgroundColor(Color.parseColor(cities.getTitle()));


        Typeface samim = Typeface.createFromAsset(context.getAssets(), "fonts/samim.ttf");
          holder.cityname.setTypeface(samim);

          Picasso.with(context).load(Connection.images+cities.getBackgroundColor())
                .fit()
                .centerCrop()
                  .into(holder.bg);

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shared =context.getSharedPreferences("City", MODE_PRIVATE);
                editor = shared.edit();
                editor.putString("city", String.valueOf(cities.getId()));
                editor.putString("cityName",cities.getPhoto());
                editor.commit();
                context.startActivity(new Intent(context,Download.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder
    {
        TextView cityname;
        ImageView bg,tint;
        RelativeLayout relativeLayout;
        RecyclerViewHolder(View view)
        {
            super(view);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.click_layout);
            cityname = (TextView) itemView.findViewById(R.id.cityname);
            bg = (ImageView) itemView.findViewById(R.id.bg_image);
            tint = (ImageView) itemView.findViewById(R.id.tint);
        }
    }

    public int isDatabaseEmpty()
    {
        String count = "SELECT count(*) FROM lines";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        return icount;
    }
}
