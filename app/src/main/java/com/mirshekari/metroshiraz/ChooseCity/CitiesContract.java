package com.mirshekari.metroshiraz.ChooseCity;

/**
 * Created by Mohsen on 10/22/2017.
 */

public class CitiesContract {


    public CitiesContract(){}

    public static class CitiesEntry
    {
        public static final String TABLE_NAME = "cities";
        public static final String CITY_ID = "city_id";
        public static final String CITY_TITLE = "city_title";
        public static final String CITY_BACKGROUND = "city_background";
        public static final String CITY_COLOR = "city_color";
        public static final String PHOTO_LINK = "photo_link";
        public static final String PHOTO_MAP = "photo_map";
    }
}
