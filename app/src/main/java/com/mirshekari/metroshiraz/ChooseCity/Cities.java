package com.mirshekari.metroshiraz.ChooseCity;

/**
 * Created by Mohsen on 10/23/2017.
 */

public class Cities {
    private int id;
    private String BackgroundColor;
    private String Photo;
    private String Title;
    private String PhotoLink;

    public Cities(int id, String backgroundColor, String photo, String title, String photoLink) {
        this.id = id;
        BackgroundColor = backgroundColor;
        Photo = photo;
        Title = title;
        PhotoLink = photoLink;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBackgroundColor() {
        return BackgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        BackgroundColor = backgroundColor;
    }

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String photo) {
        Photo = photo;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getPhotoLink() {
        return PhotoLink;
    }

    public void setPhotoLink(String photoLink) {
        PhotoLink = photoLink;
    }
}
