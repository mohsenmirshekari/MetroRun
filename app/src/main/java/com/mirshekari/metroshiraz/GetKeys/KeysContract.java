package com.mirshekari.metroshiraz.GetKeys;

/**
 * Created by Mohsen Mirshekari on 2/24/2018.
 */

public class KeysContract {
    public KeysContract() {}
    public static class KeysEntry {
        public static final String TABLE_NAME = "keys";
        public static final String KEY_ID = "key_id";
        public static final String KEY = "key";
        public static final String TEXT = "text";
        public static final String ID = "id";
    }
}
