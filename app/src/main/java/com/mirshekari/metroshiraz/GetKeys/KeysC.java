package com.mirshekari.metroshiraz.GetKeys;

/**
 * Created by Mohsen Mirshekari on 2/24/2018.
 */

public class KeysC {
    private String key;
    private String text;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


}
