package com.mirshekari.metroshiraz.GetKeys;

/**
 * Created by Mohsen Mirshekari on 2/24/2018.
 */

public class DictionaryContract {
    public static final int DESTINATION = 2; //فاصله
    public static final int CITY_INFO_DOWNLOADING = 3; //در حال دریافت اطلاعات شهر
    public static final int DOWNLOADING_STATIONS = 4; // در حال دریافت زمانبندی ایستگاه ها
    public static final int FROM = 5; // از
    public static final int TO = 6; // به
    public static final int NUMBER_OF_STATIONS = 7;  // تعداد ایستگاه ها
    public static final int STATUS = 8; // وضعیت
    public static final int INACTIVE = 9; // غیرفعال
    public static final int UNDER_CONSTRUCTION = 10; //در دست ساخت
    public static final int CHOOSE_PATH = 11;  //این خط دارای دو مسیر میباشد. لطفاً مسیر خود را انتخاب کنید.
    public static final int LIST_OF_STATION = 12; // لیست ایستگاه‌ها
    public static final int ROUTING = 13; // مسیریابی
    public static final int ONLINE_MAP = 14; // نقشه آنلاین
    public static final int NEAREST_STATION_TO_ME = 15; // نزدیکترین ایستگاه به من
    public static final int SETTING = 16; // تنظیمات
    public static final int ARMO_TITLE = 17; // گروه آرمـــو
    public static final int BY_STATION = 18; // بر اساس ایستگاه
    public static final int BY_PLACE = 19; // بر اساس مکان
    public static final int ENTER_STATION_NAME = 20; // نام ایستگاه را وارد کنید...
    public static final int STATION_NAME = 21; // نام ایستگاه
    public static final int PLACE_NAME = 22; // نام مکان
    public static final int ENTER_PLACE_NAME = 23; // نام مکان را وارد کنید...
    public static final int BUSINESS = 24; // تجاری
    public static final int PUBLIC = 25; // عمومی
    public static final int ROUTE_SEARCH = 26; // جستحوی مسیر
    public static final int STATION = 27; // ایستگاه
    public static final int LINE = 28; // خط
    public static final int UPDATE = 29; // بروزرسانی اطلاعات
    public static final int CHANGE_LANGUAGE = 30; // تغییر زبان
    public static final int CHANGE_CITY = 31; // تغییر شهر
    public static final int REMOVE_WARNING_1 = 32; // تمامی اطلاعات شهر
    public static final int REMOVE_WARNING_2 = 33; // از گوشی شما حذف و با اطلاعات جدید جایگزین میشود. \n  از انجام این عملیات مطمئن هستید؟
    public static final int YES = 34; // بله
    public static final int NO = 35; // نه
    public static final int TELEGRAM_CHANNEL = 36; // کانال تلگرام
    public static final int SCHEDULE = 37; // زمانبندی حرکت
    public static final int STATIONARY_ROUTING = 38; // مسیریابی ایستگاهی
    public static final int ROAD_ROUTING = 39; // مسیریابی نقشه‌ای
    public static final int NEARBY_PLACES = 40; // اماکن اطراف
    public static final int SATURDAY = 41; // شنبه
    public static final int SUNDAY = 42; // یکشنبه
    public static final int MONDAY = 43; // دوشنبه
    public static final int TUESDAY = 44; // سه شنبه
    public static final int WEDNESDAY = 45; // چهارشنبه
    public static final int THURSDAY = 46; // پنجشنبه
    public static final int FRIDAY = 47; // جمعه
    public static final int LASTSTAION_OF = 48; // ایستگاه پایانی
    public static final int TRAIN_NEXT_MOVE = 49; // زمان حرکت بعدی قطار:
    public static final int FACILITIES = 50; // امکانات
    public static final int PLEASE_WAIT = 51; // لطفا منتظر بمانید
    public static final int ACTIVE = 52; // فعال
    public static final int LINES_LIST = 53;  // لیست خطوط
    public static final int CHOOSE_STATION = 54; // انتخاب ایستگاه
    public static final int CHOOSE_PLACE = 55; // انتخاب مکان
    public static final int TOWARDS_1 = 56; // به سمت
    public static final int TOWARDS_2 = 57; // به سمت
    public static final int AND = 58; // و
    public static final int MINUTE = 59; // دقیقه
    public static final int CHOOSE_SUGGESTED_PATH = 60; // یکی از مسیرهای پیشنهادی را انتخاب کنید
    public static final int DISTANCE = 61; // فاصله
    public static final int INTERVAL = 62; // زمان سفر
    public static final int STATIONS_BETWEEN_PATHS = 63; // تعداد ایستگاه های بین مسیر
    public static final int LINES_BETWEEN_PATHS = 64; // خطوط بین مبدا و مقصد
    public static final int SELECT_SUGGESTED_PATH = 65; // انتخاب مسیر
    public static final int KM = 66; //کیلومتر
    public static final int TOURISM = 68; // تورییستی
    public static final int TOURISM_PRESENT_1 = 69; // توضیحات 1
    public static final int TOURISM_PRESENT_2 = 70; // توضیحات 2
    public static final int SHOW_LIST = 71; // نمایش لیست]
    public static final int RECIEVING = 83; // در حال دریافت اطلاعات
    public static final int SEARCH_BY = 84; // جستجو بر اساس
    public static final int SEARCH = 85; // جستجو
    public static final int CONTINUE = 86; // ادامه
    public static final int ACCEPT_N_CONTINUE = 87; // قبول و ادامه
    public static final int EDIT = 88; // ویرایش
    public static final int NOPLACEFOUND = 89; // مکانی یافت نشد.
    public static final int NEW_UPDATE_AVAILABLE = 90; // بسته بروزرسانی جدید در دسترس است
    public static final int MUST_UPDATE = 91;
    public static final int UPDATE_PROGRESS = 92; // در حال بروزرسانی
    public static final int LATER = 93; // بعداً
    public static final int NOTHINGFOUND = 95; // موردی یافت نشد
    public static final int SEND_COMMENT = 96; // ارسال نظر
    public static final int NOTIFICATIONS = 97; // اظلاعاعیه ها
    public static final int WRITE_COMMENT = 98; // نظر خود را بنویسید...
    public static final int SEND = 99; // ارسال
    public static final int SUBMITTED = 100;
    public static final int SUBMIT_FAIL = 101;
    public static final int SELECT_SOURCE = 102; // انتخاب مبدا
    public static final int SELECT_DESTINATION = 103; // انتخاب مقصد
    public static final int CHOOSE_FIRST = 104; // انتخاب مقصد
    public static final int DISMISS = 105; // بازگشت
    public static final int TELEPHONE = 106; // شماره تماس
    public static final int TELEPHONES = 107;
    public static final int DESCRIPTION = 108;
    public static final int FAVORITE_LOCATIONS = 109; // آدرس های منتخب
    public static final int LINES_OF = 110; // خطوط شهر
    public static final int NEAREST_STATION = 111; // نزدیکترین ایستگاه
    public static final int SAVE = 112; // ذخیره
    public static final int PLACE_TITLE = 113; // نام مکان انتخاب شده...
    public static final int SELECT_AS_ORIGIN = 114; // انتخاب به عنوان مبدا
    public static final int SELECT_AS_DESTINATION = 115; // انتخاب به عنوان مقصد
    public static final int NO_INTERNET = 116; // باید به اینترنت متصل باشید
    public static final int SERVER_ERROR = 117; // خطا در برقراری ارتباط با سرور
    public static final int MAPROUTING = 118; // مسیریابی


}
