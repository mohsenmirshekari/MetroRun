package com.mirshekari.metroshiraz.GetKeys;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.widget.TextView;

import com.mirshekari.metroshiraz.ChooseCity.BackgroundTask;
import com.mirshekari.metroshiraz.Connection;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.R;
import com.mirshekari.metroshiraz.SetLanguage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mohsen Mirshekari on 2/24/2018.
 */

public class BackgroundTaskKeys extends AsyncTask<Void,Void,Void> {
    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;
    Context ctx;
    SharedPreferences shared,sharedcity,sharedline,cityName;
    String gu_id,station_no,city_name;
    int route,numrows;
    Dialog dialog;
    String version;
    String lang_id,lang_name;
    public BackgroundTaskKeys(Context ctx) {
        this.ctx = ctx;
    }

    @Override
    protected void onPreExecute() {
        dbHelper = new DBHelper(ctx);
        sqLiteDatabase = dbHelper.getWritableDatabase();
        shared = ctx.getSharedPreferences("MyPref", MODE_PRIVATE);
        sharedcity = ctx.getSharedPreferences("Station", MODE_PRIVATE);
        cityName = ctx.getSharedPreferences("City", MODE_PRIVATE);
        city_name = cityName.getString("cityName","شما");
        gu_id = shared.getString("gu","null");
        station_no = sharedcity.getString("stid","null");
        sharedline = ctx.getSharedPreferences("LineInfo",MODE_PRIVATE);


        dialog = new Dialog(ctx);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.loading);
        Typeface samim = Typeface.createFromAsset(ctx.getAssets(), "fonts/samim.ttf");
        TextView messageTextView = (TextView)dialog.findViewById(R.id.message);
        messageTextView.setTypeface(samim);
      //  messageTextView.setText("در حال دریافت keys ");
        messageTextView.setTypeface(samim);
        dialog.show();


    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            String json_url = Connection.portal+"/getKeys.php";
            URL url = new URL(json_url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line + "\n");
                Thread.sleep(5000);
            }
            httpURLConnection.disconnect();
            String json_data = stringBuilder.toString().trim();

            DBHelper dbHelper = new DBHelper(ctx);
            SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();



            JSONArray jsonarray = new JSONArray(json_data);

            int count = 0;
            while (count < jsonarray.length()) {
                JSONObject o = jsonarray.getJSONObject(count);
                count++;
                dbHelper.putKeysInformation(o.getString("key_id"),o.getString("key_name"),o.getString("text"),sqLiteDatabase);
            }
            dbHelper.close();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        dialog.dismiss();
        BackgroundTask backgroundTask = new BackgroundTask(ctx);
        backgroundTask.execute();

    }
    public String getAppVersion()
    {
        try {
            PackageInfo pInfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }
}
