package com.mirshekari.metroshiraz.GetKeys;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mirshekari.metroshiraz.Connection;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.Place.Public.PublicPlacesC;
import com.mirshekari.metroshiraz.R;
import com.mirshekari.metroshiraz.Routing.Source.TourismQuee.QueeContract;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.mirshekari.metroshiraz.Fragments.Schedule.Schedule_saturday.faToEn;

/**
 * Created by Mohsen Mirshekari on 3/2/2018.
 */

public class TourismPlacesAdapter extends RecyclerView.Adapter <TourismPlacesAdapter.RecyclerViewHolder> {
    ArrayList<PublicPlacesC> arrayList = new ArrayList<>();
    Context context;
    public TourismPlacesAdapter(ArrayList<PublicPlacesC> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_place_multiselect,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        context = parent.getContext();
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final PublicPlacesC publicPlacesC = arrayList.get(position);

        Typeface samim = Typeface.createFromAsset(context.getAssets(), "fonts/samim.ttf");
        Typeface irsans_light = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_light.ttf");

        holder.title.setText(publicPlacesC.getTitle());
        holder.title.setTypeface(irsans_light);


        holder.title.setTextColor(Color.parseColor(publicPlacesC.getTextcolorid()));
        //   holder.click.setBackgroundColor(Color.parseColor("#"+publicPlacesC.getBackgroundcolor()));

        // Load Logo from FileSystem
        String URLL = publicPlacesC.getIcon().replace("/","-");
        File folder = new File(context.getFilesDir(),"Images");
        File file = new File(folder,URLL);

        if(file.exists())
        {
            Picasso.with(context).load(file).into(holder.icon);
        }
        else{
            String img_url = publicPlacesC.getIcon().replace("-","/");
            imageDownload(context,img_url,holder.icon);
        }


        final DBHelper dbHelper = new DBHelper(context);
        final SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();

        Cursor checkCursor = sqLiteDatabase.rawQuery("SELECT "+ QueeContract.QueeEntry.ID +" FROM "+ QueeContract.QueeEntry.TABLE_NAME,null);
        while (checkCursor.moveToNext()){
            if(checkCursor.getString(0).equals(String.valueOf(publicPlacesC.getId()))){
                holder.checkBox.setChecked(true);
                holder.click.setBackgroundColor(Color.parseColor("#B327ae60"));
            }
        }


        holder.click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.checkBox.isChecked()==true){
                    holder.click.setBackgroundColor(context.getResources().getColor(R.color.mainColor));
                    holder.checkBox.setChecked(false);
                }
                else{
                    holder.click.setBackgroundColor(Color.parseColor("#B327ae60"));
                    holder.checkBox.setChecked(true);
                }

            }
        });



        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    holder.click.setBackgroundColor(Color.parseColor("#B327ae60"));
                    dbHelper.putQueeInformation(String.valueOf(publicPlacesC.getId()),String.valueOf(publicPlacesC.getStationid()),publicPlacesC.getTitle(),sqLiteDatabase);
                }
                else{
                    holder.click.setBackgroundColor(context.getResources().getColor(R.color.mainColor));
                    dbHelper.deleteQueeInformation(String.valueOf(publicPlacesC.getId()),sqLiteDatabase);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class RecyclerViewHolder extends  RecyclerView.ViewHolder
    {
        TextView title;
        CircleImageView icon;
        LinearLayout click;
        CheckBox checkBox;
        RecyclerViewHolder(View view)
        {
            super(view);
            title = (TextView) itemView.findViewById(R.id.title);
            icon = (CircleImageView) itemView.findViewById(R.id.icon);
            click = (LinearLayout) itemView.findViewById(R.id.click_layout);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
        }
    }
    public static void imageDownload(Context ctx, String url,ImageView imageView){
        Picasso.with(ctx)
                .load(Connection.images+url)
                .into(getTarget(url,ctx));
        Picasso.with(ctx)
                .load(Connection.images+url)
                .into(imageView);
    }

    private static Target getTarget(final String url, final Context context){
        Target target = new Target(){

            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        String URLL = url.replace("/","-");
                        File folder = new File(context.getFilesDir(),"Images");
                        if(!folder.exists()){
                            folder.mkdir();
                        }
                        File file = new File(folder,URLL);
                        try {
                            file.createNewFile();
                            FileOutputStream ostream = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 80, ostream);
                            ostream.flush();
                            ostream.close();

                        } catch (IOException e) {
                            Log.e("IOException", e.getLocalizedMessage());
                        }
                    }
                }).start();

            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        return target;
    }
}

