package com.mirshekari.metroshiraz;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;
import static com.mirshekari.metroshiraz.Fragments.Schedule.Schedule_saturday.faToEn;

/**
 * Created by Mohsen Mirshekari on 3/9/2018.
 */

public class MessagesAdapter extends RecyclerView.Adapter <MessagesAdapter.RecyclerViewHolder>  {
    ArrayList<MessagesC> arrayList = new ArrayList<>();
    Context context;
String lang_name;
    public MessagesAdapter(ArrayList<MessagesC> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }


    @Override
    public MessagesAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_messages,parent,false);
        MessagesAdapter.RecyclerViewHolder recyclerViewHolder = new MessagesAdapter.RecyclerViewHolder(view);
        context = parent.getContext();
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(final MessagesAdapter.RecyclerViewHolder holder, int position) {
        final MessagesC messagesC = arrayList.get(position);
        DBHelper dbHelper = new DBHelper(context);

        SharedPreferences sharedLang = context.getSharedPreferences("MyPref",MODE_PRIVATE);
        lang_name = sharedLang.getString("langname","null");


        Typeface samim = Typeface.createFromAsset(context.getAssets(), "fonts/samim.ttf");
        Typeface irsans_light = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_light.ttf");
        Typeface irsans_medium = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_medium.ttf");

        holder.text.setText(messagesC.getText());
        holder.title.setText(messagesC.getTitle());
        holder.text.setTypeface(irsans_light);
        holder.title.setTypeface(irsans_medium);

        if(lang_name.equals("فارسي")){
            holder.title.setGravity(Gravity.CENTER_VERTICAL|Gravity.RIGHT);
            holder.text.setGravity(Gravity.CENTER_VERTICAL|Gravity.RIGHT);
        }
        else{
           /* holder.title.setGravity(Gravity.CENTER_VERTICAL|Gravity.LEFT);
            holder.text.setGravity(Gravity.CENTER_VERTICAL|Gravity.RIGHT);*/
            holder.title.setGravity(Gravity.CENTER_VERTICAL|Gravity.RIGHT);
            holder.text.setGravity(Gravity.CENTER_VERTICAL|Gravity.RIGHT);
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(context);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.dialog_message_pop);
                TextView txt_title = (TextView)dialog.findViewById(R.id.title);
                TextView txt_body = (TextView)dialog.findViewById(R.id.body);
                Typeface irsans_bold = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_bold.ttf");
                Typeface irsans_medium = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_medium.ttf");
                txt_title.setTypeface(irsans_bold);
                txt_body.setTypeface(irsans_medium);
                txt_body.setText(messagesC.getText());
                txt_title.setText(messagesC.getTitle());
                dialog.show();
                dialog.setCancelable(true);
            }
        });

    }
    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder
    {
        TextView text,title;
        CardView cardView;
        RecyclerViewHolder(View view)
        {
            super(view);
            text = (TextView) itemView.findViewById(R.id.text);
           cardView = (CardView) itemView.findViewById(R.id.card_view);
           title = (TextView) itemView.findViewById(R.id.title);
        }
    }
}
