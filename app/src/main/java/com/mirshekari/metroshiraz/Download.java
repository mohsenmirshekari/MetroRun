package com.mirshekari.metroshiraz;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mirshekari.metroshiraz.Aboutus.AboutUsContract;
import com.mirshekari.metroshiraz.ChooseCity.BackgroundTask;
import com.mirshekari.metroshiraz.ChooseLine.LinesContract;
import com.mirshekari.metroshiraz.ChooseStation.StationsContract;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.Facilities.FacilitiesContract;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.Place.Groups.GroupsContract;
import com.mirshekari.metroshiraz.Place.Private.PrivatePlacesContract;
import com.mirshekari.metroshiraz.Place.Public.PublicPlacesContract;
import com.mirshekari.metroshiraz.Schedule.BackgroundTaskSchedule;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class Download extends AppCompatActivity {
    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;
    SharedPreferences shared,sharedcity,cityName ;
    String gu_id,city_id,city_name,version;
    ArrayList<String> arr,stationsArr,publicplacesArr,privateplaceArr,groupsArr,aboutusArr,facilitiesArr;
    String icon,background,slide1,slide2,slide3,slide4,banner,instagram,telegram,dictionary;
    TextView text;
    String downloadDic;
    Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dbHelper = new DBHelper(Download.this);
        sqLiteDatabase = dbHelper.getReadableDatabase();
        //Dictionary
        Typeface irsans_light = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_light.ttf");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        text = (TextView) findViewById(R.id.text);
        text.setTypeface(irsans_light);

        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.CITY_INFO_DOWNLOADING+";",null);

        while (cursor.moveToNext()){
            dictionary = cursor.getString(2);
        }

        cursor.close();
        // ->
        shared = getSharedPreferences("MyPref", MODE_PRIVATE);
        sharedcity = getSharedPreferences("City", MODE_PRIVATE);
        gu_id = shared.getString("gu","null");
        city_id = sharedcity.getString("city","null");
        cityName = getSharedPreferences("City", MODE_PRIVATE);
        city_name = cityName.getString("cityName","شما");

        Cursor dic = sqLiteDatabase.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.DOWNLOADING_STATIONS+";",null);
        while (dic.moveToNext())
        {
            downloadDic = dic.getString(0);
        }
        dic.close();
        text.setText(downloadDic);

        arr = new ArrayList<String>();
        stationsArr = new ArrayList<String>();
        publicplacesArr = new ArrayList<String>();
        privateplaceArr = new ArrayList<String>();
        groupsArr = new ArrayList<String>();
        aboutusArr = new ArrayList<String>();
        facilitiesArr = new ArrayList<String>();

    /*    getLines();
        getStations();
        getPublicPlaces();
        getPrivatePlaces();
        getGroups();
        getAboutUs();
        getFacilities();
        getLatestUpdate();*/


        try{
            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
            final String url = Connection.portal+"getLines.php?city="+city_id;

            StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            final DBHelper dbHelper = new DBHelper(Download.this);
                            final SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();

                            try {
                                JSONArray jsonArray = new JSONArray(response);
                                final Cursor checkCursor = sqLiteDatabase.rawQuery("SELECT * FROM "+ LinesContract.LinesEntry.TABLE_NAME+";",null);

                                for(int i=0;i<jsonArray.length();i++)
                                {
                                    //Getting Lines
                                    final JSONObject o = jsonArray.getJSONObject(i);
                                    dbHelper.putLineInformation(o.getString("backgroundColor"), o.getString("city_ID"),
                                            o.getInt("firstStationNumber"), o.getInt("line_ID"), o.getInt("lastSTationNumber")
                                            , o.getInt("lineNumber"), o.getInt("status"), o.getString("textColor"), o.getString("title"), o.getString("image"),o.getString("multiend"),o.getString("multiend_detail"), sqLiteDatabase);
                                    while (checkCursor.moveToNext())
                                    {
                                        arr.add(checkCursor.getString(3));
                                    }
                                    checkCursor.close();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    });


            int socketTimeout = 30000; // 30 seconds. You can change it
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

            strRequest.setRetryPolicy(policy);
            queue.add(strRequest);
            // -------------------------------------------------------------------------------------

            // Getting Stations
            RequestQueue queue2 = Volley.newRequestQueue(getApplicationContext());
            String json_url_station = Connection.portal + "getStations.php?city="+city_id;
            StringRequest strRequest2 = new StringRequest(Request.Method.GET, json_url_station,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONArray jsonarrayStation = new JSONArray(response);
                                final Cursor checkCursor2 = sqLiteDatabase.rawQuery("SELECT * FROM "+ StationsContract.StationsEntry.TABLE_NAME+";",null);
                                for(int i=0;i<jsonarrayStation.length();i++)
                                {
                                    JSONObject os = jsonarrayStation.getJSONObject(i);

                                    while (checkCursor2.moveToNext())
                                    {
                                        stationsArr.add(checkCursor2.getString(1));
                                    }
                                    checkCursor2.close();
                                    String m = os.getString("station_id");

                                    if(!stationsArr.contains(m))
                                    {
                                        dbHelper.putStationsInformation(os.getString("address"), os.getInt("station_id"), os.getInt("line_id"),
                                                os.getString("location"), os.getInt("station_number"),
                                                os.getInt("status"), os.getString("title"), os.getString("facilities"),os.getString("iscross"), sqLiteDatabase);
                                    }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
            int socketTimeout2 = 30000; // 30 seconds. You can change it
            RetryPolicy policy2 = new DefaultRetryPolicy(socketTimeout2,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

            strRequest2.setRetryPolicy(policy2);
            queue2.add(strRequest2);

            // -------------------------------------------------------------------------------------


            final RequestQueue queue3 = Volley.newRequestQueue(getApplicationContext());
            final String url3 = Connection.portal+"/getPublicplaces.php?city="+city_id;
            final Cursor check = sqLiteDatabase.rawQuery("SELECT * FROM "+ PublicPlacesContract.PublicPlacesEntry.TABLE_NAME+";",null);

            StringRequest strRequest3 = new StringRequest(Request.Method.GET, url3,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONArray jsonarrayPubPlace = new JSONArray(response);
                                for(int i=0;i<jsonarrayPubPlace.length();i++)
                                {
                                    JSONObject opp = jsonarrayPubPlace.getJSONObject(i);
                                    JSONArray photo = opp.getJSONArray("photolinks");

                                    telegram = opp.getString("instagram");
                                    instagram = opp.getString("telegram");

                                    icon = opp.getString("icon");
                                    background = opp.getString("background_photo");
                                    slide1 = opp.getString("slideshow_1");
                                    slide2 = opp.getString("slideshow_2");
                                    slide3 = opp.getString("slideshow_3");
                                    slide4 = opp.getString("slideshow_4");
                                    banner = opp.getString("banner");


                                    while (check.moveToNext())
                                    {
                                        publicplacesArr.add(check.getString(2));
                                    }
                                    check.close();
                                    String h = opp.getString("id");
                                    if(!publicplacesArr.contains(h))
                                    {
                                        dbHelper.putPublicPlacesInformation(opp.getString("backgroundcolor"), opp.getString("description"), opp.getInt("id"), opp.getString("isprivate"), opp.getString("phone"), opp.getString("photolinks"), opp.getString("phone"), opp.getInt("placegroupeid"), instagram, telegram, opp.getInt("stationid"), opp.getString("textcolorid"), opp.getString("title"), icon, background, slide1, slide2, slide3, slide4, banner, sqLiteDatabase);
                                    }


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });


            int socketTimeout3 = 30000; // 30 seconds. You can change it
            RetryPolicy policy3 = new DefaultRetryPolicy(socketTimeout3,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

            strRequest3.setRetryPolicy(policy3);
            queue3.add(strRequest3);

            // -------------------------------------------------------------------------------------

            final Cursor check2 = sqLiteDatabase.rawQuery("SELECT * FROM "+ PrivatePlacesContract.PrivatePlacesEntry.TABLE_NAME+";",null);
            RequestQueue queue7 = Volley.newRequestQueue(getApplicationContext());
            final String url7 = Connection.portal+"getPrivateplaces.php?city="+city_id;

            StringRequest strRequest7 = new StringRequest(Request.Method.GET, url7,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONArray jsonarrayPvPlace = new JSONArray(response);
                                while (check2.moveToNext())
                                {
                                    privateplaceArr.add(check2.getString(2));
                                }
                                check2.close();
                                for(int i=0;i<jsonarrayPvPlace.length();i++)
                                {
                                    JSONObject opv = jsonarrayPvPlace.getJSONObject(i);
                                    String m = opv.getString("id");
                                    if(!privateplaceArr.contains(m))
                                    {
                                        dbHelper.putPrivatePlacesInformation(opv.getString("commercial"), opv.getString("enddate"), opv.getInt("id"), opv.getString("isvip"), opv.getString("mobiles"), opv.getString("photos"), opv.getString("website"),opv.getString("place_id"), sqLiteDatabase);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });


            int socketTimeout7 = 30000; // 30 seconds. You can change it
            RetryPolicy policy7 = new DefaultRetryPolicy(socketTimeout7,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

            strRequest7.setRetryPolicy(policy7);
            queue7.add(strRequest7);


            // -------------------------------------------------------------------------------------

            RequestQueue queue4 = Volley.newRequestQueue(getApplicationContext());
            final String url4 = Connection.portal+"/getGroups.php";
            StringRequest strRequest4 = new StringRequest(Request.Method.GET, url4,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONArray jsonarrayGroups = new JSONArray(response);
                                Cursor check = sqLiteDatabase.rawQuery("SELECT * FROM "+ GroupsContract.GroupsEntry.TABLE_NAME+";",null);
                                while (check.moveToNext())
                                {
                                    groupsArr.add(check.getString(0));
                                }
                                check.close();

                                for(int i=0;i<jsonarrayGroups.length();i++)
                                {
                                    JSONObject g = jsonarrayGroups.getJSONObject(i);
                                    String m = g.getString("id");
                                    if(!groupsArr.contains(m))
                                    {
                                        dbHelper.putGroupsInformation(g.getInt("id"), g.getString("name"),sqLiteDatabase);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
            int socketTimeout4 = 30000;
            RetryPolicy policy4 = new DefaultRetryPolicy(socketTimeout4,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            strRequest4.setRetryPolicy(policy4);
            queue4.add(strRequest4);
             // ------------------------------------------------------------------------------------

            RequestQueue queue5 = Volley.newRequestQueue(getApplicationContext());
            final String url5 = Connection.portal+"/getAboutus.php";
            StringRequest strRequest5 = new StringRequest(Request.Method.GET, url5,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONArray jsonArray = new JSONArray(response);

                                JSONObject oA = jsonArray.getJSONObject(0);

                                Cursor check = sqLiteDatabase.rawQuery("SELECT * FROM "+ AboutUsContract.AboutUsEntry.TBName+";",null);
                                while (check.moveToNext())
                                {
                                    aboutusArr.add(check.getString(4));
                                }
                                check.close();
                                String m = oA.getString("Id");
                                if(!aboutusArr.contains(m))
                                {
                                    dbHelper.putAboutUsInformation(oA.getString("Address"),oA.getString("BackendDeveloper"),oA.getString("Description"),oA.getString("FrontendDeveloper"),oA.getString("Id"),oA.getString("Logo"),oA.getString("Title"),oA.getString("WebsiteURL"),sqLiteDatabase);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });


            int socketTimeout5 = 30000; // 30 seconds. You can change it
            RetryPolicy policy5 = new DefaultRetryPolicy(socketTimeout5,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

            strRequest5.setRetryPolicy(policy5);
            queue5.add(strRequest5);

            // -------------------------------------------------------------------------------------

            RequestQueue queue6 = Volley.newRequestQueue(getApplicationContext());
            final String url6 = Connection.portal+"/getFacilities.php";
            StringRequest strRequest6 = new StringRequest(Request.Method.GET, url6,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONArray jsonArray = new JSONArray(response);
                                Cursor check = sqLiteDatabase.rawQuery("SELECT * FROM "+ FacilitiesContract.FacilitiesEntry.TABLE_NAME+";",null);
                                while (check.moveToNext())
                                {
                                    facilitiesArr.add(check.getString(1));
                                }
                                check.close();
                                for(int i=0;i<jsonArray.length();i++)
                                {
                                    JSONObject o = jsonArray.getJSONObject(i);
                                    String m = o.getString("id");
                                    if(!facilitiesArr.contains(m))
                                    {
                                        dbHelper.putFacilitiesInformation(o.getString("icon"), o.getInt("id"), o.getString("title"), sqLiteDatabase);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });


            int socketTimeout6 = 30000; // 30 seconds. You can change it
            RetryPolicy policy6 = new DefaultRetryPolicy(socketTimeout6,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            strRequest6.setRetryPolicy(policy6);
            queue6.add(strRequest6);

            // -------------------------------------------------------------------------------------

            RequestQueue queue8 = Volley.newRequestQueue(getApplicationContext());
            final String url8 = Connection.portal + "/getlatestUpdate.php?city="+city_id;
            StringRequest strRequest8 = new StringRequest(Request.Method.GET, url8,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONArray jsonArray = new JSONArray(response);
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                SharedPreferences shared = getSharedPreferences("MyPref", MODE_PRIVATE);
                                SharedPreferences.Editor editor = shared.edit();
                                editor.putString("latestupdate",jsonObject.getString("latest_id"));
                                //   editor.putString("latestmessage",array[1]);
                                editor.commit();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {}
                    });


            int socketTimeout8 = 30000; // 30 seconds. You can change it
            RetryPolicy policy8 = new DefaultRetryPolicy(socketTimeout8,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

            strRequest8.setRetryPolicy(policy8);
            queue8.add(strRequest8);
        }
        catch (Exception e){
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Debug")
                    .setAction("Download.java : "+String.valueOf(e))
                    .build());
        }
        finally {
            startActivity(new Intent(Download.this, DownloadSchedule.class));
        }


       /* new Timer().schedule(new TimerTask(){
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        startActivity(new Intent(Download.this, DownloadSchedule.class));
                    }
                });
            }
        }, 17000);*/

    }

    public void getLines()
    {
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        final String url = Connection.portal+"getLines.php?city="+city_id;
        android.util.Log.d("awdgbwkllnnnw",url);

        StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        final DBHelper dbHelper = new DBHelper(Download.this);
                        final SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();

                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            final Cursor checkCursor = sqLiteDatabase.rawQuery("SELECT * FROM "+ LinesContract.LinesEntry.TABLE_NAME+";",null);

                            for(int i=0;i<jsonArray.length();i++)
                            {
                                //Getting Lines
                                final JSONObject o = jsonArray.getJSONObject(i);
                                android.util.Log.d("awdgbwkllnnnw",o.getString("backgroundColor"));
                                dbHelper.putLineInformation(o.getString("backgroundColor"), o.getString("city_ID"),
                                        o.getInt("firstStationNumber"), o.getInt("line_ID"), o.getInt("lastSTationNumber")
                                        , o.getInt("lineNumber"), o.getInt("status"), o.getString("textColor"), o.getString("title"), o.getString("image"),o.getString("multiend"),o.getString("multiend_detail"), sqLiteDatabase);
                                while (checkCursor.moveToNext())
                                {
                                    arr.add(checkCursor.getString(3));
                                }
                                checkCursor.close();
                                String m = o.getString("line_ID");
                            //    if(!arr.contains(m));
                             //   {
                                    android.util.Log.d("awdgbwkllnnnw","true");

                               // }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    android.util.Log.d("awdgbwkllnnnw",String.valueOf(error));
                    }
                });


        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        strRequest.setRetryPolicy(policy);
        queue.add(strRequest);
    }

    public void getGroups()
    {
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        final String url = Connection.portal+"/getGroups?city="+city_id;
        StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonarrayGroups = new JSONArray(response);
                            Cursor check = sqLiteDatabase.rawQuery("SELECT * FROM "+ GroupsContract.GroupsEntry.TABLE_NAME+";",null);
                            while (check.moveToNext())
                            {
                                groupsArr.add(check.getString(0));
                            }
                            check.close();

                            for(int i=0;i<jsonarrayGroups.length();i++)
                            {
                                JSONObject g = jsonarrayGroups.getJSONObject(i);
                                String m = g.getString("id");
                                if(!groupsArr.contains(m))
                                {
                                    dbHelper.putGroupsInformation(g.getInt("id"), g.getString("name"),sqLiteDatabase);
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strRequest.setRetryPolicy(policy);
        queue.add(strRequest);
    }

    public void getAboutUs()
    {
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        final String url = Connection.portal+"/getAboutus";
        StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            JSONObject oA = jsonArray.getJSONObject(0);

                            Cursor check = sqLiteDatabase.rawQuery("SELECT * FROM "+ AboutUsContract.AboutUsEntry.TBName+";",null);
                            while (check.moveToNext())
                            {
                                aboutusArr.add(check.getString(4));
                            }
                            check.close();
                            String m = oA.getString("Id");
                            if(!aboutusArr.contains(m))
                            {
                                dbHelper.putAboutUsInformation(oA.getString("Address"),oA.getString("BackendDeveloper"),oA.getString("Description"),oA.getString("FrontendDeveloper"),oA.getString("Id"),oA.getString("Logo"),oA.getString("Title"),oA.getString("WebsiteURL"),sqLiteDatabase);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });


        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        strRequest.setRetryPolicy(policy);
        queue.add(strRequest);
    }

    public void getFacilities()
    {
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        final String url = Connection.portal+"/getFacilities";
        StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            Cursor check = sqLiteDatabase.rawQuery("SELECT * FROM "+ FacilitiesContract.FacilitiesEntry.TABLE_NAME+";",null);
                            while (check.moveToNext())
                            {
                                facilitiesArr.add(check.getString(1));
                            }
                            check.close();
                            for(int i=0;i<jsonArray.length();i++)
                            {
                                JSONObject o = jsonArray.getJSONObject(i);
                                String m = o.getString("id");
                                if(!facilitiesArr.contains(m))
                                {
                                    dbHelper.putFacilitiesInformation(o.getString("icon"), o.getInt("id"), o.getString("title"), sqLiteDatabase);
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });


        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strRequest.setRetryPolicy(policy);
        queue.add(strRequest);
    }


    public String getAppVersion()
    {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }

    public void getPrivatePlaces()
    {
        final Cursor check = sqLiteDatabase.rawQuery("SELECT * FROM "+ PrivatePlacesContract.PrivatePlacesEntry.TABLE_NAME+";",null);
        RequestQueue queue7 = Volley.newRequestQueue(getApplicationContext());
        final String url7 = Connection.portal+"getPrivateplaces?city="+city_id;

        StringRequest strRequest7 = new StringRequest(Request.Method.GET, url7,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonarrayPvPlace = new JSONArray(response);
                            while (check.moveToNext())
                            {
                                privateplaceArr.add(check.getString(2));
                            }
                            check.close();
                            for(int i=0;i<jsonarrayPvPlace.length();i++)
                            {
                                JSONObject opv = jsonarrayPvPlace.getJSONObject(i);
                                String m = opv.getString("id");
                                if(!privateplaceArr.contains(m))
                                {
                                    dbHelper.putPrivatePlacesInformation(opv.getString("commercial"), opv.getString("enddate"), opv.getInt("id"), opv.getString("isvip"), opv.getString("mobiles"), opv.getString("photos"), opv.getString("website"),opv.getString("place_id"), sqLiteDatabase);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });


        int socketTimeout7 = 30000; // 30 seconds. You can change it
        RetryPolicy policy7 = new DefaultRetryPolicy(socketTimeout7,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        strRequest7.setRetryPolicy(policy7);
        queue7.add(strRequest7);
    }

    public void getStations()
    {
        // Getting Stations
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String json_url_station = Connection.portal + "getStations?city="+city_id;
        StringRequest strRequest = new StringRequest(Request.Method.GET, json_url_station,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonarrayStation = new JSONArray(response);
                            final Cursor checkCursor2 = sqLiteDatabase.rawQuery("SELECT * FROM "+ StationsContract.StationsEntry.TABLE_NAME+";",null);
                            for(int i=0;i<jsonarrayStation.length();i++)
                            {
                                JSONObject os = jsonarrayStation.getJSONObject(i);

                                while (checkCursor2.moveToNext())
                                {
                                    stationsArr.add(checkCursor2.getString(1));
                                }
                                checkCursor2.close();
                                String m = os.getString("station_id");

                                if(!stationsArr.contains(m))
                                {
                                    dbHelper.putStationsInformation(os.getString("address"), os.getInt("station_id"), os.getInt("line_id"),
                                            os.getString("location"), os.getInt("station_number"),
                                            os.getInt("status"), os.getString("title"), os.getString("facilities"),os.getString("iscross"), sqLiteDatabase);
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        strRequest.setRetryPolicy(policy);
        queue.add(strRequest);
    }


    public void getPublicPlaces()
    {
        // Public Places

        final RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        final String url = Connection.portal+"/getPublicplaces?city="+city_id;
        final Cursor check = sqLiteDatabase.rawQuery("SELECT * FROM "+ PublicPlacesContract.PublicPlacesEntry.TABLE_NAME+";",null);

        StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonarrayPubPlace = new JSONArray(response);
                            for(int i=0;i<jsonarrayPubPlace.length();i++)
                            {
                                JSONObject opp = jsonarrayPubPlace.getJSONObject(i);
                                JSONArray photo = opp.getJSONArray("photolinks");

                                telegram = opp.getString("instagram");
                                instagram = opp.getString("telegram");

                                icon = opp.getString("icon");
                                background = opp.getString("background_photo");
                                slide1 = opp.getString("slideshow_1");
                                slide2 = opp.getString("slideshow_2");
                                slide3 = opp.getString("slideshow_3");
                                slide4 = opp.getString("slideshow_4");
                                banner = opp.getString("banner");


                                while (check.moveToNext())
                                {
                                    publicplacesArr.add(check.getString(2));
                                }
                                check.close();
                                String h = opp.getString("id");
                                if(!publicplacesArr.contains(h))
                                {
                                    dbHelper.putPublicPlacesInformation(opp.getString("backgroundcolor"), opp.getString("description"), opp.getInt("id"), opp.getString("isprivate"), opp.getString("phone"), opp.getString("photolinks"), opp.getString("phone"), opp.getInt("placegroupeid"), instagram, telegram, opp.getInt("stationid"), opp.getString("textcolorid"), opp.getString("title"), icon, background, slide1, slide2, slide3, slide4, banner, sqLiteDatabase);
                                }


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });


        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        strRequest.setRetryPolicy(policy);
        queue.add(strRequest);
    }

    public void getLatestUpdate()
    {
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        final String url = Connection.portal + "/getlatestUpdate?city="+city_id;
        StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            SharedPreferences shared = getSharedPreferences("MyPref", MODE_PRIVATE);
                            SharedPreferences.Editor editor = shared.edit();
                            editor.putString("latestupdate",jsonObject.getString("latest_id"));
                            //   editor.putString("latestmessage",array[1]);
                            editor.commit();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {}
                });


        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        strRequest.setRetryPolicy(policy);
        queue.add(strRequest);
    }

    public void getLatestMessage()
    {
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        final String url = Connection.portal + "/getlatestUpdate?city="+city_id;
        StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            SharedPreferences shared = getSharedPreferences("MyPref", MODE_PRIVATE);
                            SharedPreferences.Editor editor = shared.edit();
                            editor.putString("latestupdate",jsonObject.getString("latest_id"));
                            //   editor.putString("latestmessage",array[1]);
                            editor.commit();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {}
                });


        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        strRequest.setRetryPolicy(policy);
        queue.add(strRequest);
    }
}
