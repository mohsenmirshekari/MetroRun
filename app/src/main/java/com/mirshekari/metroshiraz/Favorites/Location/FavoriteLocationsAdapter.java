package com.mirshekari.metroshiraz.Favorites.Location;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.Fragments.Routing_fragment;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mohsen Mirshekari on 4/2/2018.
 */

public class FavoriteLocationsAdapter extends RecyclerView.Adapter <FavoriteLocationsAdapter.RecyclerViewHolder> {
    SharedPreferences sharedLang, stationshared, sharess;
    SharedPreferences.Editor editor;
    ArrayList<FavoriteC> arrayList = new ArrayList<>();
    Context context;
    String lang_name,dic1,dic2;
    SQLiteDatabase db;

    public FavoriteLocationsAdapter(ArrayList<FavoriteC> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_favorite_location, parent, false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        context = parent.getContext();
        return recyclerViewHolder;
    }


    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final FavoriteC favoriteC = arrayList.get(position);
        DBHelper dbHelper = new DBHelper(context);

        db = dbHelper.getReadableDatabase();

        Typeface samim = Typeface.createFromAsset(context.getAssets(), "fonts/samim.ttf");
        final Typeface irsans_light = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_light.ttf");

        sharedLang = context.getSharedPreferences("MyPref",MODE_PRIVATE);
        lang_name = sharedLang.getString("langname","null");

        if(lang_name.equals("فارسي") || lang_name.equals("عربی")) {
            holder.layout.setGravity(Gravity.CENTER_VERTICAL|Gravity.RIGHT);
        }
        else{
            holder.layout.setGravity(Gravity.CENTER_VERTICAL|Gravity.LEFT);
        }

        // Dictionary
        Cursor cursor = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.SELECT_AS_ORIGIN+";",null);
        while (cursor.moveToNext()){
            dic1 = cursor.getString(0);
        }
        cursor.close();
        Cursor cursor2 = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.SELECT_AS_DESTINATION+";",null);
        while (cursor2.moveToNext()){
            dic2 = cursor2.getString(0);
        }
        cursor2.close();
        // >

        holder.title.setText(favoriteC.getTitle());
        holder.station.setText(favoriteC.getStation());
        holder.title.setTypeface(irsans_light);
        holder.station.setTypeface(irsans_light);

        holder.click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.dialog_choose_option);
                Button source = (Button) dialog.findViewById(R.id.source);
                Button destination = (Button) dialog.findViewById(R.id.destination);
                source.setText(dic1);
                destination.setText(dic2);
                Typeface irsans_medium = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_medium.ttf");

                source.setTypeface(irsans_medium);
                destination.setTypeface(irsans_medium);

                source.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SharedPreferences shared = context.getSharedPreferences("RoutingInfo", MODE_PRIVATE);
                        SharedPreferences.Editor editor = shared.edit();
                        editor.putString("source_id", String.valueOf(favoriteC.getStationId()));
                        editor.putString("source_name", String.valueOf(favoriteC.getStation()));
                        editor.commit();
                        Routing_fragment fragment = new Routing_fragment();
                        ((FragmentActivity)context).getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fragment_container, fragment)
                                .commit();
                        dialog.dismiss();
                    }
                });

                destination.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SharedPreferences shared = context.getSharedPreferences("RoutingInfo", MODE_PRIVATE);
                        SharedPreferences.Editor editor = shared.edit();
                        editor.putString("destination_id", String.valueOf(favoriteC.getStationId()));
                        editor.putString("destination_name", String.valueOf(favoriteC.getStation()));
                        editor.commit();
                        Routing_fragment fragment = new Routing_fragment();
                        ((FragmentActivity)context).getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fragment_container, fragment)
                                .commit();
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }
    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder
    {
        TextView title,station;
        LinearLayout layout;
        CardView click;
        RecyclerViewHolder(View view)
        {
            super(view);
            title = (TextView) itemView.findViewById(R.id.title);
            station = (TextView) itemView.findViewById(R.id.station);
            layout = (LinearLayout) itemView.findViewById(R.id.layout);
            click = (CardView) itemView.findViewById(R.id.card_view);
        }
    }
}
