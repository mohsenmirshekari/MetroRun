package com.mirshekari.metroshiraz.Favorites.Location;

/**
 * Created by Mohsen Mirshekari on 4/2/2018.
 */

public class FavoriteC {
    private String title;
    private String latlng;
    private String station;
    private String stationId;

    public FavoriteC(String title, String latlng, String station, String stationId) {
        this.title = title;
        this.latlng = latlng;
        this.station = station;
        this.stationId = stationId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLatlng() {
        return latlng;
    }

    public void setLatlng(String latlng) {
        this.latlng = latlng;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }
}
