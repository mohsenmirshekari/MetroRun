package com.mirshekari.metroshiraz.Favorites.Location;

/**
 * Created by Mohsen Mirshekari on 4/1/2018.
 */

public class FavoriteLocationsContract {
    public static class FavoriteLocationsContractEntry{
        public static final String TABLE_NAME = "favorite_locations";
        public static final String ID = "id";
        public static final String LATLANG = "latlang";
        public static final String TITLE = "title";
        public static final String STATION = "station";
        public static final String STATIONID = "station_id";
    }
}
