package com.mirshekari.metroshiraz.Favorites.Location;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GPSTracker;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Map_choose_favorite_location extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    double latitude,longitude;
    MapView mapView;
    ImageView hereImage;
    String taget_lat,target_long,lang_name,nearestStationName,nearDic,placenameDic,saveDic;
    SQLiteDatabase db;
    DBHelper dbHelper;
    Cursor cursor;
    Double dest_latitude,dest_longitude;
    Location dest_location,mylocation;
    float distance;
    List<Float> banners;
    List<String> stationid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_choose_favorite_location);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dbHelper = new DBHelper(getApplicationContext());
        db = dbHelper.getReadableDatabase();
        banners = new ArrayList<>();
        stationid = new ArrayList<>();

        // Dictionary
        Cursor cursor = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.NEAREST_STATION+";",null);
        while (cursor.moveToNext()){
            nearDic = cursor.getString(0);
        }
        cursor = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.PLACE_TITLE+";",null);
        while (cursor.moveToNext()){
            placenameDic = cursor.getString(0);
        }
        cursor = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.SAVE+";",null);
        while (cursor.moveToNext()){
            saveDic = cursor.getString(0);
        }
        cursor.close();

        //>


        SharedPreferences sharedLang = getSharedPreferences("MyPref",MODE_PRIVATE);
        lang_name = sharedLang.getString("langname","null");


        mapView = (MapView) findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        hereImage = (ImageView) findViewById(R.id.image);

        if(lang_name.equals("فارسي")) {
            hereImage.setBackgroundResource(R.drawable.here);
        }
        else{
            hereImage.setBackgroundResource(R.drawable.here_en);
        }
         mylocation = new Location("");
         dest_location = new Location("");

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        GPSTracker tracker = new GPSTracker(this);
        if (!tracker.canGetLocation()) {
            tracker.showSettingsAlert();
        } else {
            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
        }

        CameraUpdate center= CameraUpdateFactory.newLatLng( new LatLng(latitude, longitude));
        CameraUpdate zoom=CameraUpdateFactory.zoomTo(15);
        mMap.moveCamera(center);
        mMap.animateCamera(zoom);


        hereImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 taget_lat = String.valueOf(mMap.getCameraPosition().target.latitude);
                 target_long = String.valueOf(mMap.getCameraPosition().target.longitude);

                cursor = db.rawQuery("SELECT * FROM stations", null);
                while (cursor.moveToNext()) {
                    String id = cursor.getString(1);
                    String a1 = cursor.getString(3);
                    String a2[] = a1.split(",");
                    dest_latitude = Double.valueOf(a2[0]);
                    dest_longitude = Double.valueOf(a2[1]);
                    dest_location.setLatitude(dest_latitude);
                    dest_location.setLongitude(dest_longitude);
                    mylocation.setLatitude(Double.valueOf(taget_lat));
                    mylocation.setLongitude(Double.valueOf(target_long));
                    distance = mylocation.distanceTo(dest_location);
                    banners.add(distance);
                    stationid.add(id);
                    int m = Integer.valueOf(Math.round(distance));

                }

                int minIndex = banners.indexOf(Collections.min(banners));
                final String nearestStationId = String.valueOf(stationid.get(minIndex));
                Cursor cursorStation = db.rawQuery("SELECT * from stations WHERE station_id = " + nearestStationId + ";", null);
                while (cursorStation.moveToNext())
                {nearestStationName = cursorStation.getString(6);}
                final Dialog dialog = new Dialog(Map_choose_favorite_location.this);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.dialog_add_location);
                Typeface sans_light = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_light.ttf");
                Typeface sans_medium = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_bold.ttf");

                TextView txt1 = (TextView)dialog.findViewById(R.id.nearest);
                final EditText title = (EditText) dialog.findViewById(R.id.title);
                Button save = (Button) dialog.findViewById(R.id.save);
                save.setText(saveDic);
                save.setTypeface(sans_medium);
                title.setHint(placenameDic);
                title.setTypeface(sans_light);
                txt1.setText(nearDic+nearestStationName);
                txt1.setTypeface(sans_light);
                dialog.show();


                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(title.getText().toString().length()>0)
                        {
                            dbHelper.putFavoriteLocations(title.getText().toString(),taget_lat+","+target_long,nearestStationName,nearestStationId,db);
                            dialog.dismiss();
                        }
                        else{
                            title.setHintTextColor(Color.parseColor("#e74c3c"));
                        }
                    }
                });


            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}
