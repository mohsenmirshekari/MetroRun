package com.mirshekari.metroshiraz.Place.Public;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.Place.Groups.GroupsAdapter;
import com.mirshekari.metroshiraz.Place.Groups.GroupsC;
import com.mirshekari.metroshiraz.Place.Groups.GroupsContract;
import com.mirshekari.metroshiraz.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentPublicPlaces extends Fragment {
    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;
    Cursor cursor;
    ArrayList<GroupsC> arrayList = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;

    public FragmentPublicPlaces() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_public_places, container, false);

        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        dbHelper = new DBHelper(getActivity());
        sqLiteDatabase = dbHelper.getReadableDatabase();

        cursor = sqLiteDatabase.rawQuery("SELECT * FROM "+ GroupsContract.GroupsEntry.TABLE_NAME+";", null);

        while(cursor.moveToNext())
        {
            GroupsC cities = new GroupsC(cursor.getInt(0),cursor.getString(1));
            arrayList.add(cities);
            adapter = new GroupsAdapter(arrayList,getActivity(),"false");
        }

        dbHelper.close();
        recyclerView.setAdapter(adapter);

        return v;
    }
}
