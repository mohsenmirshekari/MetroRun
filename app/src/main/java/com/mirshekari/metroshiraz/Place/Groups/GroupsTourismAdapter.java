package com.mirshekari.metroshiraz.Place.Groups;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mirshekari.metroshiraz.Connection;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.Fragments.TourismList;
import com.mirshekari.metroshiraz.Lines;
import com.mirshekari.metroshiraz.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mohsen Mirshekari on 3/1/2018.
 */

public class GroupsTourismAdapter extends RecyclerView.Adapter <GroupsTourismAdapter.RecyclerViewHolder>  {
    SharedPreferences shared;
    SharedPreferences.Editor editor;
    ArrayList<GroupsC> arrayList = new ArrayList<>();
    Context context;
    SQLiteDatabase db;

    public GroupsTourismAdapter(ArrayList<GroupsC> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_groups_selection,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        context = parent.getContext();
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final GroupsC groupsC = arrayList.get(position);
        Typeface irsans_light = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_light.ttf");

        DBHelper dbHelper = new DBHelper(context);

        db = dbHelper.getReadableDatabase();



        holder.title.setText(groupsC.getName());
        holder.title.setTypeface(irsans_light);

        holder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = context.getSharedPreferences("GroupSelected",MODE_PRIVATE);
                SharedPreferences.Editor sheditor = sharedPreferences.edit();
                sheditor.putString("typeId",String.valueOf(groupsC.getId()));
                sheditor.putString("typeName",groupsC.getName());
                sheditor.commit();
                TourismList fragment = new TourismList();
                android.support.v4.app.FragmentTransaction fragmentTransaction =  ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder
    {
        TextView title;
        LinearLayout click_layout;
        RecyclerViewHolder(View view)
        {
            super(view);
            title = (TextView) itemView.findViewById(R.id.title);
            click_layout = (LinearLayout) itemView.findViewById(R.id.click_layout);
        }
    }

}

