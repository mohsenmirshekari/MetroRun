package com.mirshekari.metroshiraz.Place.Private;

/**
 * Created by Mohsen Mirshekari on 12/17/2017.
 */

public class PrivatePlacesContract {
    public static class PrivatePlacesEntry {
        public static String TABLE_NAME = "privateplaces";
        public static String COMMERCIAL = "commercial";
        public static String ENDDATE = "enddate";
        public static String ID = "id";
        public static String ISVIP = "isvip";
        public static String MOBILES = "mobiles";
        public static String PHOTOS = "photos";
        public static String WEBSITE = "website";
        public static String PLACE_ID = "place_id";

    }
}
