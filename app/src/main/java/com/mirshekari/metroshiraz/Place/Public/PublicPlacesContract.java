package com.mirshekari.metroshiraz.Place.Public;

/**
 * Created by Mohsen Mirshekari on 12/17/2017.
 */

public class PublicPlacesContract {
    public static class PublicPlacesEntry{
        public static final String TABLE_NAME = "publicplaces";
        public static final String BACKGROUND_COLOR = "backgroundcolor";
        public static final String DESCRIPTION = "description";
        public static final String ID = "id";
        public static final String ISPRIVATE = "isprivate";
        public static final String PHONE = "phone";
        public static final String PHOTOLINKS = "photolinks";
        public static final String PHOTOS = "photos";
        public static final String PLACEGROUPEID = "placegroupeid";
        public static final String SOCIALMEDIA = "socialmedia";
        public static final String STATIONID = "stationid";
        public static final String TEXTCOLORID = "textcolorid";
        public static final String TITLE = "title";
        public static final String ICON = "icon";
        public static final String BACKGROUND_PHOTO = "background_photo";
        public static final String SLIDESHOW_1 = "slideshow_1";
        public static final String SLIDESHOW_2 = "slideshow_2";
        public static final String SLIDESHOW_3 = "slideshow_3";
        public static final String SLIDESHOW_4 = "slideshow_4";
        public static final String BANNER = "banner";
        public static final String INSTAGRAM = "instagram";
        public static final String TELEGRAM = "telegram";



    }
}
