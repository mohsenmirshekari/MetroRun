package com.mirshekari.metroshiraz.Place.Public;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mirshekari.metroshiraz.Connection;
import com.mirshekari.metroshiraz.PrivatePlaceActivity;
import com.mirshekari.metroshiraz.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.mirshekari.metroshiraz.Fragments.Schedule.Schedule_saturday.faToEn;

/**
 * Created by Mohsen Mirshekari on 12/17/2017.
 */

public class PublicPlacesAdapter extends RecyclerView.Adapter <PublicPlacesAdapter.RecyclerViewHolder>{
    ArrayList<PublicPlacesC> arrayList = new ArrayList<>();
    Context context;

    public PublicPlacesAdapter(ArrayList<PublicPlacesC> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    public PublicPlacesAdapter(ArrayList<PublicPlacesC> arrayList) {
        this.arrayList = arrayList;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_publicplaces,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        context = parent.getContext();
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final PublicPlacesC publicPlacesC = arrayList.get(position);

        Typeface samim = Typeface.createFromAsset(context.getAssets(), "fonts/samim.ttf");
        Typeface irsans_light = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_light.ttf");

        holder.title.setText(publicPlacesC.getTitle());
        holder.title.setTypeface(irsans_light);



        //تا وقتی آرش امکان اضافه کردن رنگ کاستوم به مکان رو توی ادیتور اضاف نکرده این خط باید کامنت باشه
        //holder.title.setTextColor(Color.parseColor("#"+publicPlacesC.getTextcolorid()));

        //تا وقتی آرش امکان اضافه کردن رنگ کاستوم به مکان رو توی ادیتور اضاف نکرده این خط باید کامنت باشه
        //   holder.click.setBackgroundColor(Color.parseColor("#"+publicPlacesC.getBackgroundcolor()));


        try
        {
            // Load Logo from FileSystem
            String URLL = publicPlacesC.getIcon().replace("/","-");
            File folder = new File(context.getFilesDir(),"Images");
            File file = new File(folder,URLL);

            if(file.exists())
            {
                Picasso.with(context).load(file).into(holder.icon);
            }
            else{
                String img_url = publicPlacesC.getIcon().replace("-","/");
                imageDownload(context,img_url,holder.icon);
            }        }catch (Exception e){}


        holder.click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(publicPlacesC.getIsprivate().equals("true"))
                {
                    Intent intent = new Intent(context, PrivatePlaceActivity.class);
                    intent.putExtra("title",publicPlacesC.getTitle());
                    intent.putExtra("bg_color",publicPlacesC.getBackgroundcolor());
                    intent.putExtra("description",publicPlacesC.getDesciption());
                    intent.putExtra("id",String.valueOf(publicPlacesC.getId()));
                    intent.putExtra("background",publicPlacesC.getBackground_photo());
                    intent.putExtra("slide1",publicPlacesC.getSlide1());
                    intent.putExtra("slide2",publicPlacesC.getSlide2());
                    intent.putExtra("slide3",publicPlacesC.getSlide3());
                    intent.putExtra("slide4",publicPlacesC.getSlide4());
                    intent.putExtra("icon",publicPlacesC.getIcon());
                    intent.putExtra("groupeid",publicPlacesC.getPlacegroupeid());
                    intent.putExtra("instagram",publicPlacesC.getInstagram());
                    intent.putExtra("telegram",publicPlacesC.getTelegram());
                    context.startActivity(intent);
                }
                else{

                    // create a Dialog component
                    final Dialog dialog = new Dialog(context);

                    dialog.setContentView(R.layout.layout_publicplace);

                    CircleImageView icon = (CircleImageView) dialog.findViewById(R.id.icon);
                    TextView title = (TextView) dialog.findViewById(R.id.title);
                    TextView phone = (TextView) dialog.findViewById(R.id.phone);
                    TextView phone_present = (TextView) dialog.findViewById(R.id.phonenumber_present);
                    TextView description = (TextView) dialog.findViewById(R.id.description);
                    TextView description_present = (TextView) dialog.findViewById(R.id.description_present);

                    Typeface iranmed = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_medium.ttf");
                    Typeface iranlight = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_light.ttf");


                    phone.setText(faToEn(publicPlacesC.getPhone()));
                    title.setText(publicPlacesC.getTitle());
                    description.setText(publicPlacesC.getDesciption());

                    title.setTypeface(iranmed);
                    phone.setTypeface(iranlight);
                    description.setTypeface(iranlight);
                    description_present.setTypeface(iranlight);
                    phone_present.setTypeface(iranlight);

                    Picasso.with(context).load(Connection.images+publicPlacesC.getIcon())
                            .fit()
                            .centerCrop()
                            .into(icon);

                  /*  Blurry.with(context)
                            .radius(10)
                            .sampling(8)
                            .async()
                            .capture(dialog.findViewById(R.id.background))
                            .into((ImageView) dialog.findViewById(R.id.background));
*/

                    dialog.show();

                }

            }
        });

/**
        holder.click.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                android.util.Log.d("Gholi",publicPlacesC.getIsprivate());
                return false;
            }
        });
 **/
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class RecyclerViewHolder extends  RecyclerView.ViewHolder
    {
        TextView title;
        CircleImageView icon;
        LinearLayout click;
        View line;
        RecyclerViewHolder(View view)
        {
            super(view);
            title = (TextView) itemView.findViewById(R.id.title);
            icon = (CircleImageView) itemView.findViewById(R.id.icon);
            click = (LinearLayout) itemView.findViewById(R.id.click_layout);
        }
    }
    public static void imageDownload(Context ctx, String url,ImageView imageView){
        Picasso.with(ctx)
                .load(Connection.images+url)
                .into(getTarget(url,ctx));
        Picasso.with(ctx)
                .load(Connection.images+url)
                .into(imageView);
    }

    private static Target getTarget(final String url, final Context context){
        Target target = new Target(){

            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        String URLL = url.replace("/","-");
                        File folder = new File(context.getFilesDir(),"Images");
                        if(!folder.exists()){
                            folder.mkdir();
                        }
                        File file = new File(folder,URLL);
                        try {
                            file.createNewFile();
                            FileOutputStream ostream = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 80, ostream);
                            ostream.flush();
                            ostream.close();

                        } catch (IOException e) {
                            Log.e("IOException", e.getLocalizedMessage());
                        }
                    }
                }).start();

            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        return target;
    }
}
