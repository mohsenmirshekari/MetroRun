package com.mirshekari.metroshiraz.Place.Groups;

/**
 * Created by Mohsen Mirshekari on 12/24/2017.
 */

public class GroupsC {
    private int id;
    private String name;

    public GroupsC(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
