package com.mirshekari.metroshiraz.Place.Groups;

/**
 * Created by Mohsen Mirshekari on 12/18/2017.
 */

public class GroupsContract {
    public static class GroupsEntry
    {
        public static final String TABLE_NAME = "groups";
        public static final String ID = "id";
        public static final String NAME = "name";
    }
}
