package com.mirshekari.metroshiraz.Place.Public;

/**
 * Created by Mohsen Mirshekari on 12/17/2017.
 */

public class PublicPlacesC {
    private String backgroundcolor;
    private String desciption;
    private int id;
    private String isprivate;
    private String phone;
    private String photolinks;
    private String photos;
    private int placegroupeid;
    private String instagram;
    private String telegram;
    private int stationid;
    private String textcolorid;
    private String title;
    private String icon;
    private String background_photo;
    private String slide1;
    private String slide2,slide3,slide4,banner;


    public PublicPlacesC(String backgroundcolor, String desciption, int id, String isprivate, String phone, String photolinks, String photos, int placegroupeid, String instagram, String telegram, int stationid, String textcolorid, String title, String icon, String background_photo, String slide1, String slide2, String slide3, String slide4, String banner) {
        this.backgroundcolor = backgroundcolor;
        this.desciption = desciption;
        this.id = id;
        this.isprivate = isprivate;
        this.phone = phone;
        this.photolinks = photolinks;
        this.photos = photos;
        this.placegroupeid = placegroupeid;
        this.instagram = instagram;
        this.telegram = telegram;
        this.stationid = stationid;
        this.textcolorid = textcolorid;
        this.title = title;
        this.icon = icon;
        this.background_photo = background_photo;
        this.slide1 = slide1;
        this.slide2 = slide2;
        this.slide3 = slide3;
        this.slide4 = slide4;
        this.banner = banner;
    }

    public String getBackgroundcolor() {
        return backgroundcolor;
    }

    public void setBackgroundcolor(String backgroundcolor) {
        this.backgroundcolor = backgroundcolor;
    }

    public String getDesciption() {
        return desciption;
    }

    public void setDesciption(String desciption) {
        this.desciption = desciption;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIsprivate() {
        return isprivate;
    }

    public void setIsprivate(String isprivate) {
        this.isprivate = isprivate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhotolinks() {
        return photolinks;
    }

    public void setPhotolinks(String photolinks) {
        this.photolinks = photolinks;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public int getPlacegroupeid() {
        return placegroupeid;
    }

    public void setPlacegroupeid(int placegroupeid) {
        this.placegroupeid = placegroupeid;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getTelegram() {
        return telegram;
    }

    public void setTelegram(String telegram) {
        this.telegram = telegram;
    }

    public int getStationid() {
        return stationid;
    }

    public void setStationid(int stationid) {
        this.stationid = stationid;
    }

    public String getTextcolorid() {
        return textcolorid;
    }

    public void setTextcolorid(String textcolorid) {
        this.textcolorid = textcolorid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getBackground_photo() {
        return background_photo;
    }

    public void setBackground_photo(String background_photo) {
        this.background_photo = background_photo;
    }

    public String getSlide1() {
        return slide1;
    }

    public void setSlide1(String slide1) {
        this.slide1 = slide1;
    }

    public String getSlide2() {
        return slide2;
    }

    public void setSlide2(String slide2) {
        this.slide2 = slide2;
    }

    public String getSlide3() {
        return slide3;
    }

    public void setSlide3(String slide3) {
        this.slide3 = slide3;
    }

    public String getSlide4() {
        return slide4;
    }

    public void setSlide4(String slide4) {
        this.slide4 = slide4;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }
}
