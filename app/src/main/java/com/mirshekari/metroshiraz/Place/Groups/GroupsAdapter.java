package com.mirshekari.metroshiraz.Place.Groups;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.Place.Private.PrivatePlacesContract;
import com.mirshekari.metroshiraz.Place.Public.PublicPlacesAdapter;
import com.mirshekari.metroshiraz.Place.Public.PublicPlacesC;
import com.mirshekari.metroshiraz.Place.Public.PublicPlacesContract;
import com.mirshekari.metroshiraz.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javax.xml.datatype.Duration;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mohsen Mirshekari on 12/24/2017.
 */

public class GroupsAdapter extends RecyclerView.Adapter <GroupsAdapter.RecyclerViewHolder> {
    ArrayList<GroupsC> arrayList = new ArrayList<>();
    ArrayList<PublicPlacesC> arrayListPlaces = new ArrayList<>();
    ArrayList<PublicPlacesC> arrayListPlaces2 = new ArrayList<>();
    Context context;
    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;
    Cursor cursorPlaces,cursorPrivate;
    RecyclerView.Adapter adapter;
    private static final long TICKS_AT_EPOCH = 621355968000000000L;
    private static final long TICKS_PER_MILLISECOND = 10000;
    String isPrivate,stationid,month,year,day;
    long milliseconds;
    public GroupsAdapter(ArrayList<GroupsC> arrayList, Context context,String isPrivate) {
        this.arrayList = arrayList;
        this.context = context;
        this.isPrivate = isPrivate;
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_groups,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        context = parent.getContext();
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final GroupsC groupsC = arrayList.get(position);
        adapter = new PublicPlacesAdapter(arrayListPlaces,context);
        dbHelper = new DBHelper(context);
        sqLiteDatabase = dbHelper.getReadableDatabase();

        Typeface samim = Typeface.createFromAsset(context.getAssets(), "fonts/samim.ttf");
        Typeface irsans_light = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_light.ttf");
        Typeface irsans_med = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_medium.ttf");

        String date = new SimpleDateFormat("MMddyyyy", Locale.getDefault()).format(new Date());

        android.util.Log.d("wqjdbhwq", String.valueOf(date));



        SharedPreferences shared = context.getSharedPreferences("StationInfo", MODE_PRIVATE);
        stationid = shared.getString("station_id","null");

        holder.txt404.setTypeface(irsans_light);
        holder.title.setText(groupsC.getName());
        holder.title.setTypeface(irsans_med);

        cursorPlaces = sqLiteDatabase.rawQuery("SELECT * FROM " + PublicPlacesContract.PublicPlacesEntry.TABLE_NAME + " WHERE " + PublicPlacesContract.PublicPlacesEntry.PLACEGROUPEID + "=" + groupsC.getId() + " AND "+ PublicPlacesContract.PublicPlacesEntry.ISPRIVATE+" = '"+isPrivate+"' AND "+ PublicPlacesContract.PublicPlacesEntry.STATIONID+" = "+stationid+";", null);



        while (cursorPlaces.moveToNext())
        {
            if(isPrivate.equals("true"))
            {
                Cursor cursor = sqLiteDatabase.rawQuery("SELECT "+ PrivatePlacesContract.PrivatePlacesEntry.ENDDATE+" FROM "+ PrivatePlacesContract.PrivatePlacesEntry.TABLE_NAME+" WHERE "+ PrivatePlacesContract.PrivatePlacesEntry.PLACE_ID+" = "+cursorPlaces.getString(2)+";",null);
                while (cursor.moveToNext())
                {
                    String fulldate = cursor.getString(0);
                    String placeYear = fulldate.substring(4);
                    String placeMonth = fulldate.substring(2,4);
                    String placeDay = fulldate.substring(0, fulldate.length() -6);
                    android.util.Log.d("dwqqwfbuj",placeYear+placeMonth+placeDay);
                }
            }
            PublicPlacesC publicPlacesC = new PublicPlacesC(cursorPlaces.getString(0), cursorPlaces.getString(1),
                    cursorPlaces.getInt(2),cursorPlaces.getString(3),
                    cursorPlaces.getString(4), cursorPlaces.getString(5), cursorPlaces.getString(6),
                    cursorPlaces.getInt(7),cursorPlaces.getString(8), cursorPlaces.getString(9),
                    cursorPlaces.getInt(10), cursorPlaces.getString(11),
                    cursorPlaces.getString(12),
                    cursorPlaces.getString(13), cursorPlaces.getString(14),
                    cursorPlaces.getString(15), cursorPlaces.getString(16),
                    cursorPlaces.getString(17), cursorPlaces.getString(18),
                    cursorPlaces.getString(19));
            arrayListPlaces.add(publicPlacesC);
            adapter = new PublicPlacesAdapter(arrayListPlaces, context);
            holder.recyclerView.setAdapter(adapter);
        }
        if(arrayListPlaces.size()==0)
        {
            holder.txt404.setVisibility(View.VISIBLE);
        }
        holder.recyclerView.setVisibility(View.VISIBLE);



    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder
    {
        TextView title,txt404;
        RecyclerView recyclerView;
        RecyclerViewHolder(View view)
        {
            super(view);
            title = (TextView) itemView.findViewById(R.id.title);
            recyclerView = (RecyclerView) itemView.findViewById(R.id.recyclerview);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setHasFixedSize(true);

            txt404  = (TextView) itemView.findViewById(R.id.txt_404);
        }
    }
}
