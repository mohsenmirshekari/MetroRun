package com.mirshekari.metroshiraz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mirshekari.metroshiraz.Aboutus.AboutUsContract;
import com.mirshekari.metroshiraz.ChooseLine.LinesContract;
import com.mirshekari.metroshiraz.ChooseStation.StationsContract;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.Facilities.FacilitiesContract;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.Place.Groups.GroupsContract;
import com.mirshekari.metroshiraz.Place.Private.PrivatePlacesContract;
import com.mirshekari.metroshiraz.Place.Public.PublicPlacesContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class UpdateSchedule extends AppCompatActivity {

    Context ctx;
    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;
    SharedPreferences shared,sharedcity,cityName ;
    String gu_id,city_id,city_name,line_title,version;
    int station_id=1;
    ArrayList<String> arr,stationsArr,publicplacesArr,privateplaceArr,groupsArr,aboutusArr,facilitiesArr;
    int route;
    String icon,background,slide1,slide2,slide3,slide4,banner,instagram,telegram,soctest,soctest2,dictionary;
    TextView messageTextView;
    RequestQueue queue1;
    TextView text;
    RequestQueue requestQueue;
    String linecheckid,downloadDic;
    int stationid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dbHelper = new DBHelper(UpdateSchedule.this);
        sqLiteDatabase = dbHelper.getReadableDatabase();
        //Dictionary
        Typeface irsans_light = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_light.ttf");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        text = (TextView) findViewById(R.id.text);
        text.setTypeface(irsans_light);

        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.CITY_INFO_DOWNLOADING+";",null);

        while (cursor.moveToNext()){
            dictionary = cursor.getString(2);
        }

        cursor.close();
        // ->
        shared = getSharedPreferences("MyPref", MODE_PRIVATE);
        sharedcity = getSharedPreferences("City", MODE_PRIVATE);
        gu_id = shared.getString("gu","null");
        city_id = sharedcity.getString("city","null");
        cityName = getSharedPreferences("City", MODE_PRIVATE);
        city_name = cityName.getString("cityName","شما");

        Cursor dic = sqLiteDatabase.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.DOWNLOADING_STATIONS+";",null);
        while (dic.moveToNext())
        {
            downloadDic = dic.getString(0);
        }
        dic.close();
        text.setText(downloadDic);
        String line_id = getIntent().getStringExtra("lang_id");
        String id = getIntent().getStringExtra("id");

        getData(line_id,id);

        new Timer().schedule(new TimerTask(){
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        startActivity(new Intent(UpdateSchedule.this, Update_schedule_two.class));
                    }
                });
            }
        }, 7000);

    }

    public void getData(String line_id,String id)
    {
        arr = new ArrayList<String>();
        stationsArr = new ArrayList<String>();
        publicplacesArr = new ArrayList<String>();
        privateplaceArr = new ArrayList<String>();
        groupsArr = new ArrayList<String>();
        aboutusArr = new ArrayList<String>();
        facilitiesArr = new ArrayList<String>();
        final Cursor check = sqLiteDatabase.rawQuery("SELECT * FROM "+ PublicPlacesContract.PublicPlacesEntry.TABLE_NAME+";",null);


        // Getting Stations
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String json_url_station = Connection.portal + "/GetStations/" + gu_id + "/" + line_id + "/"+id;
        StringRequest strRequest = new StringRequest(Request.Method.GET, json_url_station,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonarrayStation = new JSONArray(response);
                            final Cursor checkCursor2 = sqLiteDatabase.rawQuery("SELECT * FROM "+ StationsContract.StationsEntry.TABLE_NAME+";",null);
                            for(int i=0;i<jsonarrayStation.length();i++)
                            {
                                JSONObject os = jsonarrayStation.getJSONObject(i);

                                while (checkCursor2.moveToNext())
                                {
                                    stationsArr.add(checkCursor2.getString(1));
                                }
                                checkCursor2.close();
                                String m = os.getString("Id");

                                if(!stationsArr.contains(m))
                                {
                                    dbHelper.updateStationsInformation(os.getString("Address"), os.getInt("Id"), os.getInt("LineId"),
                                            os.getString("Location"), os.getInt("StationNumber"),
                                            os.getInt("Status"), os.getString("Title"), os.getString("Facilities"),os.getString("IsCross"), sqLiteDatabase);
                                }
                                station_id = os.getInt("Id");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        strRequest.setRetryPolicy(policy);
        queue.add(strRequest);
    }

    @Override
    public void onBackPressed() {

    }
}