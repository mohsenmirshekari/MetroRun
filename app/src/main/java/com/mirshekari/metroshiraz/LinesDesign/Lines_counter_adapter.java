package com.mirshekari.metroshiraz.LinesDesign;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mirshekari.metroshiraz.ChooseLine.LinesContract;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.R;

import java.util.ArrayList;

import static com.mirshekari.metroshiraz.Fragments.Schedule.Schedule_saturday.faToEn;

/**
 * Created by Mohsen Mirshekari on 2/2/2018.
 */

public class Lines_counter_adapter extends RecyclerView.Adapter <Lines_counter_adapter.RecyclerViewHolder> {
    ArrayList<Lines_counter_items> arrayList = new ArrayList<>();

    Context context;
    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;
    Cursor cursor;
    RecyclerView.Adapter adapter;
    String isPrivate;
    int count = 0;
    public Lines_counter_adapter(ArrayList<Lines_counter_items> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_linecounter,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        context = parent.getContext();
        return recyclerViewHolder;
    }


    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final Lines_counter_items linesCounterItems = arrayList.get(position);
        dbHelper = new DBHelper(context);
        sqLiteDatabase = dbHelper.getReadableDatabase();

        Typeface samim = Typeface.createFromAsset(context.getAssets(), "fonts/samim.ttf");
        Typeface irsans_light = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_light.ttf");
        Typeface irsans_med = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_medium.ttf");

        holder.counter.setTypeface(irsans_light);

        holder.counter.setText(faToEn(String.valueOf(linesCounterItems.getCount())));
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder
    {
        TextView counter;
        RecyclerViewHolder(View view)
        {
            super(view);
            counter = (TextView) itemView.findViewById(R.id.count);

        }
    }
}
