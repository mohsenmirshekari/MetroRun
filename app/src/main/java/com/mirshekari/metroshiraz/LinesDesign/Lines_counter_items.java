package com.mirshekari.metroshiraz.LinesDesign;

/**
 * Created by Mohsen Mirshekari on 2/2/2018.
 */

public class Lines_counter_items {
    private int count;

    public Lines_counter_items(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
