package com.mirshekari.metroshiraz.Schedule;

/**
 * Created by Mohsen on 11/9/2017.
 */

public class ScheduleC {
    private String time;
    private int route;
    private String day;
    private int stationid;
    private int stationno;

    public ScheduleC(String time, int route, String day, int stationid, int stationno) {
        this.time = time;
        this.route = route;
        this.day = day;
        this.stationid = stationid;
        this.stationno = stationno;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getRoute() {
        return route;
    }

    public void setRoute(int route) {
        this.route = route;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public int getStationid() {
        return stationid;
    }

    public void setStationid(int stationid) {
        this.stationid = stationid;
    }

    public int getStationno() {
        return stationno;
    }

    public void setStationno(int stationno) {
        this.stationno = stationno;
    }
}
