package com.mirshekari.metroshiraz.Schedule;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mirshekari.metroshiraz.ChooseLine.LinesAdapter;
import com.mirshekari.metroshiraz.ChooseStation.StationsC;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import static android.content.Context.MODE_PRIVATE;
import static com.mirshekari.metroshiraz.Fragments.Schedule.Schedule_saturday.faToEn;

/**
 * Created by Mohsen on 11/9/2017.
 */

public class ScheduleAdapter extends RecyclerView.Adapter <ScheduleAdapter.RecyclerViewHolder> {
    ArrayList<ScheduleC> arrayList = new ArrayList<>();
    Context context;
    SQLiteDatabase db;
    SharedPreferences sharedLang;
    String lang_name;
    public ScheduleAdapter(ArrayList<ScheduleC> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    public ScheduleAdapter(ArrayList<ScheduleC> arrayList) {
        this.arrayList = arrayList;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_schedule,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        context = parent.getContext();
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        final ScheduleC scheduleC = arrayList.get(position);
        DBHelper dbHelper = new DBHelper(context);


        TimeZone tz = TimeZone.getTimeZone("GMT+03:30");
        Calendar c = Calendar.getInstance(tz);
        String time = String.format("%02d" , c.get(Calendar.HOUR_OF_DAY))+
                String.format("%02d" , c.get(Calendar.MINUTE));
        final Integer firun = Integer.valueOf(time)+100;

        sharedLang = context.getSharedPreferences("MyPref",MODE_PRIVATE);
        lang_name = sharedLang.getString("langname","null");

        int myTime = Integer.valueOf(scheduleC.getTime().substring(1,scheduleC.getTime().length()-1));

        db = dbHelper.getReadableDatabase();
        Typeface samim = Typeface.createFromAsset(context.getAssets(), "fonts/samim.ttf");
        Typeface irsans_light = Typeface.createFromAsset(context.getAssets(), "fonts/iransansweb_light.ttf");





            String ftd = scheduleC.getTime().substring(1,scheduleC.getTime().length()-1);
            String ftd1 = ftd.substring(2);
            String ftd4 = ftd.substring(0,ftd.length()-2);

        if(scheduleC.getRoute()==0)
        {
            if(lang_name.equals("فارسي") || lang_name.equals("عربی"))
            {
                holder.time.setText(String.valueOf(faToEn(ftd.substring(0,ftd.length()-2))+":"+faToEn(ftd.substring(1))));
            }
            else
            {
               // holder.time.setText(String.valueOf(ftd.substring(0,ftd.length()-2)+":"+ftd.substring(1)));
                holder.time.setText(String.valueOf(faToEn(ftd.substring(0,ftd.length()-2))+":"+faToEn(ftd.substring(1))));
            }
        }

            else if(lang_name.equals("فارسي") || lang_name.equals("عربی"))
            {
                String ftd4p = faToEn(ftd1);
                String std4p = faToEn(ftd4);
                holder.time.setText(String.valueOf(std4p+":"+ftd4p));
            }
            else
            {
               /* String ftd4p = String.valueOf(ftd1);
                String std4p = String.valueOf(ftd4);
                holder.time.setText(String.valueOf(std4p+":"+ftd4p));*/
                String ftd4p = faToEn(ftd1);
                String std4p = faToEn(ftd4);
                holder.time.setText(String.valueOf(std4p+":"+ftd4p));
            }

        holder.time.setTypeface(irsans_light);

        if(myTime<firun)
        {
          //  holder.time.setTextColor(context.getResources().getColor(R.color.colorAccent));
            holder.layout.setBackgroundColor(Color.parseColor("#0DD32F2F"));
            holder.checkImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_action_check_red));
        }
        else {
            holder.checkImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_action_check_green));
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

public class RecyclerViewHolder extends RecyclerView.ViewHolder
{
    TextView time;
    LinearLayout layout;
    ImageView checkImage;
    RecyclerViewHolder(View view)
    {
        super(view);
        time = (TextView) itemView.findViewById(R.id.time);
        layout = (LinearLayout) itemView.findViewById(R.id.layout);
        checkImage = (ImageView) itemView.findViewById(R.id.checkimage);
    }

    public int isDatabaseEmpty()
    {
        String count = "SELECT count(*) FROM cities";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        return icount;
    }
}}


