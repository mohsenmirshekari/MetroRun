package com.mirshekari.metroshiraz.Schedule;

/**
 * Created by Mohsen on 11/9/2017.
 */

public class ScheduleContract {
    public static class ScheduleEntry {
        public static final String TABLE_NAME_0 = "schedule0";
        public static final String TABLE_NAME_1 = "schedule1";
        public static final String TABLE_NAME_2 = "schedule2";
        public static final String TABLE_NAME_3 = "schedule3";
        public static final String TABLE_NAME_4 = "schedule4";
        public static final String TABLE_NAME_5 = "schedule5";
        public static final String TABLE_NAME_6 = "schedule6";

        public static final String ID = "id";
        public static final String TIME = "time";
        public static final String ROUTE = "route";
        public static final String DAY = "day";
        public static final String STATION_ID = "station_id";
        public static final String STATION_NO = "station_no";
    }
}
