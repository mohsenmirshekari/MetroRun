package com.mirshekari.metroshiraz.Schedule;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.mirshekari.metroshiraz.ChooseStation.StationsContract;
import com.mirshekari.metroshiraz.Connection;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.Facilities.BackgroundTaskFacilities;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.R;
import com.mirshekari.metroshiraz.Station;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mohsen on 11/9/2017.
 */

public class BackgroundTaskSchedule extends AsyncTask<Void,Void,Void> {
    ProgressDialog progressDialog;
    Context ctx;
    SharedPreferences shared,sharedcity,sharedline;
    String gu_id,station_no,dictionary;
    int route,numrows;
    String linelenghtstr;
    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;
    int arrayLenght;
    int stationid;
    Dialog dialog;
    TextView messageTextView;
    public BackgroundTaskSchedule(Context ctx) {
        this.ctx = ctx;
    }
    @Override
    protected void onPreExecute() {
        dbHelper = new DBHelper(ctx);
        sqLiteDatabase = dbHelper.getWritableDatabase();
        shared = ctx.getSharedPreferences("MyPref", MODE_PRIVATE);
        sharedcity = ctx.getSharedPreferences("Station", MODE_PRIVATE);

        //Dictionary

       /* Cursor cursor = sqLiteDatabase.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.DOWNLOADING_STATIONS+";",null);

        while (cursor.moveToNext()){
            dictionary = cursor.getString(2);
        }*/

        dictionary = "dsadsa";

        // ->

        gu_id = shared.getString("gu","null");
        station_no = sharedcity.getString("stid","null");

        // Toast.makeText(ctx,linelenghtstr+" "+ gu_id,Toast.LENGTH_LONG).show();

        dialog = new Dialog(ctx);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.loading);
        Typeface samim = Typeface.createFromAsset(ctx.getAssets(), "fonts/samim.ttf");
        messageTextView = (TextView)dialog.findViewById(R.id.message);
        messageTextView.setTypeface(samim);
        messageTextView.setText(dictionary);
        messageTextView.setTypeface(samim);

        dialog.show();

    }
    @Override
    protected Void doInBackground(Void... voids) {

        Cursor cursorstationid = sqLiteDatabase.rawQuery("SELECT station_id FROM stations;", null);
        dialog.show();

        while (cursorstationid.moveToNext())
        {
            stationid = Integer.valueOf(cursorstationid.getString(cursorstationid.getColumnIndex(StationsContract.StationsEntry.STATION_ID)));
            Cursor cursorstation = sqLiteDatabase.rawQuery("SELECT title FROM stations WHERE station_id = "+stationid+";", null);
            while (cursorstation.moveToNext()){
                // messageTextView.setText(cursorstation.getString(cursorstation.getColumnIndex(StationsContract.StationsEntry.TITLE)));
            }

            //  messageTextView.setText(String.valueOf(stationid));
            try {
                String json_url = Connection.portal+"/GetSchedules/" + gu_id + "/" + stationid;
                URL url = new URL(json_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line + "\n");
                    Thread.sleep(5000);
                }
                httpURLConnection.disconnect();
                String json_data = stringBuilder.toString().trim();
                JSONObject object = new JSONObject(json_data);
                String stid = object.getString("StationId");
                String stno = object.getString("StationNo");
                String previus = object.getString("Previous");
                String next = object.getString("Next");

                if(next.length()!=4 && previus.length()==4)
                {
                    android.util.Log.d("adkwugdwawaa","1");
                    route = 0;
                    JSONObject object2 = object.getJSONObject("Next");
                    JSONArray Jarray2 = object2.getJSONArray("Days");
                    JSONObject ab0 = Jarray2.getJSONObject(0);
                    JSONObject ab1 = Jarray2.getJSONObject(1);
                    JSONObject ab2 = Jarray2.getJSONObject(2);
                    JSONObject ab3 = Jarray2.getJSONObject(3);
                    JSONObject ab4 = Jarray2.getJSONObject(4);
                    JSONObject ab5 = Jarray2.getJSONObject(5);
                    JSONObject ab6 = Jarray2.getJSONObject(5);

                    JSONArray array0 = ab0.getJSONArray("Plan");
                    JSONArray array1 = ab1.getJSONArray("Plan");
                    JSONArray array2 = ab2.getJSONArray("Plan");
                    JSONArray array3 = ab3.getJSONArray("Plan");
                    JSONArray array4 = ab4.getJSONArray("Plan");
                    JSONArray array5 = ab5.getJSONArray("Plan");
                    JSONArray array6 = ab6.getJSONArray("Plan");

                    dbHelper.putSchedule0Information(String.valueOf(array0), route, "saturday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule1Information(String.valueOf(array1), route, "sunday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule2Information(String.valueOf(array2), route, "monday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule3Information(String.valueOf(array3), route, "tuesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule4Information(String.valueOf(array4), route, "wednesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule5Information(String.valueOf(array5), route, "thursday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule6Information(String.valueOf(array6), route, "friday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                }

                if(next.length()!=4 && previus.length()!=4)
                {
                    android.util.Log.d("adkwugdwawaa","2");
                    route = 1;
                    // DOWNLOADING THE NEXT SCHEDULE
                    JSONObject object2 = object.getJSONObject("Next");
                    JSONArray Jarray2 = object2.getJSONArray("Days");
                    JSONObject ab0 = Jarray2.getJSONObject(0);
                    JSONObject ab1 = Jarray2.getJSONObject(1);
                    JSONObject ab2 = Jarray2.getJSONObject(2);
                    JSONObject ab3 = Jarray2.getJSONObject(3);
                    JSONObject ab4 = Jarray2.getJSONObject(4);
                    JSONObject ab5 = Jarray2.getJSONObject(5);
                    JSONObject ab6 = Jarray2.getJSONObject(5);


                    JSONArray array0 = ab0.getJSONArray("Plan");
                    JSONArray array1 = ab1.getJSONArray("Plan");
                    JSONArray array2 = ab2.getJSONArray("Plan");
                    JSONArray array3 = ab3.getJSONArray("Plan");
                    JSONArray array4 = ab4.getJSONArray("Plan");
                    JSONArray array5 = ab5.getJSONArray("Plan");
                    JSONArray array6 = ab6.getJSONArray("Plan");

                    android.util.Log.d("dwagdwa 0",String.valueOf(ab0));
                    android.util.Log.d("dwagdwa 1",String.valueOf(ab1));
                    android.util.Log.d("dwagdwa 2",String.valueOf(ab2));
                    android.util.Log.d("dwagdwa 3",String.valueOf(ab3));
                    android.util.Log.d("dwagdwa 4",String.valueOf(ab4));
                    android.util.Log.d("dwagdwa 5",String.valueOf(ab5));
                    android.util.Log.d("dwagdwa 6",String.valueOf(ab6));

                    Log.d("SCHEDULE ","DOWNLOADING NEXT SCHEDULE WITH ROUTE 1");

                    dbHelper.putSchedule0Information(String.valueOf(array0), route, "saturday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule1Information(String.valueOf(array1), route, "sunday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule2Information(String.valueOf(array2), route, "monday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule3Information(String.valueOf(array3), route, "tuesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule4Information(String.valueOf(array4), route, "wednesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule5Information(String.valueOf(array5), route, "thursday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule6Information(String.valueOf(array6), route, "friday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);


                    android.util.Log.d("adwuhawdaww --- "+stid,String.valueOf(array2));
                    android.util.Log.d("adwuhawdaww +++ ",String.valueOf(array6));



                    // DOWNLOADING THE NEXT SCHEDULE
                    Log.d("SCHEDULE ","DOWNLOADING PREVIOUS SCHEDULE WITH ROUTE 1");
                    JSONObject object3 = object.getJSONObject("Previous");
                    JSONArray Jarray3 = object3.getJSONArray("Days");
                    JSONObject aab0 = Jarray3.getJSONObject(0);
                    JSONObject aab1 = Jarray3.getJSONObject(1);
                    JSONObject aab2 = Jarray3.getJSONObject(2);
                    JSONObject aab3 = Jarray3.getJSONObject(3);
                    JSONObject aab4 = Jarray3.getJSONObject(4);
                    JSONObject aab5 = Jarray3.getJSONObject(5);
                    JSONObject aab6 = Jarray3.getJSONObject(5);

                    JSONArray arrayy0 = aab0.getJSONArray("Plan");
                    JSONArray arrayy1 = aab1.getJSONArray("Plan");
                    JSONArray arrayy2 = aab2.getJSONArray("Plan");
                    JSONArray arrayy3 = aab3.getJSONArray("Plan");
                    JSONArray arrayy4 = aab4.getJSONArray("Plan");
                    JSONArray arrayy5 = aab5.getJSONArray("Plan");
                    JSONArray arrayy6 = aab6.getJSONArray("Plan");

                    dbHelper.putSchedule0Information(String.valueOf(arrayy0), route, "saturday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule1Information(String.valueOf(arrayy1), route, "sunday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule2Information(String.valueOf(arrayy2), route, "monday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule3Information(String.valueOf(arrayy3), route, "tuesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule4Information(String.valueOf(arrayy4), route, "wednesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule5Information(String.valueOf(arrayy5), route, "thursday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule6Information(String.valueOf(arrayy6), route, "friday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                }


                if(next.length()==4 && previus.length()!=4)
                {
                    route = 2;
                    android.util.Log.d("adkwugdwawaa","3");
                    // DOWNLOADING THE PREVIUS SCHEDULE
                    JSONObject object2 = object.getJSONObject("Previous");
                    JSONArray Jarray2 = object2.getJSONArray("Days");
                    JSONObject ab0 = Jarray2.getJSONObject(0);
                    JSONObject ab1 = Jarray2.getJSONObject(1);
                    JSONObject ab2 = Jarray2.getJSONObject(2);
                    JSONObject ab3 = Jarray2.getJSONObject(3);
                    JSONObject ab4 = Jarray2.getJSONObject(4);
                    JSONObject ab5 = Jarray2.getJSONObject(5);
                    JSONObject ab6 = Jarray2.getJSONObject(5);

                    JSONArray array0 = ab0.getJSONArray("Plan");
                    JSONArray array1 = ab1.getJSONArray("Plan");
                    JSONArray array2 = ab2.getJSONArray("Plan");
                    JSONArray array3 = ab3.getJSONArray("Plan");
                    JSONArray array4 = ab4.getJSONArray("Plan");
                    JSONArray array5 = ab5.getJSONArray("Plan");
                    JSONArray array6 = ab6.getJSONArray("Plan");
                    Log.d("SCHEDULE ","DOWNLOADING PREVIUS SCHEDULE WITH ROUTE 2");

                    dbHelper.putSchedule0Information(String.valueOf(array0), route, "saturday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule1Information(String.valueOf(array1), route, "sunday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule2Information(String.valueOf(array2), route, "monday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule3Information(String.valueOf(array3), route, "tuesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule4Information(String.valueOf(array4), route, "wednesday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule5Information(String.valueOf(array5), route, "thursday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);

                    dbHelper.putSchedule6Information(String.valueOf(array6), route, "friday", Integer.valueOf(stid), Integer.valueOf(stno), sqLiteDatabase);


                }

                dialog.dismiss();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    @Override
    protected void onProgressUpdate(Void... values) {
        messageTextView.setText("123");
        super.onProgressUpdate(values);
    }
    @Override
    protected void onPostExecute(Void aVoid) {


        //ctx.startActivity(new Intent(ctx,Station.class));

    }
}

