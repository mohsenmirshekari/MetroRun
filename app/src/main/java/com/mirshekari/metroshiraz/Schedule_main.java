package com.mirshekari.metroshiraz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mirshekari.metroshiraz.ChooseLine.LinesContract;
import com.mirshekari.metroshiraz.ChooseStation.StationsContract;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.Fragments.Schedule.main.Schedule_main_next;
import com.mirshekari.metroshiraz.Fragments.Schedule.main.Schedule_main_prev;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;

import java.util.Calendar;
import java.util.TimeZone;

public class Schedule_main extends AppCompatActivity {

    Button nextStationBTN,prevStationBTN;
    boolean nextStatus = false;
    boolean prevStatus = false;
    ProgressBar pg;
    String station_next,station_prev,routeid,lineTitle,towards1,towards2,laststaionDic,scheduleDic,lang_name;
    SQLiteDatabase db;
    DBHelper dbHelper;
    int dow;
    String lineid,laststation,firststation;
    SharedPreferences shared,sharedLang;
    SharedPreferences.Editor editor;
    Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView mTitle = (TextView) findViewById(R.id.toolbar_title);
        Typeface irsans_light = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_light.ttf");
        Typeface irsans_medium = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_medium.ttf");

        mTitle.setTypeface(irsans_light);
        setTitle("");
        nextStationBTN = (Button) findViewById(R.id.nextStationBTN);
        prevStationBTN = (Button) findViewById(R.id.prevStationBTN);
        sharedLang = getSharedPreferences("MyPref",MODE_PRIVATE);
        lang_name = sharedLang.getString("langname","null");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Schedule");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        if(lang_name.equals("فارسي"))
        {
            nextStationBTN.setTypeface(irsans_light);
            prevStationBTN.setTypeface(irsans_light);
        }
        else
        {
            nextStationBTN.setTypeface(irsans_medium);
            prevStationBTN.setTypeface(irsans_medium);
            nextStationBTN.setTextSize(13);
            prevStationBTN.setTextSize(13);

        }

        shared = getSharedPreferences("route", MODE_PRIVATE);
        editor = shared.edit();


        Calendar calendar = Calendar.getInstance();

        int day = calendar.get(Calendar.DAY_OF_WEEK)-1;

        if(day==7)
        {
            dow = 1;
        }
        else if(day==1)
        {
            dow = 2;
        }
        else if(day==2)
        {
            dow = 3;
        }
        else if(day==3)
        {
            dow = 4;
        }
        else if(day==4)
        {
            dow = 5;
        }
        else if(day==5)
        {
            dow = 6;
        }
        else if(day==6)
        {
            dow = 0;
        }



        TimeZone tz = TimeZone.getTimeZone("GMT+03:30");
        Calendar c = Calendar.getInstance(tz);
        String time = String.format("%02d" , c.get(Calendar.HOUR_OF_DAY))+
                String.format("%02d" , c.get(Calendar.MINUTE));
        final Integer firun = Integer.valueOf(time);

        nextStationBTN.setBackground(getResources().getDrawable(R.drawable.radius));
        nextStationBTN.setTextColor(getResources().getColor(R.color.items));
        prevStationBTN.setBackgroundColor(Color.TRANSPARENT);
        prevStationBTN.setTextColor(getResources().getColor(R.color.items));

        Fragment newFragment = new Schedule_main_next();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment);
        // transaction.addToBackStack(null);
        transaction.commit();


        dbHelper = new DBHelper(this);
        db = dbHelper.getReadableDatabase();


        // Dictionary


        Cursor cursorToward1 = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.TOWARDS_1+";",null);
        while (cursorToward1.moveToNext()){
            towards1 = cursorToward1.getString(0);
        }
        cursorToward1.close();

        Cursor cursorToward2 = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.TOWARDS_2+";",null);
        while (cursorToward2.moveToNext()){
            towards2 = cursorToward2.getString(0);
        }
        cursorToward2.close();

        Cursor cursorLastStaion = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.LASTSTAION_OF+";",null);
        while (cursorLastStaion.moveToNext()){
            laststaionDic = cursorLastStaion.getString(0);
        }
        cursorLastStaion.close();

        Cursor cursorSchedule = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.SCHEDULE+";",null);
        while (cursorSchedule.moveToNext()){
            scheduleDic = cursorSchedule.getString(0);
        }
        cursorSchedule.close();
        // ->

        SharedPreferences shared = getSharedPreferences("StationInfo", MODE_PRIVATE);
        String stid = shared.getString("station_id", "null");
        int lineId = shared.getInt("line_id",1);

        Cursor cursor2 = db.rawQuery("SELECT * FROM "+ LinesContract.LinesEntry.TABLE_NAME+" WHERE "+ LinesContract.LinesEntry.LINE_ID+" = "+lineId+";",null);
        while (cursor2.moveToNext()){
            laststation = cursor2.getString(4);
            firststation = cursor2.getString(2);
        }
        cursor2.close();

        String st_title = shared.getString("station_title","نام ناشناس");
        mTitle.setText(scheduleDic+" - "+st_title);
        Integer stationid = Integer.valueOf(stid) + 1;
        Integer stationid2 = stationid - 1;
        Integer stationid3 = Integer.valueOf(stid) - 1;


        SQLiteDatabase sqLiteDatabase = dbHelper.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT title FROM stations WHERE station_id = " + laststation + ";", null);

        while (cursor.moveToNext()) {
            station_next = cursor.getString(cursor.getColumnIndex(StationsContract.StationsEntry.TITLE));
        }
        cursor.close();
        Cursor cursorprev = sqLiteDatabase.rawQuery("SELECT title FROM stations WHERE station_id = " + firststation + ";", null);
        while (cursorprev.moveToNext()) {
            station_prev = cursorprev.getString(cursor.getColumnIndex(StationsContract.StationsEntry.TITLE));
        }
        cursorprev.close();
        Cursor cursorroute = db.rawQuery("SELECT route FROM schedule" + dow + " WHERE station_id = " + stationid2 + ";", null);
        if (cursorroute.moveToFirst()) {
            do {
                routeid = cursorroute.getString(0);
            } while (cursorroute.moveToNext());
        }
        cursorroute.close();
        Cursor cursorLineTitle = sqLiteDatabase.rawQuery("SELECT title FROM lines WHERE line_ID = " + lineId + ";", null);
        if (cursorLineTitle.moveToFirst()) {
            do {
                lineTitle = cursorLineTitle.getString(0);
            } while (cursorLineTitle.moveToNext());
        }
        cursorLineTitle.close();

        nextStationBTN.setText(towards1+" "+station_next+" "+towards2);
        prevStationBTN.setText(towards1+" "+station_prev+" "+towards2);

        android.util.Log.d("dawdxaww",String.valueOf(stationid3));

        nextStationBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getSharedPreferences("MyPref",MODE_PRIVATE);
                SharedPreferences.Editor sheditor = sharedPreferences.edit();
                sheditor.putString("route","next");
                sheditor.commit();
                 nextStationBTN.setBackground(getResources().getDrawable(R.drawable.radius));
                 nextStationBTN.setTextColor(getResources().getColor(R.color.items));
                 prevStationBTN.setBackgroundColor(Color.TRANSPARENT);
                 prevStationBTN.setTextColor(getResources().getColor(R.color.items));

                    Fragment newFragment = new Schedule_main_next();
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.fragment_container, newFragment);
                    // transaction.addToBackStack(null);
                    transaction.commit();

            }
        });

        prevStationBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sharedPreferences = getSharedPreferences("MyPref",MODE_PRIVATE);
                SharedPreferences.Editor sheditor = sharedPreferences.edit();
                sheditor.putString("route","prev");
                sheditor.commit();

                prevStationBTN.setBackground(getResources().getDrawable(R.drawable.radius));
                prevStationBTN.setTextColor(getResources().getColor(R.color.items));
                nextStationBTN.setBackgroundColor(Color.TRANSPARENT);
                nextStationBTN.setTextColor(getResources().getColor(R.color.items));
                Fragment newFragment = new Schedule_main_prev();
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.fragment_container, newFragment);
                    //    transaction.addToBackStack(null);
                    transaction.commit();

            }
        });

        Cursor cursor1 = db.rawQuery("SELECT * FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.STATION_ID+" = "+stationid+";",null);
        while (cursor1.moveToNext())
        {
            lineid = cursor1.getString(2);
        }
        cursor1.close();

            if (Integer.valueOf(stid)==Integer.valueOf(firststation))
            {
                prevStationBTN.setEnabled(false);
                prevStationBTN.setText(laststaionDic+" "+lineTitle);
                nextStationBTN.setBackground(getResources().getDrawable(R.drawable.radius));
                nextStationBTN.setTextColor(getResources().getColor(R.color.items));
                prevStationBTN.setBackgroundColor(Color.TRANSPARENT);
                prevStationBTN.setTextColor(getResources().getColor(R.color.items));
                SharedPreferences sharedPreferences = getSharedPreferences("MyPref",MODE_PRIVATE);
                SharedPreferences.Editor sheditor = sharedPreferences.edit();
                sheditor.putString("route","next");
                sheditor.commit();
                Fragment newFragments = new Schedule_main_next();
                FragmentTransaction transactions = getSupportFragmentManager().beginTransaction();
                transactions.replace(R.id.fragment_container, newFragments);
                transactions.commit();
            }

            if (Integer.valueOf(stid).equals(Integer.valueOf(laststation))) {
                nextStationBTN.setEnabled(false);
                nextStationBTN.setText(laststaionDic + " " + lineTitle);
                prevStationBTN.setBackground(getResources().getDrawable(R.drawable.radius));
                prevStationBTN.setTextColor(getResources().getColor(R.color.mainColor));
                nextStationBTN.setBackgroundColor(Color.TRANSPARENT);
                nextStationBTN.setTextColor(getResources().getColor(R.color.md_white));
                SharedPreferences sharedPreferences = getSharedPreferences("MyPref", MODE_PRIVATE);
                SharedPreferences.Editor sheditor = sharedPreferences.edit();
                sheditor.putString("route", "prev");
                sheditor.commit();
                Fragment newFragments = new Schedule_main_prev();
                FragmentTransaction transactions = getSupportFragmentManager().beginTransaction();
                transactions.replace(R.id.fragment_container, newFragments);
                transactions.commit();


            }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
