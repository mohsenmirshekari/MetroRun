package com.mirshekari.metroshiraz;

/**
 * Created by Mohsen Mirshekari on 3/9/2018.
 */

public class MessagesC {
    private String cityid;
    private String highlight;
    private String id;
    private String text;
    private String title;

    public MessagesC(String cityid, String highlight, String id, String text, String title) {
        this.cityid = cityid;
        this.highlight = highlight;
        this.id = id;
        this.text = text;
        this.title = title;
    }

    public String getCityid() {
        return cityid;
    }

    public void setCityid(String cityid) {
        this.cityid = cityid;
    }

    public String getHighlight() {
        return highlight;
    }

    public void setHighlight(String highlight) {
        this.highlight = highlight;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
