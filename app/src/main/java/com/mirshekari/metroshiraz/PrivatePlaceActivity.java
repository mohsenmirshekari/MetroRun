package com.mirshekari.metroshiraz;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class PrivatePlaceActivity extends AppCompatActivity {
    String title,bg_color,description,id,background,slide1,slide2,slide3,slide4,groupeid,instagram,telegram,textColor,
         iconString,enddate,mobiles,mobile1,mobile2,website,phoneDic,lang_name;
    CircleImageView icon;
    ImageView bg_image,bg_image_color;
    FloatingActionButton instagramFab,telegramFab,callFab,webSiteFab;
    TextView titleTextView,phone1,phone2,phonePresent,descriptionView;
    Typeface irsans_light,irans_medium;
    SliderLayout mDemoSlider;
    Cursor cursor;
    SharedPreferences sharedLang;
    SQLiteDatabase db;
    DBHelper dbHelper;
    Cursor cursorG;
    Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_private_place_acrivity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");
        TextView topBar_title = (TextView) findViewById(R.id.topBar_title);

        // Initilizing Elements --------------------------------------------------------------------
        irsans_light = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_light.ttf");
        irans_medium = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_medium.ttf");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Private place");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        sharedLang = getSharedPreferences("MyPref",MODE_PRIVATE);
        lang_name = sharedLang.getString("langname","null");

        topBar_title.setTypeface(irans_medium);

        icon = (CircleImageView) findViewById(R.id.icon);
        bg_image = (ImageView) findViewById(R.id.bg_image);
        bg_image_color = (ImageView) findViewById(R.id.bg_image_color);
        titleTextView = (TextView) findViewById(R.id.title);

        phone1 = (TextView) findViewById(R.id.phone1);
        phone2 = (TextView) findViewById(R.id.phone2);

        descriptionView = (TextView) findViewById(R.id.description);
        descriptionView.setTypeface(irsans_light);

        phonePresent = (TextView) findViewById(R.id.phonePresent);
        phonePresent.setTypeface(irans_medium);

        mDemoSlider = (SliderLayout)findViewById(R.id.slider);

        dbHelper = new DBHelper(this);
        db = dbHelper.getReadableDatabase();

        id = getIntent().getStringExtra("id");


     /*   //Getting data from intent
        title = getIntent().getStringExtra("title");
        bg_color = getIntent().getStringExtra("bg_color");
        description = getIntent().getStringExtra("description");
        background = getIntent().getStringExtra("background");
        slide1 = getIntent().getStringExtra("slide1");
        slide2 = getIntent().getStringExtra("slide2");
        slide3 =  getIntent().getStringExtra("slide3");
        slide4 = getIntent().getStringExtra("slide4");
        groupeid = getIntent().getStringExtra("groupeid");
        instagram = getIntent().getStringExtra("instagram");
        telegram = getIntent().getStringExtra("telegram");
        iconString = getIntent().getStringExtra("icon");
*/

     // Dictionary

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.TELEPHONES+";",null);
        while (cursorG.moveToNext())
        {
        phoneDic = cursorG.getString(0);
        }
        phonePresent.setText(phoneDic);
        if(lang_name.equals("فارسي") || lang_name.equals("عربی")) {
            phonePresent.setGravity(Gravity.CENTER_VERTICAL|Gravity.RIGHT);
        }
        else {
            phonePresent.setGravity(Gravity.CENTER_VERTICAL|Gravity.LEFT);
        }


        // ->


        // Initilizing Elements --------------------------------------------------------------------



        // getting Data from publicPlace Activity --------------------------------------------------
        Cursor cursorsPublic = db.rawQuery("SELECT * FROM publicplaces WHERE id = "+id+";", null);
        while (cursorsPublic.moveToNext())
        {
            bg_color = cursorsPublic.getString(0);
            description = cursorsPublic.getString(1);
            groupeid = cursorsPublic.getString(7);
            instagram = cursorsPublic.getString(8);
            telegram = cursorsPublic.getString(9);
            textColor = cursorsPublic.getString(11);
            title = cursorsPublic.getString(12);
            iconString = cursorsPublic.getString(13);
            background = cursorsPublic.getString(14);
            slide1 = cursorsPublic.getString(15);
            slide2 = cursorsPublic.getString(16);
            slide3 = cursorsPublic.getString(17);
            slide4 = cursorsPublic.getString(18);
        }
        cursorsPublic.close();
        // getting Data from publicPlace Activity --------------------------------------------------


        topBar_title.setText(title);

        titleTextView.setText(title);
        titleTextView.setTypeface(irsans_light);

        android.util.Log.d("dawdwdadwdwd",bg_color);

        bg_image_color.setBackgroundColor(Color.parseColor(bg_color));




        if(iconString!=null)
        {
            Picasso.with(getApplicationContext()).load(Connection.images+iconString)
                    .fit()
                    .centerCrop()
                    .into(icon);
        }

      if(background!=null)
      {
          Picasso.with(getApplicationContext()).load(Connection.images+background)
                  .fit()
                  .centerCrop()
                  .into(bg_image);
      }



        // Getting data from database --------------------------------------------------------------


        try{
            Cursor cursors = db.rawQuery("SELECT * FROM privateplaces WHERE id = " + id + ";", null);

            while (cursors.moveToNext())
            {
                mobiles = cursors.getString(4);
                String [] phones = mobiles.split(",");
                mobile1 = phones[0];
                mobile2 = phones[1];
                website = cursors.getString(6);
                enddate = cursors.getString(1);
            }
        }catch (Exception e){}

        // Getting data from database --------------------------------------------------------------


        // Setting data from database to elements --------------------------------------------------

        try{
            SpannableString content = new SpannableString(mobile1);
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);

            SpannableString content2 = new SpannableString(mobile2);
            content2.setSpan(new UnderlineSpan(), 0, content2.length(), 0);

            phone1.setText(content);
            phone2.setText(content2);

            Toast.makeText(getApplicationContext(),"++ "+phone1,Toast.LENGTH_SHORT).show();

            android.util.Log.d("awwwwaasds",String.valueOf(content));
            descriptionView.setText(description);
        }catch (Exception e){}


        // Setting data from database to elements --------------------------------------------------



        // Fabs --------------------------------------
        try{
            instagramFab = (FloatingActionButton) findViewById(R.id.instagram);
            telegramFab = (FloatingActionButton) findViewById(R.id.telegram);
            callFab = (FloatingActionButton) findViewById(R.id.call);
            webSiteFab = (FloatingActionButton) findViewById(R.id.website);

            if(instagram.equals("null"))
            {
                instagramFab.setVisibility(View.GONE);
            }
            else{
                instagramFab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try{
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(instagram.substring(1)));
                            startActivity(browserIntent);
                        }catch (ActivityNotFoundException e){
                        }
                    }
                });
            }


            if(telegram.equals("null"))
            {
                telegramFab.setVisibility(View.GONE);
            }
            else{
                // Link Simple = tg://resolve?domain=channel_name
                // value must be the telegram channel username like = varzesh3
                telegramFab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(telegram.substring(1)));
                        startActivity(intent);
                    }
                });

            }


            if(phone1==null)
            {
                callFab.setVisibility(View.GONE);
            }
            else{
                callFab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:"+mobile1));
                        startActivity(intent);
                    }
                });
            }
            if(website==null)
            {
                webSiteFab.setVisibility(View.GONE);
            }
            else{
                webSiteFab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try{
                            Uri uri = Uri.parse(website);
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(browserIntent);
                        }catch (Exception e)
                        {
                            Toast.makeText(getApplicationContext(),"آدرس وبسایت نادرست است.",Toast.LENGTH_SHORT).show();
                        }

                    }
                });

            }

            telegramFab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.telegram)));
            callFab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.call)));
            webSiteFab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.website)));

        }catch (Exception e){}

        //---------------------------------------------


      // Slider Image ------------------------------------------------------------------------------
        try{
            HashMap<String,String> url_maps = new HashMap<String, String>();
            url_maps.put("1"  , Connection.images+slide1);
            url_maps.put("2" , Connection.images+slide2);
            url_maps.put("3" , Connection.images+slide3);
            url_maps.put("4" , Connection.images+slide4);
            for(String name : url_maps.keySet()){
                TextSliderView textSliderView = new TextSliderView(getApplicationContext());
                textSliderView
                        .image(url_maps.get(name))
                        .setScaleType(BaseSliderView.ScaleType.Fit);
                textSliderView.bundle(new Bundle());
                textSliderView.getBundle()
                        .putString("extra",name);
                mDemoSlider.addSlider(textSliderView);

            }
            mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
            mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
            mDemoSlider.setCustomAnimation(new DescriptionAnimation());
            mDemoSlider.setDuration(3000);
        }catch (Exception e){}

        // Slider Image ------------------------------------------------------------------------------

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
