
package com.mirshekari.metroshiraz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;

import java.util.ArrayList;
import java.util.Calendar;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mirshekari.metroshiraz.ChooseLine.LinesContract;
import com.mirshekari.metroshiraz.ChooseStation.StationsContract;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.Facilities.FacilitiesAdapter;
import com.mirshekari.metroshiraz.Facilities.FacilitiesC;
import com.mirshekari.metroshiraz.GetKeys.DictionaryContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.Place.Private.PrivatePlacesContract;
import com.mirshekari.metroshiraz.Place.Public.PublicPlacesContract;
import com.mirshekari.metroshiraz.Schedule.ScheduleContract;

import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.events.OnBannerClickListener;
import ss.com.bannerslider.views.BannerSlider;

import static com.mirshekari.metroshiraz.Fragments.Schedule.Schedule_saturday.faToEn;

public class Station extends AppCompatActivity {
    TextView st_name,st_address,facilitiestext,np,sc,loc,nptitle,destination1,origin1,time1,destination2,origin2,time2,no_facilities;
    ImageView arrow1,arrow2;
    LinearLayout locclick,npclick,scheduleclick,backgroundLayout;
    SQLiteDatabase db;
    DBHelper dbHelper;
    String station_next,station_prev,station_number,routeid,nextmoveDic,facilitiesDic;
    SharedPreferences shared,sharess,lineShares;
    Context context;
    int dow,route,dofw;
    long numberOfRows;
    ScrollView scrollView;
    String facilities1;
    String[] parts;
    String ml,lineid,laststation,firststation;
    double mylatitude,mylongitude;
    ArrayList<FacilitiesC> arrayList = new ArrayList<>();
    private RecyclerView.Adapter adapter;
    RecyclerView recyclerView;
    String commercial,commercial_id,place_id,scheduleDic,nearbyDic,routingDic,latitude,longitude;
    Cursor cursorFacilities;
    String [] array;
    RelativeLayout sliderLayout;
    BannerSlider bannerSlider;
    List<Banner> banners;
    RelativeLayout mainLayout;
    String origin1String,destination1String,destination2String;
    Cursor cursorG;
    SharedPreferences sharedLang;
    String lang_name,nothingfoundDic;
    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView topBar_title = (TextView) findViewById(R.id.topBar_title);
        setTitle("");
        setSupportActionBar(toolbar);
        Typeface irsans_light = Typeface.createFromAsset(getAssets(), "fonts/iransansweb_light.ttf");
        Typeface irsans_medium = Typeface.createFromAsset(getAssets(), "fonts/iransansweb.ttf");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedLang = getSharedPreferences("MyPref",MODE_PRIVATE);
        lang_name = sharedLang.getString("langname","null");


        SharedPreferences sharedLine = getSharedPreferences("Line",MODE_PRIVATE);
        lineid = sharedLine.getString("line","null");


        sliderLayout = (RelativeLayout) findViewById(R.id.sliderLayout);

        bannerSlider = (BannerSlider) findViewById(R.id.banner_slider1);
        banners=new ArrayList<>();

        mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);

        scrollView = (ScrollView) findViewById(R.id.scrollView);
        scrollView.scrollTo(0,0);

        recyclerView = (RecyclerView) findViewById(R.id.facilitiesRecycler);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false));
        recyclerView.setHasFixedSize(true);
        TimeZone tz = TimeZone.getTimeZone("GMT+03:30");
        Calendar c = Calendar.getInstance(tz);
        String time = String.format("%02d" , c.get(Calendar.HOUR_OF_DAY))+ String.format("%02d" , c.get(Calendar.MINUTE));
        final Integer firun = Integer.valueOf(time)+100;
        dbHelper = new DBHelper(this);
        db = dbHelper.getReadableDatabase();


        cursorG = db.rawQuery("SELECT * FROM "+ LinesContract.LinesEntry.TABLE_NAME+" WHERE "+ LinesContract.LinesEntry.LINE_ID+" = "+lineid+";",null);
        while (cursorG.moveToNext())
        {
            firststation = cursorG.getString(2);
            laststation = cursorG.getString(4);
        }


        GPSTracker tracker = new GPSTracker(getApplicationContext());
        if (!tracker.canGetLocation()) {
            tracker.showSettingsAlert();
        } else {
            mylatitude = tracker.getLatitude();
            mylongitude = tracker.getLongitude();
        }

        // Dictionary
        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.SCHEDULE+";",null);
        while (cursorG.moveToNext()){
            scheduleDic = cursorG.getString(0);
        }

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.NOTHINGFOUND+";",null);
        while (cursorG.moveToNext())
        {
            nothingfoundDic = cursorG.getString(0);
        }

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.NEARBY_PLACES+";",null);
        while (cursorG.moveToNext()){
            nearbyDic = cursorG.getString(0);
        }


        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.MAPROUTING+";",null);
        while (cursorG.moveToNext()){
            routingDic = cursorG.getString(0);
        }

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.FACILITIES+";",null);
        while (cursorG.moveToNext()){
            facilitiesDic = cursorG.getString(0);
        }

        cursorG = db.rawQuery("SELECT "+ KeysContract.KeysEntry.TEXT+" FROM "+ KeysContract.KeysEntry.TABLE_NAME+" WHERE "+ KeysContract.KeysEntry.KEY_ID+" = "+ DictionaryContract.TRAIN_NEXT_MOVE+";",null);
        while (cursorG.moveToNext()){
            nextmoveDic = cursorG.getString(0);
        }
        mTracker.setScreenName("Station ("+st_name+")");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        // ->

        SQLiteDatabase sqLiteDatabase = dbHelper.getReadableDatabase();
        st_address = (TextView) findViewById(R.id.stationaddress);
        st_name = (TextView) findViewById(R.id.stationname);
        np = (TextView) findViewById(R.id.nearby);
        sc = (TextView) findViewById(R.id.schedule);
        loc = (TextView) findViewById(R.id.location);
        nptitle = (TextView) findViewById(R.id.nextprevtitle);
        destination1 = (TextView) findViewById(R.id.destination1);
        destination2 = (TextView) findViewById(R.id.destination2);
        origin1 = (TextView) findViewById(R.id.Origin1);
        origin2 = (TextView) findViewById(R.id.Origin2);
        time1 = (TextView) findViewById(R.id.time1);
        time2 = (TextView) findViewById(R.id.time2);
        facilitiestext = (TextView) findViewById(R.id.facilitiestext);
        arrow1 = (ImageView) findViewById(R.id.arrow1);
        arrow2 = (ImageView) findViewById(R.id.arrow2);
        backgroundLayout = (LinearLayout) findViewById(R.id.linearLayout);
        locclick = (LinearLayout) findViewById(R.id.locationclick);
        npclick = (LinearLayout) findViewById(R.id.npclick);
        scheduleclick = (LinearLayout) findViewById(R.id.scheduleclick);
        no_facilities = (TextView) findViewById(R.id.no_facilities);


        no_facilities.setTypeface(irsans_light);
        st_address.setTypeface(irsans_light);
        st_name.setTypeface(irsans_light);
        np.setTypeface(irsans_light);
        sc.setTypeface(irsans_light);
        loc.setTypeface(irsans_light);
        nptitle.setTypeface(irsans_light);
        destination1.setTypeface(irsans_light);
        destination2.setTypeface(irsans_light);
        origin1.setTypeface(irsans_light);
        origin2.setTypeface(irsans_light);
        time1.setTypeface(irsans_light);
        time2.setTypeface(irsans_light);
        facilitiestext.setTypeface(irsans_light);

        sc.setText(scheduleDic);
        np.setText(nearbyDic);
        loc.setText(routingDic);
        nptitle.setText(nextmoveDic);
        facilitiestext.setText(facilitiesDic);



        lineShares = getSharedPreferences("Line",MODE_PRIVATE);
        String bgcolor = lineShares.getString("backgroundcolor","03A678");
        String bgcolor1 = bgcolor;
        String textColor = lineShares.getString("textColor","000000");
        String textColor1 = textColor;
        int color = Color.parseColor(bgcolor1);
        backgroundLayout.setBackgroundColor(color);
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        dow  = day-1;
        SharedPreferences shared = getSharedPreferences("StationInfo", MODE_PRIVATE);
        String stid = shared.getString("station_id", "null");
        final String st_title = shared.getString("station_title","خطای دریافت نام");
        st_name.setText(st_title);
        topBar_title.setText(st_title);
        topBar_title.setTypeface(irsans_medium);

        Integer stationid = Integer.valueOf(stid) + 1;
        Integer stationid2 = stationid - 1;
        Integer stationid3 = Integer.valueOf(stid) - 1;
        Integer currentStation = Integer.valueOf(stid);
        Integer nextStation = Integer.valueOf(stid)+1;
        Integer prevStation = Integer.valueOf(stid)-1;

      //  st_address.setTextColor(Color.parseColor(textColor1));
     //   st_name.setTextColor(Color.parseColor(textColor1));




        //------------------------------------------FACILITIES------------------------------------------------------------
        try{
            cursorG = sqLiteDatabase.rawQuery("SELECT "+ StationsContract.StationsEntry.FACILITIES+" FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.STATION_ID+" = "+currentStation+";",null);
            while (cursorG.moveToNext())
            {
                facilities1 =  cursorG.getString(0);
            }


            parts = facilities1.split("-");
            if(parts.length>0) {
                for (int i = 0; i <= parts.length - 1; i++) {
                    cursorFacilities = sqLiteDatabase.rawQuery("SELECT * FROM facilities where id = " + parts[i] + ";", null);
                    while ((cursorFacilities.moveToNext())) {
                        FacilitiesC cities = new FacilitiesC(cursorFacilities.getString(0), cursorFacilities.getInt(1), cursorFacilities.getString(2));
                        arrayList.add(cities);
                        adapter = new FacilitiesAdapter(arrayList, getApplicationContext());
                    }
                    adapter = new FacilitiesAdapter(arrayList);
                    recyclerView.setAdapter(adapter);
                }
                cursorFacilities.close();
            }

        }catch (Exception e){
            no_facilities.setVisibility(View.VISIBLE);
            no_facilities.setText(nothingfoundDic);
        }

        //--------------------------------------------------------------------------------------------------------------

        // Gettng station's address
        cursorG = sqLiteDatabase.rawQuery("SELECT address FROM stations WHERE station_id = " + currentStation + ";", null);
        while(cursorG.moveToNext())
        {
            String address = cursorG.getString(0);
            st_address.setText(address);
        }


        cursorG = sqLiteDatabase.rawQuery("SELECT title FROM stations WHERE station_id = " + stationid + ";", null);
        while (cursorG.moveToNext()) {
            station_next = cursorG.getString(cursorG.getColumnIndex(StationsContract.StationsEntry.TITLE));
        }

        cursorG = sqLiteDatabase.rawQuery("SELECT title FROM stations WHERE station_id = " + stationid3 + ";", null);
        while (cursorG.moveToNext()) {
            station_prev = cursorG.getString(cursorG.getColumnIndex(StationsContract.StationsEntry.TITLE));
        }


        cursorG = sqLiteDatabase.rawQuery("SELECT route FROM schedule"+dow+" WHERE station_id = " + stationid2 + ";", null);
        if (cursorG.moveToFirst()) {
            do {

                routeid = cursorG.getString(0);
            } while (cursorG.moveToNext());
        }



        scheduleclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences =getSharedPreferences("MyPref",MODE_PRIVATE);
                SharedPreferences.Editor sheditor = sharedPreferences.edit();
                sheditor.putString("route","next");
                sheditor.commit();
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Station Page")
                        .setAction("Schedule"+" ("+st_title+")")
                        .build());
                startActivity(new Intent(Station.this,Schedule_main.class));
            }
        });

        npclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Station Page")
                        .setAction("Places"+" ("+st_title+")")
                        .build());
                startActivity(new Intent(Station.this,Places.class));
            }
        });

        try{


            Cursor cursorGTime = db.rawQuery("SELECT distinct * FROM schedule"+ dow+" WHERE station_id = "+currentStation+" ORDER BY time ASC",null);


            cursorG = db.rawQuery("SELECT "+ StationsContract.StationsEntry.TITLE+" FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.STATION_ID+" = "+stid+";",null);
            while (cursorG.moveToNext())
            {
                origin1String = cursorG.getString(0);
            }


            cursorG = db.rawQuery("SELECT "+ StationsContract.StationsEntry.TITLE+" FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.STATION_ID+" = "+laststation+";",null);
            while (cursorG.moveToNext())
            {
                destination1String = cursorG.getString(0);
            }


            cursorG = db.rawQuery("SELECT "+ StationsContract.StationsEntry.TITLE+" FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.STATION_ID+" = "+firststation+";",null);
            while (cursorG.moveToNext())
            {
                destination2String = cursorG.getString(0);
            }



            if(firststation.equals(stid))
            {
                while (cursorGTime.moveToNext())
                {
                    String time1 = cursorGTime.getString(0);
                    time1 = time1.substring(2, time1.length()-2);
                    array = time1.split(",");
                    break;
                }

                for(int i=0;i<array.length;i++) {
                    array[i] = array[i].substring(1, array[i].length()-1);
                    if (Integer.valueOf(array[i])> firun) {
                        String f = array[i].substring(0,2);
                        String l = array[i].substring(2);
                        if(lang_name.equals("فارسي") || lang_name.equals("عربی"))
                        {
                            time1.setText(faToEn(f)+":"+faToEn(l));
                        }
                        else{
                           // time1.setText(f+":"+l);
                            time1.setText(faToEn(f)+":"+faToEn(l));

                        }                        origin1.setText(origin1String);
                        destination1.setText(destination1String);
                        arrow2.setVisibility(View.GONE);
                        break;
                    }
                }
            }

            if(laststation.equals(stid))
            {
                while (cursorGTime.moveToNext())
                {
                    cursorGTime.moveToLast();
                    String time1 = cursorGTime.getString(0);
                    time1 = time1.substring(2, time1.length()-2);
                    array = time1.split(",");
                }
                for(int i=0;i<array.length;i++) {
                    array[i] = array[i].substring(1, array[i].length()-1);
                    if (Integer.valueOf(array[i])> firun) {
                        String f = array[i].substring(0,2);
                        String l = array[i].substring(2);
                        if(lang_name.equals("فارسي") || lang_name.equals("عربی"))
                        {
                            time2.setText(faToEn(f)+":"+faToEn(l));
                        }
                        else{
                            //time2.setText(f+":"+l);
                            time2.setText(faToEn(f)+":"+faToEn(l));

                        }
                        origin1.setText(origin1String);
                        destination1.setText(destination2String);
                        arrow1.setVisibility(View.GONE);
                        break;
                    }
                }
            }



            if (!laststation.equals(stid) && !firststation.equals(stid))
            {
                while (cursorGTime.moveToNext())
                {
                    String time1 = cursorGTime.getString(0);
                    time1 = time1.substring(2, time1.length()-2);
                    array = time1.split(",");
                    break;
                }

                for(int i=0;i<array.length;i++) {
                    array[i] = array[i].substring(1, array[i].length()-1);
                    if (Integer.valueOf(array[i])> firun) {
                        String f = array[i].substring(0,2);
                        String l = array[i].substring(2);
                        if(lang_name.equals("فارسي") || lang_name.equals("عربی"))
                        {
                            time1.setText(faToEn(f)+":"+faToEn(l));
                        }
                        else{
                           // time1.setText(f+":"+l);
                            time1.setText(faToEn(f)+":"+faToEn(l));
                        }
                        origin1.setText(origin1String);
                        destination1.setText(destination1String);
                        break;
                    }
                }


                while (cursorGTime.moveToNext())
                {
                    cursorGTime.moveToLast();
                    String time1 = cursorGTime.getString(0);
                    time1 = time1.substring(2, time1.length()-2);
                    array = time1.split(",");
                }
                for(int i=0;i<array.length;i++) {
                    array[i] = array[i].substring(1, array[i].length()-1);
                    if (Integer.valueOf(array[i])> firun) {
                        String f = array[i].substring(0,2);
                        String l = array[i].substring(2);
                        if(lang_name.equals("فارسي") || lang_name.equals("عربی"))
                        {
                            time2.setText(faToEn(f)+":"+faToEn(l));
                        }
                        else{
                            time2.setText(faToEn(f)+":"+faToEn(l));
                            //  time2.setText(f+":"+l);
                        }
                        origin2.setText(origin1String);
                        destination2.setText(destination2String);
                        break;
                    }
                }
            }

            // }

        }catch (Exception e){}


        // Getting station Lat and Longitude

        cursorG = db.rawQuery("SELECT "+ StationsContract.StationsEntry.LOCATION+" FROM "+ StationsContract.StationsEntry.TABLE_NAME+" WHERE "+ StationsContract.StationsEntry.STATION_ID+" = "+ currentStation+";",null);
        while (cursorG.moveToNext())
        {
            String [] locArray = cursorG.getString(0).split(",");
            latitude = locArray[0];
            longitude = locArray[1];
        }
        // ->

        locclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Station Page")
                        .setAction("Google Routing"+" ("+st_title+")")
                        .build());
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr=" + mylatitude + "," + mylongitude + "&daddr=" + latitude + "," + longitude));
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(i);

            }
        });

        // getting Commercials From Sqlite ---------------------------------------------------------
        HashMap<String,String> url_maps = new HashMap<String, String>();
        cursorG = db.rawQuery("SELECT * FROM privateplaces WHERE "+ PrivatePlacesContract.PrivatePlacesEntry.ISVIP+" = 'true';", null);
        if(cursorG.getCount()==0)
        {
            sliderLayout.setVisibility(View.GONE);
        }
        else{
            while (cursorG.moveToNext())
            {
                String placeid = cursorG.getString(7);
                cursorG= db.rawQuery("SELECT * FROM publicplaces WHERE "+ PublicPlacesContract.PublicPlacesEntry.ID+" = "+placeid+";", null);
                while (cursorG.moveToNext()){
                    String banner = cursorG.getString(19);
                    if(banner != null) {
                        banners.add(new RemoteBanner(Connection.images+banner));
                        SharedPreferences sharedPreferences = getSharedPreferences("slider",MODE_PRIVATE);
                        SharedPreferences.Editor sheditor = sharedPreferences.edit();
                        sheditor.putString("index"+String.valueOf(bannerSlider.getCurrentSlidePosition()+1),cursorG.getString(2));
                        sheditor.commit();
                        Toast.makeText(getApplicationContext(),String.valueOf(bannerSlider.getCurrentSlidePosition()+1),Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
        // getting Commercials From Sqlite ---------------------------------------------------------


        // Slider Image ------------------------------------------------------------------------------
        bannerSlider.setBanners(banners);
        bannerSlider.setOnBannerClickListener(new OnBannerClickListener() {
            @Override
            public void onClick(int position) {
                Intent intent = new Intent(Station.this,PrivatePlaceActivity.class);
                SharedPreferences sharedPreferences = getSharedPreferences("slider",MODE_PRIVATE);
                String index = sharedPreferences.getString("index"+position,"5,5");
                intent.putExtra("id",index);
                startActivity(intent);
            }
        });
        // Slider Image ------------------------------------------------------------------------------


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
