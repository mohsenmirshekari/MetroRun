package com.mirshekari.metroshiraz.Aboutus;

/**
 * Created by Mohsen Mirshekari on 2/3/2018.
 */

public class AboutUsContract {
    public AboutUsContract() {}

    public static class AboutUsEntry {
        public static final String TBName = "aboutus";
        public static final String Address = "Address";
        public static final String BackendDeveloper = "BackendDeveloper";
        public static final String Description = "Description";
        public static final String FrontendDeveloper = "FrontendDeveloper";
        public static final String Id = "Id";
        public static final String Logo = "Logo";
        public static final String Title = "Title";
        public static final String WebsiteURL = "WebsiteURL";
    }
}
