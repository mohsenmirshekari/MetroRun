package com.mirshekari.metroshiraz;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mirshekari.metroshiraz.ChooseLine.LinesContract;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;
import com.mirshekari.metroshiraz.GPSTracker;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.net.DatagramSocket;
import java.util.List;
import java.util.Locale;

public class OnlineMap extends AppCompatActivity implements OnMapReadyCallback {
    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;
    Cursor cursor,cursor2,cursorLine;
    private GoogleMap mMap;
    Geocoder geocoder;
    List<Address> addresses;
    String dictionary;
    String firstSourceaddress,city,color,color2,s_latitude,s_longitude,ss_latitude,ss_longitude,sss_latitude,sss_longitude;
    double latitude,longitude,pol_lat1,pol_long1,pol_lat2,pol_long2;
    int i=0;
    Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_map);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("");
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Online map");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        GPSTracker tracker = new GPSTracker(this);
        if (!tracker.canGetLocation()) {
            tracker.showSettingsAlert();
        } else {
            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
        }

        dbHelper = new DBHelper(getApplicationContext());
        sqLiteDatabase = dbHelper.getReadableDatabase();

        cursor = sqLiteDatabase.rawQuery("SELECT * FROM stations;", null);
        cursor2 = sqLiteDatabase.rawQuery("SELECT * FROM stations;", null);

        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        CameraUpdate center=CameraUpdateFactory.newLatLng( new LatLng(latitude, longitude));
        CameraUpdate zoom=CameraUpdateFactory.zoomTo(15);
        mMap.moveCamera(center);
        mMap.animateCamera(zoom);

        while (cursor.moveToNext()){
            String lat = cursor.getString(3);
            String[] s = lat.split(",");
            s_latitude = s[0];
            s_longitude = s[1];

            if(i==0){
                if(!cursor.isLast()) {
           cursor.moveToNext();
           String lon = cursor.getString(3);
           String[] l = lon.split(",");
           ss_latitude = l[0];
           ss_longitude =l[1];
                 }
            }


            if(i!=0){
           cursor.moveToPrevious();
           String lat1 = cursor.getString(3);
           String[] s1 = lat1.split(",");
           s_latitude = s1[0];
           s_longitude = s1[1];
           if(!cursor.isLast()) {
               cursor.moveToNext();
               String lon = cursor.getString(3);
               String[] l = lon.split(",");
               ss_latitude = l[0];
               ss_longitude =l[1];
           }
       }


            String line_id = cursor.getString(2);
            Cursor colorCursor = sqLiteDatabase.rawQuery("SELECT * FROM "+ LinesContract.LinesEntry.TABLE_NAME+" WHERE "+ LinesContract.LinesEntry.LINE_ID+" = "+line_id+";",null);
            while (colorCursor.moveToNext()){
                color = colorCursor.getString(0);
            }

            Polyline line = googleMap.addPolyline(new PolylineOptions()
                    .add(new LatLng(Double.valueOf(s_latitude), Double.valueOf(s_longitude)), new LatLng(Double.valueOf(ss_latitude), Double.valueOf(ss_longitude)))
                    .width(25)
                    .color(Color.parseColor(color)));

            i++;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
