package com.mirshekari.metroshiraz;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mirshekari.metroshiraz.ChooseCity.BackgroundTask;
import com.mirshekari.metroshiraz.ChooseCity.Cities;
import com.mirshekari.metroshiraz.ChooseCity.CitiesAdapter;
import com.mirshekari.metroshiraz.ChooseLine.BackgroundTaskLine;
import com.mirshekari.metroshiraz.DBHelpers.DBHelper;

import java.util.ArrayList;

public class Choose_city extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public Context context;
    ArrayList<Cities> arrayList = new ArrayList<>();
    SQLiteDatabase db;
    DBHelper dbHelper;
    Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_city);
        recyclerView = (RecyclerView) findViewById(R.id.rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setHasFixedSize(true);
        dbHelper = new DBHelper(this);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Choose city");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());


        db = dbHelper.getReadableDatabase();

        if (isDatabaseEmpty() == 0){
            BackgroundTask backgroundTask = new BackgroundTask(Choose_city.this);
        backgroundTask.execute();
    }

        SQLiteDatabase sqLiteDatabase = dbHelper.getReadableDatabase();

        Cursor cursor = dbHelper.getInformation(sqLiteDatabase);

         while(cursor.moveToNext())
         {
             Cities cities = new Cities(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3),cursor.getString(4));
             arrayList.add(cities);
             adapter = new CitiesAdapter(arrayList,getApplicationContext());
         }
         cursor.close();
            dbHelper.close();
            adapter = new CitiesAdapter(arrayList);
            recyclerView.setAdapter(adapter);

        if (ActivityCompat.checkSelfPermission(Choose_city.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Choose_city.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Choose_city.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            return;
        }

        }

    public int isDatabaseEmpty()
    {
        String count = "SELECT count(*) FROM cities";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        mcursor.close();
        return icount;
    }
    }

