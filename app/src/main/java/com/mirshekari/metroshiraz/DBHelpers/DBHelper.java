package com.mirshekari.metroshiraz.DBHelpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.mirshekari.metroshiraz.Aboutus.AboutUsContract;
import com.mirshekari.metroshiraz.ChooseCity.CitiesContract;
import com.mirshekari.metroshiraz.ChooseLine.LinesContract;
import com.mirshekari.metroshiraz.ChooseStation.StationsContract;
import com.mirshekari.metroshiraz.Facilities.FacilitiesContract;
import com.mirshekari.metroshiraz.Favorites.Location.FavoriteLocationsContract;
import com.mirshekari.metroshiraz.GetKeys.KeysContract;
import com.mirshekari.metroshiraz.Place.Groups.GroupsContract;
import com.mirshekari.metroshiraz.Place.Private.PrivatePlacesContract;
import com.mirshekari.metroshiraz.Place.Public.PublicPlacesContract;
import com.mirshekari.metroshiraz.Routing.Source.TourismQuee.QueeContract;
import com.mirshekari.metroshiraz.Schedule.ScheduleContract;

/**
 * Created by Mohsen on 10/22/2017.
 */

public class DBHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "metrorun_app";
    public static final int DB_VERSION = 1;
    Context ctx;

    //Create Table of Cities
    public static final String CREATE_QUERY  = "create table "+ CitiesContract.CitiesEntry.TABLE_NAME+
            " ("+CitiesContract.CitiesEntry.CITY_ID+" int,"+CitiesContract.CitiesEntry.CITY_TITLE+" text,"+
            " text,"+CitiesContract.CitiesEntry.CITY_BACKGROUND+" text,"+ CitiesContract.CitiesEntry.PHOTO_MAP+
            " text,"+ CitiesContract.CitiesEntry.PHOTO_LINK+" text,"+
            CitiesContract.CitiesEntry.CITY_COLOR+" text);";

    // Create Table of Lines
    public static final String CREATE_LINES_TABLE = "create table "+ LinesContract.LinesEntry.TABLE_NAME+
            " ("+LinesContract.LinesEntry.BACKGROUND_COLOR+" text,"+LinesContract.LinesEntry.CITY_ID+" int,"+
            LinesContract.LinesEntry.FIRSTSTATIONNUMBER+" int,"+LinesContract.LinesEntry.LINE_ID+" int,"+
            LinesContract.LinesEntry.LASTSTATIONNUMBER+" int,"+LinesContract.LinesEntry.LINENUMBER+" int,"+
            LinesContract.LinesEntry.STATUS+" int,"+ LinesContract.LinesEntry.TEXTCOLOR+" text," +
            LinesContract.LinesEntry.TITLE+" text,"+ LinesContract.LinesEntry.IMAGE+" text,"+
            LinesContract.LinesEntry.MULTIEND+" text,"+ LinesContract.LinesEntry.MULTIENDDETAIL+ " text);";

    public static final String CREATE_STATIONS_TABLE = "create table "+ StationsContract.StationsEntry.TABLE_NAME+
            " ("+ StationsContract.StationsEntry.ADDRESS+" text,"+
            StationsContract.StationsEntry.STATION_ID+" int,"+ StationsContract.StationsEntry.LINE_ID+" int,"+
            StationsContract.StationsEntry.LOCATION+" text,"+ StationsContract.StationsEntry.STATION_NUMBER+" int,"+
            StationsContract.StationsEntry.STATUS+" int,"+ StationsContract.StationsEntry.TITLE+" text,"
            +StationsContract.StationsEntry.FACILITIES+ " text,"+StationsContract.StationsEntry.ISCROSS+" text);";


    public static final String CREATE_SCHEDULE_TABLE_0 = "create table "+ ScheduleContract.ScheduleEntry.TABLE_NAME_0+
            " ("+ ScheduleContract.ScheduleEntry.TIME+" text,"+ ScheduleContract.ScheduleEntry.ROUTE+" int,"+ ScheduleContract.ScheduleEntry.DAY+" text,"+
            ScheduleContract.ScheduleEntry.STATION_ID+" int,"+ ScheduleContract.ScheduleEntry.STATION_NO+" int);";

    public static final String CREATE_SCHEDULE_TABLE_1 = "create table "+ ScheduleContract.ScheduleEntry.TABLE_NAME_1+
            " ("+ ScheduleContract.ScheduleEntry.TIME+" text,"+ ScheduleContract.ScheduleEntry.ROUTE+" int,"+ ScheduleContract.ScheduleEntry.DAY+" text,"+
            ScheduleContract.ScheduleEntry.STATION_ID+" int,"+ ScheduleContract.ScheduleEntry.STATION_NO+" int);";

    public static final String CREATE_SCHEDULE_TABLE_2 = "create table "+ ScheduleContract.ScheduleEntry.TABLE_NAME_2+
            " ("+ ScheduleContract.ScheduleEntry.TIME+" text,"+ ScheduleContract.ScheduleEntry.ROUTE+" int,"+ ScheduleContract.ScheduleEntry.DAY+" text,"+
            ScheduleContract.ScheduleEntry.STATION_ID+" int,"+ ScheduleContract.ScheduleEntry.STATION_NO+" int);";

    public static final String CREATE_SCHEDULE_TABLE_3 = "create table "+ ScheduleContract.ScheduleEntry.TABLE_NAME_3+
            " ("+ ScheduleContract.ScheduleEntry.TIME+" text,"+ ScheduleContract.ScheduleEntry.ROUTE+" int,"+ ScheduleContract.ScheduleEntry.DAY+" text,"+
            ScheduleContract.ScheduleEntry.STATION_ID+" int,"+ ScheduleContract.ScheduleEntry.STATION_NO+" int);";

    public static final String CREATE_SCHEDULE_TABLE_4 = "create table "+ ScheduleContract.ScheduleEntry.TABLE_NAME_4+
            " ("+ ScheduleContract.ScheduleEntry.TIME+" text,"+ ScheduleContract.ScheduleEntry.ROUTE+" int,"+ ScheduleContract.ScheduleEntry.DAY+" text,"+
            ScheduleContract.ScheduleEntry.STATION_ID+" int,"+ ScheduleContract.ScheduleEntry.STATION_NO+" int);";

    public static final String CREATE_SCHEDULE_TABLE_5 = "create table "+ ScheduleContract.ScheduleEntry.TABLE_NAME_5+
            " ("+ ScheduleContract.ScheduleEntry.TIME+" text,"+ ScheduleContract.ScheduleEntry.ROUTE+" int,"+ ScheduleContract.ScheduleEntry.DAY+" text,"+
            ScheduleContract.ScheduleEntry.STATION_ID+" int,"+ ScheduleContract.ScheduleEntry.STATION_NO+" int);";

    public static final String CREATE_SCHEDULE_TABLE_6 = "create table "+ ScheduleContract.ScheduleEntry.TABLE_NAME_6+
            " ("+ ScheduleContract.ScheduleEntry.TIME+" text,"+ ScheduleContract.ScheduleEntry.ROUTE+" int,"+ ScheduleContract.ScheduleEntry.DAY+" text,"+
            ScheduleContract.ScheduleEntry.STATION_ID+" int,"+ ScheduleContract.ScheduleEntry.STATION_NO+" int);";


    public static final String CREATE_FACILITIES_TABLE = "create table "+ FacilitiesContract.FacilitiesEntry.TABLE_NAME+
            " ("+ FacilitiesContract.FacilitiesEntry.ICON+" text,"+ FacilitiesContract.FacilitiesEntry.ID+" int,"+ FacilitiesContract.FacilitiesEntry.TITLE+" text);";

    public static final String CREATE_KEYS_TABLE = "create table "+ KeysContract.KeysEntry.TABLE_NAME+
            " ("+ KeysContract.KeysEntry.KEY_ID+" text,"+ KeysContract.KeysEntry.KEY+" text,"+ KeysContract.KeysEntry.TEXT+" text);";

    public static final String CREATE_QUEE_TABLE = "create table "+ QueeContract.QueeEntry.TABLE_NAME+
            " ("+ QueeContract.QueeEntry.ID+" text,"+ QueeContract.QueeEntry.STATION_ID+" text,"+ QueeContract.QueeEntry.TITLE+" text);";

    public static final String CREATE_PUBLICPLACES_TABLE = "create table "+ PublicPlacesContract.PublicPlacesEntry.TABLE_NAME+
            " ("+ PublicPlacesContract.PublicPlacesEntry.BACKGROUND_COLOR+" text,"+ PublicPlacesContract.PublicPlacesEntry.DESCRIPTION+" text,"+
            PublicPlacesContract.PublicPlacesEntry.ID+" int,"+ PublicPlacesContract.PublicPlacesEntry.ISPRIVATE+" text,"+
            PublicPlacesContract.PublicPlacesEntry.PHONE+" text,"+ PublicPlacesContract.PublicPlacesEntry.PHOTOLINKS+" text,"+
            PublicPlacesContract.PublicPlacesEntry.PHOTOS+" text,"+ PublicPlacesContract.PublicPlacesEntry.PLACEGROUPEID+" int,"+
            PublicPlacesContract.PublicPlacesEntry.INSTAGRAM+" text,"+ PublicPlacesContract.PublicPlacesEntry.TELEGRAM+" text,"+
            PublicPlacesContract.PublicPlacesEntry.STATIONID+" int,"+
            PublicPlacesContract.PublicPlacesEntry.TEXTCOLORID+" text,"+PublicPlacesContract.PublicPlacesEntry.TITLE+" text,"+
            PublicPlacesContract.PublicPlacesEntry.ICON+" text,"+ PublicPlacesContract.PublicPlacesEntry.BACKGROUND_PHOTO+" text,"+
            PublicPlacesContract.PublicPlacesEntry.SLIDESHOW_1+" text,"+ PublicPlacesContract.PublicPlacesEntry.SLIDESHOW_2+" text,"+
            PublicPlacesContract.PublicPlacesEntry.SLIDESHOW_3+" text,"+ PublicPlacesContract.PublicPlacesEntry.SLIDESHOW_4+" text,"+
            PublicPlacesContract.PublicPlacesEntry.BANNER+" text);";


    public static final String CREATE_PRIVATEPLACES_TABLE = "create table "+ PrivatePlacesContract.PrivatePlacesEntry.TABLE_NAME+
            " ("+ PrivatePlacesContract.PrivatePlacesEntry.COMMERCIAL+" text,"+ PrivatePlacesContract.PrivatePlacesEntry.ENDDATE+" text,"+
            PrivatePlacesContract.PrivatePlacesEntry.ID+" int,"+ PrivatePlacesContract.PrivatePlacesEntry.ISVIP+" text,"+
            PrivatePlacesContract.PrivatePlacesEntry.MOBILES+" text,"+ PrivatePlacesContract.PrivatePlacesEntry.PHOTOS+" text,"+
            PrivatePlacesContract.PrivatePlacesEntry.WEBSITE+" text,"+ PrivatePlacesContract.PrivatePlacesEntry.PLACE_ID+" text);";

    public static final String CREATE_GROUPS_TABLE = "create table "+ GroupsContract.GroupsEntry.TABLE_NAME+
            " ("+ GroupsContract.GroupsEntry.ID+" int,"+ GroupsContract.GroupsEntry.NAME+" text);";


    // Create Table of About Us
    public static final String CREATE_ABOUTUS_TABLE = "create table "+ AboutUsContract.AboutUsEntry.TBName+
            " ("+AboutUsContract.AboutUsEntry.Address+" text,"+AboutUsContract.AboutUsEntry.BackendDeveloper+" text,"+
            AboutUsContract.AboutUsEntry.Description+" text,"+AboutUsContract.AboutUsEntry.FrontendDeveloper+" text,"+
            AboutUsContract.AboutUsEntry.Id+" int,"+AboutUsContract.AboutUsEntry.Logo+" text,"+
            AboutUsContract.AboutUsEntry.Title+" text,"+ AboutUsContract.AboutUsEntry.WebsiteURL+" text);";


    // Create Table of Favorite locations
    public static final String CREATE_FAVORITE_LOCATIONS = "create table "+ FavoriteLocationsContract.FavoriteLocationsContractEntry.TABLE_NAME+
            " ("+ FavoriteLocationsContract.FavoriteLocationsContractEntry.ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"+FavoriteLocationsContract.FavoriteLocationsContractEntry.LATLANG+" text,"+
            FavoriteLocationsContract.FavoriteLocationsContractEntry.TITLE+" text,"+FavoriteLocationsContract.FavoriteLocationsContractEntry.STATION+" text,"
            +FavoriteLocationsContract.FavoriteLocationsContractEntry.STATIONID+" text);";
    public DBHelper(Context context)
    {
        super(context,DB_NAME,null,DB_VERSION);
        Log.d("Database Operations","****** Database created ********");
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_QUERY);
        sqLiteDatabase.execSQL(CREATE_LINES_TABLE);
        sqLiteDatabase.execSQL(CREATE_STATIONS_TABLE);
        sqLiteDatabase.execSQL(CREATE_SCHEDULE_TABLE_0);
        sqLiteDatabase.execSQL(CREATE_SCHEDULE_TABLE_1);
        sqLiteDatabase.execSQL(CREATE_SCHEDULE_TABLE_2);
        sqLiteDatabase.execSQL(CREATE_SCHEDULE_TABLE_3);
        sqLiteDatabase.execSQL(CREATE_SCHEDULE_TABLE_4);
        sqLiteDatabase.execSQL(CREATE_SCHEDULE_TABLE_5);
        sqLiteDatabase.execSQL(CREATE_SCHEDULE_TABLE_6);
        sqLiteDatabase.execSQL(CREATE_FACILITIES_TABLE);
        sqLiteDatabase.execSQL(CREATE_PUBLICPLACES_TABLE);
        sqLiteDatabase.execSQL(CREATE_PRIVATEPLACES_TABLE);
        sqLiteDatabase.execSQL(CREATE_GROUPS_TABLE);
        sqLiteDatabase.execSQL(CREATE_ABOUTUS_TABLE);
        sqLiteDatabase.execSQL(CREATE_KEYS_TABLE);
        sqLiteDatabase.execSQL(CREATE_QUEE_TABLE);
        sqLiteDatabase.execSQL(CREATE_FAVORITE_LOCATIONS);
        Log.d("Database Operations","****** Tables created ********");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CitiesContract.CitiesEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + LinesContract.LinesEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + StationsContract.StationsEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ScheduleContract.ScheduleEntry.TABLE_NAME_0);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ScheduleContract.ScheduleEntry.TABLE_NAME_1);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ScheduleContract.ScheduleEntry.TABLE_NAME_2);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ScheduleContract.ScheduleEntry.TABLE_NAME_3);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ScheduleContract.ScheduleEntry.TABLE_NAME_4);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ScheduleContract.ScheduleEntry.TABLE_NAME_5);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ScheduleContract.ScheduleEntry.TABLE_NAME_6);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + FacilitiesContract.FacilitiesEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PublicPlacesContract.PublicPlacesEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PrivatePlacesContract.PrivatePlacesEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + GroupsContract.GroupsEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + AboutUsContract.AboutUsEntry.TBName);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + KeysContract.KeysEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + QueeContract.QueeEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + FavoriteLocationsContract.FavoriteLocationsContractEntry.TABLE_NAME);

        Log.d("Database Operations","****** Database Updated ********");
    }

    public void putInformation(int city_id,String city_title,String city_background,String photo_map,String city_color,String photo_link, SQLiteDatabase db)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CitiesContract.CitiesEntry.CITY_ID,city_id);
        contentValues.put(CitiesContract.CitiesEntry.CITY_TITLE,city_title);
        contentValues.put(CitiesContract.CitiesEntry.CITY_BACKGROUND,city_background);
        contentValues.put(CitiesContract.CitiesEntry.PHOTO_MAP,photo_map);
        contentValues.put(CitiesContract.CitiesEntry.CITY_COLOR,city_color);
        contentValues.put(CitiesContract.CitiesEntry.PHOTO_LINK,photo_link);
        long l = db.insert(CitiesContract.CitiesEntry.TABLE_NAME,null,contentValues);
        Log.d("Database Operations","****** One City Inserted ******** "+contentValues);
    }

    public void putFavoriteLocations(String title,String latlng,String station,String stationid,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FavoriteLocationsContract.FavoriteLocationsContractEntry.TITLE,title);
        contentValues.put(FavoriteLocationsContract.FavoriteLocationsContractEntry.LATLANG,latlng);
        contentValues.put(FavoriteLocationsContract.FavoriteLocationsContractEntry.STATION,station);
        contentValues.put(FavoriteLocationsContract.FavoriteLocationsContractEntry.STATIONID,stationid);
        long l = sqLiteDatabase.insert(FavoriteLocationsContract.FavoriteLocationsContractEntry.TABLE_NAME,null,contentValues);
        Log.d("Database Operations","****** One Favorite Locations Inserted ******** "+contentValues);
    }


    public void putLineInformation(String backgroundColor, String city_ID, int firstStationNumber, int line_ID, int lastSTationNumber, int lineNumber, int status, String textColor, String title,String image,String multiend,String multienddetails,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentLineValues = new ContentValues();
        contentLineValues.put(LinesContract.LinesEntry.BACKGROUND_COLOR,backgroundColor);
        contentLineValues.put(LinesContract.LinesEntry.CITY_ID,city_ID);
        contentLineValues.put(LinesContract.LinesEntry.FIRSTSTATIONNUMBER,firstStationNumber);
        contentLineValues.put(LinesContract.LinesEntry.LINE_ID,line_ID);
        contentLineValues.put(LinesContract.LinesEntry.LASTSTATIONNUMBER,lastSTationNumber);
        contentLineValues.put(LinesContract.LinesEntry.LINENUMBER,lineNumber);
        contentLineValues.put(LinesContract.LinesEntry.STATUS,status);
        contentLineValues.put(LinesContract.LinesEntry.TEXTCOLOR,textColor);
        contentLineValues.put(LinesContract.LinesEntry.TITLE,title);
        contentLineValues.put(LinesContract.LinesEntry.IMAGE,image);
        contentLineValues.put(LinesContract.LinesEntry.MULTIEND,multiend);
        contentLineValues.put(LinesContract.LinesEntry.MULTIENDDETAIL,multienddetails);

        long ll = sqLiteDatabase.insert(LinesContract.LinesEntry.TABLE_NAME,null,contentLineValues);
        Log.d("Database Operations","****** One Line Inserted ******** "+contentLineValues);
    }

    public void updateLineInformation(String backgroundColor, String city_ID, int firstStationNumber, int line_ID, int lastSTationNumber, int lineNumber, int status, String textColor, String title,String image,String multiend,String multienddetails,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentLineValues = new ContentValues();
        contentLineValues.put(LinesContract.LinesEntry.BACKGROUND_COLOR,backgroundColor);
        contentLineValues.put(LinesContract.LinesEntry.CITY_ID,city_ID);
        contentLineValues.put(LinesContract.LinesEntry.FIRSTSTATIONNUMBER,firstStationNumber);
        contentLineValues.put(LinesContract.LinesEntry.LINE_ID,line_ID);
        contentLineValues.put(LinesContract.LinesEntry.LASTSTATIONNUMBER,lastSTationNumber);
        contentLineValues.put(LinesContract.LinesEntry.LINENUMBER,lineNumber);
        contentLineValues.put(LinesContract.LinesEntry.STATUS,status);
        contentLineValues.put(LinesContract.LinesEntry.TEXTCOLOR,textColor);
        contentLineValues.put(LinesContract.LinesEntry.TITLE,title);
        contentLineValues.put(LinesContract.LinesEntry.IMAGE,image);
        contentLineValues.put(LinesContract.LinesEntry.MULTIEND,multiend);
        contentLineValues.put(LinesContract.LinesEntry.MULTIENDDETAIL,multienddetails);

        long ll = sqLiteDatabase.update(LinesContract.LinesEntry.TABLE_NAME, contentLineValues, LinesContract.LinesEntry.LINE_ID+"="+line_ID, null);
        Log.d("Database Operations","****** One Line Updated ******** "+contentLineValues);
    }

    public void putStationsInformation(String address,int stationid,int lineid,String location,int stationnumber,int status,String title,String facilities,String iscross,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentLineValues = new ContentValues();
        contentLineValues.put(StationsContract.StationsEntry.ADDRESS,address);
        contentLineValues.put(StationsContract.StationsEntry.STATION_ID,stationid);
        contentLineValues.put(StationsContract.StationsEntry.LINE_ID,lineid);
        contentLineValues.put(StationsContract.StationsEntry.LOCATION,location);
        contentLineValues.put(StationsContract.StationsEntry.STATION_NUMBER,stationnumber);
        contentLineValues.put(StationsContract.StationsEntry.STATUS,status);
        contentLineValues.put(StationsContract.StationsEntry.TITLE,title);
        contentLineValues.put(StationsContract.StationsEntry.FACILITIES,facilities);
        contentLineValues.put(StationsContract.StationsEntry.ISCROSS,iscross);
        long ll = sqLiteDatabase.insert(StationsContract.StationsEntry.TABLE_NAME,null,contentLineValues);
        Log.d("Database Operations","****** One Station Inserted ******** "+contentLineValues);

    }


    public void updateStationsInformation(String address,int stationid,int lineid,String location,int stationnumber,int status,String title,String facilities,String iscross,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentLineValues = new ContentValues();
        contentLineValues.put(StationsContract.StationsEntry.ADDRESS,address);
        contentLineValues.put(StationsContract.StationsEntry.STATION_ID,stationid);
        contentLineValues.put(StationsContract.StationsEntry.LINE_ID,lineid);
        contentLineValues.put(StationsContract.StationsEntry.LOCATION,location);
        contentLineValues.put(StationsContract.StationsEntry.STATION_NUMBER,stationnumber);
        contentLineValues.put(StationsContract.StationsEntry.STATUS,status);
        contentLineValues.put(StationsContract.StationsEntry.TITLE,title);
        contentLineValues.put(StationsContract.StationsEntry.FACILITIES,facilities);
        contentLineValues.put(StationsContract.StationsEntry.ISCROSS,iscross);
        long ll = sqLiteDatabase.update(StationsContract.StationsEntry.TABLE_NAME, contentLineValues, StationsContract.StationsEntry.STATION_ID+"="+stationid, null);
        Log.d("Database Operations","****** One Station Updated ******** "+contentLineValues);

    }
    public void putSchedule0Information(String time,int route,String day,int stationid,int stationno,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentScheduleValues = new ContentValues();
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.TIME,time);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.ROUTE,route);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.DAY,day);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.STATION_ID,stationid);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.STATION_NO,stationno);
        long ll = sqLiteDatabase.insert(ScheduleContract.ScheduleEntry.TABLE_NAME_0,null,contentScheduleValues);
        Log.d("Database Operations","****** One Schedule Inserted ******** "+contentScheduleValues);
    }


    public void putSchedule1Information(String time,int route,String day,int stationid,int stationno,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentScheduleValues = new ContentValues();
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.TIME,time);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.ROUTE,route);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.DAY,day);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.STATION_ID,stationid);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.STATION_NO,stationno);
        long ll = sqLiteDatabase.insert(ScheduleContract.ScheduleEntry.TABLE_NAME_1,null,contentScheduleValues);
        Log.d("Database Operations","****** One Schedule Inserted ******** "+contentScheduleValues);
    }

    public void putSchedule2Information(String time,int route,String day,int stationid,int stationno,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentScheduleValues = new ContentValues();
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.TIME,time);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.ROUTE,route);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.DAY,day);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.STATION_ID,stationid);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.STATION_NO,stationno);
        long ll = sqLiteDatabase.insert(ScheduleContract.ScheduleEntry.TABLE_NAME_2,null,contentScheduleValues);
        Log.d("Database Operations","****** One Schedule Inserted ******** "+contentScheduleValues);
    }

    public void putSchedule3Information(String time,int route,String day,int stationid,int stationno,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentScheduleValues = new ContentValues();
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.TIME,time);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.ROUTE,route);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.DAY,day);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.STATION_ID,stationid);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.STATION_NO,stationno);
        long ll = sqLiteDatabase.insert(ScheduleContract.ScheduleEntry.TABLE_NAME_3,null,contentScheduleValues);
        Log.d("Database Operations","****** One Schedule Inserted ******** "+contentScheduleValues);
    }

    public void putSchedule4Information(String time,int route,String day,int stationid,int stationno,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentScheduleValues = new ContentValues();
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.TIME,time);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.ROUTE,route);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.DAY,day);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.STATION_ID,stationid);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.STATION_NO,stationno);
        long ll = sqLiteDatabase.insert(ScheduleContract.ScheduleEntry.TABLE_NAME_4,null,contentScheduleValues);
        Log.d("Database Operations","****** One Schedule Inserted ******** "+contentScheduleValues);
    }

    public void putSchedule5Information(String time,int route,String day,int stationid,int stationno,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentScheduleValues = new ContentValues();
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.TIME,time);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.ROUTE,route);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.DAY,day);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.STATION_ID,stationid);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.STATION_NO,stationno);
        long ll = sqLiteDatabase.insert(ScheduleContract.ScheduleEntry.TABLE_NAME_5,null,contentScheduleValues);
        Log.d("Database Operations","****** One Schedule Inserted ******** "+contentScheduleValues);
    }


    public void putSchedule6Information(String time,int route,String day,int stationid,int stationno,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentScheduleValues = new ContentValues();
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.TIME,time);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.ROUTE,route);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.DAY,day);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.STATION_ID,stationid);
        contentScheduleValues.put(ScheduleContract.ScheduleEntry.STATION_NO,stationno);
        long ll = sqLiteDatabase.insert(ScheduleContract.ScheduleEntry.TABLE_NAME_6,null,contentScheduleValues);
        Log.d("Database Operations","****** One Schedule Inserted ******** "+contentScheduleValues);
    }

    public void putFacilitiesInformation(String icon,int id,String title,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FacilitiesContract.FacilitiesEntry.ICON,icon);
        contentValues.put(FacilitiesContract.FacilitiesEntry.ID,id);
        contentValues.put(FacilitiesContract.FacilitiesEntry.TITLE,title);
        long ll = sqLiteDatabase.insert(FacilitiesContract.FacilitiesEntry.TABLE_NAME,null,contentValues);
        Log.d("Database Operations","****** One Facility Inserted ******** "+contentValues);
    }

    public void updateFacilitiesInformation(String icon,int id,String title,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FacilitiesContract.FacilitiesEntry.ICON,icon);
        contentValues.put(FacilitiesContract.FacilitiesEntry.ID,id);
        contentValues.put(FacilitiesContract.FacilitiesEntry.TITLE,title);
        long ll = sqLiteDatabase.update(FacilitiesContract.FacilitiesEntry.TABLE_NAME, contentValues, FacilitiesContract.FacilitiesEntry.ID+"="+id, null);
        Log.d("Database Operations","****** One Key Updated ******** "+contentValues);
    }

    public void putKeysInformation(String key_id,String key,String text,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(KeysContract.KeysEntry.KEY_ID,key_id);
        contentValues.put(KeysContract.KeysEntry.KEY,key);
        contentValues.put(KeysContract.KeysEntry.TEXT,text);
        long ll = sqLiteDatabase.insert(KeysContract.KeysEntry.TABLE_NAME,null,contentValues);
        Log.d("Database Operations","****** One Key Inserted ******** "+contentValues);
    }

    public void updateKeysInformation(String key_id,String key,String text,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(KeysContract.KeysEntry.KEY_ID,key_id);
        contentValues.put(KeysContract.KeysEntry.KEY,key);
        contentValues.put(KeysContract.KeysEntry.TEXT,text);
        long ll = sqLiteDatabase.update(KeysContract.KeysEntry.TABLE_NAME, contentValues, KeysContract.KeysEntry.KEY_ID+"="+key_id, null);
        Log.d("Database Operations","****** One Key Updated ******** "+contentValues);
    }

    public void putQueeInformation(String id,String station_id,String title,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(QueeContract.QueeEntry.ID,id);
        contentValues.put(QueeContract.QueeEntry.STATION_ID,station_id);
        contentValues.put(QueeContract.QueeEntry.TITLE,title);
        long ll = sqLiteDatabase.insert(QueeContract.QueeEntry.TABLE_NAME,null,contentValues);
        Log.d("Database Operations","****** One Place added to Quee ******** "+contentValues);
    }

    public void deleteQueeInformation(String id,SQLiteDatabase sqLiteDatabase)
    {
        long ll = sqLiteDatabase.delete(QueeContract.QueeEntry.TABLE_NAME, QueeContract.QueeEntry.ID+"="+id,null);
        Log.d("Database Operations","****** One Place removed from Quee ******** ");
    }

    public void deleteQueeTable(SQLiteDatabase sqLiteDatabase)
    {
        long ll = sqLiteDatabase.delete(QueeContract.QueeEntry.TABLE_NAME, null,null);
        Log.d("Database Operations","****** One Place removed from Quee ******** ");
    }

    public void putPublicPlacesInformation(String backgroundcolor,String description,int id,String isPrivate,String phone,String photolinks,String photos,int placegroupeid,String instagram,String telegram,int stationid,String textcolorid,String title,String icon,String background_photo,String slideshow_1,String slideshow_2,String slideshow_3,String slideshow_4,String banner,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.BACKGROUND_COLOR,backgroundcolor);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.DESCRIPTION,description);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.ID,id);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.ISPRIVATE,isPrivate);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.PHONE,phone);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.PHOTOLINKS,photolinks);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.PHOTOS,photos);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.PLACEGROUPEID,placegroupeid);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.INSTAGRAM,instagram);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.TELEGRAM,telegram);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.STATIONID,stationid);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.TEXTCOLORID,textcolorid);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.TITLE,title);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.ICON,icon);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.BACKGROUND_PHOTO,background_photo);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.SLIDESHOW_1,slideshow_1);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.SLIDESHOW_2,slideshow_2);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.SLIDESHOW_3,slideshow_3);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.SLIDESHOW_4,slideshow_4);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.BANNER,banner);
        long ll = sqLiteDatabase.insert(PublicPlacesContract.PublicPlacesEntry.TABLE_NAME,null,contentValues);
        Log.d("Database Operations","****** One Public Place Inserted ******** "+contentValues);
    }


    public void updatePublicPlacesInformation(String backgroundcolor,String description,int id,String isPrivate,String phone,String photolinks,String photos,int placegroupeid,String instagram,String telegram,int stationid,String textcolorid,String title,String icon,String background_photo,String slideshow_1,String slideshow_2,String slideshow_3,String slideshow_4,String banner,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.BACKGROUND_COLOR,backgroundcolor);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.DESCRIPTION,description);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.ID,id);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.ISPRIVATE,isPrivate);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.PHONE,phone);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.PHOTOLINKS,photolinks);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.PHOTOS,photos);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.PLACEGROUPEID,placegroupeid);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.INSTAGRAM,instagram);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.TELEGRAM,telegram);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.STATIONID,stationid);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.TEXTCOLORID,textcolorid);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.TITLE,title);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.ICON,icon);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.BACKGROUND_PHOTO,background_photo);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.SLIDESHOW_1,slideshow_1);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.SLIDESHOW_2,slideshow_2);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.SLIDESHOW_3,slideshow_3);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.SLIDESHOW_4,slideshow_4);
        contentValues.put(PublicPlacesContract.PublicPlacesEntry.BANNER,banner);
        long ll = sqLiteDatabase.update(PublicPlacesContract.PublicPlacesEntry.TABLE_NAME, contentValues, PublicPlacesContract.PublicPlacesEntry.ID+"="+id, null);
        Log.d("Database Operations","****** One Public Place Updated ******** "+contentValues);
    }

    public void putPrivatePlacesInformation(String commercial,String endDate,int id,String isVIP,String mobiles,String photos,String website,String place_id,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(PrivatePlacesContract.PrivatePlacesEntry.COMMERCIAL,commercial);
        contentValues.put(PrivatePlacesContract.PrivatePlacesEntry.ENDDATE,endDate);
        contentValues.put(PrivatePlacesContract.PrivatePlacesEntry.ID,id);
        contentValues.put(PrivatePlacesContract.PrivatePlacesEntry.ISVIP,isVIP);
        contentValues.put(PrivatePlacesContract.PrivatePlacesEntry.MOBILES,mobiles);
        contentValues.put(PrivatePlacesContract.PrivatePlacesEntry.PHOTOS,photos);
        contentValues.put(PrivatePlacesContract.PrivatePlacesEntry.WEBSITE,website);
        contentValues.put(PrivatePlacesContract.PrivatePlacesEntry.PLACE_ID,place_id);
        long ll = sqLiteDatabase.insert(PrivatePlacesContract.PrivatePlacesEntry.TABLE_NAME,null,contentValues);
        Log.d("Database Operations","****** One Private Place Inserted ******** "+contentValues);
    }

    public void updatePrivatePlacesInformation(String commercial,String endDate,int id,String isVIP,String mobiles,String photos,String website,String place_id,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(PrivatePlacesContract.PrivatePlacesEntry.COMMERCIAL,commercial);
        contentValues.put(PrivatePlacesContract.PrivatePlacesEntry.ENDDATE,endDate);
        contentValues.put(PrivatePlacesContract.PrivatePlacesEntry.ID,id);
        contentValues.put(PrivatePlacesContract.PrivatePlacesEntry.ISVIP,isVIP);
        contentValues.put(PrivatePlacesContract.PrivatePlacesEntry.MOBILES,mobiles);
        contentValues.put(PrivatePlacesContract.PrivatePlacesEntry.PHOTOS,photos);
        contentValues.put(PrivatePlacesContract.PrivatePlacesEntry.WEBSITE,website);
        contentValues.put(PrivatePlacesContract.PrivatePlacesEntry.PLACE_ID,place_id);
        long ll = sqLiteDatabase.update(PrivatePlacesContract.PrivatePlacesEntry.TABLE_NAME, contentValues, PrivatePlacesContract.PrivatePlacesEntry.ID+"="+id, null);
        Log.d("Database Operations","****** One Private Place Updated ******** "+contentValues);
    }

    public void putGroupsInformation(int id,String name,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(GroupsContract.GroupsEntry.ID,id);
        contentValues.put(GroupsContract.GroupsEntry.NAME,name);
        long ll = sqLiteDatabase.insert(GroupsContract.GroupsEntry.TABLE_NAME,null,contentValues);
        Log.d("Database Operations","****** One Groupe Inserted ******** "+contentValues);
    }

    public void updateGroupsInformation(int id,String name,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(GroupsContract.GroupsEntry.ID,id);
        contentValues.put(GroupsContract.GroupsEntry.NAME,name);
        long ll = sqLiteDatabase.update(GroupsContract.GroupsEntry.TABLE_NAME, contentValues, GroupsContract.GroupsEntry.ID+"="+id, null);
        Log.d("Database Operations","****** One Group Updated ******** "+contentValues);
    }

    public void putAboutUsInformation(String Address,String BackendDeveloper,String Description,String FrontendDeveloper,String Id,String Logo,String Title,String WebsiteURL,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(AboutUsContract.AboutUsEntry.Address,Address);
        contentValues.put(AboutUsContract.AboutUsEntry.BackendDeveloper,BackendDeveloper);
        contentValues.put(AboutUsContract.AboutUsEntry.Description,Description);
        contentValues.put(AboutUsContract.AboutUsEntry.FrontendDeveloper,FrontendDeveloper);
        contentValues.put(AboutUsContract.AboutUsEntry.Id,Id);
        contentValues.put(AboutUsContract.AboutUsEntry.Logo,Logo);
        contentValues.put(AboutUsContract.AboutUsEntry.Title,Title);
        contentValues.put(AboutUsContract.AboutUsEntry.WebsiteURL,WebsiteURL);
        long ll = sqLiteDatabase.insert(AboutUsContract.AboutUsEntry.TBName,null,contentValues);
        Log.d("Database Operations","****** One Groupe Inserted ******** "+contentValues);
    }

    public void updateAboutUsInformation(String Address,String BackendDeveloper,String Description,String FrontendDeveloper,String Id,String Logo,String Title,String WebsiteURL,SQLiteDatabase sqLiteDatabase)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(AboutUsContract.AboutUsEntry.Address,Address);
        contentValues.put(AboutUsContract.AboutUsEntry.BackendDeveloper,BackendDeveloper);
        contentValues.put(AboutUsContract.AboutUsEntry.Description,Description);
        contentValues.put(AboutUsContract.AboutUsEntry.FrontendDeveloper,FrontendDeveloper);
        contentValues.put(AboutUsContract.AboutUsEntry.Id,Id);
        contentValues.put(AboutUsContract.AboutUsEntry.Logo,Logo);
        contentValues.put(AboutUsContract.AboutUsEntry.Title,Title);
        contentValues.put(AboutUsContract.AboutUsEntry.WebsiteURL,WebsiteURL);
        long ll = sqLiteDatabase.update(AboutUsContract.AboutUsEntry.TBName, contentValues, AboutUsContract.AboutUsEntry.Id+"="+Id, null);
        Log.d("Database Operations","****** One Line Updated ******** "+contentValues);
    }
    public Cursor getInformation(SQLiteDatabase db)
    {
        String[] projection = {CitiesContract.CitiesEntry.CITY_ID,
                CitiesContract.CitiesEntry.CITY_BACKGROUND,CitiesContract.CitiesEntry.CITY_COLOR,CitiesContract.CitiesEntry.CITY_TITLE, CitiesContract.CitiesEntry.PHOTO_LINK};
        Cursor cursor = db.query(CitiesContract.CitiesEntry.TABLE_NAME,projection,null,null,null,null,null);
        return cursor;
    }

    public void deleteTable(String tbname,SQLiteDatabase sqLiteDatabase)
    {
        long ll = sqLiteDatabase.delete(tbname,null,null);
    }

}
